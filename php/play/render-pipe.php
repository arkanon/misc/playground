<?php

  $created   = "25/07/2011 Seg 22:49:45";

  include('common.inc.php');

  $script    = $_SERVER['SCRIPT_NAME'];
  eregi('\.([a-z]+)\.php$',$script,$type_ext);
  $type_ext  = $type_ext[1];

  $abc_h     = array
  (
    'X' => '1'      ,
    'M' => '1/64'   ,
    'L' => '1/64'   ,
    'Q' => '1/4=77' ,
    'K' => 'C'      ,
  );

  if (isset($_GET['nome']))
  {
    $name    = $_GET['nome' ];
    $instr   = $_GET['instr'];
    $notes   = $_GET['notes'];
    unset($_GET['nome' ]);
    unset($_GET['instr']);
    unset($_GET['notes']);
    foreach (array_keys($_GET) as $par) $abc_h[$par] = $_GET[$par];
  }
  else
  {
    $args    = $_SERVER['argv'];
    $name    = rawurldecode(array_shift($args)).'.'.$type_ext;
    $instr   = array_shift($args);
    $notes   = rawurldecode(implode(" ",$args));
    if ($name==""||$instr==""||$notes=="")
    {
      print "Wrong usage.";
      exit;
    }
  }
  $instr     = "%%MIDI program ".$instr."�";

  $formats   = array('wav'=>'w', 'ogg'=>'v');
  $fmt       = $formats[$type_ext];
  $chn       = "M"; # [S]tereo / [M]onophonic

  $abc_h_str = '';
  foreach (array_keys($abc_h) as $par) $abc_h_str .= $par . ":" . $abc_h[$par] . "�";

# $disp_type = "attachment;";
  $disp_type = "inline;";

  $type = "audio/$type_ext";

  if ($type_ext=='abc') $type = 'text/plain';

  header("Content-Type: $type");
  header("Content-Disposition: $disp_type filename=$name;");

  file_put_contents("pipein", $abc_h_str.$instr.$notes."�");

/*
  switch($type_ext)
  {

    case 'abc': print `  echo -e "$abc_h_str$instr$notes"` ; break;
    case 'mid': print `( echo -e "$abc_h_str$instr$notes" | $abc2midi - -o /dev/stderr > /dev/null ) 2>&1` ; break;
    default   : print `( echo -e "$abc_h_str$instr$notes" | $abc2midi - -o /dev/stderr > /dev/null ) 2>&1 | $timidity -O${fmt}${chn}s1l - -o -` ;
  }
*/

?>
