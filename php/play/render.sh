#!/bin/bash

# Baseado no exemplo apresentado em:
# <http://wiki.softwarelivre.org/bin/view/TWikiBar/TWikiBarPapo011#Bloqueio_de_arquivos>

while :
do
  cat pipein | while read line
  do
    echo "$lin"e| tr "§" "\n" > pipeout
  done
done

# EOF
