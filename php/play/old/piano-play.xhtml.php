<?php

  $title        = "SVG+HTML5 Piano";
  $favicon      = "favicon.ico";

  include('common.inc.php');

  $arr_ip       = explode(".",$ip);
  $a_class      = $arr_ip[0];
  $server       = $a_class == 10 ? "az" : "ceat.net:8000";

  $vxht_alt     = 'Valid XHTML 1.1 + MathML 2.0 + SVG 1.1';
  $vxht_url     = "http://validator.w3.org/check?uri=referer";

  $vcss_warning = 2;
  $vcss_profile = 'none';
  $vcss_alt     = 'Valid CSS';
  $vcss_url     = "http://jigsaw.w3.org/css-validator/check?uri=referer&amp;warning=$vcss_warning&amp;profile=$vcss_profile";

# $encoding     = "ISO-8859-1";
  $encoding     = "UTF-8";

  echo '<?xml version="1.0" encoding="'.$encoding.'" standalone="no"?>';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN" "http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg-flat.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" dir="ltr">

<head>

  <title><?print$title?></title>

  <meta http-equiv="content-type"       content="<?print$type?>; charset=<?print$encoding?>" />
  <meta http-equiv="content-style-type" content="text/css" />

  <link href="<?print$favicon?>" rel="icon" type="image/vnd.microsoft.icon" />
  <link href="<?print$favicon?>" rel="shortcut icon" />



  <style type="text/css">

    body
    {
      font-family: sans-serif;
      font-size: 10pt;
      background: white;
      margin: 0px;
    }

    a
    {
      text-decoration: none;
      color: blue;
    }

    a:hover
    {
      text-decoration: underline;
    }

    th
    {
      text-align: right;
      padding-right: 5px;
    }

  </style>



  <script type="text/javascript">
  <![CDATA[

    function toque(tom,duração)
    {

      var t = tom.substr(0,1);		// tom
      var o = tom.substr(1,1);		// oitava
      var a = tom.substr(2,1);		// acidente
      var q;				// quantidade
      var c;				// caracter

      switch(a)
      {
        case 's': a = '^'; break;	// sustenido (sharp)
        case 'f': a = '_'; break;	// bemol (flat)
      }

      if (o<=4)
      {
        t = t.toUpperCase();
        q = 4-o;
        c = ",";
      }
      else
      {
        t = t.toLowerCase();
        q = o-5;
        c = "'";
      }

      var name = tom+','+duração;
      var abc  = a+t+Array(q+1).join(c);
      var url  = 'http://<?print$server?>/svl/audio/mkwave?'+name+'+'+abc+duração;

      var audio = document.createElement('audio');
      audio.setAttribute('src', url);
      audio.play();

    }

  ]]>
  </script>



</head>

<body>

<svg:svg
  xmlns:svg   = "http://www.w3.org/2000/svg"
  xmlns:xlink = "http://www.w3.org/1999/xlink"
  width       = "1611"
  height      = "677"
  version     = "1.1"
  >

  <svg:defs>

    <svg:rect
      id     = "branca"
      width  = "30"
      height = "200"
      rx     = "3"
      ry     = "3"
      style  = "fill:#ffffff;fill-opacity:1;stroke:#000000;stroke-width:1;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
      />

    <svg:rect
      id     = "preta"
      width  = "19"
      height = "129"
      rx     = "2"
      ry     = "2"
      style  = "fill:#000000;fill-opacity:1;stroke:#000000;stroke-width:1;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
      />

  </svg:defs>

  <svg:g
     id="layer1"
     transform="translate(317.95901,47.331836)">
    <svg:g
       id="g4124">
      <svg:g
         style="stroke:none"
         id="g12657">
        <svg:g
           id="g11504"
           transform="matrix(0.9999993,0,0,1.7695124,3.296554e-4,-330.25076)"
           style="stroke:none;stroke-width:0.75253963;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none">
          <svg:rect
             id="rect9292"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.75253963;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="160.16812"
             x="-266.45898"
             ry="1.705032"
             rx="3.0299726"
             height="269"
             width="28.99999" />
          <svg:rect
             width="28.99999"
             height="269"
             rx="3.0299726"
             ry="1.705032"
             x="-236.45898"
             y="160.16817"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:0.75253963;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12522" />
        </svg:g>
        <svg:g
           style="stroke:none"
           id="g12550">
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-86.459503"
             y="-46.83131"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12526" />
          <svg:rect
             id="rect12528"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.83131"
             x="-56.459503"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             id="rect12532"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-146.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-116.4595"
             y="-46.831203"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12534" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-206.4595"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12538" />
          <svg:rect
             id="rect12540"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-176.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-26.458485"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12548" />
        </svg:g>
        <svg:g
           style="stroke:none"
           id="g12559"
           transform="translate(209.99898,1.8329278e-6)">
          <svg:rect
             id="rect12561"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.83131"
             x="-86.459503"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-56.459503"
             y="-46.83131"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12563" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-146.4595"
             y="-46.831203"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12565" />
          <svg:rect
             id="rect12567"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-116.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             id="rect12569"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-206.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-176.4595"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12571" />
          <svg:rect
             id="rect12573"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-26.458485"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
        </svg:g>
        <svg:g
           style="stroke:none"
           transform="translate(419.99898,1.8329278e-6)"
           id="g12575">
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-86.459503"
             y="-46.83131"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12577" />
          <svg:rect
             id="rect12579"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.83131"
             x="-56.459503"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             id="rect12581"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-146.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-116.4595"
             y="-46.831203"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12583" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-206.4595"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12585" />
          <svg:rect
             id="rect12587"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-176.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-26.458485"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12589" />
        </svg:g>
        <svg:g
           style="stroke:none"
           id="g12591"
           transform="translate(629.99898,1.8329278e-6)">
          <svg:rect
             id="rect12593"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.83131"
             x="-86.459503"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-56.459503"
             y="-46.83131"
             style="fill:#ffd5d5;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12595" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-146.4595"
             y="-46.831203"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12597" />
          <svg:rect
             id="rect12599"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-116.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             id="rect12601"
             style="fill:#ffd5d5;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-206.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-176.4595"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12603" />
          <svg:rect
             id="rect12605"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-26.458485"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
        </svg:g>
        <svg:g
           style="stroke:none"
           transform="translate(839.99898,-4.3204875e-7)"
           id="g12607">
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-86.459503"
             y="-46.83131"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12609" />
          <svg:rect
             id="rect12611"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.83131"
             x="-56.459503"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             id="rect12613"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-146.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-116.4595"
             y="-46.831203"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12615" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-206.4595"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12617" />
          <svg:rect
             id="rect12619"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-176.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-26.458485"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12621" />
        </svg:g>
        <svg:g
           style="stroke:none"
           id="g12623"
           transform="translate(1049.999,1.8329278e-6)">
          <svg:rect
             id="rect12625"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.83131"
             x="-86.459503"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-56.459503"
             y="-46.83131"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12627" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-146.4595"
             y="-46.831203"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12629" />
          <svg:rect
             id="rect12631"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-116.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             id="rect12633"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-206.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-176.4595"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12635" />
          <svg:rect
             id="rect12637"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-26.458485"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
        </svg:g>
        <svg:g
           style="stroke:none"
           transform="translate(1259.999,-4.3204875e-7)"
           id="g12639">
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-86.459503"
             y="-46.83131"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12641" />
          <svg:rect
             id="rect12643"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.83131"
             x="-56.459503"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             id="rect12645"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-146.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-116.4595"
             y="-46.831203"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12647" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-206.4595"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12649" />
          <svg:rect
             id="rect12651"
             style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             y="-46.831203"
             x="-176.4595"
             ry="3.0170753"
             rx="3.0299704"
             height="475.99884"
             width="28.999969" />
          <svg:rect
             width="28.999969"
             height="475.99884"
             rx="3.0299704"
             ry="3.0170753"
             x="-26.458485"
             y="-46.831203"
             style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
             id="rect12653" />
        </svg:g>
        <svg:rect
           width="28.999969"
           height="475.99884"
           rx="3.0299704"
           ry="3.0170753"
           x="1263.5405"
           y="-46.831203"
           style="fill:#e3f4d7;fill-opacity:1;stroke:none;stroke-width:1.00105059;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
           id="rect12655" />
      </svg:g>



      <svg:g id="teclado">

        <svg:g id="o0" transform="translate(-210,-3)">
          <svg:a xlink:href="javascript:toque('a0' ,1)"><svg:use id="a0"      x="-056.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('b0' ,1)"><svg:use id="b0"      x="-026.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('a0s',1)"><svg:use id="a0s:b0f" x="-036.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
        </svg:g>

        <svg:g id="o1" transform="translate(0,-3)">
          <svg:a xlink:href="javascript:toque('c1' ,1)"><svg:use id="c1"      x="-206.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('d1' ,1)"><svg:use id="d1"      x="-176.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('e1' ,1)"><svg:use id="e1"      x="-146.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('f1' ,1)"><svg:use id="f1"      x="-116.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('g1' ,1)"><svg:use id="g1"      x="-086.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('a1' ,1)"><svg:use id="a1"      x="-056.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('b1' ,1)"><svg:use id="b1"      x="-026.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('c1s',1)"><svg:use id="c1s:d1f" x="-186.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('d1s',1)"><svg:use id="d1s:e1f" x="-156.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('f1s',1)"><svg:use id="f1s:g1f" x="-096.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('g1s',1)"><svg:use id="g1s:a1f" x="-066.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('a1s',1)"><svg:use id="a1s:b1f" x="-036.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
        </svg:g>

        <svg:g id="o2" transform="translate(210,-3)">
          <svg:a xlink:href="javascript:toque('c2' ,1)"><svg:use id="c2"      x="-206.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('d2' ,1)"><svg:use id="d2"      x="-176.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('e2' ,1)"><svg:use id="e2"      x="-146.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('f2' ,1)"><svg:use id="f2"      x="-116.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('g2' ,1)"><svg:use id="g2"      x="-086.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('a2' ,1)"><svg:use id="a2"      x="-056.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('b2' ,1)"><svg:use id="b2"      x="-026.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('c2s',1)"><svg:use id="c2s:d2f" x="-186.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('d2s',1)"><svg:use id="d2s:e2f" x="-156.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('f2s',1)"><svg:use id="f2s:g2f" x="-096.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('g2s',1)"><svg:use id="g2s:a2f" x="-066.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('a2s',1)"><svg:use id="a2s:b2f" x="-036.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
        </svg:g>

        <svg:g id="o3" transform="translate(420,-3)">
          <svg:a xlink:href="javascript:toque('c3' ,1)"><svg:use id="c3"      x="-206.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('d3' ,1)"><svg:use id="d3"      x="-176.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('e3' ,1)"><svg:use id="e3"      x="-146.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('f3' ,1)"><svg:use id="f3"      x="-116.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('g3' ,1)"><svg:use id="g3"      x="-086.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('a3' ,1)"><svg:use id="a3"      x="-056.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('b3' ,1)"><svg:use id="b3"      x="-026.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('c3s',1)"><svg:use id="c3s:d3f" x="-186.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('d3s',1)"><svg:use id="d3s:e3f" x="-156.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('f3s',1)"><svg:use id="f3s:g3f" x="-096.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('g3s',1)"><svg:use id="g3s:a3f" x="-066.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('a3s',1)"><svg:use id="a3s:b3f" x="-036.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
        </svg:g>


        <svg:g id="o4" transform="translate(630,-3)">
          <svg:a xlink:href="javascript:toque('c4' ,1)"><svg:use id="c4"      x="-206.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('d4' ,1)"><svg:use id="d4"      x="-176.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('e4' ,1)"><svg:use id="e4"      x="-146.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('f4' ,1)"><svg:use id="f4"      x="-116.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('g4' ,1)"><svg:use id="g4"      x="-086.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('a4' ,1)"><svg:use id="a4"      x="-056.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('b4' ,1)"><svg:use id="b4"      x="-026.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('c4s',1)"><svg:use id="c4s:d4f" x="-186.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('d4s',1)"><svg:use id="d4s:e4f" x="-156.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('f4s',1)"><svg:use id="f4s:g4f" x="-096.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('g4s',1)"><svg:use id="g4s:a4f" x="-066.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('a4s',1)"><svg:use id="a4s:b4f" x="-036.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
        </svg:g>

        <svg:g id="o5" transform="translate(840,-3)">
          <svg:a xlink:href="javascript:toque('c5' ,1)"><svg:use id="c5"      x="-206.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('d5' ,1)"><svg:use id="d5"      x="-176.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('e5' ,1)"><svg:use id="e5"      x="-146.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('f5' ,1)"><svg:use id="f5"      x="-116.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('g5' ,1)"><svg:use id="g5"      x="-086.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('a5' ,1)"><svg:use id="a5"      x="-056.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('b5' ,1)"><svg:use id="b5"      x="-026.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('c5s',1)"><svg:use id="c5s:d5f" x="-186.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('d5s',1)"><svg:use id="d5s:e5f" x="-156.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('f5s',1)"><svg:use id="f5s:g5f" x="-096.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('g5s',1)"><svg:use id="g5s:a5f" x="-066.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('a5s',1)"><svg:use id="a5s:b5f" x="-036.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
        </svg:g>

        <svg:g id="o6" transform="translate(1050,-3)">
          <svg:a xlink:href="javascript:toque('c6' ,1)"><svg:use id="c6"      x="-206.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('d6' ,1)"><svg:use id="d6"      x="-176.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('e6' ,1)"><svg:use id="e6"      x="-146.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('f6' ,1)"><svg:use id="f6"      x="-116.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('g6' ,1)"><svg:use id="g6"      x="-086.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('a6' ,1)"><svg:use id="a6"      x="-056.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('b6' ,1)"><svg:use id="b6"      x="-026.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('c6s',1)"><svg:use id="c6s:d6f" x="-186.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('d6s',1)"><svg:use id="d6s:e6f" x="-156.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('f6s',1)"><svg:use id="f6s:g6f" x="-096.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('g6s',1)"><svg:use id="g6s:a6f" x="-066.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('a6s',1)"><svg:use id="a6s:b6f" x="-036.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
        </svg:g>

        <svg:g id="o7" transform="translate(1260,-3)">
          <svg:a xlink:href="javascript:toque('c7' ,1)"><svg:use id="c7"      x="-206.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('d7' ,1)"><svg:use id="d7"      x="-176.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('e7' ,1)"><svg:use id="e7"      x="-146.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('f7' ,1)"><svg:use id="f7"      x="-116.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('g7' ,1)"><svg:use id="g7"      x="-086.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('a7' ,1)"><svg:use id="a8"      x="-056.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('b7' ,1)"><svg:use id="b8"      x="-026.459068" y="432.16809" xlink:href="#branca" /></svg:a>
          <svg:a xlink:href="javascript:toque('c7s',1)"><svg:use id="c7s:d7f" x="-186.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('d7s',1)"><svg:use id="d7s:e7f" x="-156.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('f7s',1)"><svg:use id="f7s:g7f" x="-096.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('g7s',1)"><svg:use id="g7s:a7f" x="-066.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
          <svg:a xlink:href="javascript:toque('a7s',1)"><svg:use id="a7s:b7f" x="-036.459068" y="436.16809" xlink:href="#preta"  /></svg:a>
        </svg:g>

        <svg:g id="o8" transform="translate(1470,-3)">
          <svg:a xlink:href="javascript:toque('c8' ,1)"><svg:use id="c8"      x="-206.459068" y="432.16809" xlink:href="#branca" /></svg:a>
        </svg:g>

      </svg:g>



      <svg:g
         transform="translate(1470,-2.0000186)"
         id="g10128" />
      <svg:g
         transform="translate(30.000001,-10.00005)"
         id="g9328">
        <svg:g
           id="g8799">
          <svg:g
             transform="translate(-0.9999955,-3.0000045)"
             id="g7952">
            <svg:path
               id="text7936"
               d="M 40.333911,383.54972 L 55.667727,383.54972 L 57.439094,388.51225 L 56.249168,388.95848 C 55.951669,388.14718 55.627145,387.5094 55.275592,387.04513 C 54.924007,386.58089 54.502576,386.18651 54.011296,385.86196 C 53.519986,385.53746 53.024185,385.32111 52.523889,385.21291 C 52.023566,385.10476 51.119855,385.05067 49.812751,385.05065 L 45.242355,385.05065 L 45.242355,391.96033 L 47.514031,391.96033 C 48.352378,391.96034 48.985653,391.81837 49.413856,391.53439 C 49.842038,391.25045 50.137265,390.89662 50.299539,390.47293 C 50.425732,390.15743 50.488834,389.60754 50.488845,388.82326 L 50.488845,388.51225 L 51.81399,388.51225 L 51.81399,396.78765 L 50.488845,396.78765 L 50.488845,396.07099 C 50.488834,395.43997 50.441507,394.98474 50.346866,394.70528 C 50.252201,394.42583 50.078671,394.16216 49.826273,393.91425 C 49.573854,393.66635 49.301163,393.49733 49.008199,393.40718 C 48.715215,393.31704 48.21716,393.27197 47.514031,393.27196 L 45.242355,393.27196 L 45.242355,399.41089 C 45.242348,400.05093 45.271646,400.44081 45.330247,400.58054 C 45.388835,400.72026 45.485742,400.82168 45.620967,400.88478 C 45.756179,400.94788 46.125777,400.97943 46.729762,400.97943 L 49.555835,400.97943 C 51.133376,400.97943 52.36612,400.79013 53.254071,400.41151 C 54.141992,400.0329 54.924007,399.45146 55.600117,398.66719 C 56.131961,398.0542 56.641285,397.1888 57.12809,396.07099 L 58.358582,396.50369 L 56.249168,402.41275 L 40.333911,402.41275 L 40.333911,401.27691 L 40.874786,401.27691 C 41.505805,401.27691 41.897939,401.23635 42.05119,401.15522 C 42.285566,401.03803 42.465858,400.84872 42.592065,400.5873 C 42.682208,400.40701 42.72728,400.01487 42.727284,399.41089 L 42.727284,386.42988 C 42.72728,385.84395 42.668686,385.45182 42.5515,385.25348 C 42.434307,385.05518 42.231479,384.90193 41.943015,384.79374 C 41.789764,384.73065 41.253397,384.6991 40.333911,384.69908 L 40.333911,383.54972 z"
               style="font-size:27.69281769px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9431515,1.060275)" />
            <svg:path
               id="text7946"
               d="M 58.601648,387.33977 L 58.099733,387.40186 C 58.072136,387.15694 58.058338,386.95687 58.058338,386.80163 C 58.058338,386.39458 58.168724,386.00995 58.389499,385.64774 C 58.610271,385.28554 58.918147,385.00268 59.313126,384.79914 C 59.708102,384.59562 60.16086,384.49386 60.671402,384.49385 C 61.164689,384.49386 61.597612,384.58959 61.970172,384.78103 C 62.342722,384.97249 62.628175,385.23466 62.826532,385.56754 C 63.024878,385.90043 63.124053,386.28075 63.124059,386.70849 C 63.124053,387.0707 63.059373,387.40617 62.93002,387.71491 C 62.800655,388.02365 62.592817,388.33497 62.306506,388.64888 C 62.092627,388.88346 61.659704,389.24222 61.007736,389.72516 C 60.186732,390.33918 59.557182,390.78418 59.119087,391.06015 L 61.809766,391.06015 C 62.127123,391.06015 62.334098,391.03083 62.430692,390.97218 C 62.56522,390.89629 62.656634,390.72036 62.704934,390.44439 L 62.751503,390.14945 L 63.191326,390.14945 L 62.839468,392.09502 L 62.508307,392.09502 L 62.451389,391.8363 L 57.928979,391.8363 L 57.928979,391.40166 L 58.75688,390.77556 C 59.384703,390.30986 59.845223,389.95283 60.13844,389.70446 C 60.693821,389.23532 61.112083,388.8343 61.393228,388.50141 C 61.674365,388.16853 61.867542,387.86842 61.972759,387.60107 C 62.077966,387.33373 62.130573,387.05345 62.130577,386.76023 C 62.130573,386.27385 61.974479,385.87542 61.662296,385.56495 C 61.350104,385.2545 60.943915,385.09926 60.443729,385.09926 C 60.084969,385.09926 59.76502,385.17688 59.483881,385.3321 C 59.202737,385.48734 58.981102,385.70467 58.818973,385.98408 C 58.656841,386.2635 58.575775,386.56534 58.575777,386.88959 C 58.575775,387.01033 58.584399,387.16039 58.601648,387.33977 L 58.601648,387.33977 z"
               style="font-size:21.19427109px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.950146,1.0524698)" />
          </svg:g>
          <svg:g
             id="g8558"
             transform="translate(29.000001,-3.0000045)">
            <svg:path
               id="text8560"
               d="M 39.380858,392.83193 L 54.157874,392.83193 L 55.944411,397.90071 L 54.739537,398.35773 C 54.231719,397.17595 53.733151,396.32654 53.243832,395.80949 C 52.754481,395.29247 52.186667,394.92317 51.540389,394.70156 C 50.894084,394.47999 49.878482,394.3692 48.493582,394.36918 L 44.324995,394.36918 L 44.324995,401.79231 L 46.222326,401.79231 C 47.219453,401.79232 47.93499,401.69538 48.36894,401.50148 C 48.802868,401.30761 49.135247,401.00062 49.366077,400.58052 C 49.596884,400.16044 49.712293,399.56262 49.712305,398.78705 L 49.712305,398.27464 L 51.069519,398.27464 L 51.069519,406.7503 L 49.712305,406.7503 L 49.712305,406.00245 C 49.712293,405.27307 49.6015,404.70525 49.379926,404.29901 C 49.158329,403.89277 48.855956,403.59963 48.472808,403.41959 C 48.089639,403.23956 47.408724,403.14954 46.430062,403.14953 L 44.324995,403.14953 L 44.324995,409.16005 C 44.324989,409.71402 44.382694,410.09948 44.49811,410.31645 C 44.613512,410.53342 44.784318,410.69961 45.010527,410.81502 C 45.236722,410.93043 45.640655,410.98813 46.222326,410.98813 L 46.734743,410.98813 L 46.734743,412.15146 L 39.380858,412.15146 L 39.380858,410.98813 L 39.851728,410.98813 C 40.442622,410.98813 40.85117,410.93274 41.077376,410.82194 C 41.303575,410.71115 41.469764,410.53804 41.575944,410.3026 C 41.682117,410.06717 41.735205,409.64015 41.735209,409.02156 L 41.735209,395.87874 C 41.735205,395.30632 41.684425,394.9047 41.582869,394.67386 C 41.481305,394.44306 41.326657,394.27457 41.118923,394.16837 C 40.911183,394.06221 40.548798,394.00912 40.031767,394.00911 L 39.380858,394.00911 L 39.380858,392.83193 z"
               style="font-size:28.36300659px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9659766,1.0352218)" />
            <svg:path
               id="text8566"
               d="M 58.601648,387.33977 L 58.099733,387.40186 C 58.072136,387.15694 58.058338,386.95687 58.058338,386.80163 C 58.058338,386.39458 58.168724,386.00995 58.389499,385.64774 C 58.610271,385.28554 58.918147,385.00268 59.313126,384.79914 C 59.708102,384.59562 60.16086,384.49386 60.671402,384.49385 C 61.164689,384.49386 61.597612,384.58959 61.970172,384.78103 C 62.342722,384.97249 62.628175,385.23466 62.826532,385.56754 C 63.024878,385.90043 63.124053,386.28075 63.124059,386.70849 C 63.124053,387.0707 63.059373,387.40617 62.93002,387.71491 C 62.800655,388.02365 62.592817,388.33497 62.306506,388.64888 C 62.092627,388.88346 61.659704,389.24222 61.007736,389.72516 C 60.186732,390.33918 59.557182,390.78418 59.119087,391.06015 L 61.809766,391.06015 C 62.127123,391.06015 62.334098,391.03083 62.430692,390.97218 C 62.56522,390.89629 62.656634,390.72036 62.704934,390.44439 L 62.751503,390.14945 L 63.191326,390.14945 L 62.839468,392.09502 L 62.508307,392.09502 L 62.451389,391.8363 L 57.928979,391.8363 L 57.928979,391.40166 L 58.75688,390.77556 C 59.384703,390.30986 59.845223,389.95283 60.13844,389.70446 C 60.693821,389.23532 61.112083,388.8343 61.393228,388.50141 C 61.674365,388.16853 61.867542,387.86842 61.972759,387.60107 C 62.077966,387.33373 62.130573,387.05345 62.130577,386.76023 C 62.130573,386.27385 61.974479,385.87542 61.662296,385.56495 C 61.350104,385.2545 60.943915,385.09926 60.443729,385.09926 C 60.084969,385.09926 59.76502,385.17688 59.483881,385.3321 C 59.202737,385.48734 58.981102,385.70467 58.818973,385.98408 C 58.656841,386.2635 58.575775,386.56534 58.575777,386.88959 C 58.575775,387.01033 58.584399,387.16039 58.601648,387.33977 L 58.601648,387.33977 z"
               style="font-size:21.19427109px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.950146,1.0524698)" />
          </svg:g>
          <svg:g
             transform="translate(59.000005,-3.0000041)"
             id="g8572">
            <svg:path
               id="text8574"
               d="M 54.600565,383.5977 L 55.554167,383.5977 L 57.043354,389.21481 L 55.86768,389.61976 C 55.083882,387.8432 54.182532,386.57609 53.16363,385.81842 C 52.144699,385.06078 50.990798,384.68195 49.701923,384.68193 C 47.846962,384.68195 46.292461,385.45267 45.038416,386.99409 C 43.784359,388.53554 43.157333,390.37308 43.157337,392.5067 C 43.157333,394.03944 43.477377,395.44589 44.117471,396.72606 C 44.757555,398.00624 45.613183,398.98162 46.68436,399.65218 C 47.755521,400.32275 48.892004,400.65804 50.093814,400.65804 C 50.868877,400.65804 51.606938,400.51652 52.308001,400.23349 C 53.009037,399.95046 53.577279,399.613 54.012728,399.2211 C 54.334934,398.92501 54.552652,398.59843 54.66588,398.24137 C 54.779077,397.88432 54.83133,397.38793 54.822637,396.75219 L 54.822637,395.89003 C 54.822621,395.20205 54.742066,394.71436 54.58097,394.42696 C 54.419844,394.13959 54.191241,393.93493 53.895161,393.813 C 53.66872,393.71721 53.059112,393.66932 52.066334,393.66931 L 52.066334,392.57201 L 59.460018,392.57201 L 59.460018,393.66931 L 58.859118,393.66931 C 58.345285,393.66932 57.994761,393.71939 57.807543,393.81953 C 57.620287,393.91969 57.483125,394.07427 57.396057,394.28327 C 57.308951,394.49229 57.265408,394.9016 57.265426,395.5112 L 57.265426,401.82065 L 55.972185,401.82065 L 55.279843,400.22696 C 54.548297,400.75819 53.76887,401.17185 52.941558,401.46795 C 51.704911,401.91209 50.48134,402.13416 49.270843,402.13416 C 46.823691,402.13416 44.727074,401.23499 42.980986,399.43664 C 41.23489,397.6383 40.361844,395.39799 40.361846,392.71571 C 40.361844,390.05086 41.245776,387.81272 43.013644,386.0013 C 44.781504,384.18991 46.897715,383.28421 49.362284,383.28419 C 49.98059,383.28421 50.590199,383.34517 51.19111,383.46707 C 51.791997,383.58901 52.340645,383.76318 52.837054,383.98959 C 53.211514,384.15507 53.646948,384.42504 54.143358,384.7995 L 54.600565,383.5977 z"
               style="font-size:26.75311661px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9424986,1.0610095)" />
            <svg:path
               id="text8578"
               d="M 58.601648,387.33977 L 58.099733,387.40186 C 58.072136,387.15694 58.058338,386.95687 58.058338,386.80163 C 58.058338,386.39458 58.168724,386.00995 58.389499,385.64774 C 58.610271,385.28554 58.918147,385.00268 59.313126,384.79914 C 59.708102,384.59562 60.16086,384.49386 60.671402,384.49385 C 61.164689,384.49386 61.597612,384.58959 61.970172,384.78103 C 62.342722,384.97249 62.628175,385.23466 62.826532,385.56754 C 63.024878,385.90043 63.124053,386.28075 63.124059,386.70849 C 63.124053,387.0707 63.059373,387.40617 62.93002,387.71491 C 62.800655,388.02365 62.592817,388.33497 62.306506,388.64888 C 62.092627,388.88346 61.659704,389.24222 61.007736,389.72516 C 60.186732,390.33918 59.557182,390.78418 59.119087,391.06015 L 61.809766,391.06015 C 62.127123,391.06015 62.334098,391.03083 62.430692,390.97218 C 62.56522,390.89629 62.656634,390.72036 62.704934,390.44439 L 62.751503,390.14945 L 63.191326,390.14945 L 62.839468,392.09502 L 62.508307,392.09502 L 62.451389,391.8363 L 57.928979,391.8363 L 57.928979,391.40166 L 58.75688,390.77556 C 59.384703,390.30986 59.845223,389.95283 60.13844,389.70446 C 60.693821,389.23532 61.112083,388.8343 61.393228,388.50141 C 61.674365,388.16853 61.867542,387.86842 61.972759,387.60107 C 62.077966,387.33373 62.130573,387.05345 62.130577,386.76023 C 62.130573,386.27385 61.974479,385.87542 61.662296,385.56495 C 61.350104,385.2545 60.943915,385.09926 60.443729,385.09926 C 60.084969,385.09926 59.76502,385.17688 59.483881,385.3321 C 59.202737,385.48734 58.981102,385.70467 58.818973,385.98408 C 58.656841,386.2635 58.575775,386.56534 58.575777,386.88959 C 58.575775,387.01033 58.584399,387.16039 58.601648,387.33977 L 58.601648,387.33977 z"
               style="font-size:21.19427109px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.950146,1.0524698)" />
          </svg:g>
          <svg:g
             id="g8584"
             transform="translate(89.000005,-3.0000041)">
            <svg:path
               id="text8586"
               d="M 48.322339,387.18937 L 50.574624,387.18937 L 56.485167,403.10552 C 56.776355,403.90633 57.069834,404.43641 57.365606,404.69577 C 57.661343,404.95512 58.059474,405.0848 58.56,405.0848 L 58.860304,405.0848 L 58.860304,406.23141 L 52.076148,406.23141 L 52.076148,405.0848 C 52.294539,405.0666 52.508392,405.0575 52.717708,405.0575 C 53.154502,405.0575 53.441156,405.012 53.577671,404.92099 C 53.841562,404.74809 53.973514,404.52969 53.973528,404.26578 C 53.973514,404.07468 53.846112,403.60603 53.591322,402.85981 L 52.335502,399.17425 L 45.428493,399.17425 L 43.95427,403.29662 C 43.799564,403.72433 43.722213,404.02008 43.722217,404.18388 C 43.722213,404.36589 43.769989,404.52514 43.865544,404.66164 C 43.961091,404.79814 44.090768,404.90052 44.254575,404.96877 C 44.418373,405.03702 44.682277,405.07115 45.046287,405.07115 C 45.291986,405.07115 45.492189,405.0757 45.646897,405.0848 L 45.646897,406.23141 L 39.954758,406.23141 L 39.954758,405.0848 C 40.482565,405.0848 40.862496,405.03247 41.09455,404.92782 C 41.326603,404.82317 41.529081,404.65482 41.701985,404.42276 C 41.874886,404.19071 42.102389,403.70158 42.384496,402.95536 L 48.322339,387.18937 z M 45.933551,397.83653 L 51.816794,397.83653 L 48.868347,389.6464 L 45.933551,397.83653 z"
               style="font-size:27.95563889px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9521015,1.0503082)" />
            <svg:path
               id="text8590"
               d="M 58.601648,387.33977 L 58.099733,387.40186 C 58.072136,387.15694 58.058338,386.95687 58.058338,386.80163 C 58.058338,386.39458 58.168724,386.00995 58.389499,385.64774 C 58.610271,385.28554 58.918147,385.00268 59.313126,384.79914 C 59.708102,384.59562 60.16086,384.49386 60.671402,384.49385 C 61.164689,384.49386 61.597612,384.58959 61.970172,384.78103 C 62.342722,384.97249 62.628175,385.23466 62.826532,385.56754 C 63.024878,385.90043 63.124053,386.28075 63.124059,386.70849 C 63.124053,387.0707 63.059373,387.40617 62.93002,387.71491 C 62.800655,388.02365 62.592817,388.33497 62.306506,388.64888 C 62.092627,388.88346 61.659704,389.24222 61.007736,389.72516 C 60.186732,390.33918 59.557182,390.78418 59.119087,391.06015 L 61.809766,391.06015 C 62.127123,391.06015 62.334098,391.03083 62.430692,390.97218 C 62.56522,390.89629 62.656634,390.72036 62.704934,390.44439 L 62.751503,390.14945 L 63.191326,390.14945 L 62.839468,392.09502 L 62.508307,392.09502 L 62.451389,391.8363 L 57.928979,391.8363 L 57.928979,391.40166 L 58.75688,390.77556 C 59.384703,390.30986 59.845223,389.95283 60.13844,389.70446 C 60.693821,389.23532 61.112083,388.8343 61.393228,388.50141 C 61.674365,388.16853 61.867542,387.86842 61.972759,387.60107 C 62.077966,387.33373 62.130573,387.05345 62.130577,386.76023 C 62.130573,386.27385 61.974479,385.87542 61.662296,385.56495 C 61.350104,385.2545 60.943915,385.09926 60.443729,385.09926 C 60.084969,385.09926 59.76502,385.17688 59.483881,385.3321 C 59.202737,385.48734 58.981102,385.70467 58.818973,385.98408 C 58.656841,386.2635 58.575775,386.56534 58.575777,386.88959 C 58.575775,387.01033 58.584399,387.16039 58.601648,387.33977 L 58.601648,387.33977 z"
               style="font-size:21.19427109px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.950146,1.0524698)" />
          </svg:g>
          <svg:g
             transform="translate(119.00001,-3.0000041)"
             id="g8596">
            <svg:path
               id="text8598"
               d="M 41.684272,371.12452 L 50.358867,371.12452 C 52.47844,371.12454 53.930746,371.22049 54.715791,371.41236 C 55.875877,371.6915 56.793926,372.19741 57.469942,372.93009 C 58.145923,373.6628 58.483923,374.54378 58.48394,375.57303 C 58.483923,376.31446 58.30511,376.97956 57.947502,377.56832 C 57.58986,378.1571 57.070868,378.66083 56.390524,379.0795 C 55.95438,379.34991 55.361246,379.58106 54.61112,379.77295 C 55.561865,379.94741 56.285837,380.15675 56.783039,380.40097 C 57.533163,380.76733 58.126298,381.29068 58.562444,381.97103 C 58.998554,382.6514 59.216618,383.41899 59.216636,384.27379 C 59.216618,385.10244 59.002915,385.87003 58.575527,386.57655 C 58.148104,387.28308 57.544067,387.86313 56.763414,388.3167 C 55.982728,388.77028 55.073401,389.0603 54.03543,389.18678 C 52.997432,389.31326 51.928918,389.3765 50.829886,389.3765 L 41.684272,389.3765 L 41.684272,388.27745 L 42.299213,388.27745 C 42.892346,388.27745 43.297945,388.18587 43.516012,388.00269 C 43.830021,387.74974 43.987027,387.33105 43.987031,386.74664 L 43.987031,374.06839 C 43.987027,373.47527 43.939053,373.06967 43.843108,372.85159 C 43.747157,372.63354 43.603234,372.47653 43.411341,372.38057 C 43.219442,372.28464 42.848733,372.23666 42.299213,372.23665 L 41.684272,372.23665 L 41.684272,371.12452 z M 46.433711,372.57683 L 46.433711,379.26268 L 49.770094,379.26268 C 51.331423,379.26269 52.399937,379.18854 52.975638,379.04025 C 53.874049,378.80475 54.560951,378.40351 55.036345,377.83654 C 55.51171,377.26958 55.7494,376.54125 55.749415,375.65153 C 55.7494,374.90141 55.533517,374.27556 55.101764,373.774 C 54.669983,373.27247 54.092113,372.93011 53.368154,372.74692 C 52.897123,372.63354 51.920196,372.57684 50.43737,372.57683 L 46.433711,372.57683 z M 46.433711,380.51873 L 46.433711,386.51113 C 46.433705,387.11299 46.459873,387.47062 46.512214,387.58401 C 46.581989,387.74102 46.72155,387.85005 46.930898,387.9111 C 47.079175,387.95472 47.497858,387.97652 48.186948,387.97652 L 49.966352,387.97652 C 51.7283,387.97652 52.986529,387.8784 53.741043,387.68214 C 54.495532,387.48588 55.138821,387.07156 55.670912,386.43917 C 56.202973,385.80679 56.469011,385.04574 56.469027,384.15604 C 56.469011,383.4059 56.253128,382.74081 55.821376,382.16075 C 55.389594,381.58071 54.792099,381.16202 54.028888,380.9047 C 53.265651,380.64739 51.911473,380.51874 49.966352,380.51873 L 46.433711,380.51873 z"
               style="font-size:26.7957325px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9125979,1.0957729)" />
            <svg:path
               id="text8602"
               d="M 58.601648,387.33977 L 58.099733,387.40186 C 58.072136,387.15694 58.058338,386.95687 58.058338,386.80163 C 58.058338,386.39458 58.168724,386.00995 58.389499,385.64774 C 58.610271,385.28554 58.918147,385.00268 59.313126,384.79914 C 59.708102,384.59562 60.16086,384.49386 60.671402,384.49385 C 61.164689,384.49386 61.597612,384.58959 61.970172,384.78103 C 62.342722,384.97249 62.628175,385.23466 62.826532,385.56754 C 63.024878,385.90043 63.124053,386.28075 63.124059,386.70849 C 63.124053,387.0707 63.059373,387.40617 62.93002,387.71491 C 62.800655,388.02365 62.592817,388.33497 62.306506,388.64888 C 62.092627,388.88346 61.659704,389.24222 61.007736,389.72516 C 60.186732,390.33918 59.557182,390.78418 59.119087,391.06015 L 61.809766,391.06015 C 62.127123,391.06015 62.334098,391.03083 62.430692,390.97218 C 62.56522,390.89629 62.656634,390.72036 62.704934,390.44439 L 62.751503,390.14945 L 63.191326,390.14945 L 62.839468,392.09502 L 62.508307,392.09502 L 62.451389,391.8363 L 57.928979,391.8363 L 57.928979,391.40166 L 58.75688,390.77556 C 59.384703,390.30986 59.845223,389.95283 60.13844,389.70446 C 60.693821,389.23532 61.112083,388.8343 61.393228,388.50141 C 61.674365,388.16853 61.867542,387.86842 61.972759,387.60107 C 62.077966,387.33373 62.130573,387.05345 62.130577,386.76023 C 62.130573,386.27385 61.974479,385.87542 61.662296,385.56495 C 61.350104,385.2545 60.943915,385.09926 60.443729,385.09926 C 60.084969,385.09926 59.76502,385.17688 59.483881,385.3321 C 59.202737,385.48734 58.981102,385.70467 58.818973,385.98408 C 58.656841,386.2635 58.575775,386.56534 58.575777,386.88959 C 58.575775,387.01033 58.584399,387.16039 58.601648,387.33977 L 58.601648,387.33977 z"
               style="font-size:21.19427109px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.950146,1.0524698)" />
          </svg:g>
          <svg:g
             id="g9058"
             transform="translate(-61,-3.0000449)">
            <svg:path
               id="text9088"
               d="M 58.601648,387.3398 L 58.099733,387.40189 C 58.072136,387.15697 58.058338,386.9569 58.058338,386.80166 C 58.058338,386.39461 58.168724,386.00999 58.389499,385.64777 C 58.610271,385.28557 58.918147,385.00271 59.313126,384.79917 C 59.708102,384.59566 60.16086,384.49389 60.671402,384.49389 C 61.164689,384.49389 61.597612,384.58962 61.970172,384.78106 C 62.342722,384.97252 62.628175,385.23469 62.826532,385.56757 C 63.024878,385.90046 63.124053,386.28078 63.124059,386.70852 C 63.124053,387.07073 63.059373,387.4062 62.93002,387.71494 C 62.800655,388.02368 62.592817,388.33501 62.306506,388.64891 C 62.092627,388.88349 61.659704,389.24225 61.007736,389.72519 C 60.186732,390.33921 59.557182,390.78421 59.119087,391.06018 L 61.809766,391.06018 C 62.127123,391.06018 62.334098,391.03086 62.430692,390.97221 C 62.56522,390.89632 62.656634,390.72039 62.704934,390.44443 L 62.751503,390.14949 L 63.191326,390.14949 L 62.839468,392.09505 L 62.508307,392.09505 L 62.451389,391.83633 L 57.928979,391.83633 L 57.928979,391.40169 L 58.75688,390.77559 C 59.384703,390.30989 59.845223,389.95286 60.13844,389.70449 C 60.693821,389.23535 61.112083,388.83433 61.393228,388.50144 C 61.674365,388.16856 61.867542,387.86845 61.972759,387.6011 C 62.077966,387.33376 62.130573,387.05348 62.130577,386.76026 C 62.130573,386.27388 61.974479,385.87545 61.662296,385.56498 C 61.350104,385.25453 60.943915,385.09929 60.443729,385.09929 C 60.084969,385.09929 59.76502,385.17691 59.483881,385.33214 C 59.202737,385.48737 58.981102,385.7047 58.818973,385.98411 C 58.656841,386.26353 58.575775,386.56537 58.575777,386.88962 C 58.575775,387.01036 58.584399,387.16042 58.601648,387.3398 L 58.601648,387.3398 z"
               style="font-size:21.19427109px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.950146,1.0524698)" />
            <svg:path
               id="text9060"
               d="M 55.06726,378.18807 L 55.994535,378.18807 L 57.630145,384.17672 L 56.432415,384.51157 C 55.788458,383.12926 55.299063,382.20198 54.964229,381.72975 C 54.414718,380.96562 53.729995,380.36031 52.910058,379.91383 C 52.090094,379.46738 51.186431,379.24415 50.199067,379.24413 C 49.039963,379.24415 47.973169,379.55539 46.99868,380.17785 C 46.024177,380.80034 45.234276,381.71259 44.628978,382.9146 C 44.023669,384.11663 43.721017,385.46891 43.721021,386.97142 C 43.721017,388.51689 44.027962,389.89492 44.641857,391.10553 C 45.255741,392.31614 46.086424,393.24771 47.133908,393.90023 C 48.181376,394.55276 49.310418,394.87902 50.521037,394.87902 C 51.662947,394.87902 52.830625,394.55276 54.024076,393.90023 C 55.217497,393.24771 56.252095,392.33331 57.127871,391.15704 L 58.158176,391.86538 C 57.625834,392.65528 57.050581,393.32498 56.432415,393.87447 C 55.453609,394.75882 54.436183,395.41134 53.380135,395.83205 C 52.324059,396.25276 51.164966,396.46311 49.902854,396.46311 C 47.112436,396.46311 44.845766,395.42852 43.102838,393.35932 C 41.660408,391.64215 40.939195,389.59871 40.939196,387.229 C 40.939195,385.48608 41.338438,383.90842 42.136926,382.49604 C 42.93541,381.08367 44.036548,379.96107 45.440343,379.12822 C 46.844127,378.29541 48.357387,377.879 49.980127,377.87898 C 50.847289,377.879 51.652215,377.99705 52.394905,378.23315 C 53.13757,378.46928 53.89098,378.8492 54.655138,379.37292 L 55.06726,378.18807 z"
               style="font-size:26.37582016px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9292071,1.0761864)" />
          </svg:g>
          <svg:g
             transform="translate(-31,-3.0000649)"
             id="g9070">
            <svg:path
               id="text9082"
               d="M 58.601648,387.33983 L 58.099733,387.40192 C 58.072136,387.157 58.058338,386.95693 58.058338,386.80169 C 58.058338,386.39464 58.168724,386.01002 58.389499,385.6478 C 58.610271,385.2856 58.918147,385.00274 59.313126,384.7992 C 59.708102,384.59569 60.16086,384.49392 60.671402,384.49392 C 61.164689,384.49392 61.597612,384.58965 61.970172,384.78109 C 62.342722,384.97255 62.628175,385.23472 62.826532,385.5676 C 63.024878,385.90049 63.124053,386.28081 63.124059,386.70855 C 63.124053,387.07076 63.059373,387.40624 62.93002,387.71497 C 62.800655,388.02371 62.592817,388.33504 62.306506,388.64895 C 62.092627,388.88352 61.659704,389.24228 61.007736,389.72522 C 60.186732,390.33924 59.557182,390.78424 59.119087,391.06021 L 61.809766,391.06021 C 62.127123,391.06021 62.334098,391.03089 62.430692,390.97224 C 62.56522,390.89635 62.656634,390.72042 62.704934,390.44446 L 62.751503,390.14952 L 63.191326,390.14952 L 62.839468,392.09508 L 62.508307,392.09508 L 62.451389,391.83636 L 57.928979,391.83636 L 57.928979,391.40172 L 58.75688,390.77562 C 59.384703,390.30992 59.845223,389.95289 60.13844,389.70452 C 60.693821,389.23538 61.112083,388.83436 61.393228,388.50148 C 61.674365,388.16859 61.867542,387.86848 61.972759,387.60113 C 62.077966,387.33379 62.130573,387.05352 62.130577,386.7603 C 62.130573,386.27391 61.974479,385.87548 61.662296,385.56501 C 61.350104,385.25456 60.943915,385.09933 60.443729,385.09932 C 60.084969,385.09933 59.76502,385.17694 59.483881,385.33217 C 59.202737,385.4874 58.981102,385.70473 58.818973,385.98414 C 58.656841,386.26356 58.575775,386.5654 58.575777,386.88965 C 58.575775,387.0104 58.584399,387.16045 58.601648,387.33983 L 58.601648,387.33983 z"
               style="font-size:21.19427109px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.950146,1.0524698)" />
            <svg:path
               id="text9078"
               d="M 43.136549,359.70468 L 43.136549,358.63 L 49.432932,358.63 C 52.315602,358.63002 54.325891,358.81124 55.463804,359.17366 C 57.141139,359.7047 58.502404,360.69509 59.547603,362.14485 C 60.702342,363.74635 61.279721,365.58806 61.27974,367.66999 C 61.279721,369.08605 60.995246,370.40306 60.426315,371.62103 C 59.857347,372.83901 59.109283,373.81887 58.182122,374.56061 C 57.415077,375.18434 56.492115,375.62475 55.413231,375.88183 C 54.33432,376.13892 53.246994,376.26746 52.151249,376.26746 L 43.136549,376.26746 L 43.136549,375.20542 L 43.869862,375.20542 L 44.110086,375.21806 C 44.421952,375.21806 44.676926,375.15484 44.875008,375.02841 C 45.073084,374.90198 45.214267,374.7334 45.29856,374.52268 C 45.357558,374.36253 45.38706,373.99166 45.387063,373.41006 L 45.387063,361.61383 C 45.38706,360.94796 45.344915,360.51809 45.26063,360.32421 C 45.142622,360.06293 44.952972,359.8817 44.691679,359.78054 C 44.539957,359.72999 44.02158,359.7047 43.136549,359.70468 L 43.136549,359.70468 z M 47.738724,374.90198 L 51.557013,374.90198 C 51.936303,374.90198 52.566362,374.83455 53.447192,374.69968 C 54.327999,374.56482 54.958058,374.40046 55.337371,374.20659 C 55.893664,373.92001 56.433112,373.45853 56.955718,372.82215 C 57.478294,372.18577 57.880773,371.42295 58.163157,370.5337 C 58.445509,369.64446 58.586692,368.62667 58.586709,367.48034 C 58.586692,366.34244 58.445509,365.35627 58.163157,364.52179 C 57.880773,363.68735 57.455115,362.93718 56.88618,362.27128 C 56.317215,361.60541 55.685049,361.09757 54.989679,360.74776 C 54.294283,360.39797 53.516719,360.17672 52.656983,360.08398 C 52.193384,360.04185 51.417926,360.02078 50.330609,360.02077 L 47.738724,360.02077 L 47.738724,374.90198 z"
               style="font-size:25.8935585px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8818737,1.1339493)" />
          </svg:g>
        </svg:g>
        <svg:g
           id="g8754">
          <svg:g
             id="g8608"
             transform="translate(149,-3.00002)">
            <svg:path
               id="text8610"
               d="M 55.06726,378.18807 L 55.994535,378.18807 L 57.630145,384.17672 L 56.432415,384.51157 C 55.788458,383.12926 55.299063,382.20198 54.964229,381.72975 C 54.414718,380.96562 53.729995,380.36031 52.910058,379.91383 C 52.090094,379.46738 51.186431,379.24415 50.199067,379.24413 C 49.039963,379.24415 47.973169,379.55539 46.99868,380.17785 C 46.024177,380.80034 45.234276,381.71259 44.628978,382.9146 C 44.023669,384.11663 43.721017,385.46891 43.721021,386.97142 C 43.721017,388.51689 44.027962,389.89492 44.641857,391.10553 C 45.255741,392.31614 46.086424,393.24771 47.133908,393.90023 C 48.181376,394.55276 49.310418,394.87902 50.521037,394.87902 C 51.662947,394.87902 52.830625,394.55276 54.024076,393.90023 C 55.217497,393.24771 56.252095,392.33331 57.127871,391.15704 L 58.158176,391.86538 C 57.625834,392.65528 57.050581,393.32498 56.432415,393.87447 C 55.453609,394.75882 54.436183,395.41134 53.380135,395.83205 C 52.324059,396.25276 51.164966,396.46311 49.902854,396.46311 C 47.112436,396.46311 44.845766,395.42852 43.102838,393.35932 C 41.660408,391.64215 40.939195,389.59871 40.939196,387.229 C 40.939195,385.48608 41.338438,383.90842 42.136926,382.49604 C 42.93541,381.08367 44.036548,379.96107 45.440343,379.12822 C 46.844127,378.29541 48.357387,377.879 49.980127,377.87898 C 50.847289,377.879 51.652215,377.99705 52.394905,378.23315 C 53.13757,378.46928 53.89098,378.8492 54.655138,379.37292 L 55.06726,378.18807 z"
               style="font-size:26.37582016px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9292071,1.0761864)" />
            <svg:path
               id="text8614"
               d="M 59.769406,380.35412 L 59.271865,380.27638 C 59.316781,379.90323 59.413525,379.59399 59.562097,379.34867 C 59.759039,379.01699 60.037177,378.75871 60.396514,378.57386 C 60.755846,378.38901 61.168735,378.29659 61.635182,378.29658 C 62.094712,378.29659 62.498963,378.37865 62.847936,378.54276 C 63.1969,378.70689 63.460355,378.92456 63.638299,379.19578 C 63.816234,379.46702 63.905204,379.75984 63.905209,380.07425 C 63.905204,380.35067 63.837828,380.59771 63.703083,380.81538 C 63.568328,381.03306 63.36793,381.22827 63.101889,381.40103 C 62.915307,381.52196 62.67172,381.62734 62.371127,381.71717 C 62.69245,381.76209 62.955041,381.83638 63.158899,381.94003 C 63.490587,382.10933 63.75145,382.34255 63.941488,382.63969 C 64.131515,382.93684 64.226531,383.26335 64.226537,383.61923 C 64.226531,384.18933 63.976034,384.69118 63.475044,385.1248 C 62.974044,385.55842 62.326206,385.77523 61.531527,385.77523 C 60.747208,385.77523 60.087277,385.5662 59.551732,385.14813 C 59.102563,384.79225 58.877979,384.43464 58.877979,384.0753 C 58.877979,383.93365 58.923759,383.81531 59.015321,383.72029 C 59.106882,383.62527 59.221765,383.57777 59.359972,383.57776 C 59.511997,383.57777 59.641564,383.6365 59.748675,383.75398 C 59.807411,383.81963 59.874786,383.97683 59.950801,384.2256 C 60.057908,384.57457 60.239303,384.84062 60.494985,385.02374 C 60.750663,385.20686 61.068536,385.29842 61.448604,385.29842 C 61.787204,385.29842 62.090393,385.22414 62.35817,385.07557 C 62.625939,384.927 62.837566,384.71623 62.993052,384.44328 C 63.148528,384.17032 63.226269,383.87145 63.226274,383.54667 C 63.226269,383.28408 63.170123,383.04827 63.057836,382.83923 C 62.945539,382.63019 62.785739,382.45312 62.578435,382.308 C 62.419495,382.19398 62.216505,382.11452 61.969467,382.0696 C 61.722421,382.02468 61.500428,382.00222 61.303488,382.00222 L 60.821496,382.00222 L 60.821496,381.4995 L 61.127276,381.4995 C 61.431325,381.4995 61.721557,381.46409 61.997971,381.39325 C 62.274379,381.32243 62.495508,381.17645 62.661358,380.95531 C 62.827201,380.73419 62.910124,380.48369 62.910129,380.20382 C 62.910124,379.96197 62.85225,379.74429 62.736508,379.5508 C 62.620756,379.35732 62.450591,379.20356 62.226011,379.08954 C 62.001423,378.97552 61.742288,378.91851 61.448604,378.91851 C 61.161825,378.91851 60.89837,378.97725 60.658241,379.09472 C 60.418106,379.2122 60.225482,379.36941 60.080368,379.56635 C 59.935251,379.76329 59.831597,380.02589 59.769406,380.35412 L 59.769406,380.35412 z"
               style="font-size:10.61419201px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9348315,1.0697115)" />
          </svg:g>
          <svg:g
             transform="translate(179,-3.0000449)"
             id="g8632">
            <svg:path
               id="text8656"
               d="M 59.769406,380.35412 L 59.271865,380.27638 C 59.316781,379.90323 59.413525,379.59399 59.562097,379.34867 C 59.759039,379.01699 60.037177,378.75871 60.396514,378.57386 C 60.755846,378.38901 61.168735,378.29659 61.635182,378.29658 C 62.094712,378.29659 62.498963,378.37865 62.847936,378.54276 C 63.1969,378.70689 63.460355,378.92456 63.638299,379.19578 C 63.816234,379.46702 63.905204,379.75984 63.905209,380.07425 C 63.905204,380.35067 63.837828,380.59771 63.703083,380.81538 C 63.568328,381.03306 63.36793,381.22827 63.101889,381.40103 C 62.915307,381.52196 62.67172,381.62734 62.371127,381.71717 C 62.69245,381.76209 62.955041,381.83638 63.158899,381.94003 C 63.490587,382.10933 63.75145,382.34255 63.941488,382.63969 C 64.131515,382.93684 64.226531,383.26335 64.226537,383.61923 C 64.226531,384.18933 63.976034,384.69118 63.475044,385.1248 C 62.974044,385.55842 62.326206,385.77523 61.531527,385.77523 C 60.747208,385.77523 60.087277,385.5662 59.551732,385.14813 C 59.102563,384.79225 58.877979,384.43464 58.877979,384.0753 C 58.877979,383.93365 58.923759,383.81531 59.015321,383.72029 C 59.106882,383.62527 59.221765,383.57777 59.359972,383.57776 C 59.511997,383.57777 59.641564,383.6365 59.748675,383.75398 C 59.807411,383.81963 59.874786,383.97683 59.950801,384.2256 C 60.057908,384.57457 60.239303,384.84062 60.494985,385.02374 C 60.750663,385.20686 61.068536,385.29842 61.448604,385.29842 C 61.787204,385.29842 62.090393,385.22414 62.35817,385.07557 C 62.625939,384.927 62.837566,384.71623 62.993052,384.44328 C 63.148528,384.17032 63.226269,383.87145 63.226274,383.54667 C 63.226269,383.28408 63.170123,383.04827 63.057836,382.83923 C 62.945539,382.63019 62.785739,382.45312 62.578435,382.308 C 62.419495,382.19398 62.216505,382.11452 61.969467,382.0696 C 61.722421,382.02468 61.500428,382.00222 61.303488,382.00222 L 60.821496,382.00222 L 60.821496,381.4995 L 61.127276,381.4995 C 61.431325,381.4995 61.721557,381.46409 61.997971,381.39325 C 62.274379,381.32243 62.495508,381.17645 62.661358,380.95531 C 62.827201,380.73419 62.910124,380.48369 62.910129,380.20382 C 62.910124,379.96197 62.85225,379.74429 62.736508,379.5508 C 62.620756,379.35732 62.450591,379.20356 62.226011,379.08954 C 62.001423,378.97552 61.742288,378.91851 61.448604,378.91851 C 61.161825,378.91851 60.89837,378.97725 60.658241,379.09472 C 60.418106,379.2122 60.225482,379.36941 60.080368,379.56635 C 59.935251,379.76329 59.831597,380.02589 59.769406,380.35412 L 59.769406,380.35412 z"
               style="font-size:10.61419201px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9348315,1.0697115)" />
            <svg:path
               id="text8634"
               d="M 43.136549,359.70468 L 43.136549,358.63 L 49.432932,358.63 C 52.315602,358.63002 54.325891,358.81124 55.463804,359.17366 C 57.141139,359.7047 58.502404,360.69509 59.547603,362.14485 C 60.702342,363.74635 61.279721,365.58806 61.27974,367.66999 C 61.279721,369.08605 60.995246,370.40306 60.426315,371.62103 C 59.857347,372.83901 59.109283,373.81887 58.182122,374.56061 C 57.415077,375.18434 56.492115,375.62475 55.413231,375.88183 C 54.33432,376.13892 53.246994,376.26746 52.151249,376.26746 L 43.136549,376.26746 L 43.136549,375.20542 L 43.869862,375.20542 L 44.110086,375.21806 C 44.421952,375.21806 44.676926,375.15484 44.875008,375.02841 C 45.073084,374.90198 45.214267,374.7334 45.29856,374.52268 C 45.357558,374.36253 45.38706,373.99166 45.387063,373.41006 L 45.387063,361.61383 C 45.38706,360.94796 45.344915,360.51809 45.26063,360.32421 C 45.142622,360.06293 44.952972,359.8817 44.691679,359.78054 C 44.539957,359.72999 44.02158,359.7047 43.136549,359.70468 L 43.136549,359.70468 z M 47.738724,374.90198 L 51.557013,374.90198 C 51.936303,374.90198 52.566362,374.83455 53.447192,374.69968 C 54.327999,374.56482 54.958058,374.40046 55.337371,374.20659 C 55.893664,373.92001 56.433112,373.45853 56.955718,372.82215 C 57.478294,372.18577 57.880773,371.42295 58.163157,370.5337 C 58.445509,369.64446 58.586692,368.62667 58.586709,367.48034 C 58.586692,366.34244 58.445509,365.35627 58.163157,364.52179 C 57.880773,363.68735 57.455115,362.93718 56.88618,362.27128 C 56.317215,361.60541 55.685049,361.09757 54.989679,360.74776 C 54.294283,360.39797 53.516719,360.17672 52.656983,360.08398 C 52.193384,360.04185 51.417926,360.02078 50.330609,360.02077 L 47.738724,360.02077 L 47.738724,374.90198 z"
               style="font-size:25.8935585px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8818737,1.1339493)" />
          </svg:g>
          <svg:g
             transform="translate(209,-3.0000041)"
             id="g8662">
            <svg:path
               id="text8724"
               d="M 59.769406,380.35409 L 59.271865,380.27635 C 59.316781,379.9032 59.413525,379.59396 59.562097,379.34864 C 59.759039,379.01695 60.037177,378.75868 60.396514,378.57383 C 60.755846,378.38898 61.168735,378.29656 61.635182,378.29655 C 62.094712,378.29656 62.498963,378.37862 62.847936,378.54273 C 63.1969,378.70686 63.460355,378.92453 63.638299,379.19575 C 63.816234,379.46699 63.905204,379.75981 63.905209,380.07422 C 63.905204,380.35064 63.837828,380.59768 63.703083,380.81535 C 63.568328,381.03303 63.36793,381.22824 63.101889,381.40099 C 62.915307,381.52193 62.67172,381.62731 62.371127,381.71714 C 62.69245,381.76206 62.955041,381.83635 63.158899,381.94 C 63.490587,382.1093 63.75145,382.34252 63.941488,382.63966 C 64.131515,382.93681 64.226531,383.26332 64.226537,383.6192 C 64.226531,384.18929 63.976034,384.69115 63.475044,385.12477 C 62.974044,385.55839 62.326206,385.7752 61.531527,385.7752 C 60.747208,385.7752 60.087277,385.56617 59.551732,385.14809 C 59.102563,384.79222 58.877979,384.43461 58.877979,384.07527 C 58.877979,383.93361 58.923759,383.81528 59.015321,383.72026 C 59.106882,383.62524 59.221765,383.57774 59.359972,383.57773 C 59.511997,383.57774 59.641564,383.63647 59.748675,383.75395 C 59.807411,383.8196 59.874786,383.9768 59.950801,384.22557 C 60.057908,384.57454 60.239303,384.84059 60.494985,385.02371 C 60.750663,385.20683 61.068536,385.29839 61.448604,385.29839 C 61.787204,385.29839 62.090393,385.22411 62.35817,385.07554 C 62.625939,384.92697 62.837566,384.7162 62.993052,384.44325 C 63.148528,384.17029 63.226269,383.87142 63.226274,383.54664 C 63.226269,383.28405 63.170123,383.04824 63.057836,382.8392 C 62.945539,382.63016 62.785739,382.45309 62.578435,382.30797 C 62.419495,382.19395 62.216505,382.11448 61.969467,382.06956 C 61.722421,382.02465 61.500428,382.00219 61.303488,382.00219 L 60.821496,382.00219 L 60.821496,381.49947 L 61.127276,381.49947 C 61.431325,381.49947 61.721557,381.46406 61.997971,381.39322 C 62.274379,381.32239 62.495508,381.17642 62.661358,380.95528 C 62.827201,380.73416 62.910124,380.48366 62.910129,380.20379 C 62.910124,379.96193 62.85225,379.74426 62.736508,379.55077 C 62.620756,379.35729 62.450591,379.20353 62.226011,379.08951 C 62.001423,378.97549 61.742288,378.91848 61.448604,378.91848 C 61.161825,378.91848 60.89837,378.97722 60.658241,379.09469 C 60.418106,379.21217 60.225482,379.36938 60.080368,379.56632 C 59.935251,379.76326 59.831597,380.02585 59.769406,380.35409 L 59.769406,380.35409 z"
               style="font-size:10.61419201px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9348315,1.0697115)" />
            <svg:path
               id="text8664"
               d="M 40.333911,383.54972 L 55.667727,383.54972 L 57.439094,388.51225 L 56.249168,388.95848 C 55.951669,388.14718 55.627145,387.5094 55.275592,387.04513 C 54.924007,386.58089 54.502576,386.18651 54.011296,385.86196 C 53.519986,385.53746 53.024185,385.32111 52.523889,385.21291 C 52.023566,385.10476 51.119855,385.05067 49.812751,385.05065 L 45.242355,385.05065 L 45.242355,391.96033 L 47.514031,391.96033 C 48.352378,391.96034 48.985653,391.81837 49.413856,391.53439 C 49.842038,391.25045 50.137265,390.89662 50.299539,390.47293 C 50.425732,390.15743 50.488834,389.60754 50.488845,388.82326 L 50.488845,388.51225 L 51.81399,388.51225 L 51.81399,396.78765 L 50.488845,396.78765 L 50.488845,396.07099 C 50.488834,395.43997 50.441507,394.98474 50.346866,394.70528 C 50.252201,394.42583 50.078671,394.16216 49.826273,393.91425 C 49.573854,393.66635 49.301163,393.49733 49.008199,393.40718 C 48.715215,393.31704 48.21716,393.27197 47.514031,393.27196 L 45.242355,393.27196 L 45.242355,399.41089 C 45.242348,400.05093 45.271646,400.44081 45.330247,400.58054 C 45.388835,400.72026 45.485742,400.82168 45.620967,400.88478 C 45.756179,400.94788 46.125777,400.97943 46.729762,400.97943 L 49.555835,400.97943 C 51.133376,400.97943 52.36612,400.79013 53.254071,400.41151 C 54.141992,400.0329 54.924007,399.45146 55.600117,398.66719 C 56.131961,398.0542 56.641285,397.1888 57.12809,396.07099 L 58.358582,396.50369 L 56.249168,402.41275 L 40.333911,402.41275 L 40.333911,401.27691 L 40.874786,401.27691 C 41.505805,401.27691 41.897939,401.23635 42.05119,401.15522 C 42.285566,401.03803 42.465858,400.84872 42.592065,400.5873 C 42.682208,400.40701 42.72728,400.01487 42.727284,399.41089 L 42.727284,386.42988 C 42.72728,385.84395 42.668686,385.45182 42.5515,385.25348 C 42.434307,385.05518 42.231479,384.90193 41.943015,384.79374 C 41.789764,384.73065 41.253397,384.6991 40.333911,384.69908 L 40.333911,383.54972 z"
               style="font-size:27.69281769px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9431515,1.060275)" />
          </svg:g>
          <svg:g
             id="g8676"
             transform="translate(239,-3.0000041)">
            <svg:path
               id="text8730"
               d="M 59.769406,380.35409 L 59.271865,380.27635 C 59.316781,379.9032 59.413525,379.59396 59.562097,379.34864 C 59.759039,379.01695 60.037177,378.75868 60.396514,378.57383 C 60.755846,378.38898 61.168735,378.29656 61.635182,378.29655 C 62.094712,378.29656 62.498963,378.37862 62.847936,378.54273 C 63.1969,378.70686 63.460355,378.92453 63.638299,379.19575 C 63.816234,379.46699 63.905204,379.75981 63.905209,380.07422 C 63.905204,380.35064 63.837828,380.59768 63.703083,380.81535 C 63.568328,381.03303 63.36793,381.22824 63.101889,381.40099 C 62.915307,381.52193 62.67172,381.62731 62.371127,381.71714 C 62.69245,381.76206 62.955041,381.83635 63.158899,381.94 C 63.490587,382.1093 63.75145,382.34252 63.941488,382.63966 C 64.131515,382.93681 64.226531,383.26332 64.226537,383.6192 C 64.226531,384.18929 63.976034,384.69115 63.475044,385.12477 C 62.974044,385.55839 62.326206,385.7752 61.531527,385.7752 C 60.747208,385.7752 60.087277,385.56617 59.551732,385.14809 C 59.102563,384.79222 58.877979,384.43461 58.877979,384.07527 C 58.877979,383.93361 58.923759,383.81528 59.015321,383.72026 C 59.106882,383.62524 59.221765,383.57774 59.359972,383.57773 C 59.511997,383.57774 59.641564,383.63647 59.748675,383.75395 C 59.807411,383.8196 59.874786,383.9768 59.950801,384.22557 C 60.057908,384.57454 60.239303,384.84059 60.494985,385.02371 C 60.750663,385.20683 61.068536,385.29839 61.448604,385.29839 C 61.787204,385.29839 62.090393,385.22411 62.35817,385.07554 C 62.625939,384.92697 62.837566,384.7162 62.993052,384.44325 C 63.148528,384.17029 63.226269,383.87142 63.226274,383.54664 C 63.226269,383.28405 63.170123,383.04824 63.057836,382.8392 C 62.945539,382.63016 62.785739,382.45309 62.578435,382.30797 C 62.419495,382.19395 62.216505,382.11448 61.969467,382.06956 C 61.722421,382.02465 61.500428,382.00219 61.303488,382.00219 L 60.821496,382.00219 L 60.821496,381.49947 L 61.127276,381.49947 C 61.431325,381.49947 61.721557,381.46406 61.997971,381.39322 C 62.274379,381.32239 62.495508,381.17642 62.661358,380.95528 C 62.827201,380.73416 62.910124,380.48366 62.910129,380.20379 C 62.910124,379.96193 62.85225,379.74426 62.736508,379.55077 C 62.620756,379.35729 62.450591,379.20353 62.226011,379.08951 C 62.001423,378.97549 61.742288,378.91848 61.448604,378.91848 C 61.161825,378.91848 60.89837,378.97722 60.658241,379.09469 C 60.418106,379.21217 60.225482,379.36938 60.080368,379.56632 C 59.935251,379.76326 59.831597,380.02585 59.769406,380.35409 L 59.769406,380.35409 z"
               style="font-size:10.61419201px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9348315,1.0697115)" />
            <svg:path
               id="text8678"
               d="M 39.380858,392.83193 L 54.157874,392.83193 L 55.944411,397.90071 L 54.739537,398.35773 C 54.231719,397.17595 53.733151,396.32654 53.243832,395.80949 C 52.754481,395.29247 52.186667,394.92317 51.540389,394.70156 C 50.894084,394.47999 49.878482,394.3692 48.493582,394.36918 L 44.324995,394.36918 L 44.324995,401.79231 L 46.222326,401.79231 C 47.219453,401.79232 47.93499,401.69538 48.36894,401.50148 C 48.802868,401.30761 49.135247,401.00062 49.366077,400.58052 C 49.596884,400.16044 49.712293,399.56262 49.712305,398.78705 L 49.712305,398.27464 L 51.069519,398.27464 L 51.069519,406.7503 L 49.712305,406.7503 L 49.712305,406.00245 C 49.712293,405.27307 49.6015,404.70525 49.379926,404.29901 C 49.158329,403.89277 48.855956,403.59963 48.472808,403.41959 C 48.089639,403.23956 47.408724,403.14954 46.430062,403.14953 L 44.324995,403.14953 L 44.324995,409.16005 C 44.324989,409.71402 44.382694,410.09948 44.49811,410.31645 C 44.613512,410.53342 44.784318,410.69961 45.010527,410.81502 C 45.236722,410.93043 45.640655,410.98813 46.222326,410.98813 L 46.734743,410.98813 L 46.734743,412.15146 L 39.380858,412.15146 L 39.380858,410.98813 L 39.851728,410.98813 C 40.442622,410.98813 40.85117,410.93274 41.077376,410.82194 C 41.303575,410.71115 41.469764,410.53804 41.575944,410.3026 C 41.682117,410.06717 41.735205,409.64015 41.735209,409.02156 L 41.735209,395.87874 C 41.735205,395.30632 41.684425,394.9047 41.582869,394.67386 C 41.481305,394.44306 41.326657,394.27457 41.118923,394.16837 C 40.911183,394.06221 40.548798,394.00912 40.031767,394.00911 L 39.380858,394.00911 L 39.380858,392.83193 z"
               style="font-size:28.36300659px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9659766,1.0352218)" />
          </svg:g>
          <svg:g
             transform="translate(269,-3.0000041)"
             id="g8688">
            <svg:path
               id="text8736"
               d="M 59.769406,380.35409 L 59.271865,380.27635 C 59.316781,379.9032 59.413525,379.59396 59.562097,379.34864 C 59.759039,379.01695 60.037177,378.75868 60.396514,378.57383 C 60.755846,378.38898 61.168735,378.29656 61.635182,378.29655 C 62.094712,378.29656 62.498963,378.37862 62.847936,378.54273 C 63.1969,378.70686 63.460355,378.92453 63.638299,379.19575 C 63.816234,379.46699 63.905204,379.75981 63.905209,380.07422 C 63.905204,380.35064 63.837828,380.59768 63.703083,380.81535 C 63.568328,381.03303 63.36793,381.22824 63.101889,381.40099 C 62.915307,381.52193 62.67172,381.62731 62.371127,381.71714 C 62.69245,381.76206 62.955041,381.83635 63.158899,381.94 C 63.490587,382.1093 63.75145,382.34252 63.941488,382.63966 C 64.131515,382.93681 64.226531,383.26332 64.226537,383.6192 C 64.226531,384.18929 63.976034,384.69115 63.475044,385.12477 C 62.974044,385.55839 62.326206,385.7752 61.531527,385.7752 C 60.747208,385.7752 60.087277,385.56617 59.551732,385.14809 C 59.102563,384.79222 58.877979,384.43461 58.877979,384.07527 C 58.877979,383.93361 58.923759,383.81528 59.015321,383.72026 C 59.106882,383.62524 59.221765,383.57774 59.359972,383.57773 C 59.511997,383.57774 59.641564,383.63647 59.748675,383.75395 C 59.807411,383.8196 59.874786,383.9768 59.950801,384.22557 C 60.057908,384.57454 60.239303,384.84059 60.494985,385.02371 C 60.750663,385.20683 61.068536,385.29839 61.448604,385.29839 C 61.787204,385.29839 62.090393,385.22411 62.35817,385.07554 C 62.625939,384.92697 62.837566,384.7162 62.993052,384.44325 C 63.148528,384.17029 63.226269,383.87142 63.226274,383.54664 C 63.226269,383.28405 63.170123,383.04824 63.057836,382.8392 C 62.945539,382.63016 62.785739,382.45309 62.578435,382.30797 C 62.419495,382.19395 62.216505,382.11448 61.969467,382.06956 C 61.722421,382.02465 61.500428,382.00219 61.303488,382.00219 L 60.821496,382.00219 L 60.821496,381.49947 L 61.127276,381.49947 C 61.431325,381.49947 61.721557,381.46406 61.997971,381.39322 C 62.274379,381.32239 62.495508,381.17642 62.661358,380.95528 C 62.827201,380.73416 62.910124,380.48366 62.910129,380.20379 C 62.910124,379.96193 62.85225,379.74426 62.736508,379.55077 C 62.620756,379.35729 62.450591,379.20353 62.226011,379.08951 C 62.001423,378.97549 61.742288,378.91848 61.448604,378.91848 C 61.161825,378.91848 60.89837,378.97722 60.658241,379.09469 C 60.418106,379.21217 60.225482,379.36938 60.080368,379.56632 C 59.935251,379.76326 59.831597,380.02585 59.769406,380.35409 L 59.769406,380.35409 z"
               style="font-size:10.61419201px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9348315,1.0697115)" />
            <svg:path
               id="text8690"
               d="M 54.600565,383.5977 L 55.554167,383.5977 L 57.043354,389.21481 L 55.86768,389.61976 C 55.083882,387.8432 54.182532,386.57609 53.16363,385.81842 C 52.144699,385.06078 50.990798,384.68195 49.701923,384.68193 C 47.846962,384.68195 46.292461,385.45267 45.038416,386.99409 C 43.784359,388.53554 43.157333,390.37308 43.157337,392.5067 C 43.157333,394.03944 43.477377,395.44589 44.117471,396.72606 C 44.757555,398.00624 45.613183,398.98162 46.68436,399.65218 C 47.755521,400.32275 48.892004,400.65804 50.093814,400.65804 C 50.868877,400.65804 51.606938,400.51652 52.308001,400.23349 C 53.009037,399.95046 53.577279,399.613 54.012728,399.2211 C 54.334934,398.92501 54.552652,398.59843 54.66588,398.24137 C 54.779077,397.88432 54.83133,397.38793 54.822637,396.75219 L 54.822637,395.89003 C 54.822621,395.20205 54.742066,394.71436 54.58097,394.42696 C 54.419844,394.13959 54.191241,393.93493 53.895161,393.813 C 53.66872,393.71721 53.059112,393.66932 52.066334,393.66931 L 52.066334,392.57201 L 59.460018,392.57201 L 59.460018,393.66931 L 58.859118,393.66931 C 58.345285,393.66932 57.994761,393.71939 57.807543,393.81953 C 57.620287,393.91969 57.483125,394.07427 57.396057,394.28327 C 57.308951,394.49229 57.265408,394.9016 57.265426,395.5112 L 57.265426,401.82065 L 55.972185,401.82065 L 55.279843,400.22696 C 54.548297,400.75819 53.76887,401.17185 52.941558,401.46795 C 51.704911,401.91209 50.48134,402.13416 49.270843,402.13416 C 46.823691,402.13416 44.727074,401.23499 42.980986,399.43664 C 41.23489,397.6383 40.361844,395.39799 40.361846,392.71571 C 40.361844,390.05086 41.245776,387.81272 43.013644,386.0013 C 44.781504,384.18991 46.897715,383.28421 49.362284,383.28419 C 49.98059,383.28421 50.590199,383.34517 51.19111,383.46707 C 51.791997,383.58901 52.340645,383.76318 52.837054,383.98959 C 53.211514,384.15507 53.646948,384.42504 54.143358,384.7995 L 54.600565,383.5977 z"
               style="font-size:26.75311661px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9424986,1.0610095)" />
          </svg:g>
          <svg:g
             id="g8700"
             transform="translate(299,-3.0000041)">
            <svg:path
               id="text8742"
               d="M 59.769406,380.35409 L 59.271865,380.27635 C 59.316781,379.9032 59.413525,379.59396 59.562097,379.34864 C 59.759039,379.01695 60.037177,378.75868 60.396514,378.57383 C 60.755846,378.38898 61.168735,378.29656 61.635182,378.29655 C 62.094712,378.29656 62.498963,378.37862 62.847936,378.54273 C 63.1969,378.70686 63.460355,378.92453 63.638299,379.19575 C 63.816234,379.46699 63.905204,379.75981 63.905209,380.07422 C 63.905204,380.35064 63.837828,380.59768 63.703083,380.81535 C 63.568328,381.03303 63.36793,381.22824 63.101889,381.40099 C 62.915307,381.52193 62.67172,381.62731 62.371127,381.71714 C 62.69245,381.76206 62.955041,381.83635 63.158899,381.94 C 63.490587,382.1093 63.75145,382.34252 63.941488,382.63966 C 64.131515,382.93681 64.226531,383.26332 64.226537,383.6192 C 64.226531,384.18929 63.976034,384.69115 63.475044,385.12477 C 62.974044,385.55839 62.326206,385.7752 61.531527,385.7752 C 60.747208,385.7752 60.087277,385.56617 59.551732,385.14809 C 59.102563,384.79222 58.877979,384.43461 58.877979,384.07527 C 58.877979,383.93361 58.923759,383.81528 59.015321,383.72026 C 59.106882,383.62524 59.221765,383.57774 59.359972,383.57773 C 59.511997,383.57774 59.641564,383.63647 59.748675,383.75395 C 59.807411,383.8196 59.874786,383.9768 59.950801,384.22557 C 60.057908,384.57454 60.239303,384.84059 60.494985,385.02371 C 60.750663,385.20683 61.068536,385.29839 61.448604,385.29839 C 61.787204,385.29839 62.090393,385.22411 62.35817,385.07554 C 62.625939,384.92697 62.837566,384.7162 62.993052,384.44325 C 63.148528,384.17029 63.226269,383.87142 63.226274,383.54664 C 63.226269,383.28405 63.170123,383.04824 63.057836,382.8392 C 62.945539,382.63016 62.785739,382.45309 62.578435,382.30797 C 62.419495,382.19395 62.216505,382.11448 61.969467,382.06956 C 61.722421,382.02465 61.500428,382.00219 61.303488,382.00219 L 60.821496,382.00219 L 60.821496,381.49947 L 61.127276,381.49947 C 61.431325,381.49947 61.721557,381.46406 61.997971,381.39322 C 62.274379,381.32239 62.495508,381.17642 62.661358,380.95528 C 62.827201,380.73416 62.910124,380.48366 62.910129,380.20379 C 62.910124,379.96193 62.85225,379.74426 62.736508,379.55077 C 62.620756,379.35729 62.450591,379.20353 62.226011,379.08951 C 62.001423,378.97549 61.742288,378.91848 61.448604,378.91848 C 61.161825,378.91848 60.89837,378.97722 60.658241,379.09469 C 60.418106,379.21217 60.225482,379.36938 60.080368,379.56632 C 59.935251,379.76326 59.831597,380.02585 59.769406,380.35409 L 59.769406,380.35409 z"
               style="font-size:10.61419201px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9348315,1.0697115)" />
            <svg:path
               id="text8702"
               d="M 48.322339,387.18937 L 50.574624,387.18937 L 56.485167,403.10552 C 56.776355,403.90633 57.069834,404.43641 57.365606,404.69577 C 57.661343,404.95512 58.059474,405.0848 58.56,405.0848 L 58.860304,405.0848 L 58.860304,406.23141 L 52.076148,406.23141 L 52.076148,405.0848 C 52.294539,405.0666 52.508392,405.0575 52.717708,405.0575 C 53.154502,405.0575 53.441156,405.012 53.577671,404.92099 C 53.841562,404.74809 53.973514,404.52969 53.973528,404.26578 C 53.973514,404.07468 53.846112,403.60603 53.591322,402.85981 L 52.335502,399.17425 L 45.428493,399.17425 L 43.95427,403.29662 C 43.799564,403.72433 43.722213,404.02008 43.722217,404.18388 C 43.722213,404.36589 43.769989,404.52514 43.865544,404.66164 C 43.961091,404.79814 44.090768,404.90052 44.254575,404.96877 C 44.418373,405.03702 44.682277,405.07115 45.046287,405.07115 C 45.291986,405.07115 45.492189,405.0757 45.646897,405.0848 L 45.646897,406.23141 L 39.954758,406.23141 L 39.954758,405.0848 C 40.482565,405.0848 40.862496,405.03247 41.09455,404.92782 C 41.326603,404.82317 41.529081,404.65482 41.701985,404.42276 C 41.874886,404.19071 42.102389,403.70158 42.384496,402.95536 L 48.322339,387.18937 z M 45.933551,397.83653 L 51.816794,397.83653 L 48.868347,389.6464 L 45.933551,397.83653 z"
               style="font-size:27.95563889px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9521015,1.0503082)" />
          </svg:g>
          <svg:g
             transform="translate(329.00001,-3.0000041)"
             id="g8712">
            <svg:path
               id="text8748"
               d="M 59.769394,380.35409 L 59.271854,380.27635 C 59.31677,379.9032 59.413514,379.59396 59.562086,379.34864 C 59.759027,379.01695 60.037166,378.75868 60.396502,378.57383 C 60.755835,378.38898 61.168723,378.29656 61.63517,378.29655 C 62.0947,378.29656 62.498951,378.37862 62.847925,378.54273 C 63.196889,378.70686 63.460343,378.92453 63.638288,379.19575 C 63.816222,379.46699 63.905192,379.75981 63.905198,380.07422 C 63.905192,380.35064 63.837817,380.59768 63.703072,380.81535 C 63.568316,381.03303 63.367918,381.22824 63.101877,381.40099 C 62.915295,381.52193 62.671708,381.62731 62.371115,381.71714 C 62.692439,381.76206 62.955029,381.83635 63.158887,381.94 C 63.490576,382.1093 63.751438,382.34252 63.941477,382.63966 C 64.131504,382.93681 64.22652,383.26332 64.226526,383.6192 C 64.22652,384.18929 63.976022,384.69115 63.475033,385.12477 C 62.974033,385.55839 62.326194,385.7752 61.531516,385.7752 C 60.747197,385.7752 60.087265,385.56617 59.55172,385.14809 C 59.102551,384.79222 58.877967,384.43461 58.877968,384.07527 C 58.877967,383.93361 58.923748,383.81528 59.01531,383.72026 C 59.10687,383.62524 59.221754,383.57774 59.35996,383.57773 C 59.511985,383.57774 59.641553,383.63647 59.748663,383.75395 C 59.807399,383.8196 59.874775,383.9768 59.950789,384.22557 C 60.057897,384.57454 60.239292,384.84059 60.494974,385.02371 C 60.750652,385.20683 61.068524,385.29839 61.448593,385.29839 C 61.787193,385.29839 62.090381,385.22411 62.358158,385.07554 C 62.625928,384.92697 62.837555,384.7162 62.99304,384.44325 C 63.148517,384.17029 63.226258,383.87142 63.226262,383.54664 C 63.226258,383.28405 63.170112,383.04824 63.057824,382.8392 C 62.945528,382.63016 62.785728,382.45309 62.578424,382.30797 C 62.419483,382.19395 62.216494,382.11448 61.969455,382.06956 C 61.722409,382.02465 61.500417,382.00219 61.303477,382.00219 L 60.821485,382.00219 L 60.821485,381.49947 L 61.127265,381.49947 C 61.431314,381.49947 61.721545,381.46406 61.99796,381.39322 C 62.274367,381.32239 62.495496,381.17642 62.661347,380.95528 C 62.827189,380.73416 62.910113,380.48366 62.910117,380.20379 C 62.910113,379.96193 62.852239,379.74426 62.736496,379.55077 C 62.620745,379.35729 62.450579,379.20353 62.225999,379.08951 C 62.001411,378.97549 61.742276,378.91848 61.448593,378.91848 C 61.161813,378.91848 60.898359,378.97722 60.658229,379.09469 C 60.418095,379.21217 60.225471,379.36938 60.080357,379.56632 C 59.935239,379.76326 59.831585,380.02585 59.769394,380.35409 L 59.769394,380.35409 z"
               style="font-size:10.61419201px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9348315,1.0697115)" />
            <svg:path
               id="text8714"
               d="M 41.684272,371.12452 L 50.358867,371.12452 C 52.47844,371.12454 53.930746,371.22049 54.715791,371.41236 C 55.875877,371.6915 56.793926,372.19741 57.469942,372.93009 C 58.145923,373.6628 58.483923,374.54378 58.48394,375.57303 C 58.483923,376.31446 58.30511,376.97956 57.947502,377.56832 C 57.58986,378.1571 57.070868,378.66083 56.390524,379.0795 C 55.95438,379.34991 55.361246,379.58106 54.61112,379.77295 C 55.561865,379.94741 56.285837,380.15675 56.783039,380.40097 C 57.533163,380.76733 58.126298,381.29068 58.562444,381.97103 C 58.998554,382.6514 59.216618,383.41899 59.216636,384.27379 C 59.216618,385.10244 59.002915,385.87003 58.575527,386.57655 C 58.148104,387.28308 57.544067,387.86313 56.763414,388.3167 C 55.982728,388.77028 55.073401,389.0603 54.03543,389.18678 C 52.997432,389.31326 51.928918,389.3765 50.829886,389.3765 L 41.684272,389.3765 L 41.684272,388.27745 L 42.299213,388.27745 C 42.892346,388.27745 43.297945,388.18587 43.516012,388.00269 C 43.830021,387.74974 43.987027,387.33105 43.987031,386.74664 L 43.987031,374.06839 C 43.987027,373.47527 43.939053,373.06967 43.843108,372.85159 C 43.747157,372.63354 43.603234,372.47653 43.411341,372.38057 C 43.219442,372.28464 42.848733,372.23666 42.299213,372.23665 L 41.684272,372.23665 L 41.684272,371.12452 z M 46.433711,372.57683 L 46.433711,379.26268 L 49.770094,379.26268 C 51.331423,379.26269 52.399937,379.18854 52.975638,379.04025 C 53.874049,378.80475 54.560951,378.40351 55.036345,377.83654 C 55.51171,377.26958 55.7494,376.54125 55.749415,375.65153 C 55.7494,374.90141 55.533517,374.27556 55.101764,373.774 C 54.669983,373.27247 54.092113,372.93011 53.368154,372.74692 C 52.897123,372.63354 51.920196,372.57684 50.43737,372.57683 L 46.433711,372.57683 z M 46.433711,380.51873 L 46.433711,386.51113 C 46.433705,387.11299 46.459873,387.47062 46.512214,387.58401 C 46.581989,387.74102 46.72155,387.85005 46.930898,387.9111 C 47.079175,387.95472 47.497858,387.97652 48.186948,387.97652 L 49.966352,387.97652 C 51.7283,387.97652 52.986529,387.8784 53.741043,387.68214 C 54.495532,387.48588 55.138821,387.07156 55.670912,386.43917 C 56.202973,385.80679 56.469011,385.04574 56.469027,384.15604 C 56.469011,383.4059 56.253128,382.74081 55.821376,382.16075 C 55.389594,381.58071 54.792099,381.16202 54.028888,380.9047 C 53.265651,380.64739 51.911473,380.51874 49.966352,380.51873 L 46.433711,380.51873 z"
               style="font-size:26.7957325px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9125979,1.0957729)" />
          </svg:g>
        </svg:g>
        <svg:g
           id="g9094"
           transform="translate(-210,8.6590926e-6)">
          <svg:g
             id="g9096"
             transform="translate(-0.9999955,-3.0000045)">
            <svg:path
               id="text9098"
               d="M 40.333911,383.54972 L 55.667727,383.54972 L 57.439094,388.51225 L 56.249168,388.95848 C 55.951669,388.14718 55.627145,387.5094 55.275592,387.04513 C 54.924007,386.58089 54.502576,386.18651 54.011296,385.86196 C 53.519986,385.53746 53.024185,385.32111 52.523889,385.21291 C 52.023566,385.10476 51.119855,385.05067 49.812751,385.05065 L 45.242355,385.05065 L 45.242355,391.96033 L 47.514031,391.96033 C 48.352378,391.96034 48.985653,391.81837 49.413856,391.53439 C 49.842038,391.25045 50.137265,390.89662 50.299539,390.47293 C 50.425732,390.15743 50.488834,389.60754 50.488845,388.82326 L 50.488845,388.51225 L 51.81399,388.51225 L 51.81399,396.78765 L 50.488845,396.78765 L 50.488845,396.07099 C 50.488834,395.43997 50.441507,394.98474 50.346866,394.70528 C 50.252201,394.42583 50.078671,394.16216 49.826273,393.91425 C 49.573854,393.66635 49.301163,393.49733 49.008199,393.40718 C 48.715215,393.31704 48.21716,393.27197 47.514031,393.27196 L 45.242355,393.27196 L 45.242355,399.41089 C 45.242348,400.05093 45.271646,400.44081 45.330247,400.58054 C 45.388835,400.72026 45.485742,400.82168 45.620967,400.88478 C 45.756179,400.94788 46.125777,400.97943 46.729762,400.97943 L 49.555835,400.97943 C 51.133376,400.97943 52.36612,400.79013 53.254071,400.41151 C 54.141992,400.0329 54.924007,399.45146 55.600117,398.66719 C 56.131961,398.0542 56.641285,397.1888 57.12809,396.07099 L 58.358582,396.50369 L 56.249168,402.41275 L 40.333911,402.41275 L 40.333911,401.27691 L 40.874786,401.27691 C 41.505805,401.27691 41.897939,401.23635 42.05119,401.15522 C 42.285566,401.03803 42.465858,400.84872 42.592065,400.5873 C 42.682208,400.40701 42.72728,400.01487 42.727284,399.41089 L 42.727284,386.42988 C 42.72728,385.84395 42.668686,385.45182 42.5515,385.25348 C 42.434307,385.05518 42.231479,384.90193 41.943015,384.79374 C 41.789764,384.73065 41.253397,384.6991 40.333911,384.69908 L 40.333911,383.54972 z"
               style="font-size:27.69281769px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9431515,1.060275)" />
            <svg:path
               id="text9216"
               d="M 56.51377,416.32093 L 56.956263,416.32093 L 56.956263,423.15302 C 56.956258,423.47555 56.976908,423.68303 57.018212,423.77546 C 57.059507,423.8679 57.127356,423.93771 57.221759,423.98491 C 57.316152,424.03211 57.502982,424.05571 57.78225,424.05571 L 58.36044,424.05571 L 58.36044,424.5513 L 54.472403,424.5513 L 54.472403,424.05571 L 55.115493,424.05571 C 55.394752,424.05571 55.580599,424.03113 55.673034,423.98196 C 55.765462,423.93279 55.837244,423.85413 55.88838,423.74596 C 55.939509,423.6378 55.965075,423.44015 55.965079,423.15302 L 55.965079,417.53631 L 54.472403,417.53631 L 54.472403,417.02892 C 55.286587,416.91486 55.967042,416.67887 56.51377,416.32093 L 56.51377,416.32093 z"
               style="font-size:12.08300495px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(1.0287959,0.9720101)" />
          </svg:g>
          <svg:g
             transform="translate(29.000001,-3.0000045)"
             id="g9110">
            <svg:path
               id="text9112"
               d="M 39.380858,392.83193 L 54.157874,392.83193 L 55.944411,397.90071 L 54.739537,398.35773 C 54.231719,397.17595 53.733151,396.32654 53.243832,395.80949 C 52.754481,395.29247 52.186667,394.92317 51.540389,394.70156 C 50.894084,394.47999 49.878482,394.3692 48.493582,394.36918 L 44.324995,394.36918 L 44.324995,401.79231 L 46.222326,401.79231 C 47.219453,401.79232 47.93499,401.69538 48.36894,401.50148 C 48.802868,401.30761 49.135247,401.00062 49.366077,400.58052 C 49.596884,400.16044 49.712293,399.56262 49.712305,398.78705 L 49.712305,398.27464 L 51.069519,398.27464 L 51.069519,406.7503 L 49.712305,406.7503 L 49.712305,406.00245 C 49.712293,405.27307 49.6015,404.70525 49.379926,404.29901 C 49.158329,403.89277 48.855956,403.59963 48.472808,403.41959 C 48.089639,403.23956 47.408724,403.14954 46.430062,403.14953 L 44.324995,403.14953 L 44.324995,409.16005 C 44.324989,409.71402 44.382694,410.09948 44.49811,410.31645 C 44.613512,410.53342 44.784318,410.69961 45.010527,410.81502 C 45.236722,410.93043 45.640655,410.98813 46.222326,410.98813 L 46.734743,410.98813 L 46.734743,412.15146 L 39.380858,412.15146 L 39.380858,410.98813 L 39.851728,410.98813 C 40.442622,410.98813 40.85117,410.93274 41.077376,410.82194 C 41.303575,410.71115 41.469764,410.53804 41.575944,410.3026 C 41.682117,410.06717 41.735205,409.64015 41.735209,409.02156 L 41.735209,395.87874 C 41.735205,395.30632 41.684425,394.9047 41.582869,394.67386 C 41.481305,394.44306 41.326657,394.27457 41.118923,394.16837 C 40.911183,394.06221 40.548798,394.00912 40.031767,394.00911 L 39.380858,394.00911 L 39.380858,392.83193 z"
               style="font-size:28.36300659px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9659766,1.0352218)" />
            <svg:path
               id="text9210"
               d="M 56.513774,416.32093 L 56.956267,416.32093 L 56.956267,423.15302 C 56.956262,423.47555 56.976912,423.68303 57.018216,423.77546 C 57.05951,423.8679 57.127359,423.93771 57.221762,423.98491 C 57.316156,424.03211 57.502986,424.05571 57.782253,424.05571 L 58.360444,424.05571 L 58.360444,424.5513 L 54.472407,424.5513 L 54.472407,424.05571 L 55.115496,424.05571 C 55.394756,424.05571 55.580603,424.03113 55.673037,423.98196 C 55.765466,423.93279 55.837248,423.85413 55.888384,423.74596 C 55.939513,423.6378 55.965079,423.44015 55.965083,423.15302 L 55.965083,417.53631 L 54.472407,417.53631 L 54.472407,417.02892 C 55.286591,416.91486 55.967046,416.67887 56.513774,416.32093 L 56.513774,416.32093 z"
               style="font-size:12.08300495px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(1.0287959,0.9720101)" />
          </svg:g>
          <svg:g
             id="g9122"
             transform="translate(59.000005,-3.0000041)">
            <svg:path
               id="text9124"
               d="M 54.600565,383.5977 L 55.554167,383.5977 L 57.043354,389.21481 L 55.86768,389.61976 C 55.083882,387.8432 54.182532,386.57609 53.16363,385.81842 C 52.144699,385.06078 50.990798,384.68195 49.701923,384.68193 C 47.846962,384.68195 46.292461,385.45267 45.038416,386.99409 C 43.784359,388.53554 43.157333,390.37308 43.157337,392.5067 C 43.157333,394.03944 43.477377,395.44589 44.117471,396.72606 C 44.757555,398.00624 45.613183,398.98162 46.68436,399.65218 C 47.755521,400.32275 48.892004,400.65804 50.093814,400.65804 C 50.868877,400.65804 51.606938,400.51652 52.308001,400.23349 C 53.009037,399.95046 53.577279,399.613 54.012728,399.2211 C 54.334934,398.92501 54.552652,398.59843 54.66588,398.24137 C 54.779077,397.88432 54.83133,397.38793 54.822637,396.75219 L 54.822637,395.89003 C 54.822621,395.20205 54.742066,394.71436 54.58097,394.42696 C 54.419844,394.13959 54.191241,393.93493 53.895161,393.813 C 53.66872,393.71721 53.059112,393.66932 52.066334,393.66931 L 52.066334,392.57201 L 59.460018,392.57201 L 59.460018,393.66931 L 58.859118,393.66931 C 58.345285,393.66932 57.994761,393.71939 57.807543,393.81953 C 57.620287,393.91969 57.483125,394.07427 57.396057,394.28327 C 57.308951,394.49229 57.265408,394.9016 57.265426,395.5112 L 57.265426,401.82065 L 55.972185,401.82065 L 55.279843,400.22696 C 54.548297,400.75819 53.76887,401.17185 52.941558,401.46795 C 51.704911,401.91209 50.48134,402.13416 49.270843,402.13416 C 46.823691,402.13416 44.727074,401.23499 42.980986,399.43664 C 41.23489,397.6383 40.361844,395.39799 40.361846,392.71571 C 40.361844,390.05086 41.245776,387.81272 43.013644,386.0013 C 44.781504,384.18991 46.897715,383.28421 49.362284,383.28419 C 49.98059,383.28421 50.590199,383.34517 51.19111,383.46707 C 51.791997,383.58901 52.340645,383.76318 52.837054,383.98959 C 53.211514,384.15507 53.646948,384.42504 54.143358,384.7995 L 54.600565,383.5977 z"
               style="font-size:26.75311661px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9424986,1.0610095)" />
            <svg:path
               id="text9204"
               d="M 56.587516,416.32093 L 57.030009,416.32093 L 57.030009,423.15302 C 57.030004,423.47555 57.050654,423.68303 57.091958,423.77546 C 57.133252,423.8679 57.201101,423.93771 57.295504,423.98491 C 57.389898,424.03211 57.576728,424.05571 57.855995,424.05571 L 58.434186,424.05571 L 58.434186,424.5513 L 54.546149,424.5513 L 54.546149,424.05571 L 55.189238,424.05571 C 55.468498,424.05571 55.654344,424.03113 55.746779,423.98196 C 55.839208,423.93279 55.91099,423.85413 55.962126,423.74596 C 56.013255,423.6378 56.038821,423.44015 56.038825,423.15302 L 56.038825,417.53631 L 54.546149,417.53631 L 54.546149,417.02892 C 55.360333,416.91486 56.040788,416.67887 56.587516,416.32093 L 56.587516,416.32093 z"
               style="font-size:12.08300495px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(1.0287959,0.9720101)" />
          </svg:g>
          <svg:g
             transform="translate(89.000005,-3.0000041)"
             id="g9134">
            <svg:path
               id="text9136"
               d="M 48.322339,387.18937 L 50.574624,387.18937 L 56.485167,403.10552 C 56.776355,403.90633 57.069834,404.43641 57.365606,404.69577 C 57.661343,404.95512 58.059474,405.0848 58.56,405.0848 L 58.860304,405.0848 L 58.860304,406.23141 L 52.076148,406.23141 L 52.076148,405.0848 C 52.294539,405.0666 52.508392,405.0575 52.717708,405.0575 C 53.154502,405.0575 53.441156,405.012 53.577671,404.92099 C 53.841562,404.74809 53.973514,404.52969 53.973528,404.26578 C 53.973514,404.07468 53.846112,403.60603 53.591322,402.85981 L 52.335502,399.17425 L 45.428493,399.17425 L 43.95427,403.29662 C 43.799564,403.72433 43.722213,404.02008 43.722217,404.18388 C 43.722213,404.36589 43.769989,404.52514 43.865544,404.66164 C 43.961091,404.79814 44.090768,404.90052 44.254575,404.96877 C 44.418373,405.03702 44.682277,405.07115 45.046287,405.07115 C 45.291986,405.07115 45.492189,405.0757 45.646897,405.0848 L 45.646897,406.23141 L 39.954758,406.23141 L 39.954758,405.0848 C 40.482565,405.0848 40.862496,405.03247 41.09455,404.92782 C 41.326603,404.82317 41.529081,404.65482 41.701985,404.42276 C 41.874886,404.19071 42.102389,403.70158 42.384496,402.95536 L 48.322339,387.18937 z M 45.933551,397.83653 L 51.816794,397.83653 L 48.868347,389.6464 L 45.933551,397.83653 z"
               style="font-size:27.95563889px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9521015,1.0503082)" />
            <svg:text
               xml:space="preserve"
               style="font-size:20px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               x="229.61066"
               y="255.06474"
               id="text9188"
               transform="translate(-135.95901,159.10765)"><svg:tspan
                 y="255.06474"
                 x="229.61066"
                 id="tspan9190" /></svg:text>
            <svg:path
               id="text9198"
               d="M 56.51377,416.32093 L 56.956263,416.32093 L 56.956263,423.15302 C 56.956258,423.47555 56.976908,423.68303 57.018212,423.77546 C 57.059507,423.8679 57.127356,423.93771 57.221759,423.98491 C 57.316152,424.03211 57.502982,424.05571 57.78225,424.05571 L 58.36044,424.05571 L 58.36044,424.5513 L 54.472403,424.5513 L 54.472403,424.05571 L 55.115493,424.05571 C 55.394752,424.05571 55.580599,424.03113 55.673034,423.98196 C 55.765462,423.93279 55.837244,423.85413 55.88838,423.74596 C 55.939509,423.6378 55.965075,423.44015 55.965079,423.15302 L 55.965079,417.53631 L 54.472403,417.53631 L 54.472403,417.02892 C 55.286587,416.91486 55.967042,416.67887 56.51377,416.32093 L 56.51377,416.32093 z"
               style="font-size:12.08300495px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(1.0287959,0.9720101)" />
          </svg:g>
          <svg:g
             id="g9146"
             transform="translate(119.00001,-3.0000041)">
            <svg:path
               id="text9182"
               d="M 56.513766,416.32096 L 56.956259,416.32096 L 56.956259,423.15305 C 56.956255,423.47558 56.976904,423.68306 57.018208,423.77549 C 57.059503,423.86793 57.127352,423.93774 57.221755,423.98494 C 57.316148,424.03214 57.502979,424.05574 57.782246,424.05574 L 58.360436,424.05574 L 58.360436,424.55133 L 54.472399,424.55133 L 54.472399,424.05574 L 55.115489,424.05574 C 55.394748,424.05574 55.580595,424.03116 55.67303,423.98199 C 55.765458,423.93282 55.83724,423.85416 55.888376,423.74599 C 55.939505,423.63783 55.965072,423.44018 55.965075,423.15305 L 55.965075,417.53634 L 54.472399,417.53634 L 54.472399,417.02895 C 55.286583,416.91489 55.967038,416.6789 56.513766,416.32096 L 56.513766,416.32096 z"
               style="font-size:12.08300495px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(1.0287959,0.9720101)" />
            <svg:path
               id="text9148"
               d="M 41.684272,371.12452 L 50.358867,371.12452 C 52.47844,371.12454 53.930746,371.22049 54.715791,371.41236 C 55.875877,371.6915 56.793926,372.19741 57.469942,372.93009 C 58.145923,373.6628 58.483923,374.54378 58.48394,375.57303 C 58.483923,376.31446 58.30511,376.97956 57.947502,377.56832 C 57.58986,378.1571 57.070868,378.66083 56.390524,379.0795 C 55.95438,379.34991 55.361246,379.58106 54.61112,379.77295 C 55.561865,379.94741 56.285837,380.15675 56.783039,380.40097 C 57.533163,380.76733 58.126298,381.29068 58.562444,381.97103 C 58.998554,382.6514 59.216618,383.41899 59.216636,384.27379 C 59.216618,385.10244 59.002915,385.87003 58.575527,386.57655 C 58.148104,387.28308 57.544067,387.86313 56.763414,388.3167 C 55.982728,388.77028 55.073401,389.0603 54.03543,389.18678 C 52.997432,389.31326 51.928918,389.3765 50.829886,389.3765 L 41.684272,389.3765 L 41.684272,388.27745 L 42.299213,388.27745 C 42.892346,388.27745 43.297945,388.18587 43.516012,388.00269 C 43.830021,387.74974 43.987027,387.33105 43.987031,386.74664 L 43.987031,374.06839 C 43.987027,373.47527 43.939053,373.06967 43.843108,372.85159 C 43.747157,372.63354 43.603234,372.47653 43.411341,372.38057 C 43.219442,372.28464 42.848733,372.23666 42.299213,372.23665 L 41.684272,372.23665 L 41.684272,371.12452 z M 46.433711,372.57683 L 46.433711,379.26268 L 49.770094,379.26268 C 51.331423,379.26269 52.399937,379.18854 52.975638,379.04025 C 53.874049,378.80475 54.560951,378.40351 55.036345,377.83654 C 55.51171,377.26958 55.7494,376.54125 55.749415,375.65153 C 55.7494,374.90141 55.533517,374.27556 55.101764,373.774 C 54.669983,373.27247 54.092113,372.93011 53.368154,372.74692 C 52.897123,372.63354 51.920196,372.57684 50.43737,372.57683 L 46.433711,372.57683 z M 46.433711,380.51873 L 46.433711,386.51113 C 46.433705,387.11299 46.459873,387.47062 46.512214,387.58401 C 46.581989,387.74102 46.72155,387.85005 46.930898,387.9111 C 47.079175,387.95472 47.497858,387.97652 48.186948,387.97652 L 49.966352,387.97652 C 51.7283,387.97652 52.986529,387.8784 53.741043,387.68214 C 54.495532,387.48588 55.138821,387.07156 55.670912,386.43917 C 56.202973,385.80679 56.469011,385.04574 56.469027,384.15604 C 56.469011,383.4059 56.253128,382.74081 55.821376,382.16075 C 55.389594,381.58071 54.792099,381.16202 54.028888,380.9047 C 53.265651,380.64739 51.911473,380.51874 49.966352,380.51873 L 46.433711,380.51873 z"
               style="font-size:26.7957325px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9125979,1.0957729)" />
          </svg:g>
          <svg:g
             transform="translate(-61,-3.0000449)"
             id="g9158">
            <svg:path
               id="text9166"
               d="M 55.06726,378.18807 L 55.994535,378.18807 L 57.630145,384.17672 L 56.432415,384.51157 C 55.788458,383.12926 55.299063,382.20198 54.964229,381.72975 C 54.414718,380.96562 53.729995,380.36031 52.910058,379.91383 C 52.090094,379.46738 51.186431,379.24415 50.199067,379.24413 C 49.039963,379.24415 47.973169,379.55539 46.99868,380.17785 C 46.024177,380.80034 45.234276,381.71259 44.628978,382.9146 C 44.023669,384.11663 43.721017,385.46891 43.721021,386.97142 C 43.721017,388.51689 44.027962,389.89492 44.641857,391.10553 C 45.255741,392.31614 46.086424,393.24771 47.133908,393.90023 C 48.181376,394.55276 49.310418,394.87902 50.521037,394.87902 C 51.662947,394.87902 52.830625,394.55276 54.024076,393.90023 C 55.217497,393.24771 56.252095,392.33331 57.127871,391.15704 L 58.158176,391.86538 C 57.625834,392.65528 57.050581,393.32498 56.432415,393.87447 C 55.453609,394.75882 54.436183,395.41134 53.380135,395.83205 C 52.324059,396.25276 51.164966,396.46311 49.902854,396.46311 C 47.112436,396.46311 44.845766,395.42852 43.102838,393.35932 C 41.660408,391.64215 40.939195,389.59871 40.939196,387.229 C 40.939195,385.48608 41.338438,383.90842 42.136926,382.49604 C 42.93541,381.08367 44.036548,379.96107 45.440343,379.12822 C 46.844127,378.29541 48.357387,377.879 49.980127,377.87898 C 50.847289,377.879 51.652215,377.99705 52.394905,378.23315 C 53.13757,378.46928 53.89098,378.8492 54.655138,379.37292 L 55.06726,378.18807 z"
               style="font-size:26.37582016px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9292071,1.0761864)" />
            <svg:path
               id="text9228"
               d="M 56.513774,416.32099 L 56.956267,416.32099 L 56.956267,423.15308 C 56.956262,423.47561 56.976912,423.68309 57.018216,423.77552 C 57.05951,423.86796 57.127359,423.93777 57.221762,423.98497 C 57.316156,424.03217 57.502986,424.05577 57.782253,424.05577 L 58.360444,424.05577 L 58.360444,424.55136 L 54.472407,424.55136 L 54.472407,424.05577 L 55.115496,424.05577 C 55.394756,424.05577 55.580603,424.03119 55.673037,423.98202 C 55.765466,423.93286 55.837248,423.85419 55.888384,423.74602 C 55.939513,423.63786 55.965079,423.44021 55.965083,423.15308 L 55.965083,417.53637 L 54.472407,417.53637 L 54.472407,417.02898 C 55.286591,416.91493 55.967046,416.67893 56.513774,416.32099 L 56.513774,416.32099 z"
               style="font-size:12.08300495px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(1.0287959,0.9720101)" />
          </svg:g>
          <svg:g
             id="g9170"
             transform="translate(-31,-3.0000649)">
            <svg:path
               id="text9178"
               d="M 43.136549,359.70468 L 43.136549,358.63 L 49.432932,358.63 C 52.315602,358.63002 54.325891,358.81124 55.463804,359.17366 C 57.141139,359.7047 58.502404,360.69509 59.547603,362.14485 C 60.702342,363.74635 61.279721,365.58806 61.27974,367.66999 C 61.279721,369.08605 60.995246,370.40306 60.426315,371.62103 C 59.857347,372.83901 59.109283,373.81887 58.182122,374.56061 C 57.415077,375.18434 56.492115,375.62475 55.413231,375.88183 C 54.33432,376.13892 53.246994,376.26746 52.151249,376.26746 L 43.136549,376.26746 L 43.136549,375.20542 L 43.869862,375.20542 L 44.110086,375.21806 C 44.421952,375.21806 44.676926,375.15484 44.875008,375.02841 C 45.073084,374.90198 45.214267,374.7334 45.29856,374.52268 C 45.357558,374.36253 45.38706,373.99166 45.387063,373.41006 L 45.387063,361.61383 C 45.38706,360.94796 45.344915,360.51809 45.26063,360.32421 C 45.142622,360.06293 44.952972,359.8817 44.691679,359.78054 C 44.539957,359.72999 44.02158,359.7047 43.136549,359.70468 L 43.136549,359.70468 z M 47.738724,374.90198 L 51.557013,374.90198 C 51.936303,374.90198 52.566362,374.83455 53.447192,374.69968 C 54.327999,374.56482 54.958058,374.40046 55.337371,374.20659 C 55.893664,373.92001 56.433112,373.45853 56.955718,372.82215 C 57.478294,372.18577 57.880773,371.42295 58.163157,370.5337 C 58.445509,369.64446 58.586692,368.62667 58.586709,367.48034 C 58.586692,366.34244 58.445509,365.35627 58.163157,364.52179 C 57.880773,363.68735 57.455115,362.93718 56.88618,362.27128 C 56.317215,361.60541 55.685049,361.09757 54.989679,360.74776 C 54.294283,360.39797 53.516719,360.17672 52.656983,360.08398 C 52.193384,360.04185 51.417926,360.02078 50.330609,360.02077 L 47.738724,360.02077 L 47.738724,374.90198 z"
               style="font-size:25.8935585px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8818737,1.1339493)" />
            <svg:path
               id="text9222"
               d="M 56.513774,416.32099 L 56.956267,416.32099 L 56.956267,423.15308 C 56.956262,423.47561 56.976912,423.68309 57.018216,423.77552 C 57.05951,423.86796 57.127359,423.93777 57.221762,423.98497 C 57.316156,424.03217 57.502986,424.05577 57.782253,424.05577 L 58.360444,424.05577 L 58.360444,424.55136 L 54.472407,424.55136 L 54.472407,424.05577 L 55.115496,424.05577 C 55.394756,424.05577 55.580603,424.03119 55.673037,423.98202 C 55.765466,423.93286 55.837248,423.85419 55.888384,423.74602 C 55.939513,423.63786 55.965079,423.44021 55.965083,423.15308 L 55.965083,417.53637 L 54.472407,417.53637 L 54.472407,417.02898 C 55.286591,416.91493 55.967046,416.67893 56.513774,416.32099 L 56.513774,416.32099 z"
               style="font-size:12.08300495px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(1.0287959,0.9720101)" />
          </svg:g>
        </svg:g>
        <svg:g
           transform="translate(0,1.0000378)"
           id="g9262">
          <svg:g
             id="g9234"
             transform="translate(-331.18085,-3.9879715)">
            <svg:path
               id="text9236"
               d="M 48.512292,387.17789 L 50.764577,387.17789 L 56.67512,403.09404 C 56.966308,403.89486 57.259787,404.42494 57.555559,404.68429 C 57.851296,404.94365 58.249426,405.07332 58.749952,405.07332 L 59.050257,405.07332 L 59.050257,406.21994 L 52.266101,406.21994 L 52.266101,405.07332 C 52.484492,405.05512 52.698345,405.04602 52.907661,405.04602 C 53.344454,405.04602 53.631109,405.00052 53.767624,404.90952 C 54.031514,404.73662 54.163466,404.51822 54.16348,404.25431 C 54.163466,404.06321 54.036065,403.59455 53.781274,402.84834 L 52.525455,399.16278 L 45.618446,399.16278 L 44.144223,403.28514 C 43.989517,403.71285 43.912166,404.00861 43.91217,404.17241 C 43.912166,404.35441 43.959941,404.51367 44.055497,404.65017 C 44.151044,404.78667 44.280721,404.88905 44.444528,404.9573 C 44.608326,405.02555 44.87223,405.05967 45.23624,405.05967 C 45.481939,405.05967 45.682142,405.06422 45.83685,405.07332 L 45.83685,406.21994 L 40.14471,406.21994 L 40.14471,405.07332 C 40.672518,405.07332 41.052449,405.021 41.284503,404.91634 C 41.516556,404.81169 41.719034,404.64334 41.891938,404.41129 C 42.064839,404.17924 42.292342,403.6901 42.574449,402.94389 L 48.512292,387.17789 z M 46.123504,397.82506 L 52.006747,397.82506 L 49.0583,389.63493 L 46.123504,397.82506 z"
               style="font-size:27.95563889px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9521015,1.0503082)" />
            <svg:text
               transform="translate(-135.95901,159.10765)"
               id="text9240"
               y="255.06474"
               x="229.61066"
               style="font-size:20px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               xml:space="preserve"><svg:tspan
                 y="255.06474"
                 x="229.61066"
                 id="tspan9242"
                 /></svg:text>
            <svg:path
               id="text9244"
               d="M 62.65208,372.90401 C 63.176598,372.90401 63.634703,373.0377 64.026396,373.30506 C 64.41808,373.57244 64.73654,373.99223 64.981776,374.56443 C 65.227002,375.13664 65.349617,375.82976 65.349623,376.64378 C 65.349617,377.84951 65.048187,378.78786 64.445333,379.45883 C 63.954866,380.00379 63.334976,380.27627 62.585663,380.27627 C 62.078168,380.27627 61.628577,380.14088 61.236891,379.87011 C 60.845201,379.59933 60.528444,379.19317 60.28662,378.65161 C 60.044794,378.11006 59.923881,377.42206 59.923882,376.58759 C 59.923881,375.74631 60.042239,375.06 60.278956,374.52866 C 60.515672,373.99734 60.838389,373.59373 61.247109,373.31784 C 61.655825,373.04196 62.124148,372.90401 62.65208,372.90401 L 62.65208,372.90401 z M 62.626535,373.4098 C 62.272309,373.4098 61.960661,373.53923 61.69159,373.79808 C 61.456575,374.02288 61.270949,374.3771 61.134711,374.86075 C 60.978034,375.41252 60.899696,376.02219 60.899698,376.68976 C 60.899696,377.20748 60.948232,377.67665 61.045304,378.09729 C 61.142373,378.51793 61.270949,378.84491 61.431032,379.07821 C 61.591112,379.31153 61.77248,379.48438 61.975139,379.59678 C 62.177793,379.70917 62.391518,379.76537 62.616317,379.76537 C 62.953506,379.76537 63.26856,379.63424 63.561479,379.37198 C 63.796488,379.15741 63.982114,378.83724 64.118358,378.41149 C 64.281841,377.89378 64.363584,377.28582 64.363589,376.58759 C 64.363584,375.86552 64.280138,375.25244 64.113249,374.74835 C 63.946351,374.24427 63.74114,373.89516 63.497616,373.70101 C 63.254084,373.50687 62.963724,373.4098 62.626535,373.4098 L 62.626535,373.4098 z"
               style="font-size:10.46319962px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9215331,1.0851483)" />
          </svg:g>
          <svg:g
             transform="translate(-301.18084,-3.9879715)"
             id="g9250">
            <svg:path
               id="text9324"
               d="M 62.652072,372.90401 C 63.17659,372.90401 63.634696,373.0377 64.026389,373.30506 C 64.418072,373.57244 64.736532,373.99223 64.981769,374.56443 C 65.226994,375.13664 65.34961,375.82976 65.349616,376.64378 C 65.34961,377.84951 65.04818,378.78786 64.445325,379.45883 C 63.954858,380.00379 63.334969,380.27627 62.585655,380.27627 C 62.07816,380.27627 61.62857,380.14088 61.236883,379.87011 C 60.845193,379.59933 60.528436,379.19317 60.286612,378.65161 C 60.044786,378.11006 59.923874,377.42206 59.923874,376.58759 C 59.923874,375.74631 60.042232,375.06 60.278949,374.52866 C 60.515664,373.99734 60.838381,373.59373 61.247101,373.31784 C 61.655818,373.04196 62.124141,372.90401 62.652072,372.90401 L 62.652072,372.90401 z M 62.626527,373.4098 C 62.272301,373.4098 61.960653,373.53923 61.691583,373.79808 C 61.456568,374.02288 61.270941,374.3771 61.134704,374.86075 C 60.978026,375.41252 60.899689,376.02219 60.89969,376.68976 C 60.899689,377.20748 60.948224,377.67665 61.045296,378.09729 C 61.142365,378.51793 61.270941,378.84491 61.431025,379.07821 C 61.591104,379.31153 61.772473,379.48438 61.975131,379.59678 C 62.177785,379.70917 62.391511,379.76537 62.616309,379.76537 C 62.953498,379.76537 63.268552,379.63424 63.561471,379.37198 C 63.79648,379.15741 63.982106,378.83724 64.11835,378.41149 C 64.281833,377.89378 64.363577,377.28582 64.363582,376.58759 C 64.363577,375.86552 64.28013,375.25244 64.113241,374.74835 C 63.946343,374.24427 63.741133,373.89516 63.497609,373.70101 C 63.254077,373.50687 62.963716,373.4098 62.626527,373.4098 L 62.626527,373.4098 z"
               style="font-size:10.46319962px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9215331,1.0851483)" />
            <svg:path
               id="text9258"
               d="M 41.882446,371.1135 L 50.557041,371.1135 C 52.676613,371.11352 54.12892,371.20947 54.913964,371.40135 C 56.074051,371.68049 56.9921,372.1864 57.668115,372.91907 C 58.344097,373.65179 58.682096,374.53276 58.682114,375.56201 C 58.682096,376.30344 58.503284,376.96854 58.145676,377.5573 C 57.788034,378.14609 57.269041,378.64981 56.588697,379.06849 C 56.152554,379.3389 55.559419,379.57004 54.809293,379.76193 C 55.760038,379.93639 56.484011,380.14573 56.981213,380.38995 C 57.731337,380.75631 58.324471,381.27966 58.760617,381.96002 C 59.196727,382.64038 59.414791,383.40797 59.41481,384.26278 C 59.414791,385.09142 59.201088,385.85901 58.773701,386.56553 C 58.346278,387.27206 57.74224,387.85211 56.961587,388.30569 C 56.180902,388.75926 55.271575,389.04929 54.233604,389.17576 C 53.195606,389.30224 52.127092,389.36548 51.02806,389.36548 L 41.882446,389.36548 L 41.882446,388.26643 L 42.497387,388.26643 C 43.090519,388.26644 43.496118,388.17485 43.714185,387.99167 C 44.028195,387.73872 44.185201,387.32004 44.185204,386.73562 L 44.185204,374.05737 C 44.185201,373.46425 44.137227,373.05865 44.041282,372.84057 C 43.94533,372.62252 43.801408,372.46552 43.609514,372.36955 C 43.417615,372.27362 43.046906,372.22565 42.497387,372.22563 L 41.882446,372.22563 L 41.882446,371.1135 z M 46.631885,372.56581 L 46.631885,379.25166 L 49.968267,379.25166 C 51.529597,379.25167 52.59811,379.17753 53.173812,379.02923 C 54.072223,378.79374 54.759125,378.3925 55.234519,377.82552 C 55.709884,377.25856 55.947573,376.53023 55.947589,375.64052 C 55.947573,374.89039 55.73169,374.26455 55.299938,373.76298 C 54.868157,373.26145 54.290287,372.91909 53.566327,372.7359 C 53.095296,372.62252 52.118369,372.56583 50.635544,372.56581 L 46.631885,372.56581 z M 46.631885,380.50771 L 46.631885,386.50011 C 46.631879,387.10197 46.658047,387.4596 46.710388,387.57299 C 46.780162,387.73 46.919723,387.83903 47.129071,387.90009 C 47.277348,387.9437 47.696031,387.96551 48.385121,387.96551 L 50.164525,387.96551 C 51.926473,387.96551 53.184702,387.86738 53.939217,387.67112 C 54.693705,387.47486 55.336994,387.06054 55.869085,386.42815 C 56.401147,385.79577 56.667185,385.03473 56.667201,384.14502 C 56.667185,383.39489 56.451301,382.72979 56.01955,382.14973 C 55.587768,381.56969 54.990272,381.15101 54.227062,380.89368 C 53.463824,380.63638 52.109647,380.50772 50.164525,380.50771 L 46.631885,380.50771 z"
               style="font-size:26.7957325px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9125979,1.0957729)" />
          </svg:g>
        </svg:g>
      </svg:g>
      <svg:g
         transform="translate(-6.0000018,-203.00001)"
         id="g9819">
        <svg:g
           id="g8832"
           transform="translate(246,-230.00004)">
          <svg:g
             style="fill:#ff0000"
             transform="translate(149,-3.00002)"
             id="g8834">
            <svg:path
               id="text8836"
               d="M 55.06726,378.18807 L 55.994535,378.18807 L 57.630145,384.17672 L 56.432415,384.51157 C 55.788458,383.12926 55.299063,382.20198 54.964229,381.72975 C 54.414718,380.96562 53.729995,380.36031 52.910058,379.91383 C 52.090094,379.46738 51.186431,379.24415 50.199067,379.24413 C 49.039963,379.24415 47.973169,379.55539 46.99868,380.17785 C 46.024177,380.80034 45.234276,381.71259 44.628978,382.9146 C 44.023669,384.11663 43.721017,385.46891 43.721021,386.97142 C 43.721017,388.51689 44.027962,389.89492 44.641857,391.10553 C 45.255741,392.31614 46.086424,393.24771 47.133908,393.90023 C 48.181376,394.55276 49.310418,394.87902 50.521037,394.87902 C 51.662947,394.87902 52.830625,394.55276 54.024076,393.90023 C 55.217497,393.24771 56.252095,392.33331 57.127871,391.15704 L 58.158176,391.86538 C 57.625834,392.65528 57.050581,393.32498 56.432415,393.87447 C 55.453609,394.75882 54.436183,395.41134 53.380135,395.83205 C 52.324059,396.25276 51.164966,396.46311 49.902854,396.46311 C 47.112436,396.46311 44.845766,395.42852 43.102838,393.35932 C 41.660408,391.64215 40.939195,389.59871 40.939196,387.229 C 40.939195,385.48608 41.338438,383.90842 42.136926,382.49604 C 42.93541,381.08367 44.036548,379.96107 45.440343,379.12822 C 46.844127,378.29541 48.357387,377.879 49.980127,377.87898 C 50.847289,377.879 51.652215,377.99705 52.394905,378.23315 C 53.13757,378.46928 53.89098,378.8492 54.655138,379.37292 L 55.06726,378.18807 z"
               style="font-size:26.37582016px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#ff0000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9292071,1.0761864)" />
            <svg:path
               id="text8840"
               d="M 64.858451,362.2528 L 65.726042,362.2528 L 65.726042,366.41621 L 67.071065,366.41621 L 67.071065,367.05792 L 65.726042,367.05792 L 65.726042,369.41428 L 64.858451,369.41428 L 64.858451,367.05792 L 61.485625,367.05792 L 61.485625,366.15439 L 64.858451,362.2528 z M 64.858451,366.41621 L 64.858451,363.04852 L 61.95279,366.41621 L 64.858451,366.41621 z"
               style="font-size:10.5137682px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#ff0000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8951847,1.1170879)" />
          </svg:g>
          <svg:g
             id="g8846"
             transform="translate(179,-3.0000449)">
            <svg:path
               id="text9034"
               d="M 64.858451,362.25283 L 65.726042,362.25283 L 65.726042,366.41624 L 67.071065,366.41624 L 67.071065,367.05795 L 65.726042,367.05795 L 65.726042,369.41431 L 64.858451,369.41431 L 64.858451,367.05795 L 61.485625,367.05795 L 61.485625,366.15442 L 64.858451,362.25283 z M 64.858451,366.41624 L 64.858451,363.04855 L 61.95279,366.41624 L 64.858451,366.41624 z"
               style="font-size:10.5137682px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8951847,1.1170879)" />
            <svg:path
               id="text8854"
               d="M 43.136549,359.70468 L 43.136549,358.63 L 49.432932,358.63 C 52.315602,358.63002 54.325891,358.81124 55.463804,359.17366 C 57.141139,359.7047 58.502404,360.69509 59.547603,362.14485 C 60.702342,363.74635 61.279721,365.58806 61.27974,367.66999 C 61.279721,369.08605 60.995246,370.40306 60.426315,371.62103 C 59.857347,372.83901 59.109283,373.81887 58.182122,374.56061 C 57.415077,375.18434 56.492115,375.62475 55.413231,375.88183 C 54.33432,376.13892 53.246994,376.26746 52.151249,376.26746 L 43.136549,376.26746 L 43.136549,375.20542 L 43.869862,375.20542 L 44.110086,375.21806 C 44.421952,375.21806 44.676926,375.15484 44.875008,375.02841 C 45.073084,374.90198 45.214267,374.7334 45.29856,374.52268 C 45.357558,374.36253 45.38706,373.99166 45.387063,373.41006 L 45.387063,361.61383 C 45.38706,360.94796 45.344915,360.51809 45.26063,360.32421 C 45.142622,360.06293 44.952972,359.8817 44.691679,359.78054 C 44.539957,359.72999 44.02158,359.7047 43.136549,359.70468 L 43.136549,359.70468 z M 47.738724,374.90198 L 51.557013,374.90198 C 51.936303,374.90198 52.566362,374.83455 53.447192,374.69968 C 54.327999,374.56482 54.958058,374.40046 55.337371,374.20659 C 55.893664,373.92001 56.433112,373.45853 56.955718,372.82215 C 57.478294,372.18577 57.880773,371.42295 58.163157,370.5337 C 58.445509,369.64446 58.586692,368.62667 58.586709,367.48034 C 58.586692,366.34244 58.445509,365.35627 58.163157,364.52179 C 57.880773,363.68735 57.455115,362.93718 56.88618,362.27128 C 56.317215,361.60541 55.685049,361.09757 54.989679,360.74776 C 54.294283,360.39797 53.516719,360.17672 52.656983,360.08398 C 52.193384,360.04185 51.417926,360.02078 50.330609,360.02077 L 47.738724,360.02077 L 47.738724,374.90198 z"
               style="font-size:25.8935585px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8818737,1.1339493)" />
          </svg:g>
          <svg:g
             id="g8858"
             transform="translate(209,-3.0000041)">
            <svg:path
               id="text9038"
               d="M 64.858451,362.2528 L 65.726042,362.2528 L 65.726042,366.41621 L 67.071065,366.41621 L 67.071065,367.05792 L 65.726042,367.05792 L 65.726042,369.41428 L 64.858451,369.41428 L 64.858451,367.05792 L 61.485625,367.05792 L 61.485625,366.15439 L 64.858451,362.2528 z M 64.858451,366.41621 L 64.858451,363.04852 L 61.95279,366.41621 L 64.858451,366.41621 z"
               style="font-size:10.5137682px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8951847,1.1170879)" />
            <svg:path
               id="text8866"
               d="M 40.333911,383.54972 L 55.667727,383.54972 L 57.439094,388.51225 L 56.249168,388.95848 C 55.951669,388.14718 55.627145,387.5094 55.275592,387.04513 C 54.924007,386.58089 54.502576,386.18651 54.011296,385.86196 C 53.519986,385.53746 53.024185,385.32111 52.523889,385.21291 C 52.023566,385.10476 51.119855,385.05067 49.812751,385.05065 L 45.242355,385.05065 L 45.242355,391.96033 L 47.514031,391.96033 C 48.352378,391.96034 48.985653,391.81837 49.413856,391.53439 C 49.842038,391.25045 50.137265,390.89662 50.299539,390.47293 C 50.425732,390.15743 50.488834,389.60754 50.488845,388.82326 L 50.488845,388.51225 L 51.81399,388.51225 L 51.81399,396.78765 L 50.488845,396.78765 L 50.488845,396.07099 C 50.488834,395.43997 50.441507,394.98474 50.346866,394.70528 C 50.252201,394.42583 50.078671,394.16216 49.826273,393.91425 C 49.573854,393.66635 49.301163,393.49733 49.008199,393.40718 C 48.715215,393.31704 48.21716,393.27197 47.514031,393.27196 L 45.242355,393.27196 L 45.242355,399.41089 C 45.242348,400.05093 45.271646,400.44081 45.330247,400.58054 C 45.388835,400.72026 45.485742,400.82168 45.620967,400.88478 C 45.756179,400.94788 46.125777,400.97943 46.729762,400.97943 L 49.555835,400.97943 C 51.133376,400.97943 52.36612,400.79013 53.254071,400.41151 C 54.141992,400.0329 54.924007,399.45146 55.600117,398.66719 C 56.131961,398.0542 56.641285,397.1888 57.12809,396.07099 L 58.358582,396.50369 L 56.249168,402.41275 L 40.333911,402.41275 L 40.333911,401.27691 L 40.874786,401.27691 C 41.505805,401.27691 41.897939,401.23635 42.05119,401.15522 C 42.285566,401.03803 42.465858,400.84872 42.592065,400.5873 C 42.682208,400.40701 42.72728,400.01487 42.727284,399.41089 L 42.727284,386.42988 C 42.72728,385.84395 42.668686,385.45182 42.5515,385.25348 C 42.434307,385.05518 42.231479,384.90193 41.943015,384.79374 C 41.789764,384.73065 41.253397,384.6991 40.333911,384.69908 L 40.333911,383.54972 z"
               style="font-size:27.69281769px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9431515,1.060275)" />
          </svg:g>
          <svg:g
             transform="translate(239,-3.0000041)"
             id="g8872">
            <svg:path
               id="text9042"
               d="M 64.858451,362.2528 L 65.726042,362.2528 L 65.726042,366.41621 L 67.071065,366.41621 L 67.071065,367.05792 L 65.726042,367.05792 L 65.726042,369.41428 L 64.858451,369.41428 L 64.858451,367.05792 L 61.485625,367.05792 L 61.485625,366.15439 L 64.858451,362.2528 z M 64.858451,366.41621 L 64.858451,363.04852 L 61.95279,366.41621 L 64.858451,366.41621 z"
               style="font-size:10.5137682px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8951847,1.1170879)" />
            <svg:path
               id="text8880"
               d="M 39.380858,392.83193 L 54.157874,392.83193 L 55.944411,397.90071 L 54.739537,398.35773 C 54.231719,397.17595 53.733151,396.32654 53.243832,395.80949 C 52.754481,395.29247 52.186667,394.92317 51.540389,394.70156 C 50.894084,394.47999 49.878482,394.3692 48.493582,394.36918 L 44.324995,394.36918 L 44.324995,401.79231 L 46.222326,401.79231 C 47.219453,401.79232 47.93499,401.69538 48.36894,401.50148 C 48.802868,401.30761 49.135247,401.00062 49.366077,400.58052 C 49.596884,400.16044 49.712293,399.56262 49.712305,398.78705 L 49.712305,398.27464 L 51.069519,398.27464 L 51.069519,406.7503 L 49.712305,406.7503 L 49.712305,406.00245 C 49.712293,405.27307 49.6015,404.70525 49.379926,404.29901 C 49.158329,403.89277 48.855956,403.59963 48.472808,403.41959 C 48.089639,403.23956 47.408724,403.14954 46.430062,403.14953 L 44.324995,403.14953 L 44.324995,409.16005 C 44.324989,409.71402 44.382694,410.09948 44.49811,410.31645 C 44.613512,410.53342 44.784318,410.69961 45.010527,410.81502 C 45.236722,410.93043 45.640655,410.98813 46.222326,410.98813 L 46.734743,410.98813 L 46.734743,412.15146 L 39.380858,412.15146 L 39.380858,410.98813 L 39.851728,410.98813 C 40.442622,410.98813 40.85117,410.93274 41.077376,410.82194 C 41.303575,410.71115 41.469764,410.53804 41.575944,410.3026 C 41.682117,410.06717 41.735205,409.64015 41.735209,409.02156 L 41.735209,395.87874 C 41.735205,395.30632 41.684425,394.9047 41.582869,394.67386 C 41.481305,394.44306 41.326657,394.27457 41.118923,394.16837 C 40.911183,394.06221 40.548798,394.00912 40.031767,394.00911 L 39.380858,394.00911 L 39.380858,392.83193 z"
               style="font-size:28.36300659px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9659766,1.0352218)" />
          </svg:g>
          <svg:g
             id="g8884"
             transform="translate(269,-3.0000041)">
            <svg:path
               id="text9046"
               d="M 64.858451,362.2528 L 65.726042,362.2528 L 65.726042,366.41621 L 67.071065,366.41621 L 67.071065,367.05792 L 65.726042,367.05792 L 65.726042,369.41428 L 64.858451,369.41428 L 64.858451,367.05792 L 61.485625,367.05792 L 61.485625,366.15439 L 64.858451,362.2528 z M 64.858451,366.41621 L 64.858451,363.04852 L 61.95279,366.41621 L 64.858451,366.41621 z"
               style="font-size:10.5137682px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8951847,1.1170879)" />
            <svg:path
               id="text8892"
               d="M 54.600565,383.5977 L 55.554167,383.5977 L 57.043354,389.21481 L 55.86768,389.61976 C 55.083882,387.8432 54.182532,386.57609 53.16363,385.81842 C 52.144699,385.06078 50.990798,384.68195 49.701923,384.68193 C 47.846962,384.68195 46.292461,385.45267 45.038416,386.99409 C 43.784359,388.53554 43.157333,390.37308 43.157337,392.5067 C 43.157333,394.03944 43.477377,395.44589 44.117471,396.72606 C 44.757555,398.00624 45.613183,398.98162 46.68436,399.65218 C 47.755521,400.32275 48.892004,400.65804 50.093814,400.65804 C 50.868877,400.65804 51.606938,400.51652 52.308001,400.23349 C 53.009037,399.95046 53.577279,399.613 54.012728,399.2211 C 54.334934,398.92501 54.552652,398.59843 54.66588,398.24137 C 54.779077,397.88432 54.83133,397.38793 54.822637,396.75219 L 54.822637,395.89003 C 54.822621,395.20205 54.742066,394.71436 54.58097,394.42696 C 54.419844,394.13959 54.191241,393.93493 53.895161,393.813 C 53.66872,393.71721 53.059112,393.66932 52.066334,393.66931 L 52.066334,392.57201 L 59.460018,392.57201 L 59.460018,393.66931 L 58.859118,393.66931 C 58.345285,393.66932 57.994761,393.71939 57.807543,393.81953 C 57.620287,393.91969 57.483125,394.07427 57.396057,394.28327 C 57.308951,394.49229 57.265408,394.9016 57.265426,395.5112 L 57.265426,401.82065 L 55.972185,401.82065 L 55.279843,400.22696 C 54.548297,400.75819 53.76887,401.17185 52.941558,401.46795 C 51.704911,401.91209 50.48134,402.13416 49.270843,402.13416 C 46.823691,402.13416 44.727074,401.23499 42.980986,399.43664 C 41.23489,397.6383 40.361844,395.39799 40.361846,392.71571 C 40.361844,390.05086 41.245776,387.81272 43.013644,386.0013 C 44.781504,384.18991 46.897715,383.28421 49.362284,383.28419 C 49.98059,383.28421 50.590199,383.34517 51.19111,383.46707 C 51.791997,383.58901 52.340645,383.76318 52.837054,383.98959 C 53.211514,384.15507 53.646948,384.42504 54.143358,384.7995 L 54.600565,383.5977 z"
               style="font-size:26.75311661px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9424986,1.0610095)" />
          </svg:g>
          <svg:g
             style="fill:#ff0000"
             transform="translate(299,-3.0000041)"
             id="g8896">
            <svg:path
               id="text9050"
               d="M 64.858451,362.2528 L 65.726042,362.2528 L 65.726042,366.41621 L 67.071065,366.41621 L 67.071065,367.05792 L 65.726042,367.05792 L 65.726042,369.41428 L 64.858451,369.41428 L 64.858451,367.05792 L 61.485625,367.05792 L 61.485625,366.15439 L 64.858451,362.2528 z M 64.858451,366.41621 L 64.858451,363.04852 L 61.95279,366.41621 L 64.858451,366.41621 z"
               style="font-size:10.5137682px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#ff0000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8951847,1.1170879)" />
            <svg:path
               id="text8904"
               d="M 48.322339,387.18937 L 50.574624,387.18937 L 56.485167,403.10552 C 56.776355,403.90633 57.069834,404.43641 57.365606,404.69577 C 57.661343,404.95512 58.059474,405.0848 58.56,405.0848 L 58.860304,405.0848 L 58.860304,406.23141 L 52.076148,406.23141 L 52.076148,405.0848 C 52.294539,405.0666 52.508392,405.0575 52.717708,405.0575 C 53.154502,405.0575 53.441156,405.012 53.577671,404.92099 C 53.841562,404.74809 53.973514,404.52969 53.973528,404.26578 C 53.973514,404.07468 53.846112,403.60603 53.591322,402.85981 L 52.335502,399.17425 L 45.428493,399.17425 L 43.95427,403.29662 C 43.799564,403.72433 43.722213,404.02008 43.722217,404.18388 C 43.722213,404.36589 43.769989,404.52514 43.865544,404.66164 C 43.961091,404.79814 44.090768,404.90052 44.254575,404.96877 C 44.418373,405.03702 44.682277,405.07115 45.046287,405.07115 C 45.291986,405.07115 45.492189,405.0757 45.646897,405.0848 L 45.646897,406.23141 L 39.954758,406.23141 L 39.954758,405.0848 C 40.482565,405.0848 40.862496,405.03247 41.09455,404.92782 C 41.326603,404.82317 41.529081,404.65482 41.701985,404.42276 C 41.874886,404.19071 42.102389,403.70158 42.384496,402.95536 L 48.322339,387.18937 z M 45.933551,397.83653 L 51.816794,397.83653 L 48.868347,389.6464 L 45.933551,397.83653 z"
               style="font-size:27.95563889px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#ff0000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9521015,1.0503082)" />
          </svg:g>
          <svg:g
             id="g8908"
             transform="translate(329.00001,-3.0000041)">
            <svg:path
               id="text9054"
               d="M 64.858439,362.2528 L 65.72603,362.2528 L 65.72603,366.41621 L 67.071053,366.41621 L 67.071053,367.05792 L 65.72603,367.05792 L 65.72603,369.41428 L 64.858439,369.41428 L 64.858439,367.05792 L 61.485614,367.05792 L 61.485614,366.15439 L 64.858439,362.2528 z M 64.858439,366.41621 L 64.858439,363.04852 L 61.952779,366.41621 L 64.858439,366.41621 z"
               style="font-size:10.5137682px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8951847,1.1170879)" />
            <svg:path
               id="text8916"
               d="M 41.684272,371.12452 L 50.358867,371.12452 C 52.47844,371.12454 53.930746,371.22049 54.715791,371.41236 C 55.875877,371.6915 56.793926,372.19741 57.469942,372.93009 C 58.145923,373.6628 58.483923,374.54378 58.48394,375.57303 C 58.483923,376.31446 58.30511,376.97956 57.947502,377.56832 C 57.58986,378.1571 57.070868,378.66083 56.390524,379.0795 C 55.95438,379.34991 55.361246,379.58106 54.61112,379.77295 C 55.561865,379.94741 56.285837,380.15675 56.783039,380.40097 C 57.533163,380.76733 58.126298,381.29068 58.562444,381.97103 C 58.998554,382.6514 59.216618,383.41899 59.216636,384.27379 C 59.216618,385.10244 59.002915,385.87003 58.575527,386.57655 C 58.148104,387.28308 57.544067,387.86313 56.763414,388.3167 C 55.982728,388.77028 55.073401,389.0603 54.03543,389.18678 C 52.997432,389.31326 51.928918,389.3765 50.829886,389.3765 L 41.684272,389.3765 L 41.684272,388.27745 L 42.299213,388.27745 C 42.892346,388.27745 43.297945,388.18587 43.516012,388.00269 C 43.830021,387.74974 43.987027,387.33105 43.987031,386.74664 L 43.987031,374.06839 C 43.987027,373.47527 43.939053,373.06967 43.843108,372.85159 C 43.747157,372.63354 43.603234,372.47653 43.411341,372.38057 C 43.219442,372.28464 42.848733,372.23666 42.299213,372.23665 L 41.684272,372.23665 L 41.684272,371.12452 z M 46.433711,372.57683 L 46.433711,379.26268 L 49.770094,379.26268 C 51.331423,379.26269 52.399937,379.18854 52.975638,379.04025 C 53.874049,378.80475 54.560951,378.40351 55.036345,377.83654 C 55.51171,377.26958 55.7494,376.54125 55.749415,375.65153 C 55.7494,374.90141 55.533517,374.27556 55.101764,373.774 C 54.669983,373.27247 54.092113,372.93011 53.368154,372.74692 C 52.897123,372.63354 51.920196,372.57684 50.43737,372.57683 L 46.433711,372.57683 z M 46.433711,380.51873 L 46.433711,386.51113 C 46.433705,387.11299 46.459873,387.47062 46.512214,387.58401 C 46.581989,387.74102 46.72155,387.85005 46.930898,387.9111 C 47.079175,387.95472 47.497858,387.97652 48.186948,387.97652 L 49.966352,387.97652 C 51.7283,387.97652 52.986529,387.8784 53.741043,387.68214 C 54.495532,387.48588 55.138821,387.07156 55.670912,386.43917 C 56.202973,385.80679 56.469011,385.04574 56.469027,384.15604 C 56.469011,383.4059 56.253128,382.74081 55.821376,382.16075 C 55.389594,381.58071 54.792099,381.16202 54.028888,380.9047 C 53.265651,380.64739 51.911473,380.51874 49.966352,380.51873 L 46.433711,380.51873 z"
               style="font-size:26.7957325px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9125979,1.0957729)" />
          </svg:g>
        </svg:g>
        <svg:g
           transform="translate(456,-230.00004)"
           id="g8920">
          <svg:g
             id="g8922"
             transform="translate(149,-3.00002)">
            <svg:path
               id="text8924"
               d="M 55.06726,378.18807 L 55.994535,378.18807 L 57.630145,384.17672 L 56.432415,384.51157 C 55.788458,383.12926 55.299063,382.20198 54.964229,381.72975 C 54.414718,380.96562 53.729995,380.36031 52.910058,379.91383 C 52.090094,379.46738 51.186431,379.24415 50.199067,379.24413 C 49.039963,379.24415 47.973169,379.55539 46.99868,380.17785 C 46.024177,380.80034 45.234276,381.71259 44.628978,382.9146 C 44.023669,384.11663 43.721017,385.46891 43.721021,386.97142 C 43.721017,388.51689 44.027962,389.89492 44.641857,391.10553 C 45.255741,392.31614 46.086424,393.24771 47.133908,393.90023 C 48.181376,394.55276 49.310418,394.87902 50.521037,394.87902 C 51.662947,394.87902 52.830625,394.55276 54.024076,393.90023 C 55.217497,393.24771 56.252095,392.33331 57.127871,391.15704 L 58.158176,391.86538 C 57.625834,392.65528 57.050581,393.32498 56.432415,393.87447 C 55.453609,394.75882 54.436183,395.41134 53.380135,395.83205 C 52.324059,396.25276 51.164966,396.46311 49.902854,396.46311 C 47.112436,396.46311 44.845766,395.42852 43.102838,393.35932 C 41.660408,391.64215 40.939195,389.59871 40.939196,387.229 C 40.939195,385.48608 41.338438,383.90842 42.136926,382.49604 C 42.93541,381.08367 44.036548,379.96107 45.440343,379.12822 C 46.844127,378.29541 48.357387,377.879 49.980127,377.87898 C 50.847289,377.879 51.652215,377.99705 52.394905,378.23315 C 53.13757,378.46928 53.89098,378.8492 54.655138,379.37292 L 55.06726,378.18807 z"
               style="font-size:26.37582016px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9292071,1.0761864)" />
            <svg:path
               id="text8928"
               d="M 58.251463,386.90613 L 61.49019,386.90613 C 61.688049,386.90613 61.807809,386.89572 61.84947,386.87488 C 61.908477,386.84018 61.94319,386.76555 61.953609,386.65099 L 62.333717,386.65099 L 62.333717,387.74965 L 58.73571,387.74965 L 58.647191,389.5825 C 58.796455,389.54085 58.985641,389.50614 59.21475,389.47836 C 59.509808,389.44365 59.773627,389.4263 60.006207,389.42629 C 60.585911,389.4263 61.090985,389.53044 61.521431,389.73871 C 61.951868,389.94699 62.279038,390.23251 62.502943,390.59525 C 62.726836,390.95801 62.838785,391.36328 62.838791,391.81108 C 62.838785,392.50534 62.578438,393.09286 62.057748,393.57363 C 61.537047,394.05441 60.874029,394.2948 60.06869,394.2948 C 59.603533,394.2948 59.182638,394.21149 58.806003,394.04486 C 58.429365,393.87824 58.135173,393.65868 57.923424,393.38618 C 57.711674,393.11369 57.605799,392.87504 57.6058,392.67023 C 57.605799,392.53485 57.650926,392.41769 57.741181,392.31876 C 57.831434,392.21983 57.947722,392.17036 58.090047,392.17036 C 58.23584,392.17036 58.364278,392.23111 58.475362,392.3526 C 58.537843,392.41509 58.612476,392.56782 58.699261,392.81081 C 58.817283,393.151 58.992584,393.40094 59.225163,393.56062 C 59.454267,393.72377 59.742385,393.80534 60.089518,393.80534 C 60.436645,393.80534 60.745591,393.72984 61.016357,393.57884 C 61.287114,393.42784 61.500599,393.21088 61.656812,392.92797 C 61.813016,392.64506 61.89112,392.33004 61.891125,391.98291 C 61.89112,391.61842 61.806941,391.29386 61.638588,391.00921 C 61.470225,390.72456 61.22897,390.50414 60.914821,390.34792 C 60.600664,390.19172 60.217953,390.11362 59.766687,390.11361 C 59.492451,390.11362 59.198258,390.14312 58.884108,390.20213 C 58.569953,390.26115 58.300059,390.33578 58.074426,390.42603 L 58.251463,386.90613 z"
               style="font-size:10.66384697px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9554765,1.0465982)" />
          </svg:g>
          <svg:g
             transform="translate(179,-3.0000449)"
             id="g8934">
            <svg:path
               id="text9568"
               d="M 58.251463,386.90616 L 61.49019,386.90616 C 61.688049,386.90616 61.807809,386.89575 61.84947,386.87491 C 61.908477,386.84021 61.94319,386.76558 61.953609,386.65102 L 62.333717,386.65102 L 62.333717,387.74968 L 58.73571,387.74968 L 58.647191,389.58253 C 58.796455,389.54088 58.985641,389.50617 59.21475,389.47839 C 59.509808,389.44368 59.773627,389.42633 60.006207,389.42632 C 60.585911,389.42633 61.090985,389.53047 61.521431,389.73874 C 61.951868,389.94702 62.279038,390.23254 62.502943,390.59529 C 62.726836,390.95804 62.838785,391.36331 62.838791,391.81111 C 62.838785,392.50537 62.578438,393.09289 62.057748,393.57366 C 61.537047,394.05444 60.874029,394.29483 60.06869,394.29483 C 59.603533,394.29483 59.182638,394.21152 58.806003,394.04489 C 58.429365,393.87827 58.135173,393.65871 57.923424,393.38621 C 57.711674,393.11372 57.605799,392.87507 57.6058,392.67026 C 57.605799,392.53488 57.650926,392.41772 57.741181,392.31879 C 57.831434,392.21986 57.947722,392.17039 58.090047,392.17039 C 58.23584,392.17039 58.364278,392.23114 58.475362,392.35263 C 58.537843,392.41512 58.612476,392.56786 58.699261,392.81085 C 58.817283,393.15103 58.992584,393.40097 59.225163,393.56065 C 59.454267,393.7238 59.742385,393.80537 60.089518,393.80537 C 60.436645,393.80537 60.745591,393.72987 61.016357,393.57887 C 61.287114,393.42787 61.500599,393.21091 61.656812,392.928 C 61.813016,392.64509 61.89112,392.33007 61.891125,391.98294 C 61.89112,391.61845 61.806941,391.29389 61.638588,391.00924 C 61.470225,390.72459 61.22897,390.50417 60.914821,390.34795 C 60.600664,390.19175 60.217953,390.11365 59.766687,390.11364 C 59.492451,390.11365 59.198258,390.14315 58.884108,390.20216 C 58.569953,390.26118 58.300059,390.33581 58.074426,390.42606 L 58.251463,386.90616 z"
               style="font-size:10.66384697px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9554765,1.0465982)" />
            <svg:path
               id="text8942"
               d="M 43.136549,359.70468 L 43.136549,358.63 L 49.432932,358.63 C 52.315602,358.63002 54.325891,358.81124 55.463804,359.17366 C 57.141139,359.7047 58.502404,360.69509 59.547603,362.14485 C 60.702342,363.74635 61.279721,365.58806 61.27974,367.66999 C 61.279721,369.08605 60.995246,370.40306 60.426315,371.62103 C 59.857347,372.83901 59.109283,373.81887 58.182122,374.56061 C 57.415077,375.18434 56.492115,375.62475 55.413231,375.88183 C 54.33432,376.13892 53.246994,376.26746 52.151249,376.26746 L 43.136549,376.26746 L 43.136549,375.20542 L 43.869862,375.20542 L 44.110086,375.21806 C 44.421952,375.21806 44.676926,375.15484 44.875008,375.02841 C 45.073084,374.90198 45.214267,374.7334 45.29856,374.52268 C 45.357558,374.36253 45.38706,373.99166 45.387063,373.41006 L 45.387063,361.61383 C 45.38706,360.94796 45.344915,360.51809 45.26063,360.32421 C 45.142622,360.06293 44.952972,359.8817 44.691679,359.78054 C 44.539957,359.72999 44.02158,359.7047 43.136549,359.70468 L 43.136549,359.70468 z M 47.738724,374.90198 L 51.557013,374.90198 C 51.936303,374.90198 52.566362,374.83455 53.447192,374.69968 C 54.327999,374.56482 54.958058,374.40046 55.337371,374.20659 C 55.893664,373.92001 56.433112,373.45853 56.955718,372.82215 C 57.478294,372.18577 57.880773,371.42295 58.163157,370.5337 C 58.445509,369.64446 58.586692,368.62667 58.586709,367.48034 C 58.586692,366.34244 58.445509,365.35627 58.163157,364.52179 C 57.880773,363.68735 57.455115,362.93718 56.88618,362.27128 C 56.317215,361.60541 55.685049,361.09757 54.989679,360.74776 C 54.294283,360.39797 53.516719,360.17672 52.656983,360.08398 C 52.193384,360.04185 51.417926,360.02078 50.330609,360.02077 L 47.738724,360.02077 L 47.738724,374.90198 z"
               style="font-size:25.8935585px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8818737,1.1339493)" />
          </svg:g>
          <svg:g
             transform="translate(209,-3.0000041)"
             id="g8946">
            <svg:path
               id="text9572"
               d="M 58.251463,386.90613 L 61.49019,386.90613 C 61.688049,386.90613 61.807809,386.89572 61.84947,386.87488 C 61.908477,386.84018 61.94319,386.76555 61.953609,386.65099 L 62.333717,386.65099 L 62.333717,387.74965 L 58.73571,387.74965 L 58.647191,389.5825 C 58.796455,389.54085 58.985641,389.50614 59.21475,389.47836 C 59.509808,389.44365 59.773627,389.4263 60.006207,389.42629 C 60.585911,389.4263 61.090985,389.53044 61.521431,389.73871 C 61.951868,389.94699 62.279038,390.23251 62.502943,390.59525 C 62.726836,390.95801 62.838785,391.36328 62.838791,391.81108 C 62.838785,392.50534 62.578438,393.09286 62.057748,393.57363 C 61.537047,394.05441 60.874029,394.2948 60.06869,394.2948 C 59.603533,394.2948 59.182638,394.21149 58.806003,394.04486 C 58.429365,393.87824 58.135173,393.65868 57.923424,393.38618 C 57.711674,393.11369 57.605799,392.87504 57.6058,392.67023 C 57.605799,392.53485 57.650926,392.41769 57.741181,392.31876 C 57.831434,392.21983 57.947722,392.17036 58.090047,392.17036 C 58.23584,392.17036 58.364278,392.23111 58.475362,392.3526 C 58.537843,392.41509 58.612476,392.56782 58.699261,392.81081 C 58.817283,393.151 58.992584,393.40094 59.225163,393.56062 C 59.454267,393.72377 59.742385,393.80534 60.089518,393.80534 C 60.436645,393.80534 60.745591,393.72984 61.016357,393.57884 C 61.287114,393.42784 61.500599,393.21088 61.656812,392.92797 C 61.813016,392.64506 61.89112,392.33004 61.891125,391.98291 C 61.89112,391.61842 61.806941,391.29386 61.638588,391.00921 C 61.470225,390.72456 61.22897,390.50414 60.914821,390.34792 C 60.600664,390.19172 60.217953,390.11362 59.766687,390.11361 C 59.492451,390.11362 59.198258,390.14312 58.884108,390.20213 C 58.569953,390.26115 58.300059,390.33578 58.074426,390.42603 L 58.251463,386.90613 z"
               style="font-size:10.66384697px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9554765,1.0465982)" />
            <svg:path
               id="text8954"
               d="M 40.333911,383.54972 L 55.667727,383.54972 L 57.439094,388.51225 L 56.249168,388.95848 C 55.951669,388.14718 55.627145,387.5094 55.275592,387.04513 C 54.924007,386.58089 54.502576,386.18651 54.011296,385.86196 C 53.519986,385.53746 53.024185,385.32111 52.523889,385.21291 C 52.023566,385.10476 51.119855,385.05067 49.812751,385.05065 L 45.242355,385.05065 L 45.242355,391.96033 L 47.514031,391.96033 C 48.352378,391.96034 48.985653,391.81837 49.413856,391.53439 C 49.842038,391.25045 50.137265,390.89662 50.299539,390.47293 C 50.425732,390.15743 50.488834,389.60754 50.488845,388.82326 L 50.488845,388.51225 L 51.81399,388.51225 L 51.81399,396.78765 L 50.488845,396.78765 L 50.488845,396.07099 C 50.488834,395.43997 50.441507,394.98474 50.346866,394.70528 C 50.252201,394.42583 50.078671,394.16216 49.826273,393.91425 C 49.573854,393.66635 49.301163,393.49733 49.008199,393.40718 C 48.715215,393.31704 48.21716,393.27197 47.514031,393.27196 L 45.242355,393.27196 L 45.242355,399.41089 C 45.242348,400.05093 45.271646,400.44081 45.330247,400.58054 C 45.388835,400.72026 45.485742,400.82168 45.620967,400.88478 C 45.756179,400.94788 46.125777,400.97943 46.729762,400.97943 L 49.555835,400.97943 C 51.133376,400.97943 52.36612,400.79013 53.254071,400.41151 C 54.141992,400.0329 54.924007,399.45146 55.600117,398.66719 C 56.131961,398.0542 56.641285,397.1888 57.12809,396.07099 L 58.358582,396.50369 L 56.249168,402.41275 L 40.333911,402.41275 L 40.333911,401.27691 L 40.874786,401.27691 C 41.505805,401.27691 41.897939,401.23635 42.05119,401.15522 C 42.285566,401.03803 42.465858,400.84872 42.592065,400.5873 C 42.682208,400.40701 42.72728,400.01487 42.727284,399.41089 L 42.727284,386.42988 C 42.72728,385.84395 42.668686,385.45182 42.5515,385.25348 C 42.434307,385.05518 42.231479,384.90193 41.943015,384.79374 C 41.789764,384.73065 41.253397,384.6991 40.333911,384.69908 L 40.333911,383.54972 z"
               style="font-size:27.69281769px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9431515,1.060275)" />
          </svg:g>
          <svg:g
             id="g8960"
             transform="translate(239,-3.0000041)">
            <svg:path
               id="text9576"
               d="M 58.251463,386.90613 L 61.49019,386.90613 C 61.688049,386.90613 61.807809,386.89572 61.84947,386.87488 C 61.908477,386.84018 61.94319,386.76555 61.953609,386.65099 L 62.333717,386.65099 L 62.333717,387.74965 L 58.73571,387.74965 L 58.647191,389.5825 C 58.796455,389.54085 58.985641,389.50614 59.21475,389.47836 C 59.509808,389.44365 59.773627,389.4263 60.006207,389.42629 C 60.585911,389.4263 61.090985,389.53044 61.521431,389.73871 C 61.951868,389.94699 62.279038,390.23251 62.502943,390.59525 C 62.726836,390.95801 62.838785,391.36328 62.838791,391.81108 C 62.838785,392.50534 62.578438,393.09286 62.057748,393.57363 C 61.537047,394.05441 60.874029,394.2948 60.06869,394.2948 C 59.603533,394.2948 59.182638,394.21149 58.806003,394.04486 C 58.429365,393.87824 58.135173,393.65868 57.923424,393.38618 C 57.711674,393.11369 57.605799,392.87504 57.6058,392.67023 C 57.605799,392.53485 57.650926,392.41769 57.741181,392.31876 C 57.831434,392.21983 57.947722,392.17036 58.090047,392.17036 C 58.23584,392.17036 58.364278,392.23111 58.475362,392.3526 C 58.537843,392.41509 58.612476,392.56782 58.699261,392.81081 C 58.817283,393.151 58.992584,393.40094 59.225163,393.56062 C 59.454267,393.72377 59.742385,393.80534 60.089518,393.80534 C 60.436645,393.80534 60.745591,393.72984 61.016357,393.57884 C 61.287114,393.42784 61.500599,393.21088 61.656812,392.92797 C 61.813016,392.64506 61.89112,392.33004 61.891125,391.98291 C 61.89112,391.61842 61.806941,391.29386 61.638588,391.00921 C 61.470225,390.72456 61.22897,390.50414 60.914821,390.34792 C 60.600664,390.19172 60.217953,390.11362 59.766687,390.11361 C 59.492451,390.11362 59.198258,390.14312 58.884108,390.20213 C 58.569953,390.26115 58.300059,390.33578 58.074426,390.42603 L 58.251463,386.90613 z"
               style="font-size:10.66384697px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9554765,1.0465982)" />
            <svg:path
               id="text8968"
               d="M 39.380858,392.83193 L 54.157874,392.83193 L 55.944411,397.90071 L 54.739537,398.35773 C 54.231719,397.17595 53.733151,396.32654 53.243832,395.80949 C 52.754481,395.29247 52.186667,394.92317 51.540389,394.70156 C 50.894084,394.47999 49.878482,394.3692 48.493582,394.36918 L 44.324995,394.36918 L 44.324995,401.79231 L 46.222326,401.79231 C 47.219453,401.79232 47.93499,401.69538 48.36894,401.50148 C 48.802868,401.30761 49.135247,401.00062 49.366077,400.58052 C 49.596884,400.16044 49.712293,399.56262 49.712305,398.78705 L 49.712305,398.27464 L 51.069519,398.27464 L 51.069519,406.7503 L 49.712305,406.7503 L 49.712305,406.00245 C 49.712293,405.27307 49.6015,404.70525 49.379926,404.29901 C 49.158329,403.89277 48.855956,403.59963 48.472808,403.41959 C 48.089639,403.23956 47.408724,403.14954 46.430062,403.14953 L 44.324995,403.14953 L 44.324995,409.16005 C 44.324989,409.71402 44.382694,410.09948 44.49811,410.31645 C 44.613512,410.53342 44.784318,410.69961 45.010527,410.81502 C 45.236722,410.93043 45.640655,410.98813 46.222326,410.98813 L 46.734743,410.98813 L 46.734743,412.15146 L 39.380858,412.15146 L 39.380858,410.98813 L 39.851728,410.98813 C 40.442622,410.98813 40.85117,410.93274 41.077376,410.82194 C 41.303575,410.71115 41.469764,410.53804 41.575944,410.3026 C 41.682117,410.06717 41.735205,409.64015 41.735209,409.02156 L 41.735209,395.87874 C 41.735205,395.30632 41.684425,394.9047 41.582869,394.67386 C 41.481305,394.44306 41.326657,394.27457 41.118923,394.16837 C 40.911183,394.06221 40.548798,394.00912 40.031767,394.00911 L 39.380858,394.00911 L 39.380858,392.83193 z"
               style="font-size:28.36300659px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9659766,1.0352218)" />
          </svg:g>
          <svg:g
             transform="translate(269,-3.0000041)"
             id="g8972">
            <svg:path
               id="text9580"
               d="M 58.251463,386.90613 L 61.49019,386.90613 C 61.688049,386.90613 61.807809,386.89572 61.84947,386.87488 C 61.908477,386.84018 61.94319,386.76555 61.953609,386.65099 L 62.333717,386.65099 L 62.333717,387.74965 L 58.73571,387.74965 L 58.647191,389.5825 C 58.796455,389.54085 58.985641,389.50614 59.21475,389.47836 C 59.509808,389.44365 59.773627,389.4263 60.006207,389.42629 C 60.585911,389.4263 61.090985,389.53044 61.521431,389.73871 C 61.951868,389.94699 62.279038,390.23251 62.502943,390.59525 C 62.726836,390.95801 62.838785,391.36328 62.838791,391.81108 C 62.838785,392.50534 62.578438,393.09286 62.057748,393.57363 C 61.537047,394.05441 60.874029,394.2948 60.06869,394.2948 C 59.603533,394.2948 59.182638,394.21149 58.806003,394.04486 C 58.429365,393.87824 58.135173,393.65868 57.923424,393.38618 C 57.711674,393.11369 57.605799,392.87504 57.6058,392.67023 C 57.605799,392.53485 57.650926,392.41769 57.741181,392.31876 C 57.831434,392.21983 57.947722,392.17036 58.090047,392.17036 C 58.23584,392.17036 58.364278,392.23111 58.475362,392.3526 C 58.537843,392.41509 58.612476,392.56782 58.699261,392.81081 C 58.817283,393.151 58.992584,393.40094 59.225163,393.56062 C 59.454267,393.72377 59.742385,393.80534 60.089518,393.80534 C 60.436645,393.80534 60.745591,393.72984 61.016357,393.57884 C 61.287114,393.42784 61.500599,393.21088 61.656812,392.92797 C 61.813016,392.64506 61.89112,392.33004 61.891125,391.98291 C 61.89112,391.61842 61.806941,391.29386 61.638588,391.00921 C 61.470225,390.72456 61.22897,390.50414 60.914821,390.34792 C 60.600664,390.19172 60.217953,390.11362 59.766687,390.11361 C 59.492451,390.11362 59.198258,390.14312 58.884108,390.20213 C 58.569953,390.26115 58.300059,390.33578 58.074426,390.42603 L 58.251463,386.90613 z"
               style="font-size:10.66384697px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9554765,1.0465982)" />
            <svg:path
               id="text8980"
               d="M 54.600565,383.5977 L 55.554167,383.5977 L 57.043354,389.21481 L 55.86768,389.61976 C 55.083882,387.8432 54.182532,386.57609 53.16363,385.81842 C 52.144699,385.06078 50.990798,384.68195 49.701923,384.68193 C 47.846962,384.68195 46.292461,385.45267 45.038416,386.99409 C 43.784359,388.53554 43.157333,390.37308 43.157337,392.5067 C 43.157333,394.03944 43.477377,395.44589 44.117471,396.72606 C 44.757555,398.00624 45.613183,398.98162 46.68436,399.65218 C 47.755521,400.32275 48.892004,400.65804 50.093814,400.65804 C 50.868877,400.65804 51.606938,400.51652 52.308001,400.23349 C 53.009037,399.95046 53.577279,399.613 54.012728,399.2211 C 54.334934,398.92501 54.552652,398.59843 54.66588,398.24137 C 54.779077,397.88432 54.83133,397.38793 54.822637,396.75219 L 54.822637,395.89003 C 54.822621,395.20205 54.742066,394.71436 54.58097,394.42696 C 54.419844,394.13959 54.191241,393.93493 53.895161,393.813 C 53.66872,393.71721 53.059112,393.66932 52.066334,393.66931 L 52.066334,392.57201 L 59.460018,392.57201 L 59.460018,393.66931 L 58.859118,393.66931 C 58.345285,393.66932 57.994761,393.71939 57.807543,393.81953 C 57.620287,393.91969 57.483125,394.07427 57.396057,394.28327 C 57.308951,394.49229 57.265408,394.9016 57.265426,395.5112 L 57.265426,401.82065 L 55.972185,401.82065 L 55.279843,400.22696 C 54.548297,400.75819 53.76887,401.17185 52.941558,401.46795 C 51.704911,401.91209 50.48134,402.13416 49.270843,402.13416 C 46.823691,402.13416 44.727074,401.23499 42.980986,399.43664 C 41.23489,397.6383 40.361844,395.39799 40.361846,392.71571 C 40.361844,390.05086 41.245776,387.81272 43.013644,386.0013 C 44.781504,384.18991 46.897715,383.28421 49.362284,383.28419 C 49.98059,383.28421 50.590199,383.34517 51.19111,383.46707 C 51.791997,383.58901 52.340645,383.76318 52.837054,383.98959 C 53.211514,384.15507 53.646948,384.42504 54.143358,384.7995 L 54.600565,383.5977 z"
               style="font-size:26.75311661px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9424986,1.0610095)" />
          </svg:g>
          <svg:g
             id="g8984"
             transform="translate(299,-3.0000041)">
            <svg:path
               id="text9584"
               d="M 58.251463,386.90613 L 61.49019,386.90613 C 61.688049,386.90613 61.807809,386.89572 61.84947,386.87488 C 61.908477,386.84018 61.94319,386.76555 61.953609,386.65099 L 62.333717,386.65099 L 62.333717,387.74965 L 58.73571,387.74965 L 58.647191,389.5825 C 58.796455,389.54085 58.985641,389.50614 59.21475,389.47836 C 59.509808,389.44365 59.773627,389.4263 60.006207,389.42629 C 60.585911,389.4263 61.090985,389.53044 61.521431,389.73871 C 61.951868,389.94699 62.279038,390.23251 62.502943,390.59525 C 62.726836,390.95801 62.838785,391.36328 62.838791,391.81108 C 62.838785,392.50534 62.578438,393.09286 62.057748,393.57363 C 61.537047,394.05441 60.874029,394.2948 60.06869,394.2948 C 59.603533,394.2948 59.182638,394.21149 58.806003,394.04486 C 58.429365,393.87824 58.135173,393.65868 57.923424,393.38618 C 57.711674,393.11369 57.605799,392.87504 57.6058,392.67023 C 57.605799,392.53485 57.650926,392.41769 57.741181,392.31876 C 57.831434,392.21983 57.947722,392.17036 58.090047,392.17036 C 58.23584,392.17036 58.364278,392.23111 58.475362,392.3526 C 58.537843,392.41509 58.612476,392.56782 58.699261,392.81081 C 58.817283,393.151 58.992584,393.40094 59.225163,393.56062 C 59.454267,393.72377 59.742385,393.80534 60.089518,393.80534 C 60.436645,393.80534 60.745591,393.72984 61.016357,393.57884 C 61.287114,393.42784 61.500599,393.21088 61.656812,392.92797 C 61.813016,392.64506 61.89112,392.33004 61.891125,391.98291 C 61.89112,391.61842 61.806941,391.29386 61.638588,391.00921 C 61.470225,390.72456 61.22897,390.50414 60.914821,390.34792 C 60.600664,390.19172 60.217953,390.11362 59.766687,390.11361 C 59.492451,390.11362 59.198258,390.14312 58.884108,390.20213 C 58.569953,390.26115 58.300059,390.33578 58.074426,390.42603 L 58.251463,386.90613 z"
               style="font-size:10.66384697px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9554765,1.0465982)" />
            <svg:path
               id="text8992"
               d="M 48.322339,387.18937 L 50.574624,387.18937 L 56.485167,403.10552 C 56.776355,403.90633 57.069834,404.43641 57.365606,404.69577 C 57.661343,404.95512 58.059474,405.0848 58.56,405.0848 L 58.860304,405.0848 L 58.860304,406.23141 L 52.076148,406.23141 L 52.076148,405.0848 C 52.294539,405.0666 52.508392,405.0575 52.717708,405.0575 C 53.154502,405.0575 53.441156,405.012 53.577671,404.92099 C 53.841562,404.74809 53.973514,404.52969 53.973528,404.26578 C 53.973514,404.07468 53.846112,403.60603 53.591322,402.85981 L 52.335502,399.17425 L 45.428493,399.17425 L 43.95427,403.29662 C 43.799564,403.72433 43.722213,404.02008 43.722217,404.18388 C 43.722213,404.36589 43.769989,404.52514 43.865544,404.66164 C 43.961091,404.79814 44.090768,404.90052 44.254575,404.96877 C 44.418373,405.03702 44.682277,405.07115 45.046287,405.07115 C 45.291986,405.07115 45.492189,405.0757 45.646897,405.0848 L 45.646897,406.23141 L 39.954758,406.23141 L 39.954758,405.0848 C 40.482565,405.0848 40.862496,405.03247 41.09455,404.92782 C 41.326603,404.82317 41.529081,404.65482 41.701985,404.42276 C 41.874886,404.19071 42.102389,403.70158 42.384496,402.95536 L 48.322339,387.18937 z M 45.933551,397.83653 L 51.816794,397.83653 L 48.868347,389.6464 L 45.933551,397.83653 z"
               style="font-size:27.95563889px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9521015,1.0503082)" />
          </svg:g>
          <svg:g
             transform="translate(329.00001,-3.0000041)"
             id="g8996">
            <svg:path
               id="text9588"
               d="M 58.251455,386.90613 L 61.490182,386.90613 C 61.688041,386.90613 61.807801,386.89572 61.849462,386.87488 C 61.908469,386.84018 61.943182,386.76555 61.953601,386.65099 L 62.333709,386.65099 L 62.333709,387.74965 L 58.735702,387.74965 L 58.647184,389.5825 C 58.796448,389.54085 58.985634,389.50614 59.214742,389.47836 C 59.5098,389.44365 59.773619,389.4263 60.006199,389.42629 C 60.585903,389.4263 61.090978,389.53044 61.521424,389.73871 C 61.95186,389.94699 62.27903,390.23251 62.502935,390.59525 C 62.726828,390.95801 62.838778,391.36328 62.838784,391.81108 C 62.838778,392.50534 62.57843,393.09286 62.05774,393.57363 C 61.53704,394.05441 60.874021,394.2948 60.068683,394.2948 C 59.603525,394.2948 59.18263,394.21149 58.805996,394.04486 C 58.429358,393.87824 58.135165,393.65868 57.923417,393.38618 C 57.711666,393.11369 57.605792,392.87504 57.605792,392.67023 C 57.605792,392.53485 57.650918,392.41769 57.741173,392.31876 C 57.831426,392.21983 57.947715,392.17036 58.090039,392.17036 C 58.235833,392.17036 58.364271,392.23111 58.475354,392.3526 C 58.537836,392.41509 58.612469,392.56782 58.699253,392.81081 C 58.817276,393.151 58.992576,393.40094 59.225156,393.56062 C 59.454259,393.72377 59.742377,393.80534 60.089511,393.80534 C 60.436637,393.80534 60.745583,393.72984 61.016349,393.57884 C 61.287106,393.42784 61.500591,393.21088 61.656805,392.92797 C 61.813008,392.64506 61.891113,392.33004 61.891118,391.98291 C 61.891113,391.61842 61.806933,391.29386 61.63858,391.00921 C 61.470217,390.72456 61.228962,390.50414 60.914813,390.34792 C 60.600656,390.19172 60.217945,390.11362 59.766679,390.11361 C 59.492444,390.11362 59.198251,390.14312 58.8841,390.20213 C 58.569945,390.26115 58.300052,390.33578 58.074418,390.42603 L 58.251455,386.90613 z"
               style="font-size:10.66384697px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9554765,1.0465982)" />
            <svg:path
               id="text9004"
               d="M 41.684272,371.12452 L 50.358867,371.12452 C 52.47844,371.12454 53.930746,371.22049 54.715791,371.41236 C 55.875877,371.6915 56.793926,372.19741 57.469942,372.93009 C 58.145923,373.6628 58.483923,374.54378 58.48394,375.57303 C 58.483923,376.31446 58.30511,376.97956 57.947502,377.56832 C 57.58986,378.1571 57.070868,378.66083 56.390524,379.0795 C 55.95438,379.34991 55.361246,379.58106 54.61112,379.77295 C 55.561865,379.94741 56.285837,380.15675 56.783039,380.40097 C 57.533163,380.76733 58.126298,381.29068 58.562444,381.97103 C 58.998554,382.6514 59.216618,383.41899 59.216636,384.27379 C 59.216618,385.10244 59.002915,385.87003 58.575527,386.57655 C 58.148104,387.28308 57.544067,387.86313 56.763414,388.3167 C 55.982728,388.77028 55.073401,389.0603 54.03543,389.18678 C 52.997432,389.31326 51.928918,389.3765 50.829886,389.3765 L 41.684272,389.3765 L 41.684272,388.27745 L 42.299213,388.27745 C 42.892346,388.27745 43.297945,388.18587 43.516012,388.00269 C 43.830021,387.74974 43.987027,387.33105 43.987031,386.74664 L 43.987031,374.06839 C 43.987027,373.47527 43.939053,373.06967 43.843108,372.85159 C 43.747157,372.63354 43.603234,372.47653 43.411341,372.38057 C 43.219442,372.28464 42.848733,372.23666 42.299213,372.23665 L 41.684272,372.23665 L 41.684272,371.12452 z M 46.433711,372.57683 L 46.433711,379.26268 L 49.770094,379.26268 C 51.331423,379.26269 52.399937,379.18854 52.975638,379.04025 C 53.874049,378.80475 54.560951,378.40351 55.036345,377.83654 C 55.51171,377.26958 55.7494,376.54125 55.749415,375.65153 C 55.7494,374.90141 55.533517,374.27556 55.101764,373.774 C 54.669983,373.27247 54.092113,372.93011 53.368154,372.74692 C 52.897123,372.63354 51.920196,372.57684 50.43737,372.57683 L 46.433711,372.57683 z M 46.433711,380.51873 L 46.433711,386.51113 C 46.433705,387.11299 46.459873,387.47062 46.512214,387.58401 C 46.581989,387.74102 46.72155,387.85005 46.930898,387.9111 C 47.079175,387.95472 47.497858,387.97652 48.186948,387.97652 L 49.966352,387.97652 C 51.7283,387.97652 52.986529,387.8784 53.741043,387.68214 C 54.495532,387.48588 55.138821,387.07156 55.670912,386.43917 C 56.202973,385.80679 56.469011,385.04574 56.469027,384.15604 C 56.469011,383.4059 56.253128,382.74081 55.821376,382.16075 C 55.389594,381.58071 54.792099,381.16202 54.028888,380.9047 C 53.265651,380.64739 51.911473,380.51874 49.966352,380.51873 L 46.433711,380.51873 z"
               style="font-size:26.7957325px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9125979,1.0957729)" />
          </svg:g>
        </svg:g>
        <svg:g
           id="g9008"
           transform="translate(1235,-233.00003)">
          <svg:path
             id="text9010"
             d="M 55.06726,378.18807 L 55.994535,378.18807 L 57.630145,384.17672 L 56.432415,384.51157 C 55.788458,383.12926 55.299063,382.20198 54.964229,381.72975 C 54.414718,380.96562 53.729995,380.36031 52.910058,379.91383 C 52.090094,379.46738 51.186431,379.24415 50.199067,379.24413 C 49.039963,379.24415 47.973169,379.55539 46.99868,380.17785 C 46.024177,380.80034 45.234276,381.71259 44.628978,382.9146 C 44.023669,384.11663 43.721017,385.46891 43.721021,386.97142 C 43.721017,388.51689 44.027962,389.89492 44.641857,391.10553 C 45.255741,392.31614 46.086424,393.24771 47.133908,393.90023 C 48.181376,394.55276 49.310418,394.87902 50.521037,394.87902 C 51.662947,394.87902 52.830625,394.55276 54.024076,393.90023 C 55.217497,393.24771 56.252095,392.33331 57.127871,391.15704 L 58.158176,391.86538 C 57.625834,392.65528 57.050581,393.32498 56.432415,393.87447 C 55.453609,394.75882 54.436183,395.41134 53.380135,395.83205 C 52.324059,396.25276 51.164966,396.46311 49.902854,396.46311 C 47.112436,396.46311 44.845766,395.42852 43.102838,393.35932 C 41.660408,391.64215 40.939195,389.59871 40.939196,387.229 C 40.939195,385.48608 41.338438,383.90842 42.136926,382.49604 C 42.93541,381.08367 44.036548,379.96107 45.440343,379.12822 C 46.844127,378.29541 48.357387,377.879 49.980127,377.87898 C 50.847289,377.879 51.652215,377.99705 52.394905,378.23315 C 53.13757,378.46928 53.89098,378.8492 54.655138,379.37292 L 55.06726,378.18807 z"
             style="font-size:26.37582016px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
             transform="scale(0.9292071,1.0761864)" />
          <svg:path
             id="text9014"
             d="M 62.600928,380.16874 C 63.007059,380.32363 63.316823,380.46991 63.530221,380.60758 C 63.843422,380.80721 64.079187,381.04211 64.237516,381.31229 C 64.395835,381.58248 64.474996,381.87589 64.475002,382.19254 C 64.474996,382.76044 64.230627,383.25176 63.741894,383.6665 C 63.25315,384.08124 62.594041,384.28861 61.764565,384.28861 C 60.886897,384.28861 60.198532,384.05457 59.699469,383.58648 C 59.303658,383.21132 59.105754,382.78625 59.105754,382.31128 C 59.105754,381.92924 59.239985,381.58678 59.508448,381.2839 C 59.831978,380.92251 60.293182,380.64028 60.892062,380.43721 C 60.423972,380.21693 60.086673,379.99321 59.880165,379.76605 C 59.615143,379.46662 59.482633,379.10695 59.482634,378.68704 C 59.482633,378.16733 59.678817,377.74743 60.071186,377.42733 C 60.55304,377.03497 61.155359,376.83878 61.878145,376.83878 C 62.538971,376.83878 63.073314,377.00829 63.481175,377.34731 C 63.889026,377.68633 64.092954,378.0744 64.09296,378.5115 C 64.092954,378.72834 64.042187,378.93571 63.940659,379.13361 C 63.83912,379.33152 63.683377,379.51566 63.473431,379.68603 C 63.263475,379.8564 62.972641,380.01731 62.600928,380.16874 L 62.600928,380.16874 z M 62.131119,379.97772 C 62.523483,379.68173 62.778178,379.47522 62.895204,379.35819 C 63.012222,379.24118 63.10257,379.10781 63.166248,378.95808 C 63.229917,378.80837 63.261754,378.66295 63.261759,378.52183 C 63.261754,378.31877 63.201522,378.12517 63.081063,377.94102 C 62.960595,377.75689 62.784201,377.60803 62.551882,377.49444 C 62.319555,377.38087 62.058837,377.32408 61.769727,377.32407 C 61.342938,377.32408 60.98843,377.44196 60.706203,377.67772 C 60.423972,377.91349 60.282857,378.18626 60.282859,378.49602 C 60.282857,378.74383 60.370623,378.96583 60.546158,379.16201 C 60.721689,379.3582 61.048662,379.55094 61.527079,379.74024 L 62.131119,379.97772 z M 61.403173,380.63855 C 60.917873,380.85195 60.53411,381.12902 60.251882,381.46975 C 60.031604,381.73822 59.921465,382.01701 59.921467,382.30612 C 59.921465,382.69849 60.095277,383.04697 60.442903,383.35157 C 60.790526,383.65617 61.239683,383.80847 61.790378,383.80847 C 62.351392,383.80847 62.81948,383.63466 63.194643,383.28704 C 63.500961,383.00481 63.654122,382.70193 63.654127,382.3784 C 63.654122,382.1237 63.553449,381.88019 63.352107,381.64787 C 63.150755,381.41555 62.776457,381.18925 62.229211,380.96897 L 61.403173,380.63855 z"
             style="font-size:10.57328892px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
             transform="scale(0.931229,1.0738497)" />
        </svg:g>
        <svg:g
           id="g9671"
           transform="translate(666,-230.00004)">
          <svg:g
             transform="translate(149,-3.00002)"
             id="g9673">
            <svg:path
               id="text9974"
               d="M 64.046951,374.94294 L 64.046951,375.40974 C 63.202264,375.42 62.519165,375.54311 61.997654,375.77907 C 61.476136,376.01504 61.092267,376.36813 60.846046,376.83834 C 60.599821,377.30857 60.46132,377.88736 60.430544,378.57473 C 60.60495,378.30799 60.783633,378.10024 60.966593,377.95148 C 61.149548,377.80272 61.363284,377.68816 61.6078,377.60779 C 61.85231,377.52743 62.119907,377.48725 62.410591,377.48725 C 62.875676,377.48725 63.289468,377.58984 63.651968,377.79503 C 64.014458,378.00022 64.296589,378.27807 64.498361,378.62859 C 64.700122,378.97912 64.801005,379.39206 64.801011,379.86741 C 64.801005,380.62318 64.57359,381.21651 64.118766,381.6474 C 63.616055,382.11933 62.96459,382.35529 62.164368,382.35529 C 61.586424,382.35529 61.099107,382.2356 60.702416,381.99621 C 60.305721,381.75683 59.990247,381.39092 59.755994,380.89847 C 59.521739,380.40602 59.404612,379.7973 59.404613,379.07231 C 59.404612,378.3439 59.528579,377.69757 59.776513,377.1333 C 60.024445,376.56904 60.367277,376.11592 60.805009,375.77394 C 61.242737,375.43197 61.722359,375.20798 62.243877,375.10196 C 62.765388,374.99595 63.233041,374.94294 63.646838,374.94294 L 64.046951,374.94294 z M 62.174627,378.03099 C 61.682177,378.03099 61.27779,378.21737 60.961463,378.59012 C 60.645133,378.96288 60.486968,379.41087 60.48697,379.93409 C 60.486968,380.30343 60.563913,380.64455 60.717805,380.95746 C 60.871692,381.27037 61.070039,381.50291 61.312845,381.65509 C 61.555646,381.80727 61.829227,381.88336 62.13359,381.88336 C 62.605514,381.88336 62.997932,381.70468 63.310846,381.34731 C 63.62375,380.98995 63.780204,380.51032 63.780209,379.90844 C 63.780204,379.33393 63.628025,378.87739 63.32367,378.53883 C 63.019306,378.20027 62.636292,378.03099 62.174627,378.03099 L 62.174627,378.03099 z"
               style="font-size:10.50553513px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.926544,1.0792795)" />
            <svg:path
               id="text9675"
               d="M 55.06726,378.18807 L 55.994535,378.18807 L 57.630145,384.17672 L 56.432415,384.51157 C 55.788458,383.12926 55.299063,382.20198 54.964229,381.72975 C 54.414718,380.96562 53.729995,380.36031 52.910058,379.91383 C 52.090094,379.46738 51.186431,379.24415 50.199067,379.24413 C 49.039963,379.24415 47.973169,379.55539 46.99868,380.17785 C 46.024177,380.80034 45.234276,381.71259 44.628978,382.9146 C 44.023669,384.11663 43.721017,385.46891 43.721021,386.97142 C 43.721017,388.51689 44.027962,389.89492 44.641857,391.10553 C 45.255741,392.31614 46.086424,393.24771 47.133908,393.90023 C 48.181376,394.55276 49.310418,394.87902 50.521037,394.87902 C 51.662947,394.87902 52.830625,394.55276 54.024076,393.90023 C 55.217497,393.24771 56.252095,392.33331 57.127871,391.15704 L 58.158176,391.86538 C 57.625834,392.65528 57.050581,393.32498 56.432415,393.87447 C 55.453609,394.75882 54.436183,395.41134 53.380135,395.83205 C 52.324059,396.25276 51.164966,396.46311 49.902854,396.46311 C 47.112436,396.46311 44.845766,395.42852 43.102838,393.35932 C 41.660408,391.64215 40.939195,389.59871 40.939196,387.229 C 40.939195,385.48608 41.338438,383.90842 42.136926,382.49604 C 42.93541,381.08367 44.036548,379.96107 45.440343,379.12822 C 46.844127,378.29541 48.357387,377.879 49.980127,377.87898 C 50.847289,377.879 51.652215,377.99705 52.394905,378.23315 C 53.13757,378.46928 53.89098,378.8492 54.655138,379.37292 L 55.06726,378.18807 z"
               style="font-size:26.37582016px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9292071,1.0761864)" />
          </svg:g>
          <svg:g
             id="g9683"
             transform="translate(179,-3.0000449)">
            <svg:path
               id="text9978"
               d="M 64.046951,374.94294 L 64.046951,375.40974 C 63.202264,375.42 62.519165,375.54311 61.997654,375.77907 C 61.476136,376.01504 61.092267,376.36813 60.846046,376.83834 C 60.599821,377.30857 60.46132,377.88736 60.430544,378.57473 C 60.60495,378.30799 60.783633,378.10024 60.966593,377.95148 C 61.149548,377.80272 61.363284,377.68816 61.6078,377.60779 C 61.85231,377.52743 62.119907,377.48725 62.410591,377.48725 C 62.875676,377.48725 63.289468,377.58984 63.651968,377.79503 C 64.014458,378.00022 64.296589,378.27807 64.498361,378.62859 C 64.700122,378.97912 64.801005,379.39206 64.801011,379.86741 C 64.801005,380.62318 64.57359,381.21651 64.118766,381.6474 C 63.616055,382.11933 62.96459,382.35529 62.164368,382.35529 C 61.586424,382.35529 61.099107,382.2356 60.702416,381.99621 C 60.305721,381.75683 59.990247,381.39092 59.755994,380.89847 C 59.521739,380.40602 59.404612,379.7973 59.404613,379.07231 C 59.404612,378.3439 59.528579,377.69757 59.776513,377.1333 C 60.024445,376.56904 60.367277,376.11592 60.805009,375.77394 C 61.242737,375.43197 61.722359,375.20798 62.243877,375.10196 C 62.765388,374.99595 63.233041,374.94294 63.646838,374.94294 L 64.046951,374.94294 z M 62.174627,378.03099 C 61.682177,378.03099 61.27779,378.21737 60.961463,378.59012 C 60.645133,378.96288 60.486968,379.41087 60.48697,379.93409 C 60.486968,380.30343 60.563913,380.64455 60.717805,380.95746 C 60.871692,381.27037 61.070039,381.50291 61.312845,381.65509 C 61.555646,381.80727 61.829227,381.88336 62.13359,381.88336 C 62.605514,381.88336 62.997932,381.70468 63.310846,381.34731 C 63.62375,380.98995 63.780204,380.51032 63.780209,379.90844 C 63.780204,379.33393 63.628025,378.87739 63.32367,378.53883 C 63.019306,378.20027 62.636292,378.03099 62.174627,378.03099 L 62.174627,378.03099 z"
               style="font-size:10.50553513px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.926544,1.0792795)" />
            <svg:path
               id="text9689"
               d="M 43.136549,359.70468 L 43.136549,358.63 L 49.432932,358.63 C 52.315602,358.63002 54.325891,358.81124 55.463804,359.17366 C 57.141139,359.7047 58.502404,360.69509 59.547603,362.14485 C 60.702342,363.74635 61.279721,365.58806 61.27974,367.66999 C 61.279721,369.08605 60.995246,370.40306 60.426315,371.62103 C 59.857347,372.83901 59.109283,373.81887 58.182122,374.56061 C 57.415077,375.18434 56.492115,375.62475 55.413231,375.88183 C 54.33432,376.13892 53.246994,376.26746 52.151249,376.26746 L 43.136549,376.26746 L 43.136549,375.20542 L 43.869862,375.20542 L 44.110086,375.21806 C 44.421952,375.21806 44.676926,375.15484 44.875008,375.02841 C 45.073084,374.90198 45.214267,374.7334 45.29856,374.52268 C 45.357558,374.36253 45.38706,373.99166 45.387063,373.41006 L 45.387063,361.61383 C 45.38706,360.94796 45.344915,360.51809 45.26063,360.32421 C 45.142622,360.06293 44.952972,359.8817 44.691679,359.78054 C 44.539957,359.72999 44.02158,359.7047 43.136549,359.70468 L 43.136549,359.70468 z M 47.738724,374.90198 L 51.557013,374.90198 C 51.936303,374.90198 52.566362,374.83455 53.447192,374.69968 C 54.327999,374.56482 54.958058,374.40046 55.337371,374.20659 C 55.893664,373.92001 56.433112,373.45853 56.955718,372.82215 C 57.478294,372.18577 57.880773,371.42295 58.163157,370.5337 C 58.445509,369.64446 58.586692,368.62667 58.586709,367.48034 C 58.586692,366.34244 58.445509,365.35627 58.163157,364.52179 C 57.880773,363.68735 57.455115,362.93718 56.88618,362.27128 C 56.317215,361.60541 55.685049,361.09757 54.989679,360.74776 C 54.294283,360.39797 53.516719,360.17672 52.656983,360.08398 C 52.193384,360.04185 51.417926,360.02078 50.330609,360.02077 L 47.738724,360.02077 L 47.738724,374.90198 z"
               style="font-size:25.8935585px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8818737,1.1339493)" />
          </svg:g>
          <svg:g
             id="g9693"
             transform="translate(209,-3.0000041)">
            <svg:path
               id="text9982"
               d="M 64.046951,374.94291 L 64.046951,375.4097 C 63.202264,375.41997 62.519165,375.54308 61.997654,375.77904 C 61.476136,376.01501 61.092267,376.3681 60.846046,376.83831 C 60.599821,377.30854 60.46132,377.88733 60.430544,378.5747 C 60.60495,378.30796 60.783633,378.10021 60.966593,377.95145 C 61.149548,377.80269 61.363284,377.68813 61.6078,377.60776 C 61.85231,377.5274 62.119907,377.48722 62.410591,377.48722 C 62.875676,377.48722 63.289468,377.58981 63.651968,377.79499 C 64.014458,378.00019 64.296589,378.27804 64.498361,378.62856 C 64.700122,378.97909 64.801005,379.39203 64.801011,379.86738 C 64.801005,380.62315 64.57359,381.21648 64.118766,381.64737 C 63.616055,382.11929 62.96459,382.35526 62.164368,382.35526 C 61.586424,382.35526 61.099107,382.23557 60.702416,381.99618 C 60.305721,381.7568 59.990247,381.39088 59.755994,380.89844 C 59.521739,380.40599 59.404612,379.79727 59.404613,379.07228 C 59.404612,378.34387 59.528579,377.69754 59.776513,377.13327 C 60.024445,376.56901 60.367277,376.11589 60.805009,375.77391 C 61.242737,375.43194 61.722359,375.20795 62.243877,375.10193 C 62.765388,374.99592 63.233041,374.94291 63.646838,374.94291 L 64.046951,374.94291 z M 62.174627,378.03096 C 61.682177,378.03096 61.27779,378.21734 60.961463,378.59009 C 60.645133,378.96285 60.486968,379.41084 60.48697,379.93406 C 60.486968,380.3034 60.563913,380.64452 60.717805,380.95743 C 60.871692,381.27034 61.070039,381.50288 61.312845,381.65506 C 61.555646,381.80724 61.829227,381.88333 62.13359,381.88333 C 62.605514,381.88333 62.997932,381.70465 63.310846,381.34728 C 63.62375,380.98992 63.780204,380.51029 63.780209,379.90841 C 63.780204,379.33389 63.628025,378.87736 63.32367,378.5388 C 63.019306,378.20024 62.636292,378.03096 62.174627,378.03096 L 62.174627,378.03096 z"
               style="font-size:10.50553513px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.926544,1.0792795)" />
            <svg:path
               id="text9699"
               d="M 40.333911,383.54972 L 55.667727,383.54972 L 57.439094,388.51225 L 56.249168,388.95848 C 55.951669,388.14718 55.627145,387.5094 55.275592,387.04513 C 54.924007,386.58089 54.502576,386.18651 54.011296,385.86196 C 53.519986,385.53746 53.024185,385.32111 52.523889,385.21291 C 52.023566,385.10476 51.119855,385.05067 49.812751,385.05065 L 45.242355,385.05065 L 45.242355,391.96033 L 47.514031,391.96033 C 48.352378,391.96034 48.985653,391.81837 49.413856,391.53439 C 49.842038,391.25045 50.137265,390.89662 50.299539,390.47293 C 50.425732,390.15743 50.488834,389.60754 50.488845,388.82326 L 50.488845,388.51225 L 51.81399,388.51225 L 51.81399,396.78765 L 50.488845,396.78765 L 50.488845,396.07099 C 50.488834,395.43997 50.441507,394.98474 50.346866,394.70528 C 50.252201,394.42583 50.078671,394.16216 49.826273,393.91425 C 49.573854,393.66635 49.301163,393.49733 49.008199,393.40718 C 48.715215,393.31704 48.21716,393.27197 47.514031,393.27196 L 45.242355,393.27196 L 45.242355,399.41089 C 45.242348,400.05093 45.271646,400.44081 45.330247,400.58054 C 45.388835,400.72026 45.485742,400.82168 45.620967,400.88478 C 45.756179,400.94788 46.125777,400.97943 46.729762,400.97943 L 49.555835,400.97943 C 51.133376,400.97943 52.36612,400.79013 53.254071,400.41151 C 54.141992,400.0329 54.924007,399.45146 55.600117,398.66719 C 56.131961,398.0542 56.641285,397.1888 57.12809,396.07099 L 58.358582,396.50369 L 56.249168,402.41275 L 40.333911,402.41275 L 40.333911,401.27691 L 40.874786,401.27691 C 41.505805,401.27691 41.897939,401.23635 42.05119,401.15522 C 42.285566,401.03803 42.465858,400.84872 42.592065,400.5873 C 42.682208,400.40701 42.72728,400.01487 42.727284,399.41089 L 42.727284,386.42988 C 42.72728,385.84395 42.668686,385.45182 42.5515,385.25348 C 42.434307,385.05518 42.231479,384.90193 41.943015,384.79374 C 41.789764,384.73065 41.253397,384.6991 40.333911,384.69908 L 40.333911,383.54972 z"
               style="font-size:27.69281769px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9431515,1.060275)" />
          </svg:g>
          <svg:g
             transform="translate(239,-3.0000041)"
             id="g9705">
            <svg:path
               id="text9986"
               d="M 64.046951,374.94291 L 64.046951,375.4097 C 63.202264,375.41997 62.519165,375.54308 61.997654,375.77904 C 61.476136,376.01501 61.092267,376.3681 60.846046,376.83831 C 60.599821,377.30854 60.46132,377.88733 60.430544,378.5747 C 60.60495,378.30796 60.783633,378.10021 60.966593,377.95145 C 61.149548,377.80269 61.363284,377.68813 61.6078,377.60776 C 61.85231,377.5274 62.119907,377.48722 62.410591,377.48722 C 62.875676,377.48722 63.289468,377.58981 63.651968,377.79499 C 64.014458,378.00019 64.296589,378.27804 64.498361,378.62856 C 64.700122,378.97909 64.801005,379.39203 64.801011,379.86738 C 64.801005,380.62315 64.57359,381.21648 64.118766,381.64737 C 63.616055,382.11929 62.96459,382.35526 62.164368,382.35526 C 61.586424,382.35526 61.099107,382.23557 60.702416,381.99618 C 60.305721,381.7568 59.990247,381.39088 59.755994,380.89844 C 59.521739,380.40599 59.404612,379.79727 59.404613,379.07228 C 59.404612,378.34387 59.528579,377.69754 59.776513,377.13327 C 60.024445,376.56901 60.367277,376.11589 60.805009,375.77391 C 61.242737,375.43194 61.722359,375.20795 62.243877,375.10193 C 62.765388,374.99592 63.233041,374.94291 63.646838,374.94291 L 64.046951,374.94291 z M 62.174627,378.03096 C 61.682177,378.03096 61.27779,378.21734 60.961463,378.59009 C 60.645133,378.96285 60.486968,379.41084 60.48697,379.93406 C 60.486968,380.3034 60.563913,380.64452 60.717805,380.95743 C 60.871692,381.27034 61.070039,381.50288 61.312845,381.65506 C 61.555646,381.80724 61.829227,381.88333 62.13359,381.88333 C 62.605514,381.88333 62.997932,381.70465 63.310846,381.34728 C 63.62375,380.98992 63.780204,380.51029 63.780209,379.90841 C 63.780204,379.33389 63.628025,378.87736 63.32367,378.5388 C 63.019306,378.20024 62.636292,378.03096 62.174627,378.03096 L 62.174627,378.03096 z"
               style="font-size:10.50553513px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.926544,1.0792795)" />
            <svg:path
               id="text9711"
               d="M 39.380858,392.83193 L 54.157874,392.83193 L 55.944411,397.90071 L 54.739537,398.35773 C 54.231719,397.17595 53.733151,396.32654 53.243832,395.80949 C 52.754481,395.29247 52.186667,394.92317 51.540389,394.70156 C 50.894084,394.47999 49.878482,394.3692 48.493582,394.36918 L 44.324995,394.36918 L 44.324995,401.79231 L 46.222326,401.79231 C 47.219453,401.79232 47.93499,401.69538 48.36894,401.50148 C 48.802868,401.30761 49.135247,401.00062 49.366077,400.58052 C 49.596884,400.16044 49.712293,399.56262 49.712305,398.78705 L 49.712305,398.27464 L 51.069519,398.27464 L 51.069519,406.7503 L 49.712305,406.7503 L 49.712305,406.00245 C 49.712293,405.27307 49.6015,404.70525 49.379926,404.29901 C 49.158329,403.89277 48.855956,403.59963 48.472808,403.41959 C 48.089639,403.23956 47.408724,403.14954 46.430062,403.14953 L 44.324995,403.14953 L 44.324995,409.16005 C 44.324989,409.71402 44.382694,410.09948 44.49811,410.31645 C 44.613512,410.53342 44.784318,410.69961 45.010527,410.81502 C 45.236722,410.93043 45.640655,410.98813 46.222326,410.98813 L 46.734743,410.98813 L 46.734743,412.15146 L 39.380858,412.15146 L 39.380858,410.98813 L 39.851728,410.98813 C 40.442622,410.98813 40.85117,410.93274 41.077376,410.82194 C 41.303575,410.71115 41.469764,410.53804 41.575944,410.3026 C 41.682117,410.06717 41.735205,409.64015 41.735209,409.02156 L 41.735209,395.87874 C 41.735205,395.30632 41.684425,394.9047 41.582869,394.67386 C 41.481305,394.44306 41.326657,394.27457 41.118923,394.16837 C 40.911183,394.06221 40.548798,394.00912 40.031767,394.00911 L 39.380858,394.00911 L 39.380858,392.83193 z"
               style="font-size:28.36300659px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9659766,1.0352218)" />
          </svg:g>
          <svg:g
             id="g9715"
             transform="translate(269,-3.0000041)">
            <svg:path
               id="text9990"
               d="M 64.046951,374.94291 L 64.046951,375.4097 C 63.202264,375.41997 62.519165,375.54308 61.997654,375.77904 C 61.476136,376.01501 61.092267,376.3681 60.846046,376.83831 C 60.599821,377.30854 60.46132,377.88733 60.430544,378.5747 C 60.60495,378.30796 60.783633,378.10021 60.966593,377.95145 C 61.149548,377.80269 61.363284,377.68813 61.6078,377.60776 C 61.85231,377.5274 62.119907,377.48722 62.410591,377.48722 C 62.875676,377.48722 63.289468,377.58981 63.651968,377.79499 C 64.014458,378.00019 64.296589,378.27804 64.498361,378.62856 C 64.700122,378.97909 64.801005,379.39203 64.801011,379.86738 C 64.801005,380.62315 64.57359,381.21648 64.118766,381.64737 C 63.616055,382.11929 62.96459,382.35526 62.164368,382.35526 C 61.586424,382.35526 61.099107,382.23557 60.702416,381.99618 C 60.305721,381.7568 59.990247,381.39088 59.755994,380.89844 C 59.521739,380.40599 59.404612,379.79727 59.404613,379.07228 C 59.404612,378.34387 59.528579,377.69754 59.776513,377.13327 C 60.024445,376.56901 60.367277,376.11589 60.805009,375.77391 C 61.242737,375.43194 61.722359,375.20795 62.243877,375.10193 C 62.765388,374.99592 63.233041,374.94291 63.646838,374.94291 L 64.046951,374.94291 z M 62.174627,378.03096 C 61.682177,378.03096 61.27779,378.21734 60.961463,378.59009 C 60.645133,378.96285 60.486968,379.41084 60.48697,379.93406 C 60.486968,380.3034 60.563913,380.64452 60.717805,380.95743 C 60.871692,381.27034 61.070039,381.50288 61.312845,381.65506 C 61.555646,381.80724 61.829227,381.88333 62.13359,381.88333 C 62.605514,381.88333 62.997932,381.70465 63.310846,381.34728 C 63.62375,380.98992 63.780204,380.51029 63.780209,379.90841 C 63.780204,379.33389 63.628025,378.87736 63.32367,378.5388 C 63.019306,378.20024 62.636292,378.03096 62.174627,378.03096 L 62.174627,378.03096 z"
               style="font-size:10.50553513px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.926544,1.0792795)" />
            <svg:path
               id="text9721"
               d="M 54.600565,383.5977 L 55.554167,383.5977 L 57.043354,389.21481 L 55.86768,389.61976 C 55.083882,387.8432 54.182532,386.57609 53.16363,385.81842 C 52.144699,385.06078 50.990798,384.68195 49.701923,384.68193 C 47.846962,384.68195 46.292461,385.45267 45.038416,386.99409 C 43.784359,388.53554 43.157333,390.37308 43.157337,392.5067 C 43.157333,394.03944 43.477377,395.44589 44.117471,396.72606 C 44.757555,398.00624 45.613183,398.98162 46.68436,399.65218 C 47.755521,400.32275 48.892004,400.65804 50.093814,400.65804 C 50.868877,400.65804 51.606938,400.51652 52.308001,400.23349 C 53.009037,399.95046 53.577279,399.613 54.012728,399.2211 C 54.334934,398.92501 54.552652,398.59843 54.66588,398.24137 C 54.779077,397.88432 54.83133,397.38793 54.822637,396.75219 L 54.822637,395.89003 C 54.822621,395.20205 54.742066,394.71436 54.58097,394.42696 C 54.419844,394.13959 54.191241,393.93493 53.895161,393.813 C 53.66872,393.71721 53.059112,393.66932 52.066334,393.66931 L 52.066334,392.57201 L 59.460018,392.57201 L 59.460018,393.66931 L 58.859118,393.66931 C 58.345285,393.66932 57.994761,393.71939 57.807543,393.81953 C 57.620287,393.91969 57.483125,394.07427 57.396057,394.28327 C 57.308951,394.49229 57.265408,394.9016 57.265426,395.5112 L 57.265426,401.82065 L 55.972185,401.82065 L 55.279843,400.22696 C 54.548297,400.75819 53.76887,401.17185 52.941558,401.46795 C 51.704911,401.91209 50.48134,402.13416 49.270843,402.13416 C 46.823691,402.13416 44.727074,401.23499 42.980986,399.43664 C 41.23489,397.6383 40.361844,395.39799 40.361846,392.71571 C 40.361844,390.05086 41.245776,387.81272 43.013644,386.0013 C 44.781504,384.18991 46.897715,383.28421 49.362284,383.28419 C 49.98059,383.28421 50.590199,383.34517 51.19111,383.46707 C 51.791997,383.58901 52.340645,383.76318 52.837054,383.98959 C 53.211514,384.15507 53.646948,384.42504 54.143358,384.7995 L 54.600565,383.5977 z"
               style="font-size:26.75311661px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9424986,1.0610095)" />
          </svg:g>
          <svg:g
             transform="translate(299,-3.0000041)"
             id="g9725">
            <svg:path
               id="text9994"
               d="M 64.046951,374.94291 L 64.046951,375.4097 C 63.202264,375.41997 62.519165,375.54308 61.997654,375.77904 C 61.476136,376.01501 61.092267,376.3681 60.846046,376.83831 C 60.599821,377.30854 60.46132,377.88733 60.430544,378.5747 C 60.60495,378.30796 60.783633,378.10021 60.966593,377.95145 C 61.149548,377.80269 61.363284,377.68813 61.6078,377.60776 C 61.85231,377.5274 62.119907,377.48722 62.410591,377.48722 C 62.875676,377.48722 63.289468,377.58981 63.651968,377.79499 C 64.014458,378.00019 64.296589,378.27804 64.498361,378.62856 C 64.700122,378.97909 64.801005,379.39203 64.801011,379.86738 C 64.801005,380.62315 64.57359,381.21648 64.118766,381.64737 C 63.616055,382.11929 62.96459,382.35526 62.164368,382.35526 C 61.586424,382.35526 61.099107,382.23557 60.702416,381.99618 C 60.305721,381.7568 59.990247,381.39088 59.755994,380.89844 C 59.521739,380.40599 59.404612,379.79727 59.404613,379.07228 C 59.404612,378.34387 59.528579,377.69754 59.776513,377.13327 C 60.024445,376.56901 60.367277,376.11589 60.805009,375.77391 C 61.242737,375.43194 61.722359,375.20795 62.243877,375.10193 C 62.765388,374.99592 63.233041,374.94291 63.646838,374.94291 L 64.046951,374.94291 z M 62.174627,378.03096 C 61.682177,378.03096 61.27779,378.21734 60.961463,378.59009 C 60.645133,378.96285 60.486968,379.41084 60.48697,379.93406 C 60.486968,380.3034 60.563913,380.64452 60.717805,380.95743 C 60.871692,381.27034 61.070039,381.50288 61.312845,381.65506 C 61.555646,381.80724 61.829227,381.88333 62.13359,381.88333 C 62.605514,381.88333 62.997932,381.70465 63.310846,381.34728 C 63.62375,380.98992 63.780204,380.51029 63.780209,379.90841 C 63.780204,379.33389 63.628025,378.87736 63.32367,378.5388 C 63.019306,378.20024 62.636292,378.03096 62.174627,378.03096 L 62.174627,378.03096 z"
               style="font-size:10.50553513px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.926544,1.0792795)" />
            <svg:path
               id="text9731"
               d="M 48.322339,387.18937 L 50.574624,387.18937 L 56.485167,403.10552 C 56.776355,403.90633 57.069834,404.43641 57.365606,404.69577 C 57.661343,404.95512 58.059474,405.0848 58.56,405.0848 L 58.860304,405.0848 L 58.860304,406.23141 L 52.076148,406.23141 L 52.076148,405.0848 C 52.294539,405.0666 52.508392,405.0575 52.717708,405.0575 C 53.154502,405.0575 53.441156,405.012 53.577671,404.92099 C 53.841562,404.74809 53.973514,404.52969 53.973528,404.26578 C 53.973514,404.07468 53.846112,403.60603 53.591322,402.85981 L 52.335502,399.17425 L 45.428493,399.17425 L 43.95427,403.29662 C 43.799564,403.72433 43.722213,404.02008 43.722217,404.18388 C 43.722213,404.36589 43.769989,404.52514 43.865544,404.66164 C 43.961091,404.79814 44.090768,404.90052 44.254575,404.96877 C 44.418373,405.03702 44.682277,405.07115 45.046287,405.07115 C 45.291986,405.07115 45.492189,405.0757 45.646897,405.0848 L 45.646897,406.23141 L 39.954758,406.23141 L 39.954758,405.0848 C 40.482565,405.0848 40.862496,405.03247 41.09455,404.92782 C 41.326603,404.82317 41.529081,404.65482 41.701985,404.42276 C 41.874886,404.19071 42.102389,403.70158 42.384496,402.95536 L 48.322339,387.18937 z M 45.933551,397.83653 L 51.816794,397.83653 L 48.868347,389.6464 L 45.933551,397.83653 z"
               style="font-size:27.95563889px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9521015,1.0503082)" />
          </svg:g>
          <svg:g
             id="g9735"
             transform="translate(329.00001,-3.0000041)">
            <svg:path
               id="text9998"
               d="M 64.04694,374.94291 L 64.04694,375.4097 C 63.202252,375.41997 62.519154,375.54308 61.997642,375.77904 C 61.476125,376.01501 61.092256,376.3681 60.846035,376.83831 C 60.599809,377.30854 60.461309,377.88733 60.430533,378.5747 C 60.604939,378.30796 60.783622,378.10021 60.966582,377.95145 C 61.149537,377.80269 61.363272,377.68813 61.607789,377.60776 C 61.852299,377.5274 62.119896,377.48722 62.41058,377.48722 C 62.875664,377.48722 63.289456,377.58981 63.651956,377.79499 C 64.014447,378.00019 64.296578,378.27804 64.49835,378.62856 C 64.70011,378.97909 64.800993,379.39203 64.800999,379.86738 C 64.800993,380.62315 64.573579,381.21648 64.118755,381.64737 C 63.616044,382.11929 62.964578,382.35526 62.164356,382.35526 C 61.586412,382.35526 61.099095,382.23557 60.702404,381.99618 C 60.305709,381.7568 59.990236,381.39088 59.755983,380.89844 C 59.521728,380.40599 59.404601,379.79727 59.404601,379.07228 C 59.404601,378.34387 59.528567,377.69754 59.776501,377.13327 C 60.024433,376.56901 60.367265,376.11589 60.804997,375.77391 C 61.242725,375.43194 61.722348,375.20795 62.243866,375.10193 C 62.765377,374.99592 63.23303,374.94291 63.646827,374.94291 L 64.04694,374.94291 z M 62.174615,378.03096 C 61.682166,378.03096 61.277778,378.21734 60.961452,378.59009 C 60.645121,378.96285 60.486957,379.41084 60.486959,379.93406 C 60.486957,380.3034 60.563902,380.64452 60.717793,380.95743 C 60.871681,381.27034 61.070027,381.50288 61.312833,381.65506 C 61.555634,381.80724 61.829216,381.88333 62.133578,381.88333 C 62.605503,381.88333 62.997921,381.70465 63.310834,381.34728 C 63.623738,380.98992 63.780193,380.51029 63.780198,379.90841 C 63.780193,379.33389 63.628013,378.87736 63.323658,378.5388 C 63.019295,378.20024 62.636281,378.03096 62.174615,378.03096 L 62.174615,378.03096 z"
               style="font-size:10.50553513px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.926544,1.0792795)" />
            <svg:path
               id="text9741"
               d="M 41.684272,371.12452 L 50.358867,371.12452 C 52.47844,371.12454 53.930746,371.22049 54.715791,371.41236 C 55.875877,371.6915 56.793926,372.19741 57.469942,372.93009 C 58.145923,373.6628 58.483923,374.54378 58.48394,375.57303 C 58.483923,376.31446 58.30511,376.97956 57.947502,377.56832 C 57.58986,378.1571 57.070868,378.66083 56.390524,379.0795 C 55.95438,379.34991 55.361246,379.58106 54.61112,379.77295 C 55.561865,379.94741 56.285837,380.15675 56.783039,380.40097 C 57.533163,380.76733 58.126298,381.29068 58.562444,381.97103 C 58.998554,382.6514 59.216618,383.41899 59.216636,384.27379 C 59.216618,385.10244 59.002915,385.87003 58.575527,386.57655 C 58.148104,387.28308 57.544067,387.86313 56.763414,388.3167 C 55.982728,388.77028 55.073401,389.0603 54.03543,389.18678 C 52.997432,389.31326 51.928918,389.3765 50.829886,389.3765 L 41.684272,389.3765 L 41.684272,388.27745 L 42.299213,388.27745 C 42.892346,388.27745 43.297945,388.18587 43.516012,388.00269 C 43.830021,387.74974 43.987027,387.33105 43.987031,386.74664 L 43.987031,374.06839 C 43.987027,373.47527 43.939053,373.06967 43.843108,372.85159 C 43.747157,372.63354 43.603234,372.47653 43.411341,372.38057 C 43.219442,372.28464 42.848733,372.23666 42.299213,372.23665 L 41.684272,372.23665 L 41.684272,371.12452 z M 46.433711,372.57683 L 46.433711,379.26268 L 49.770094,379.26268 C 51.331423,379.26269 52.399937,379.18854 52.975638,379.04025 C 53.874049,378.80475 54.560951,378.40351 55.036345,377.83654 C 55.51171,377.26958 55.7494,376.54125 55.749415,375.65153 C 55.7494,374.90141 55.533517,374.27556 55.101764,373.774 C 54.669983,373.27247 54.092113,372.93011 53.368154,372.74692 C 52.897123,372.63354 51.920196,372.57684 50.43737,372.57683 L 46.433711,372.57683 z M 46.433711,380.51873 L 46.433711,386.51113 C 46.433705,387.11299 46.459873,387.47062 46.512214,387.58401 C 46.581989,387.74102 46.72155,387.85005 46.930898,387.9111 C 47.079175,387.95472 47.497858,387.97652 48.186948,387.97652 L 49.966352,387.97652 C 51.7283,387.97652 52.986529,387.8784 53.741043,387.68214 C 54.495532,387.48588 55.138821,387.07156 55.670912,386.43917 C 56.202973,385.80679 56.469011,385.04574 56.469027,384.15604 C 56.469011,383.4059 56.253128,382.74081 55.821376,382.16075 C 55.389594,381.58071 54.792099,381.16202 54.028888,380.9047 C 53.265651,380.64739 51.911473,380.51874 49.966352,380.51873 L 46.433711,380.51873 z"
               style="font-size:26.7957325px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9125979,1.0957729)" />
          </svg:g>
        </svg:g>
        <svg:g
           transform="translate(876,-230.00004)"
           id="g9745">
          <svg:g
             id="g9747"
             transform="translate(149,-3.00002)">
            <svg:path
               transform="scale(0.9224905,1.084022)"
               style="font-size:34.84113312px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               d="M 60.22705,373.3026 L 60.563893,373.3026 L 60.625137,373.56289 L 65.085755,373.56289 L 65.085755,373.9967 C 64.551564,374.61255 64.067565,375.31431 63.633757,376.10197 C 63.19994,376.88964 62.863948,377.69177 62.62578,378.50836 C 62.452251,379.09698 62.31105,379.82171 62.202174,380.68253 L 61.15592,380.68253 C 61.257991,379.92378 61.425562,379.21437 61.658632,378.55429 C 61.891698,377.89422 62.22854,377.20437 62.669161,376.48475 C 63.109775,375.76513 63.644811,375.04637 64.274269,374.32844 L 61.094676,374.32844 C 60.863307,374.32845 60.705093,374.34036 60.620033,374.36417 C 60.53497,374.38799 60.468623,374.42542 60.42099,374.47645 C 60.373354,374.52749 60.332524,374.61766 60.298501,374.74694 L 60.272983,374.84391 L 60.099458,375.44615 L 59.665645,375.35428 L 60.22705,373.3026 z"
               id="text2855" />
            <svg:path
               id="text9749"
               d="M 55.06726,378.18807 L 55.994535,378.18807 L 57.630145,384.17672 L 56.432415,384.51157 C 55.788458,383.12926 55.299063,382.20198 54.964229,381.72975 C 54.414718,380.96562 53.729995,380.36031 52.910058,379.91383 C 52.090094,379.46738 51.186431,379.24415 50.199067,379.24413 C 49.039963,379.24415 47.973169,379.55539 46.99868,380.17785 C 46.024177,380.80034 45.234276,381.71259 44.628978,382.9146 C 44.023669,384.11663 43.721017,385.46891 43.721021,386.97142 C 43.721017,388.51689 44.027962,389.89492 44.641857,391.10553 C 45.255741,392.31614 46.086424,393.24771 47.133908,393.90023 C 48.181376,394.55276 49.310418,394.87902 50.521037,394.87902 C 51.662947,394.87902 52.830625,394.55276 54.024076,393.90023 C 55.217497,393.24771 56.252095,392.33331 57.127871,391.15704 L 58.158176,391.86538 C 57.625834,392.65528 57.050581,393.32498 56.432415,393.87447 C 55.453609,394.75882 54.436183,395.41134 53.380135,395.83205 C 52.324059,396.25276 51.164966,396.46311 49.902854,396.46311 C 47.112436,396.46311 44.845766,395.42852 43.102838,393.35932 C 41.660408,391.64215 40.939195,389.59871 40.939196,387.229 C 40.939195,385.48608 41.338438,383.90842 42.136926,382.49604 C 42.93541,381.08367 44.036548,379.96107 45.440343,379.12822 C 46.844127,378.29541 48.357387,377.879 49.980127,377.87898 C 50.847289,377.879 51.652215,377.99705 52.394905,378.23315 C 53.13757,378.46928 53.89098,378.8492 54.655138,379.37292 L 55.06726,378.18807 z"
               style="font-size:26.37582016px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9292071,1.0761864)" />
          </svg:g>
          <svg:g
             transform="translate(179,-3.0000449)"
             id="g9757">
            <svg:path
               style="font-size:34.84113312px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               d="M 55.558883,404.66825 L 55.869617,404.66825 L 55.926114,404.95041 L 60.040992,404.95041 L 60.040992,405.42067 C 59.548206,406.08827 59.101721,406.84899 58.701537,407.70283 C 58.301345,408.55668 57.991396,409.42621 57.771688,410.31141 C 57.611609,410.94949 57.481353,411.73511 57.380916,412.66826 L 56.415756,412.66826 C 56.509916,411.84576 56.664498,411.07674 56.879503,410.3612 C 57.094504,409.64567 57.405238,408.89786 57.811707,408.11777 C 58.218169,407.33769 58.711735,406.55854 59.292404,405.78028 L 56.359259,405.78028 C 56.145824,405.78029 55.999873,405.79321 55.921406,405.81902 C 55.842936,405.84484 55.781731,405.88541 55.73779,405.94073 C 55.693847,405.99606 55.656181,406.0938 55.624795,406.23395 L 55.601255,406.33906 L 55.44118,406.99191 L 55.040992,406.89232 L 55.558883,404.66825 z"
               id="path2860" />
            <svg:path
               id="text9763"
               d="M 43.136549,359.70468 L 43.136549,358.63 L 49.432932,358.63 C 52.315602,358.63002 54.325891,358.81124 55.463804,359.17366 C 57.141139,359.7047 58.502404,360.69509 59.547603,362.14485 C 60.702342,363.74635 61.279721,365.58806 61.27974,367.66999 C 61.279721,369.08605 60.995246,370.40306 60.426315,371.62103 C 59.857347,372.83901 59.109283,373.81887 58.182122,374.56061 C 57.415077,375.18434 56.492115,375.62475 55.413231,375.88183 C 54.33432,376.13892 53.246994,376.26746 52.151249,376.26746 L 43.136549,376.26746 L 43.136549,375.20542 L 43.869862,375.20542 L 44.110086,375.21806 C 44.421952,375.21806 44.676926,375.15484 44.875008,375.02841 C 45.073084,374.90198 45.214267,374.7334 45.29856,374.52268 C 45.357558,374.36253 45.38706,373.99166 45.387063,373.41006 L 45.387063,361.61383 C 45.38706,360.94796 45.344915,360.51809 45.26063,360.32421 C 45.142622,360.06293 44.952972,359.8817 44.691679,359.78054 C 44.539957,359.72999 44.02158,359.7047 43.136549,359.70468 L 43.136549,359.70468 z M 47.738724,374.90198 L 51.557013,374.90198 C 51.936303,374.90198 52.566362,374.83455 53.447192,374.69968 C 54.327999,374.56482 54.958058,374.40046 55.337371,374.20659 C 55.893664,373.92001 56.433112,373.45853 56.955718,372.82215 C 57.478294,372.18577 57.880773,371.42295 58.163157,370.5337 C 58.445509,369.64446 58.586692,368.62667 58.586709,367.48034 C 58.586692,366.34244 58.445509,365.35627 58.163157,364.52179 C 57.880773,363.68735 57.455115,362.93718 56.88618,362.27128 C 56.317215,361.60541 55.685049,361.09757 54.989679,360.74776 C 54.294283,360.39797 53.516719,360.17672 52.656983,360.08398 C 52.193384,360.04185 51.417926,360.02078 50.330609,360.02077 L 47.738724,360.02077 L 47.738724,374.90198 z"
               style="font-size:25.8935585px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.8818737,1.1339493)" />
          </svg:g>
          <svg:g
             transform="translate(209,-3.0000041)"
             id="g9767">
            <svg:path
               style="font-size:34.84113312px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               d="M 55.558883,404.66822 L 55.869617,404.66822 L 55.926114,404.95038 L 60.040992,404.95038 L 60.040992,405.42064 C 59.548206,406.08823 59.101721,406.84896 58.701538,407.7028 C 58.301346,408.55665 57.991396,409.42618 57.771689,410.31138 C 57.61161,410.94945 57.481353,411.73508 57.380916,412.66823 L 56.415757,412.66823 C 56.509916,411.84572 56.664499,411.07671 56.879504,410.36117 C 57.094505,409.64564 57.405238,408.89782 57.811707,408.11774 C 58.218169,407.33766 58.711735,406.5585 59.292404,405.78025 L 56.35926,405.78025 C 56.145824,405.78026 55.999873,405.79317 55.921406,405.81898 C 55.842936,405.84481 55.781732,405.88538 55.737791,405.9407 C 55.693847,405.99603 55.656182,406.09377 55.624796,406.23392 L 55.601256,406.33903 L 55.44118,406.99187 L 55.040992,406.89229 L 55.558883,404.66822 z"
               id="path2862" />
            <svg:path
               id="text9773"
               d="M 40.333911,383.54972 L 55.667727,383.54972 L 57.439094,388.51225 L 56.249168,388.95848 C 55.951669,388.14718 55.627145,387.5094 55.275592,387.04513 C 54.924007,386.58089 54.502576,386.18651 54.011296,385.86196 C 53.519986,385.53746 53.024185,385.32111 52.523889,385.21291 C 52.023566,385.10476 51.119855,385.05067 49.812751,385.05065 L 45.242355,385.05065 L 45.242355,391.96033 L 47.514031,391.96033 C 48.352378,391.96034 48.985653,391.81837 49.413856,391.53439 C 49.842038,391.25045 50.137265,390.89662 50.299539,390.47293 C 50.425732,390.15743 50.488834,389.60754 50.488845,388.82326 L 50.488845,388.51225 L 51.81399,388.51225 L 51.81399,396.78765 L 50.488845,396.78765 L 50.488845,396.07099 C 50.488834,395.43997 50.441507,394.98474 50.346866,394.70528 C 50.252201,394.42583 50.078671,394.16216 49.826273,393.91425 C 49.573854,393.66635 49.301163,393.49733 49.008199,393.40718 C 48.715215,393.31704 48.21716,393.27197 47.514031,393.27196 L 45.242355,393.27196 L 45.242355,399.41089 C 45.242348,400.05093 45.271646,400.44081 45.330247,400.58054 C 45.388835,400.72026 45.485742,400.82168 45.620967,400.88478 C 45.756179,400.94788 46.125777,400.97943 46.729762,400.97943 L 49.555835,400.97943 C 51.133376,400.97943 52.36612,400.79013 53.254071,400.41151 C 54.141992,400.0329 54.924007,399.45146 55.600117,398.66719 C 56.131961,398.0542 56.641285,397.1888 57.12809,396.07099 L 58.358582,396.50369 L 56.249168,402.41275 L 40.333911,402.41275 L 40.333911,401.27691 L 40.874786,401.27691 C 41.505805,401.27691 41.897939,401.23635 42.05119,401.15522 C 42.285566,401.03803 42.465858,400.84872 42.592065,400.5873 C 42.682208,400.40701 42.72728,400.01487 42.727284,399.41089 L 42.727284,386.42988 C 42.72728,385.84395 42.668686,385.45182 42.5515,385.25348 C 42.434307,385.05518 42.231479,384.90193 41.943015,384.79374 C 41.789764,384.73065 41.253397,384.6991 40.333911,384.69908 L 40.333911,383.54972 z"
               style="font-size:27.69281769px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9431515,1.060275)" />
          </svg:g>
          <svg:g
             id="g9779"
             transform="translate(239,-3.0000041)">
            <svg:path
               style="font-size:34.84113312px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               d="M 55.558882,404.66822 L 55.869617,404.66822 L 55.926114,404.95038 L 60.040992,404.95038 L 60.040992,405.42064 C 59.548206,406.08823 59.101721,406.84896 58.701537,407.7028 C 58.301345,408.55665 57.991396,409.42618 57.771688,410.31138 C 57.611609,410.94945 57.481353,411.73508 57.380916,412.66823 L 56.415756,412.66823 C 56.509916,411.84572 56.664498,411.07671 56.879503,410.36117 C 57.094504,409.64564 57.405238,408.89782 57.811707,408.11774 C 58.218169,407.33766 58.711735,406.5585 59.292404,405.78025 L 56.359259,405.78025 C 56.145823,405.78026 55.999873,405.79317 55.921406,405.81898 C 55.842936,405.84481 55.781731,405.88538 55.73779,405.9407 C 55.693847,405.99603 55.656181,406.09377 55.624795,406.23392 L 55.601255,406.33903 L 55.44118,406.99187 L 55.040992,406.89229 L 55.558882,404.66822 z"
               id="path2864" />
            <svg:path
               id="text9785"
               d="M 39.380858,392.83193 L 54.157874,392.83193 L 55.944411,397.90071 L 54.739537,398.35773 C 54.231719,397.17595 53.733151,396.32654 53.243832,395.80949 C 52.754481,395.29247 52.186667,394.92317 51.540389,394.70156 C 50.894084,394.47999 49.878482,394.3692 48.493582,394.36918 L 44.324995,394.36918 L 44.324995,401.79231 L 46.222326,401.79231 C 47.219453,401.79232 47.93499,401.69538 48.36894,401.50148 C 48.802868,401.30761 49.135247,401.00062 49.366077,400.58052 C 49.596884,400.16044 49.712293,399.56262 49.712305,398.78705 L 49.712305,398.27464 L 51.069519,398.27464 L 51.069519,406.7503 L 49.712305,406.7503 L 49.712305,406.00245 C 49.712293,405.27307 49.6015,404.70525 49.379926,404.29901 C 49.158329,403.89277 48.855956,403.59963 48.472808,403.41959 C 48.089639,403.23956 47.408724,403.14954 46.430062,403.14953 L 44.324995,403.14953 L 44.324995,409.16005 C 44.324989,409.71402 44.382694,410.09948 44.49811,410.31645 C 44.613512,410.53342 44.784318,410.69961 45.010527,410.81502 C 45.236722,410.93043 45.640655,410.98813 46.222326,410.98813 L 46.734743,410.98813 L 46.734743,412.15146 L 39.380858,412.15146 L 39.380858,410.98813 L 39.851728,410.98813 C 40.442622,410.98813 40.85117,410.93274 41.077376,410.82194 C 41.303575,410.71115 41.469764,410.53804 41.575944,410.3026 C 41.682117,410.06717 41.735205,409.64015 41.735209,409.02156 L 41.735209,395.87874 C 41.735205,395.30632 41.684425,394.9047 41.582869,394.67386 C 41.481305,394.44306 41.326657,394.27457 41.118923,394.16837 C 40.911183,394.06221 40.548798,394.00912 40.031767,394.00911 L 39.380858,394.00911 L 39.380858,392.83193 z"
               style="font-size:28.36300659px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9659766,1.0352218)" />
          </svg:g>
          <svg:g
             transform="translate(269,-3.0000041)"
             id="g9789">
            <svg:path
               style="font-size:34.84113312px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               d="M 55.558883,404.66822 L 55.869618,404.66822 L 55.926115,404.95038 L 60.040992,404.95038 L 60.040992,405.42064 C 59.548206,406.08823 59.101722,406.84896 58.701538,407.7028 C 58.301346,408.55665 57.991397,409.42618 57.771689,410.31138 C 57.61161,410.94945 57.481353,411.73508 57.380916,412.66823 L 56.415757,412.66823 C 56.509917,411.84572 56.664499,411.07671 56.879504,410.36117 C 57.094505,409.64564 57.405239,408.89782 57.811707,408.11774 C 58.21817,407.33766 58.711735,406.5585 59.292404,405.78025 L 56.35926,405.78025 C 56.145824,405.78026 55.999873,405.79317 55.921406,405.81898 C 55.842937,405.84481 55.781732,405.88538 55.737791,405.9407 C 55.693847,405.99603 55.656182,406.09377 55.624796,406.23392 L 55.601256,406.33903 L 55.441181,406.99187 L 55.040992,406.89229 L 55.558883,404.66822 z"
               id="path2866" />
            <svg:path
               id="text9795"
               d="M 54.600565,383.5977 L 55.554167,383.5977 L 57.043354,389.21481 L 55.86768,389.61976 C 55.083882,387.8432 54.182532,386.57609 53.16363,385.81842 C 52.144699,385.06078 50.990798,384.68195 49.701923,384.68193 C 47.846962,384.68195 46.292461,385.45267 45.038416,386.99409 C 43.784359,388.53554 43.157333,390.37308 43.157337,392.5067 C 43.157333,394.03944 43.477377,395.44589 44.117471,396.72606 C 44.757555,398.00624 45.613183,398.98162 46.68436,399.65218 C 47.755521,400.32275 48.892004,400.65804 50.093814,400.65804 C 50.868877,400.65804 51.606938,400.51652 52.308001,400.23349 C 53.009037,399.95046 53.577279,399.613 54.012728,399.2211 C 54.334934,398.92501 54.552652,398.59843 54.66588,398.24137 C 54.779077,397.88432 54.83133,397.38793 54.822637,396.75219 L 54.822637,395.89003 C 54.822621,395.20205 54.742066,394.71436 54.58097,394.42696 C 54.419844,394.13959 54.191241,393.93493 53.895161,393.813 C 53.66872,393.71721 53.059112,393.66932 52.066334,393.66931 L 52.066334,392.57201 L 59.460018,392.57201 L 59.460018,393.66931 L 58.859118,393.66931 C 58.345285,393.66932 57.994761,393.71939 57.807543,393.81953 C 57.620287,393.91969 57.483125,394.07427 57.396057,394.28327 C 57.308951,394.49229 57.265408,394.9016 57.265426,395.5112 L 57.265426,401.82065 L 55.972185,401.82065 L 55.279843,400.22696 C 54.548297,400.75819 53.76887,401.17185 52.941558,401.46795 C 51.704911,401.91209 50.48134,402.13416 49.270843,402.13416 C 46.823691,402.13416 44.727074,401.23499 42.980986,399.43664 C 41.23489,397.6383 40.361844,395.39799 40.361846,392.71571 C 40.361844,390.05086 41.245776,387.81272 43.013644,386.0013 C 44.781504,384.18991 46.897715,383.28421 49.362284,383.28419 C 49.98059,383.28421 50.590199,383.34517 51.19111,383.46707 C 51.791997,383.58901 52.340645,383.76318 52.837054,383.98959 C 53.211514,384.15507 53.646948,384.42504 54.143358,384.7995 L 54.600565,383.5977 z"
               style="font-size:26.75311661px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9424986,1.0610095)" />
          </svg:g>
          <svg:g
             id="g9799"
             transform="translate(299,-3.0000041)">
            <svg:path
               style="font-size:34.84113312px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               d="M 55.558882,404.66822 L 55.869617,404.66822 L 55.926114,404.95038 L 60.040991,404.95038 L 60.040991,405.42064 C 59.548205,406.08823 59.101721,406.84896 58.701537,407.7028 C 58.301345,408.55665 57.991396,409.42618 57.771688,410.31138 C 57.611609,410.94946 57.481352,411.73508 57.380915,412.66823 L 56.415756,412.66823 C 56.509916,411.84573 56.664498,411.07671 56.879503,410.36117 C 57.094504,409.64564 57.405238,408.89782 57.811706,408.11774 C 58.218169,407.33766 58.711734,406.55851 59.292403,405.78025 L 56.359259,405.78025 C 56.145823,405.78026 55.999872,405.79317 55.921405,405.81899 C 55.842936,405.84481 55.781731,405.88538 55.73779,405.9407 C 55.693846,405.99603 55.656181,406.09377 55.624795,406.23392 L 55.601255,406.33903 L 55.44118,406.99188 L 55.040992,406.89229 L 55.558882,404.66822 z"
               id="path2868" />
            <svg:path
               id="text9805"
               d="M 48.322339,387.18937 L 50.574624,387.18937 L 56.485167,403.10552 C 56.776355,403.90633 57.069834,404.43641 57.365606,404.69577 C 57.661343,404.95512 58.059474,405.0848 58.56,405.0848 L 58.860304,405.0848 L 58.860304,406.23141 L 52.076148,406.23141 L 52.076148,405.0848 C 52.294539,405.0666 52.508392,405.0575 52.717708,405.0575 C 53.154502,405.0575 53.441156,405.012 53.577671,404.92099 C 53.841562,404.74809 53.973514,404.52969 53.973528,404.26578 C 53.973514,404.07468 53.846112,403.60603 53.591322,402.85981 L 52.335502,399.17425 L 45.428493,399.17425 L 43.95427,403.29662 C 43.799564,403.72433 43.722213,404.02008 43.722217,404.18388 C 43.722213,404.36589 43.769989,404.52514 43.865544,404.66164 C 43.961091,404.79814 44.090768,404.90052 44.254575,404.96877 C 44.418373,405.03702 44.682277,405.07115 45.046287,405.07115 C 45.291986,405.07115 45.492189,405.0757 45.646897,405.0848 L 45.646897,406.23141 L 39.954758,406.23141 L 39.954758,405.0848 C 40.482565,405.0848 40.862496,405.03247 41.09455,404.92782 C 41.326603,404.82317 41.529081,404.65482 41.701985,404.42276 C 41.874886,404.19071 42.102389,403.70158 42.384496,402.95536 L 48.322339,387.18937 z M 45.933551,397.83653 L 51.816794,397.83653 L 48.868347,389.6464 L 45.933551,397.83653 z"
               style="font-size:27.95563889px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9521015,1.0503082)" />
          </svg:g>
          <svg:g
             transform="translate(329.00001,-3.0000041)"
             id="g9809">
            <svg:path
               style="font-size:34.84113312px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               d="M 55.558873,404.66821 L 55.869608,404.66821 L 55.926105,404.95037 L 60.040982,404.95037 L 60.040982,405.42063 C 59.548196,406.08822 59.101712,406.84895 58.701528,407.70279 C 58.301336,408.55664 57.991387,409.42617 57.771679,410.31137 C 57.6116,410.94945 57.481343,411.73507 57.380906,412.66822 L 56.415747,412.66822 C 56.509906,411.84571 56.664489,411.0767 56.879494,410.36116 C 57.094495,409.64563 57.405229,408.89781 57.811697,408.11773 C 58.21816,407.33765 58.711725,406.55849 59.292394,405.78024 L 56.35925,405.78024 C 56.145814,405.78025 55.999863,405.79316 55.921396,405.81898 C 55.842926,405.8448 55.781722,405.88537 55.737781,405.94069 C 55.693837,405.99602 55.656172,406.09376 55.624786,406.23391 L 55.601246,406.33902 L 55.441171,406.99187 L 55.040982,406.89228 L 55.558873,404.66821 z"
               id="path2870" />
            <svg:path
               id="text9815"
               d="M 41.684272,371.12452 L 50.358867,371.12452 C 52.47844,371.12454 53.930746,371.22049 54.715791,371.41236 C 55.875877,371.6915 56.793926,372.19741 57.469942,372.93009 C 58.145923,373.6628 58.483923,374.54378 58.48394,375.57303 C 58.483923,376.31446 58.30511,376.97956 57.947502,377.56832 C 57.58986,378.1571 57.070868,378.66083 56.390524,379.0795 C 55.95438,379.34991 55.361246,379.58106 54.61112,379.77295 C 55.561865,379.94741 56.285837,380.15675 56.783039,380.40097 C 57.533163,380.76733 58.126298,381.29068 58.562444,381.97103 C 58.998554,382.6514 59.216618,383.41899 59.216636,384.27379 C 59.216618,385.10244 59.002915,385.87003 58.575527,386.57655 C 58.148104,387.28308 57.544067,387.86313 56.763414,388.3167 C 55.982728,388.77028 55.073401,389.0603 54.03543,389.18678 C 52.997432,389.31326 51.928918,389.3765 50.829886,389.3765 L 41.684272,389.3765 L 41.684272,388.27745 L 42.299213,388.27745 C 42.892346,388.27745 43.297945,388.18587 43.516012,388.00269 C 43.830021,387.74974 43.987027,387.33105 43.987031,386.74664 L 43.987031,374.06839 C 43.987027,373.47527 43.939053,373.06967 43.843108,372.85159 C 43.747157,372.63354 43.603234,372.47653 43.411341,372.38057 C 43.219442,372.28464 42.848733,372.23666 42.299213,372.23665 L 41.684272,372.23665 L 41.684272,371.12452 z M 46.433711,372.57683 L 46.433711,379.26268 L 49.770094,379.26268 C 51.331423,379.26269 52.399937,379.18854 52.975638,379.04025 C 53.874049,378.80475 54.560951,378.40351 55.036345,377.83654 C 55.51171,377.26958 55.7494,376.54125 55.749415,375.65153 C 55.7494,374.90141 55.533517,374.27556 55.101764,373.774 C 54.669983,373.27247 54.092113,372.93011 53.368154,372.74692 C 52.897123,372.63354 51.920196,372.57684 50.43737,372.57683 L 46.433711,372.57683 z M 46.433711,380.51873 L 46.433711,386.51113 C 46.433705,387.11299 46.459873,387.47062 46.512214,387.58401 C 46.581989,387.74102 46.72155,387.85005 46.930898,387.9111 C 47.079175,387.95472 47.497858,387.97652 48.186948,387.97652 L 49.966352,387.97652 C 51.7283,387.97652 52.986529,387.8784 53.741043,387.68214 C 54.495532,387.48588 55.138821,387.07156 55.670912,386.43917 C 56.202973,385.80679 56.469011,385.04574 56.469027,384.15604 C 56.469011,383.4059 56.253128,382.74081 55.821376,382.16075 C 55.389594,381.58071 54.792099,381.16202 54.028888,380.9047 C 53.265651,380.64739 51.911473,380.51874 49.966352,380.51873 L 46.433711,380.51873 z"
               style="font-size:26.7957325px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
               transform="scale(0.9125979,1.0957729)" />
          </svg:g>
        </svg:g>
      </svg:g>
      <svg:g
         style="fill:#ff00ff;stroke:none"
         id="g13683"
         transform="translate(502.46805,436.88916)">
        <svg:path
           style="fill:#000000;stroke:none"
           d="M 119.57294,149.76995 C 118.59351,149.61623 117.59786,149.61645 116.60868,149.61759 C 116.5877,148.89382 116.70326,148.12421 116.41627,147.44491 C 116.22522,146.99268 115.86137,146.63587 115.53786,146.26907 C 115.02164,146.73074 114.59762,147.34 114.55699,148.04885 C 114.51702,148.57056 114.5463,149.09481 114.5372,149.61759 C 113.54814,149.63255 112.54871,149.5847 111.57294,149.779 C 111.7611,148.80874 111.71834,147.81595 111.73539,146.8334 C 112.48854,146.81304 113.29458,146.92168 113.99406,146.59402 C 114.40682,146.40064 114.75631,146.0976 115.1064,145.80935 C 114.65067,145.29321 114.05888,144.83848 113.34314,144.79568 C 112.80858,144.75347 112.27116,144.78424 111.73539,144.77496 C 111.70182,143.79965 111.81567,142.81174 111.61322,141.84873 C 112.57783,142.02296 113.5618,141.97825 114.5372,141.99079 C 114.55168,142.68544 114.47404,143.40542 114.67016,144.07433 C 114.8198,144.58484 115.22731,144.97758 115.56774,145.38062 C 116.04832,144.96182 116.4817,144.43641 116.56109,143.77916 C 116.63294,143.18424 116.57591,142.58669 116.60868,141.99079 C 117.59111,141.96497 118.60381,142.08984 119.55474,141.779 C 119.41282,142.76982 119.39946,143.7757 119.4105,144.77496 C 118.7093,144.79476 117.97366,144.71697 117.30701,144.9531 C 116.8006,145.13247 116.40246,145.52489 115.99919,145.87133 C 116.43642,146.31856 116.96307,146.73103 117.61467,146.7919 C 118.21206,146.84772 118.812,146.82245 119.4105,146.8334 C 119.42531,147.81311 119.37685,148.80367 119.57294,149.76995 z"
           id="path13709" />
        <svg:path
           d="M 99.18211,113.13209 L 99.18211,107.18787 L 101.91996,106.49033 L 101.91996,112.40423 L 99.18211,113.13209 z M 104.57294,111.69405 L 102.69067,112.19194 L 102.69067,106.27803 L 104.57294,105.79279 L 104.57294,103.33625 L 102.69067,103.82149 L 102.69067,97.77899 L 101.91996,97.77899 L 101.91996,104.00472 L 99.18211,104.73132 L 99.18211,98.855631 L 98.45521,98.855631 L 98.45521,104.95498 L 96.57294,105.44149 L 96.57294,107.90308 L 98.45521,107.41785 L 98.45521,113.32038 L 96.57294,113.80436 L 96.57294,116.25585 L 98.45521,115.77061 L 98.45521,121.77899 L 99.18211,121.77899 L 99.18211,115.55579 L 101.91996,114.86077 L 101.91996,120.70614 L 102.69067,120.70614 L 102.69067,114.6409 L 104.57294,114.15439 L 104.57294,111.69405 z"
           id="path13715"
           style="fill:#ffffff;stroke:none" />
        <svg:path
           style="fill:#ffffff;stroke:none"
           d="M 71.691605,116.08146 C 71.691605,116.79008 71.34881,117.46881 70.408361,118.37972 C 69.412035,119.34477 68.573339,119.89713 67.46819,120.5466 L 67.46819,116.31684 C 67.719431,115.82511 68.089935,115.42705 68.581289,115.12141 C 69.071052,114.817 69.567175,114.66419 70.069661,114.66419 C 70.899714,114.66419 71.426051,115.02899 71.653442,115.7561 C 71.678883,115.81525 71.691605,115.92371 71.691605,116.08146 z M 71.572345,113.1237 C 70.886994,113.1237 70.190511,113.27035 69.481309,113.56489 C 68.772106,113.8582 68.101067,114.25134 67.46819,114.7406 L 67.46819,105.779 L 66.57294,105.779 L 66.57294,121.12829 C 66.57294,121.5621 66.725595,121.779 67.030901,121.779 C 67.207407,121.779 67.426708,121.66438 67.754415,121.51281 C 68.682009,121.08373 69.260175,120.79701 69.888385,120.49453 C 70.60493,120.14951 71.41174,119.7466 72.478724,118.95788 C 73.214959,118.38481 73.747658,117.80682 74.078407,117.22512 C 74.407566,116.6422 74.57294,116.06544 74.57294,115.49236 C 74.57294,114.64447 74.281945,114.04183 73.699954,113.68566 C 73.041635,113.31103 72.330841,113.1237 71.572345,113.1237 z"
           id="path13721" />
        <svg:g
           style="fill:#000000;stroke:none"
           transform="matrix(1.30571,0,0,1.2289035,8.9520516,90.31813)"
           id="g13723">
          <svg:path
             style="fill:#000000;stroke:none"
             d="M 40.391475,47.003951 C 40.391475,47.580587 40.12894,48.132884 39.408682,48.874137 C 38.645629,49.659425 38.003298,50.10889 37.156902,50.637389 L 37.156902,47.195494 C 37.34932,46.795359 37.633077,46.47144 38.009387,46.222735 C 38.384481,45.975032 38.764447,45.850679 39.149282,45.850679 C 39.784993,45.850679 40.188096,46.147521 40.362247,46.7392 C 40.381733,46.787337 40.391475,46.875587 40.391475,47.003951 z M 40.300138,44.597123 C 39.77525,44.597123 39.241839,44.716462 38.698684,44.956142 C 38.155528,45.194819 37.641601,45.514726 37.156902,45.912856 L 37.156902,38.620498 L 36.47126,38.620498 L 36.47126,51.110732 C 36.47126,51.463733 36.588172,51.640234 36.821997,51.640234 C 36.957177,51.640234 37.125132,51.546969 37.376113,51.423619 C 38.086525,51.074479 38.529325,50.841157 39.010449,50.595019 C 39.559228,50.314262 40.177136,49.986412 40.994305,49.344591 C 41.558163,48.878268 41.966138,48.407934 42.219448,47.934591 C 42.47154,47.460246 42.598195,46.990914 42.598195,46.524591 C 42.598195,45.834634 42.375331,45.344243 41.929603,45.05442 C 41.425419,44.749555 40.881045,44.597123 40.300138,44.597123 z"
             id="path13729" />
          <svg:path
             id="path13735"
             d="M 32.732806,47.003946 C 32.732806,47.580582 32.470271,48.132879 31.750013,48.874132 C 30.986959,49.65942 30.344629,50.108885 29.498233,50.637384 L 29.498233,47.195489 C 29.690652,46.795354 29.974407,46.471435 30.350718,46.22273 C 30.725812,45.975027 31.105777,45.850674 31.490613,45.850674 C 32.126324,45.850674 32.529428,46.147516 32.703579,46.739195 C 32.723064,46.787332 32.732806,46.875582 32.732806,47.003946 z M 32.641469,44.597118 C 32.116581,44.597118 31.583169,44.716457 31.040014,44.956137 C 30.496859,45.194814 29.982932,45.514721 29.498233,45.912851 L 29.498233,38.620493 L 28.812591,38.620493 L 28.812591,51.110727 C 28.812591,51.463728 28.929503,51.640229 29.163328,51.640229 C 29.298508,51.640229 29.466463,51.546964 29.717444,51.423614 C 30.427856,51.074474 30.870655,50.841152 31.35178,50.595014 C 31.900558,50.314257 32.518468,49.986407 33.335636,49.344586 C 33.899494,48.878263 34.307469,48.407929 34.560779,47.934586 C 34.812871,47.460241 34.939526,46.990909 34.939526,46.524586 C 34.939526,45.834629 34.716662,45.344238 34.270934,45.054415 C 33.76675,44.74955 33.222376,44.597118 32.641469,44.597118 z"
             style="fill:#000000;stroke:none" />
        </svg:g>
        <svg:g
           style="fill:#000000"
           transform="matrix(1,0,0,0.9599998,0,-9.0488122)"
           id="g13783">
          <svg:path
             id="rect13693"
             d="M 81.57294,148.77901 L 81.57294,167.74126 L 88.430083,166.5463 L 88.430083,173.779 L 89.57294,173.779 L 89.57294,154.87963 L 82.715797,156.0746 L 82.715797,148.77901 L 81.57294,148.77901 z M 88.430083,158.43309 L 88.430083,162.67837 L 82.715797,163.62177 L 82.715797,159.37648 L 88.430083,158.43309 z"
             style="fill:#000000;fill-opacity:1;stroke:none" />
          <svg:path
             style="font-size:25.07051849px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:center;line-height:125%;text-anchor:middle;fill:#000000;fill-opacity:1;stroke-width:0.2;stroke-miterlimit:4;stroke-opacity:1;font-family:Antique Olive"
             d="M 79.662804,173.77899 C 78.518406,172.14874 77.550665,170.24101 76.759578,168.05579 C 75.968486,165.87057 75.572941,163.6073 75.572942,161.26599 C 75.572941,159.20218 75.868639,157.22507 76.460039,155.33468 C 77.15128,153.14079 78.218867,150.95557 79.662804,148.779 L 81.148979,148.779 C 80.219633,150.5827 79.605195,151.87041 79.305661,152.64216 C 78.837146,153.83885 78.468483,155.08755 78.19967,156.38826 C 77.869405,158.00985 77.704275,159.64009 77.704278,161.27899 C 77.704275,165.45 78.852507,169.61666 81.148979,173.77899 L 79.662804,173.77899 z"
             id="path13781" />
          <svg:path
             style="font-size:25.07051849px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:center;line-height:125%;text-anchor:middle;fill:#000000;fill-opacity:1;stroke-width:0.2;stroke-miterlimit:4;stroke-opacity:1;font-family:Antique Olive"
             d="M 91.48308,173.77899 L 89.996904,173.77899 C 92.293368,169.61666 93.4416,165.45 93.441605,161.27899 C 93.4416,159.64876 93.27647,158.03152 92.946213,156.42728 C 92.685073,155.12656 92.32025,153.87786 91.851743,152.68119 C 91.552201,151.90076 90.933922,150.60004 89.996904,148.779 L 91.48308,148.779 C 92.927008,150.95557 93.994595,153.14079 94.685846,155.33468 C 95.277239,157.22507 95.572931,159.20218 95.57294,161.26599 C 95.572931,163.6073 95.175466,165.87057 94.380546,168.05579 C 93.585609,170.24101 92.619788,172.14874 91.48308,173.77899 L 91.48308,173.77899 z"
             id="text13737" />
        </svg:g>
      </svg:g>
      <svg:g
         transform="translate(119.51276,-84.424107)"
         id="g13816">
        <svg:g
           id="g13796"
           transform="translate(269.48724,279.42406)"
           style="fill:#ff0000">
          <svg:path
             id="text13798"
             d="M 55.06726,378.18807 L 55.994535,378.18807 L 57.630145,384.17672 L 56.432415,384.51157 C 55.788458,383.12926 55.299063,382.20198 54.964229,381.72975 C 54.414718,380.96562 53.729995,380.36031 52.910058,379.91383 C 52.090094,379.46738 51.186431,379.24415 50.199067,379.24413 C 49.039963,379.24415 47.973169,379.55539 46.99868,380.17785 C 46.024177,380.80034 45.234276,381.71259 44.628978,382.9146 C 44.023669,384.11663 43.721017,385.46891 43.721021,386.97142 C 43.721017,388.51689 44.027962,389.89492 44.641857,391.10553 C 45.255741,392.31614 46.086424,393.24771 47.133908,393.90023 C 48.181376,394.55276 49.310418,394.87902 50.521037,394.87902 C 51.662947,394.87902 52.830625,394.55276 54.024076,393.90023 C 55.217497,393.24771 56.252095,392.33331 57.127871,391.15704 L 58.158176,391.86538 C 57.625834,392.65528 57.050581,393.32498 56.432415,393.87447 C 55.453609,394.75882 54.436183,395.41134 53.380135,395.83205 C 52.324059,396.25276 51.164966,396.46311 49.902854,396.46311 C 47.112436,396.46311 44.845766,395.42852 43.102838,393.35932 C 41.660408,391.64215 40.939195,389.59871 40.939196,387.229 C 40.939195,385.48608 41.338438,383.90842 42.136926,382.49604 C 42.93541,381.08367 44.036548,379.96107 45.440343,379.12822 C 46.844127,378.29541 48.357387,377.879 49.980127,377.87898 C 50.847289,377.879 51.652215,377.99705 52.394905,378.23315 C 53.13757,378.46928 53.89098,378.8492 54.655138,379.37292 L 55.06726,378.18807 z"
             style="font-size:26.37582016px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#ff0000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
             transform="scale(0.9292071,1.0761864)" />
          <svg:path
             id="text13802"
             d="M 64.858451,362.2528 L 65.726042,362.2528 L 65.726042,366.41621 L 67.071065,366.41621 L 67.071065,367.05792 L 65.726042,367.05792 L 65.726042,369.41428 L 64.858451,369.41428 L 64.858451,367.05792 L 61.485625,367.05792 L 61.485625,366.15439 L 64.858451,362.2528 z M 64.858451,366.41621 L 64.858451,363.04852 L 61.95279,366.41621 L 64.858451,366.41621 z"
             style="font-size:10.5137682px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#ff0000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
             transform="scale(0.8951847,1.1170879)" />
        </svg:g>
        <svg:g
           id="g13806"
           transform="translate(419.48724,279.42407)"
           style="fill:#ff0000">
          <svg:path
             id="text13808"
             d="M 64.858451,362.2528 L 65.726042,362.2528 L 65.726042,366.41621 L 67.071065,366.41621 L 67.071065,367.05792 L 65.726042,367.05792 L 65.726042,369.41428 L 64.858451,369.41428 L 64.858451,367.05792 L 61.485625,367.05792 L 61.485625,366.15439 L 64.858451,362.2528 z M 64.858451,366.41621 L 64.858451,363.04852 L 61.95279,366.41621 L 64.858451,366.41621 z"
             style="font-size:10.5137682px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#ff0000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
             transform="scale(0.8951847,1.1170879)" />
          <svg:path
             id="text13812"
             d="M 48.322339,387.18937 L 50.574624,387.18937 L 56.485167,403.10552 C 56.776355,403.90633 57.069834,404.43641 57.365606,404.69577 C 57.661343,404.95512 58.059474,405.0848 58.56,405.0848 L 58.860304,405.0848 L 58.860304,406.23141 L 52.076148,406.23141 L 52.076148,405.0848 C 52.294539,405.0666 52.508392,405.0575 52.717708,405.0575 C 53.154502,405.0575 53.441156,405.012 53.577671,404.92099 C 53.841562,404.74809 53.973514,404.52969 53.973528,404.26578 C 53.973514,404.07468 53.846112,403.60603 53.591322,402.85981 L 52.335502,399.17425 L 45.428493,399.17425 L 43.95427,403.29662 C 43.799564,403.72433 43.722213,404.02008 43.722217,404.18388 C 43.722213,404.36589 43.769989,404.52514 43.865544,404.66164 C 43.961091,404.79814 44.090768,404.90052 44.254575,404.96877 C 44.418373,405.03702 44.682277,405.07115 45.046287,405.07115 C 45.291986,405.07115 45.492189,405.0757 45.646897,405.0848 L 45.646897,406.23141 L 39.954758,406.23141 L 39.954758,405.0848 C 40.482565,405.0848 40.862496,405.03247 41.09455,404.92782 C 41.326603,404.82317 41.529081,404.65482 41.701985,404.42276 C 41.874886,404.19071 42.102389,403.70158 42.384496,402.95536 L 48.322339,387.18937 z M 45.933551,397.83653 L 51.816794,397.83653 L 48.868347,389.6464 L 45.933551,397.83653 z"
             style="font-size:27.95563889px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:100%;text-anchor:start;fill:#ff0000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Arial;-inkscape-font-specification:Arial"
             transform="scale(0.9521015,1.0503082)" />
        </svg:g>
      </svg:g>
      <svg:g
         id="g4058">
        <svg:g
           id="g3944">
          <svg:g
             id="g3636">
            <svg:g
               id="g12263"
               style="stroke:#0000ff"
               transform="translate(0,-20)">
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#0000ff;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.4588,340.16804 L 1292.5405,340.16804"
                 id="path10172"
                 />
              <svg:path
                 id="path10174"
                 d="M -317.45881,354.16803 L 1292.5405,354.16803"
                 style="fill:none;fill-rule:evenodd;stroke:#0000ff;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#0000ff;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.4588,368.16801 L 1292.5405,368.16801"
                 id="path10176"
                 />
              <svg:path
                 id="path10178"
                 d="M -317.4588,382.16796 L 1292.5405,382.16796"
                 style="fill:none;fill-rule:evenodd;stroke:#0000ff;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#0000ff;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.45881,396.16797 L 1292.5405,396.16797"
                 id="path10180"
                 />
              <svg:line
                 style="fill:#000000;stroke:#0000ff;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
                 x1="3.5405273"
                 y1="326.16895"
                 x2="32.541046"
                 y2="326.16895"
                 id="line10286" />
              <svg:line
                 style="fill:#000000;stroke:#0000ff;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
                 x1="33.540924"
                 y1="326.16824"
                 x2="62.541443"
                 y2="326.16824"
                 id="line10298" />
            </svg:g>
            <svg:g
               id="g3290"
               transform="matrix(0.1386763,0,0,0.1386763,-131.43252,455.65147)"
               style="fill:#000000;stroke:none">
              <svg:g
                 id="g3350"
                 transform="matrix(1.0023926,0,0,1.0023926,535.64668,564.91152)"
                 style="fill:#000000;stroke:none">
                <svg:path
                   id="path3352"
                   style="font-size:176.79682922000001000px;font-style:italic;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;text-anchor:start;fill:#000000;fill-opacity:1;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Times New Roman"
                   d="M -1696.9368,-1259.709 L -1725.5523,-1165.7045 C -1727.2112,-1160.17 -1728.0406,-1156.1727 -1728.0406,-1153.7129 C -1728.0406,-1151.6444 -1727.2853,-1150.0651 -1725.7745,-1148.9749 C -1724.2638,-1147.8847 -1720.7238,-1147.1441 -1715.1547,-1146.7527 L -1716.0434,-1143.6499 L -1761.8105,-1143.6499 L -1760.5663,-1146.7527 C -1755.7083,-1146.8645 -1752.509,-1147.2279 -1750.9686,-1147.8429 C -1748.4803,-1148.8492 -1746.6141,-1150.1908 -1745.3699,-1151.868 C -1743.4148,-1154.4397 -1741.4301,-1159.0518 -1739.4157,-1165.7045 L -1718.7983,-1233.21 C -1717.5542,-1237.291 -1716.8729,-1239.639 -1716.7543,-1240.2541 C -1716.5766,-1241.3721 -1716.4878,-1242.4622 -1716.4877,-1243.5245 C -1716.4878,-1245.4811 -1717.0506,-1247.0184 -1718.1763,-1248.1367 C -1719.302,-1249.2547 -1720.8423,-1249.8138 -1722.7973,-1249.8139 C -1724.3378,-1249.8138 -1726.7372,-1249.4783 -1729.9957,-1248.8076 L -1731.1509,-1251.8264 L -1701.2025,-1259.709 L -1696.9368,-1259.709 z M -1658.4304,-1258.5258 L -1618.1731,-1258.5258 L -1622.2611,-1244.6892 L -1658.4304,-1244.6892 L -1665.0955,-1230.6849 C -1655.4978,-1229.2313 -1648.0329,-1226.4081 -1642.7008,-1222.2153 C -1634.4656,-1215.6744 -1630.3482,-1206.5339 -1630.3481,-1194.7939 C -1630.3482,-1187.023 -1631.8737,-1179.9929 -1634.9248,-1173.7036 C -1637.9759,-1167.4143 -1641.7085,-1162.1452 -1646.1221,-1157.8965 C -1650.536,-1153.6477 -1655.2608,-1150.2933 -1660.2966,-1147.8336 C -1667.1691,-1144.4792 -1674.1009,-1142.8021 -1681.0918,-1142.8021 C -1687.0755,-1142.8021 -1691.8152,-1144.032 -1695.3107,-1146.4918 C -1697.6805,-1148.169 -1698.8654,-1150.2933 -1698.8654,-1152.865 C -1698.8654,-1154.8776 -1698.0952,-1156.5967 -1696.5548,-1158.0222 C -1695.0145,-1159.4478 -1693.1186,-1160.1606 -1690.8673,-1160.1606 C -1687.7273,-1160.1606 -1684.2318,-1158.6791 -1680.3808,-1155.7162 C -1678.011,-1153.8713 -1676.0559,-1152.6414 -1674.5155,-1152.0264 C -1673.2713,-1151.5232 -1671.7903,-1151.2717 -1670.0722,-1151.2717 C -1663.3774,-1151.2717 -1657.2159,-1154.4163 -1651.5875,-1160.7057 C -1645.9593,-1166.995 -1643.1452,-1175.1711 -1643.1451,-1185.234 C -1643.1452,-1192.2222 -1644.6411,-1198.176 -1647.6329,-1203.0958 C -1650.6249,-1208.0153 -1654.4313,-1211.4815 -1659.0525,-1213.4941 C -1663.6737,-1215.5066 -1669.8352,-1216.9042 -1677.537,-1217.687 L -1658.4304,-1258.5258 z" />
              </svg:g>
              <svg:path
                 class="fil1 str1"
                 d="M -1308.9943,-641.08631 C -1279.9928,-660.66901 -1258.3335,-675.4933 -1244.3834,-685.74218 C -1230.4333,-695.80805 -1215.749,-708.43615 -1200.6976,-723.44345 C -1185.6462,-738.45075 -1172.981,-755.47122 -1162.702,-774.32185 C -1154.6257,-788.23106 -1147.6506,-804.33645 -1141.9605,-822.63804 C -1136.2703,-840.75661 -1133.3335,-858.32613 -1132.7828,-874.98057 C -1132.7828,-890.53692 -1134.8019,-905.36121 -1139.0236,-919.27041 C -1143.0618,-933.36263 -1150.0368,-944.89263 -1159.9487,-954.22644 C -1169.8606,-963.37723 -1182.7094,-967.95263 -1198.6786,-967.95263 C -1214.0971,-967.95263 -1228.5978,-964.84136 -1242.1808,-958.80183 C -1255.5802,-952.57929 -1265.125,-942.69644 -1270.448,-928.78724 C -1270.448,-927.50612 -1271.1822,-925.85898 -1272.2836,-923.47977 C -1271.9165,-920.55152 -1270.448,-918.35533 -1267.6947,-916.70819 C -1264.9414,-915.06104 -1262.5552,-914.32898 -1260.3526,-914.32898 C -1259.2513,-914.32898 -1256.1308,-914.87803 -1251.3584,-915.97612 C -1246.4025,-917.07422 -1242.3643,-917.80628 -1239.0603,-917.80628 C -1229.332,-917.80628 -1220.705,-914.32898 -1212.8122,-907.55739 C -1205.1029,-900.78581 -1201.2483,-892.55009 -1201.2483,-882.85026 C -1201.2483,-875.89565 -1203.2674,-869.30708 -1207.122,-863.26756 C -1210.9766,-857.22804 -1216.2997,-852.28661 -1223.0912,-848.80931 C -1229.8827,-845.14899 -1237.4084,-843.50185 -1245.4847,-843.50185 C -1260.169,-843.50185 -1272.6507,-847.89423 -1282.9297,-856.862 C -1293.0251,-866.0128 -1298.1646,-877.54279 -1298.1646,-892.00105 C -1298.1646,-910.48565 -1292.4745,-926.40803 -1281.2777,-939.9512 C -1269.8974,-953.49437 -1255.5802,-963.56025 -1237.959,-970.33183 C -1220.5214,-977.28644 -1202.9003,-980.58072 -1184.912,-980.58072 C -1165.2718,-980.58072 -1146.5493,-975.63929 -1129.1117,-965.57342 C -1111.4906,-955.69056 -1097.724,-941.96438 -1087.445,-924.9439 C -1077.166,-907.74041 -1071.843,-889.43883 -1071.843,-869.67312 C -1071.843,-834.53407 -1083.5904,-801.95725 -1107.0853,-771.75963 C -1130.5801,-741.56202 -1159.5816,-715.39075 -1194.2733,-693.06282 C -1217.401,-677.8725 -1254.846,-657.37472 -1306.4245,-631.56949 L -1308.9943,-641.08631 z M -1054.0383,-924.21184 C -1054.0383,-930.80041 -1051.6521,-936.29089 -1046.8797,-940.68326 C -1042.2908,-945.25866 -1036.6007,-947.45485 -1029.8092,-947.45485 C -1023.9355,-947.45485 -1018.4289,-944.89263 -1013.4729,-939.9512 C -1008.517,-935.19279 -1006.1308,-929.5193 -1006.1308,-923.29676 C -1006.1308,-916.70819 -1008.7005,-911.0347 -1013.4729,-906.4593 C -1018.6124,-902.06692 -1024.3026,-899.87073 -1030.7269,-899.87073 C -1037.5184,-899.87073 -1043.025,-902.06692 -1047.4303,-907.00835 C -1051.8356,-911.76676 -1054.0383,-917.44025 -1054.0383,-924.21184 z M -1054.0383,-828.31153 C -1054.0383,-834.9001 -1051.6521,-840.57359 -1047.2468,-844.96597 C -1042.6579,-849.54137 -1036.9678,-851.73756 -1029.8092,-851.73756 C -1023.9355,-851.73756 -1018.6124,-849.35835 -1013.4729,-844.41693 C -1008.7005,-839.4755 -1006.1308,-834.16804 -1006.1308,-828.31153 C -1006.1308,-821.17391 -1008.517,-815.50042 -1013.1058,-810.92502 C -1017.8782,-806.34963 -1023.3848,-803.97042 -1029.8092,-803.97042 C -1036.9678,-803.97042 -1042.6579,-806.34963 -1047.2468,-810.74201 C -1051.6521,-815.13439 -1054.0383,-820.9909 -1054.0383,-828.31153 z"
                 id="path2433"
                 style="fill:#000000;fill-rule:evenodd;stroke:none;stroke-width:0.54985398" />
            </svg:g>
          </svg:g>
          <svg:g
             transform="translate(0,-20)"
             style="fill:#0000ff"
             id="g12283">
            <svg:path
               id="path10254"
               d="M -249.60296,382.66811 C -245.34268,382.71849 -242.987,385.6424 -244.34183,389.21817 C -245.64497,392.84747 -250.15429,395.71942 -254.36601,395.66746 C -258.57617,395.61865 -260.92872,392.64436 -259.57702,389.06859 C -258.27545,385.49125 -253.81312,382.61772 -249.60296,382.66811 z"
               style="fill:#0000ff" />
            <svg:path
               d="M -219.60296,375.66811 C -215.34268,375.71849 -212.987,378.6424 -214.34183,382.21817 C -215.64497,385.84747 -220.15429,388.71942 -224.36601,388.66746 C -228.57617,388.61865 -230.92872,385.64436 -229.57702,382.06859 C -228.27545,378.49125 -223.81312,375.61772 -219.60296,375.66811 z"
               id="path10258"
               style="fill:#0000ff" />
            <svg:path
               id="path10262"
               d="M -189.60296,368.66811 C -185.34268,368.71849 -182.987,371.6424 -184.34183,375.21817 C -185.64497,378.84747 -190.15429,381.71942 -194.36601,381.66746 C -198.57617,381.61865 -200.92872,378.64436 -199.57702,375.06859 C -198.27545,371.49125 -193.81312,368.61772 -189.60296,368.66811 z"
               style="fill:#0000ff" />
            <svg:path
               d="M -159.60296,361.66811 C -155.34268,361.71849 -152.987,364.6424 -154.34183,368.21817 C -155.64497,371.84747 -160.15429,374.71942 -164.36601,374.66746 C -168.57617,374.61865 -170.92872,371.64436 -169.57702,368.06859 C -168.27545,364.49125 -163.81312,361.61772 -159.60296,361.66811 z"
               id="path10266"
               style="fill:#0000ff" />
            <svg:path
               id="path10270"
               d="M -129.60296,354.66811 C -125.34268,354.71849 -122.987,357.6424 -124.34183,361.21817 C -125.64497,364.84747 -130.15429,367.71942 -134.36601,367.66746 C -138.57617,367.61865 -140.92872,364.64436 -139.57702,361.06859 C -138.27545,357.49125 -133.81312,354.61772 -129.60296,354.66811 z"
               style="fill:#0000ff" />
            <svg:path
               d="M -69.602959,340.66811 C -65.34268,340.71849 -62.986997,343.6424 -64.341828,347.21817 C -65.644972,350.84747 -70.154289,353.71942 -74.366013,353.66746 C -78.576171,353.61865 -80.928722,350.64436 -79.577024,347.06859 C -78.275446,343.49125 -73.813117,340.61772 -69.602959,340.66811 z"
               id="path10274"
               style="fill:#0000ff" />
            <svg:path
               id="path10278"
               d="M -39.602959,333.66811 C -35.34268,333.71849 -32.986997,336.6424 -34.341828,340.21817 C -35.644972,343.84747 -40.154289,346.71942 -44.366013,346.66746 C -48.576171,346.61865 -50.928722,343.64436 -49.577024,340.06859 C -48.275446,336.49125 -43.813117,333.61772 -39.602959,333.66811 z"
               style="fill:#0000ff" />
            <svg:path
               d="M -9.602959,326.66811 C -5.3426801,326.71849 -2.9869965,329.6424 -4.3418279,333.21817 C -5.644972,336.84747 -10.154289,339.71942 -14.366013,339.66746 C -18.576171,339.61865 -20.928722,336.64436 -19.577024,333.06859 C -18.275446,329.49125 -13.813117,326.61772 -9.602959,326.66811 z"
               id="path10282"
               style="fill:#0000ff" />
            <svg:path
               style="fill:#0000ff"
               d="M -102.62485,347.66816 C -97.34622,347.71704 -92.35294,349.76966 -91.9725,354.31477 C -91.68716,357.6396 -95.96712,360.66816 -101.24574,360.66816 C -106.57191,360.61928 -111.51616,358.61552 -111.94565,354.07194 C -112.23099,350.69671 -107.90642,347.66816 -102.62485,347.66816 z M -101.34382,359.5441 C -99.05971,359.5441 -97.58548,357.29445 -97.82325,355.48771 C -98.34785,351.92004 -99.53526,349.03506 -102.67538,349.03506 C -104.95802,349.03506 -106.28955,351.33204 -106.00275,353.14031 C -105.47814,356.70951 -104.4795,359.5441 -101.34382,359.5441 z"
               id="path10284" />
            <svg:g
               transform="matrix(1.566279,0,0,1.5745344,91.0872,-280.75178)"
               id="g10288"
               style="fill:#0000ff">
              <svg:path
                 d="M -45.132546,381.33215 C -42.412546,381.36415 -40.908546,383.22115 -41.773546,385.49215 C -42.605546,387.79715 -45.484546,389.62115 -48.173546,389.58815 C -50.861546,389.55715 -52.363546,387.66815 -51.500546,385.39715 C -50.669546,383.12515 -47.820546,381.30015 -45.132546,381.33215 z"
                 id="path10290"
                 style="fill:#0000ff" />
            </svg:g>
            <svg:g
               id="g10294"
               transform="matrix(1.566279,0,0,1.5745344,121.0872,-287.75248)"
               style="fill:#0000ff">
              <svg:path
                 id="path10296"
                 d="M -45.132546,381.33215 C -42.412546,381.36415 -40.908546,383.22115 -41.773546,385.49215 C -42.605546,387.79715 -45.484546,389.62115 -48.173546,389.58815 C -50.861546,389.55715 -52.363546,387.66815 -51.500546,385.39715 C -50.669546,383.12515 -47.820546,381.30015 -45.132546,381.33215 z"
                 style="fill:#0000ff" />
            </svg:g>
          </svg:g>
        </svg:g>
        <svg:g
           id="g3916">
          <svg:g
             transform="translate(0,-20)"
             id="g12184">
            <svg:g
               id="g12174"
               style="stroke:#d40000">
              <svg:path
                 id="path10142"
                 d="M -317.45846,242.16824 L 1292.5408,242.16824"
                 style="fill:none;fill-rule:evenodd;stroke:#d40000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#d40000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.45847,256.16823 L 1292.5408,256.16823"
                 id="path10144"
                 />
              <svg:path
                 id="path10146"
                 d="M -317.45846,270.16821 L 1292.5408,270.16821"
                 style="fill:none;fill-rule:evenodd;stroke:#d40000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#d40000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.45846,284.16816 L 1292.5408,284.16816"
                 id="path10148"
                 />
              <svg:path
                 id="path10150"
                 d="M -317.45847,298.16817 L 1292.5408,298.16817"
                 style="fill:none;fill-rule:evenodd;stroke:#d40000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:line
                 style="fill:#d40000;stroke:#d40000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
                 x1="423.54056"
                 y1="228.16809"
                 x2="452.54105"
                 y2="228.16809"
                 id="line10349" />
              <svg:line
                 style="fill:#d40000;stroke:#d40000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
                 x1="63.540543"
                 y1="312.16824"
                 x2="92.541046"
                 y2="312.16824"
                 id="line3837" />
              <svg:line
                 id="line10292"
                 y2="312.16824"
                 x2="62.541447"
                 y1="312.16824"
                 x1="33.540928"
                 style="fill:#d40000;stroke:#d40000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none" />
            </svg:g>
            <svg:g
               id="g2860"
               transform="matrix(0.1386763,0,0,0.1386763,-131.43252,377.65147)"
               style="fill:#000000;stroke:none">
              <svg:path
                 class="fil1 str1"
                 d="M -1308.9943,-641.08631 C -1279.9928,-660.66901 -1258.3335,-675.4933 -1244.3834,-685.74218 C -1230.4333,-695.80805 -1215.749,-708.43615 -1200.6976,-723.44345 C -1185.6462,-738.45075 -1172.981,-755.47122 -1162.702,-774.32185 C -1154.6257,-788.23106 -1147.6506,-804.33645 -1141.9605,-822.63804 C -1136.2703,-840.75661 -1133.3335,-858.32613 -1132.7828,-874.98057 C -1132.7828,-890.53692 -1134.8019,-905.36121 -1139.0236,-919.27041 C -1143.0618,-933.36263 -1150.0368,-944.89263 -1159.9487,-954.22644 C -1169.8606,-963.37723 -1182.7094,-967.95263 -1198.6786,-967.95263 C -1214.0971,-967.95263 -1228.5978,-964.84136 -1242.1808,-958.80183 C -1255.5802,-952.57929 -1265.125,-942.69644 -1270.448,-928.78724 C -1270.448,-927.50612 -1271.1822,-925.85898 -1272.2836,-923.47977 C -1271.9165,-920.55152 -1270.448,-918.35533 -1267.6947,-916.70819 C -1264.9414,-915.06104 -1262.5552,-914.32898 -1260.3526,-914.32898 C -1259.2513,-914.32898 -1256.1308,-914.87803 -1251.3584,-915.97612 C -1246.4025,-917.07422 -1242.3643,-917.80628 -1239.0603,-917.80628 C -1229.332,-917.80628 -1220.705,-914.32898 -1212.8122,-907.55739 C -1205.1029,-900.78581 -1201.2483,-892.55009 -1201.2483,-882.85026 C -1201.2483,-875.89565 -1203.2674,-869.30708 -1207.122,-863.26756 C -1210.9766,-857.22804 -1216.2997,-852.28661 -1223.0912,-848.80931 C -1229.8827,-845.14899 -1237.4084,-843.50185 -1245.4847,-843.50185 C -1260.169,-843.50185 -1272.6507,-847.89423 -1282.9297,-856.862 C -1293.0251,-866.0128 -1298.1646,-877.54279 -1298.1646,-892.00105 C -1298.1646,-910.48565 -1292.4745,-926.40803 -1281.2777,-939.9512 C -1269.8974,-953.49437 -1255.5802,-963.56025 -1237.959,-970.33183 C -1220.5214,-977.28644 -1202.9003,-980.58072 -1184.912,-980.58072 C -1165.2718,-980.58072 -1146.5493,-975.63929 -1129.1117,-965.57342 C -1111.4906,-955.69056 -1097.724,-941.96438 -1087.445,-924.9439 C -1077.166,-907.74041 -1071.843,-889.43883 -1071.843,-869.67312 C -1071.843,-834.53407 -1083.5904,-801.95725 -1107.0853,-771.75963 C -1130.5801,-741.56202 -1159.5816,-715.39075 -1194.2733,-693.06282 C -1217.401,-677.8725 -1254.846,-657.37472 -1306.4245,-631.56949 L -1308.9943,-641.08631 z M -1054.0383,-924.21184 C -1054.0383,-930.80041 -1051.6521,-936.29089 -1046.8797,-940.68326 C -1042.2908,-945.25866 -1036.6007,-947.45485 -1029.8092,-947.45485 C -1023.9355,-947.45485 -1018.4289,-944.89263 -1013.4729,-939.9512 C -1008.517,-935.19279 -1006.1308,-929.5193 -1006.1308,-923.29676 C -1006.1308,-916.70819 -1008.7005,-911.0347 -1013.4729,-906.4593 C -1018.6124,-902.06692 -1024.3026,-899.87073 -1030.7269,-899.87073 C -1037.5184,-899.87073 -1043.025,-902.06692 -1047.4303,-907.00835 C -1051.8356,-911.76676 -1054.0383,-917.44025 -1054.0383,-924.21184 z M -1054.0383,-828.31153 C -1054.0383,-834.9001 -1051.6521,-840.57359 -1047.2468,-844.96597 C -1042.6579,-849.54137 -1036.9678,-851.73756 -1029.8092,-851.73756 C -1023.9355,-851.73756 -1018.6124,-849.35835 -1013.4729,-844.41693 C -1008.7005,-839.4755 -1006.1308,-834.16804 -1006.1308,-828.31153 C -1006.1308,-821.17391 -1008.517,-815.50042 -1013.1058,-810.92502 C -1017.8782,-806.34963 -1023.3848,-803.97042 -1029.8092,-803.97042 C -1036.9678,-803.97042 -1042.6579,-806.34963 -1047.2468,-810.74201 C -1051.6521,-815.13439 -1054.0383,-820.9909 -1054.0383,-828.31153 z"
                 id="path2863"
                 style="fill:#000000;fill-rule:evenodd;stroke:none;stroke-width:0.54985398" />
            </svg:g>
          </svg:g>
          <svg:g
             transform="translate(0,-20)"
             id="g12196">
            <svg:path
               d="M 440.39704,221.66812 C 444.65732,221.7185 447.013,224.64241 445.65817,228.21818 C 444.35503,231.84748 439.84571,234.71943 435.63399,234.66747 C 431.42383,234.61866 429.07128,231.64437 430.42298,228.0686 C 431.72455,224.49126 436.18688,221.61773 440.39704,221.66812 z"
               id="path10347"
               style="fill:#d40000" />
            <svg:path
               id="path7641"
               d="M 80.39704,305.66812 C 84.657319,305.7185 87.013002,308.6424 85.658171,312.21818 C 84.355027,315.84748 79.84571,318.71943 75.633986,318.66746 C 71.423828,318.61865 69.071277,315.64436 70.422975,312.0686 C 71.724553,308.49126 76.186882,305.61773 80.39704,305.66812 z"
               style="fill:#d40000" />
            <svg:path
               style="fill:#d40000"
               d="M 110.39704,298.66812 C 114.65732,298.7185 117.013,301.6424 115.65817,305.21818 C 114.35503,308.84748 109.84571,311.71943 105.63399,311.66746 C 101.42383,311.61865 99.071277,308.64436 100.42298,305.0686 C 101.72455,301.49126 106.18688,298.61773 110.39704,298.66812 z"
               id="path7755" />
            <svg:path
               style="fill:#d40000"
               id="path7759"
               d="M 140.39704,291.66881 C 144.65732,291.7192 147.013,294.6431 145.65817,298.21888 C 144.35503,301.84818 139.84571,304.72012 135.63399,304.66816 C 131.42383,304.61935 129.07128,301.64506 130.42298,298.0693 C 131.72455,294.49196 136.18688,291.61843 140.39704,291.66881 z" />
            <svg:path
               style="fill:#d40000"
               d="M 170.39704,284.66812 C 174.65732,284.7185 177.013,287.6424 175.65817,291.21818 C 174.35503,294.84748 169.84571,297.71943 165.63399,297.66746 C 161.42383,297.61865 159.07128,294.64436 160.42298,291.0686 C 161.72455,287.49126 166.18688,284.61773 170.39704,284.66812 z"
               id="path7763" />
            <svg:path
               style="fill:#d40000"
               id="path7767"
               d="M 200.39704,277.66812 C 204.65732,277.7185 207.013,280.6424 205.65817,284.21818 C 204.35503,287.84748 199.84571,290.71943 195.63399,290.66746 C 191.42383,290.61865 189.07128,287.64436 190.42298,284.0686 C 191.72455,280.49126 196.18688,277.61773 200.39704,277.66812 z" />
            <svg:path
               style="fill:#d40000"
               d="M 230.39704,270.66812 C 234.65732,270.7185 237.013,273.6424 235.65817,277.21818 C 234.35503,280.84748 229.84571,283.71943 225.63399,283.66746 C 221.42383,283.61865 219.07128,280.64436 220.42298,277.0686 C 221.72455,273.49126 226.18688,270.61773 230.39704,270.66812 z"
               id="path7771" />
            <svg:path
               style="fill:#d40000"
               id="path7775"
               d="M 260.39704,263.66812 C 264.65732,263.7185 267.013,266.6424 265.65817,270.21818 C 264.35503,273.84748 259.84571,276.71943 255.63399,276.66746 C 251.42383,276.61865 249.07128,273.64436 250.42298,270.0686 C 251.72455,266.49126 256.18688,263.61773 260.39704,263.66812 z" />
            <svg:path
               style="fill:#d40000"
               d="M 290.39704,256.66812 C 294.65732,256.7185 297.013,259.6424 295.65817,263.21818 C 294.35503,266.84748 289.84571,269.71943 285.63399,269.66746 C 281.42383,269.61865 279.07128,266.64436 280.42298,263.0686 C 281.72455,259.49126 286.18688,256.61773 290.39704,256.66812 z"
               id="path7779" />
            <svg:path
               style="fill:#d40000"
               id="path7783"
               d="M 350.39704,242.66812 C 354.65732,242.7185 357.013,245.6424 355.65817,249.21818 C 354.35503,252.84748 349.84571,255.71943 345.63399,255.66746 C 341.42383,255.61865 339.07128,252.64436 340.42298,249.0686 C 341.72455,245.49126 346.18688,242.61773 350.39704,242.66812 z" />
            <svg:path
               style="fill:#d40000"
               d="M 380.39704,235.66812 C 384.65732,235.7185 387.013,238.6424 385.65817,242.21818 C 384.35503,245.84748 379.84571,248.71943 375.63399,248.66746 C 371.42383,248.61865 369.07128,245.64436 370.42298,242.0686 C 371.72455,238.49126 376.18688,235.61773 380.39704,235.66812 z"
               id="path7787" />
            <svg:path
               style="fill:#d40000"
               id="path7791"
               d="M 410.39704,228.66812 C 414.65732,228.7185 417.013,231.6424 415.65817,235.21818 C 414.35503,238.84748 409.84571,241.71943 405.63399,241.66746 C 401.42383,241.61865 399.07128,238.64436 400.42298,235.0686 C 401.72455,231.49126 406.18688,228.61773 410.39704,228.66812 z" />
            <svg:path
               id="path10302"
               d="M 50.39744,312.66812 C 54.65772,312.7185 57.0134,315.6424 55.65857,319.21818 C 54.35543,322.84748 49.84611,325.71943 45.63439,325.66746 C 41.42423,325.61865 39.07168,322.64436 40.42338,319.0686 C 41.72495,315.49126 46.18728,312.61773 50.39744,312.66812 z"
               style="fill:#d40000" />
            <svg:path
               style="fill:#d40000"
               id="path3897"
               d="M 317.37515,249.66816 C 322.65378,249.71704 327.64706,251.76968 328.0275,256.31477 C 328.31284,259.63961 324.03288,262.66816 318.75426,262.66816 C 313.42809,262.61928 308.48384,260.61552 308.05435,256.07196 C 307.76901,252.69673 312.09358,249.66816 317.37515,249.66816 z M 318.65618,261.54412 C 320.94029,261.54412 322.41452,259.29447 322.17675,257.48773 C 321.65215,253.92005 320.46474,251.03508 317.32462,251.03508 C 315.04198,251.03508 313.71045,253.33204 313.99725,255.14031 C 314.52186,258.70952 315.5205,261.54412 318.65618,261.54412 z" />
          </svg:g>
        </svg:g>
        <svg:g
           id="g3972">
          <svg:g
             id="g3806">
            <svg:g
               id="g12083"
               style="stroke:#808000"
               transform="translate(0,-20)">
              <svg:path
                 id="path5102"
                 d="M -317.4588,158.16821 L 1292.5405,158.16821"
                 style="fill:none;fill-rule:evenodd;stroke:#808000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#808000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.45881,172.1682 L 1292.5405,172.1682"
                 id="path5881"
                 />
              <svg:path
                 id="path5883"
                 d="M -317.4588,186.16818 L 1292.5405,186.16818"
                 style="fill:none;fill-rule:evenodd;stroke:#808000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#808000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.4588,200.16813 L 1292.5405,200.16813"
                 id="path5885"
                 />
              <svg:path
                 id="path5887"
                 d="M -317.45881,214.16814 L 1292.5405,214.16814"
                 style="fill:none;fill-rule:evenodd;stroke:#808000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:line
                 style="fill:#808000;stroke:#808000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
                 x1="423.54092"
                 y1="228.16809"
                 x2="452.54141"
                 y2="228.16809"
                 id="line7849" />
              <svg:line
                 style="fill:#808000;stroke:#808000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
                 x1="783.54059"
                 y1="144.16891"
                 x2="812.54108"
                 y2="144.16891"
                 id="line10988" />
              <svg:line
                 style="fill:#808000;stroke:#808000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
                 x1="813.54059"
                 y1="144.16891"
                 x2="842.54108"
                 y2="144.16891"
                 id="line11085" />
            </svg:g>
            <svg:path
               style="fill:#000000;fill-rule:evenodd;stroke:none;stroke-width:0.51757997"
               d="M -291.55849,172.93115 C -293.00877,173.33435 -294.31641,174.23562 -295.52895,175.61123 C -296.74147,177.01056 -297.33585,178.55219 -297.33585,180.21243 C -297.33585,181.25599 -296.97923,182.44187 -296.28974,183.69889 C -295.60027,184.97965 -294.55416,185.90463 -293.19898,186.49756 C -292.74725,186.59244 -292.53327,186.8296 -292.53327,187.16165 C -292.53327,187.28024 -292.69971,187.39883 -293.10388,187.49369 C -295.26742,186.94819 -297.05055,185.78604 -298.42951,184.05466 C -299.80847,182.29956 -300.52172,180.30729 -300.56926,178.03042 C -300.49794,175.5875 -299.76091,173.31063 -298.35819,171.22349 C -296.93167,169.11264 -295.10099,167.61843 -292.86612,166.74088 L -294.50661,158.32117 C -298.16798,161.357 -301.13987,164.51144 -303.44606,167.83188 C -305.75224,171.12863 -306.941,174.70996 -307.03611,178.57592 C -306.98855,180.30729 -306.63193,181.99124 -305.96622,183.60403 C -305.30052,185.24054 -304.30196,186.71101 -302.97056,188.06292 C -300.28397,190.74299 -296.78903,192.14233 -292.53327,192.28464 C -291.08299,192.18976 -289.5376,191.92887 -287.87335,191.50196 L -291.55849,172.93115 z M -289.84668,172.69397 L -286.13777,190.93274 C -282.47639,189.46225 -280.64571,186.26038 -280.64571,181.37458 C -280.85968,179.73807 -281.33519,178.26758 -282.14354,176.96313 C -282.92812,175.63495 -283.97423,174.59137 -285.30563,173.83242 C -286.63704,173.07346 -288.13487,172.69397 -289.84668,172.69397 z M -294.72058,148.02777 C -293.936,147.55342 -293.03256,146.72331 -292.05777,145.56115 C -291.08299,144.42272 -290.13198,143.07082 -289.22853,141.55289 C -288.30131,140.01125 -287.56427,138.44589 -287.01744,136.85683 C -286.47062,135.29147 -286.20909,133.79726 -286.20909,132.42165 C -286.20909,131.82872 -286.25663,131.23577 -286.37551,130.71399 C -286.47062,129.86016 -286.73215,129.19607 -287.18387,128.74544 C -287.63559,128.31853 -288.2062,128.08135 -288.91945,128.08135 C -290.34597,128.08135 -291.62981,128.95889 -292.77103,130.71399 C -293.6507,132.23192 -294.38773,134.03444 -294.91078,136.07415 C -295.45761,138.13757 -295.76669,140.17727 -295.81424,142.24069 C -295.69536,144.58873 -295.31496,146.50985 -294.72058,148.02777 z M -296.21842,149.40338 C -297.28829,145.56115 -297.88267,141.64776 -298.00155,137.66322 C -297.97778,135.10173 -297.71625,132.70626 -297.21697,130.47682 C -296.74147,128.24737 -296.052,126.32625 -295.14854,124.66603 C -294.26885,123.00579 -293.24653,121.74877 -292.10533,120.89494 C -291.08299,120.13598 -290.34597,119.73278 -289.94179,119.73278 C -289.63271,119.73278 -289.37118,119.85137 -289.13343,120.06483 C -288.89568,120.27828 -288.5866,120.63405 -288.2062,121.10839 C -285.37696,125.11666 -283.95045,129.95503 -283.95045,135.59979 C -283.95045,138.27987 -284.30708,140.88879 -285.02034,143.49773 C -285.70981,146.08294 -286.73215,148.54955 -288.08732,150.85015 C -289.46628,153.17447 -291.08299,155.19046 -292.96122,156.92184 L -291.03544,166.26653 C -289.98933,166.14795 -289.27607,166.05307 -288.8719,166.05307 C -287.065,166.05307 -285.44828,166.43255 -283.95045,167.19152 C -282.45261,167.95047 -281.16876,168.97033 -280.12265,170.27479 C -279.07655,171.55554 -278.2682,173.02603 -277.69759,174.68625 C -277.15076,176.34647 -276.84168,178.07785 -276.84168,179.88038 C -276.84168,182.67905 -277.57871,185.24054 -279.05278,187.54114 C -280.52683,189.84174 -282.73792,191.52567 -285.70981,192.61668 C -285.51961,193.77884 -285.18675,195.46278 -284.68749,197.62107 C -284.21197,199.80308 -283.85535,201.53446 -283.61759,202.8152 C -283.37985,204.09595 -283.28474,205.32926 -283.28474,206.53886 C -283.28474,208.41254 -283.73647,210.07276 -284.63993,211.54325 C -285.56716,213.01374 -286.80347,214.15217 -288.37263,214.95857 C -289.91801,215.76496 -291.62981,216.16816 -293.48427,216.16816 C -296.09954,216.16816 -298.38196,215.43291 -300.33152,213.98615 C -302.28107,212.51567 -303.32718,210.54711 -303.42229,208.03306 C -303.35096,206.91834 -303.08944,205.87476 -302.61394,204.87863 C -302.13842,203.88249 -301.49649,203.07609 -300.66437,202.45945 C -299.85601,201.81907 -298.88124,201.48703 -297.76381,201.41587 C -296.83657,201.41587 -295.95689,201.67677 -295.12477,202.17484 C -294.31641,202.69663 -293.6507,203.38443 -293.15142,204.26197 C -292.67592,205.13951 -292.4144,206.11194 -292.4144,207.1555 C -292.4144,208.55485 -292.88989,209.74071 -293.84091,210.71314 C -294.79191,211.68556 -296.00445,212.18361 -297.45473,212.18361 L -298.00155,212.18361 C -297.07432,213.60667 -295.55272,214.34191 -293.43673,214.34191 C -292.36684,214.34191 -291.27319,214.10474 -290.17953,213.67783 C -289.0621,213.22719 -288.13487,212.63426 -287.35029,211.87529 C -286.56571,211.11634 -286.04266,210.30994 -285.82869,209.4561 C -285.42451,208.48369 -285.23431,207.13179 -285.23431,205.44785 C -285.23431,204.3094 -285.35319,203.17097 -285.56716,202.03253 C -285.78113,200.9178 -286.11398,199.4236 -286.56571,197.57364 C -287.01744,195.74739 -287.35029,194.32434 -287.5405,193.35193 C -288.96701,193.70768 -290.44106,193.89742 -291.98645,193.89742 C -294.57793,193.89742 -297.02677,193.37564 -299.33297,192.30835 C -301.63915,191.24106 -303.66003,189.77058 -305.41939,187.87318 C -307.15499,185.97578 -308.51016,183.8412 -309.48494,181.42202 C -310.43596,179.02655 -310.93522,176.51249 -310.95901,173.90357 C -310.8639,171.48438 -310.41217,169.16007 -309.55627,166.97806 C -308.70036,164.77233 -307.60672,162.68518 -306.25153,160.74035 C -304.89634,158.79552 -303.49361,157.0167 -302.04333,155.42763 C -300.56926,153.86227 -298.64348,151.84628 -296.21842,149.40338 z"
               id="path3758" />
          </svg:g>
          <svg:g
             id="g3899">
            <svg:g
               id="g12120"
               transform="translate(0,-20)">
              <svg:path
                 style="fill:#808000"
                 d="M 470.39741,214.66812 C 474.65769,214.7185 477.01337,217.6424 475.65854,221.21818 C 474.3554,224.84748 469.84608,227.71943 465.63436,227.66746 C 461.4242,227.61865 459.07165,224.64436 460.42335,221.0686 C 461.72492,217.49126 466.18725,214.61773 470.39741,214.66812 z"
                 id="path7799" />
              <svg:path
                 style="fill:#808000"
                 id="path7803"
                 d="M 500.39741,207.66812 C 504.65769,207.7185 507.01337,210.6424 505.65854,214.21818 C 504.3554,217.84748 499.84608,220.71943 495.63436,220.66746 C 491.4242,220.61865 489.07165,217.64436 490.42335,214.0686 C 491.72492,210.49126 496.18725,207.61773 500.39741,207.66812 z" />
              <svg:path
                 style="fill:#808000"
                 d="M 530.39741,200.66812 C 534.65769,200.7185 537.01337,203.6424 535.65854,207.21818 C 534.3554,210.84748 529.84608,213.71943 525.63436,213.66746 C 521.4242,213.61865 519.07165,210.64436 520.42335,207.0686 C 521.72492,203.49126 526.18725,200.61773 530.39741,200.66812 z"
                 id="path7807" />
              <svg:path
                 style="fill:#808000"
                 d="M 590.39741,186.66812 C 594.65769,186.7185 597.01337,189.6424 595.65854,193.21818 C 594.3554,196.84748 589.84608,199.71943 585.63436,199.66746 C 581.4242,199.61865 579.07165,196.64436 580.42335,193.0686 C 581.72492,189.49126 586.18725,186.61773 590.39741,186.66812 z"
                 id="path7815" />
              <svg:path
                 style="fill:#808000"
                 id="path7819"
                 d="M 620.39741,179.66812 C 624.65769,179.7185 627.01337,182.6424 625.65854,186.21818 C 624.3554,189.84748 619.84608,192.71943 615.63436,192.66746 C 611.4242,192.61865 609.07165,189.64436 610.42335,186.0686 C 611.72492,182.49126 616.18725,179.61773 620.39741,179.66812 z" />
              <svg:path
                 style="fill:#808000"
                 d="M 650.39741,172.66812 C 654.65769,172.7185 657.01337,175.6424 655.65854,179.21817 C 654.3554,182.84747 649.84608,185.71943 645.63436,185.66746 C 641.4242,185.61865 639.07165,182.64436 640.42335,179.0686 C 641.72492,175.49126 646.18725,172.61773 650.39741,172.66812 z"
                 id="path7823" />
              <svg:path
                 style="fill:#808000"
                 id="path7827"
                 d="M 680.39741,165.66812 C 684.65769,165.7185 687.01337,168.6424 685.65854,172.21817 C 684.3554,175.84747 679.84608,178.71943 675.63436,178.66746 C 671.4242,178.61865 669.07165,175.64436 670.42335,172.0686 C 671.72492,168.49126 676.18725,165.61773 680.39741,165.66812 z" />
              <svg:path
                 style="fill:#808000"
                 d="M 710.39741,158.66812 C 714.65769,158.7185 717.01337,161.6424 715.65854,165.21817 C 714.3554,168.84747 709.84608,171.71943 705.63436,171.66746 C 701.4242,171.61865 699.07165,168.64436 700.42335,165.0686 C 701.72492,161.49126 706.18725,158.61773 710.39741,158.66812 z"
                 id="path7831" />
              <svg:path
                 style="fill:#808000"
                 id="path7835"
                 d="M 740.39741,151.66812 C 744.65769,151.7185 747.01337,154.6424 745.65854,158.21817 C 744.3554,161.84747 739.84608,164.71943 735.63436,164.66746 C 731.4242,164.61865 729.07165,161.64436 730.42335,158.0686 C 731.72492,154.49126 736.18725,151.61773 740.39741,151.66812 z" />
              <svg:path
                 style="fill:#808000"
                 d="M 770.39741,144.66812 C 774.65769,144.7185 777.01337,147.6424 775.65854,151.21817 C 774.3554,154.84747 769.84608,157.71943 765.63436,157.66746 C 761.4242,157.61865 759.07165,154.64436 760.42335,151.0686 C 761.72492,147.49126 766.18725,144.61773 770.39741,144.66812 z"
                 id="path7839" />
              <svg:path
                 style="fill:#808000"
                 id="path7795"
                 d="M 440.39741,221.66812 C 444.65769,221.7185 447.01337,224.6424 445.65854,228.21818 C 444.3554,231.84748 439.84608,234.71943 435.63436,234.66746 C 431.4242,234.61865 429.07165,231.64436 430.42335,228.0686 C 431.72492,224.49126 436.18725,221.61773 440.39741,221.66812 z" />
              <svg:path
                 d="M 557.37552,193.66816 C 562.65415,193.71704 567.64743,195.76967 568.02787,200.31477 C 568.31321,203.63961 564.03325,206.66816 558.75463,206.66816 C 553.42846,206.61928 548.48421,204.61552 548.05472,200.07195 C 547.76938,196.69672 552.09395,193.66816 557.37552,193.66816 z M 558.65655,205.54411 C 560.94066,205.54411 562.41489,203.29446 562.17712,201.48772 C 561.65252,197.92005 560.46511,195.03507 557.32499,195.03507 C 555.04235,195.03507 553.71082,197.33204 553.99762,199.14031 C 554.52223,202.70952 555.52087,205.54411 558.65655,205.54411 z"
                 id="path7859"
                 style="fill:#808000" />
              <svg:path
                 style="fill:#808000"
                 id="path10986"
                 d="M 800.397,137.66881 C 804.65728,137.71919 807.01296,140.6431 805.65813,144.21887 C 804.35499,147.84817 799.84567,150.72012 795.63395,150.66816 C 791.42379,150.61935 789.07124,147.64506 790.42294,144.06929 C 791.72451,140.49195 796.18684,137.61842 800.397,137.66881 z" />
              <svg:path
                 style="fill:#808000"
                 id="path11081"
                 d="M 830.397,130.66811 C 834.65728,130.71849 837.01296,133.6424 835.65813,137.21817 C 834.35499,140.84747 829.84567,143.71942 825.63395,143.66746 C 821.42379,143.61865 819.07124,140.64436 820.42294,137.06859 C 821.72451,133.49125 826.18684,130.61772 830.397,130.66811 z" />
            </svg:g>
          </svg:g>
        </svg:g>
        <svg:g
           id="g4019">
          <svg:g
             id="g3786">
            <svg:g
               id="g12012"
               style="stroke:#008000"
               transform="translate(0,-20)">
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#008000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.4588,60.168201 L 1292.5405,60.168201"
                 id="path10156"
                 />
              <svg:path
                 id="path10158"
                 d="M -317.45881,74.168187 L 1292.5405,74.168187"
                 style="fill:none;fill-rule:evenodd;stroke:#008000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#008000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.4588,88.168172 L 1292.5405,88.168172"
                 id="path10160"
                 />
              <svg:path
                 id="path10162"
                 d="M -317.4588,102.16812 L 1292.5405,102.16812"
                 style="fill:none;fill-rule:evenodd;stroke:#008000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
              <svg:path
                 style="fill:none;fill-rule:evenodd;stroke:#008000;stroke-width:1.00039113;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                 d="M -317.45881,116.16813 L 1292.5405,116.16813"
                 id="path10164"
                 />
              <svg:line
                 id="line11042"
                 y2="130.16878"
                 x2="872.54144"
                 y1="130.16878"
                 x1="843.54095"
                 style="fill:#008000;stroke:#008000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none" />
              <svg:line
                 style="fill:#008000;stroke:#008000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
                 x1="813.54059"
                 y1="130.16821"
                 x2="842.54108"
                 y2="130.16821"
                 id="line10972" />
              <svg:line
                 id="line11938"
                 y2="46.168091"
                 x2="1232.5414"
                 y1="46.168091"
                 x1="1203.5409"
                 style="fill:#008000;stroke:#008000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none" />
              <svg:line
                 id="line11946"
                 y2="46.168797"
                 x2="1262.541"
                 y1="46.168797"
                 x1="1233.5405"
                 style="fill:#008000;stroke:#008000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none" />
              <svg:line
                 id="line11952"
                 y2="32.167522"
                 x2="1292.5414"
                 y1="32.167522"
                 x1="1263.5409"
                 style="fill:#008000;stroke:#008000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none" />
              <svg:line
                 style="fill:#008000;stroke:#008000;stroke-width:0.99987274;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
                 x1="1263.5409"
                 y1="46.168224"
                 x2="1292.5414"
                 y2="46.168224"
                 id="line11954" />
            </svg:g>
            <svg:g
               id="g3656"
               style="fill:#000000;stroke:none"
               transform="matrix(0.1376384,0,0,0.1376384,106.75921,265.89147)">
              <svg:g
                 id="g3326"
                 transform="matrix(1.0099511,0,0,1.0099511,-1174.3805,-618.0869)"
                 style="fill:#000000;stroke:none">
                <svg:path
                   id="text3287"
                   style="font-size:176.79682922000001000px;font-style:italic;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;text-anchor:start;fill:#000000;fill-opacity:1;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Times New Roman"
                   d="M -1696.9368,-1259.709 L -1725.5523,-1165.7045 C -1727.2112,-1160.17 -1728.0406,-1156.1727 -1728.0406,-1153.7129 C -1728.0406,-1151.6444 -1727.2853,-1150.0651 -1725.7745,-1148.9749 C -1724.2638,-1147.8847 -1720.7238,-1147.1441 -1715.1547,-1146.7527 L -1716.0434,-1143.6499 L -1761.8105,-1143.6499 L -1760.5663,-1146.7527 C -1755.7083,-1146.8645 -1752.509,-1147.2279 -1750.9686,-1147.8429 C -1748.4803,-1148.8492 -1746.6141,-1150.1908 -1745.3699,-1151.868 C -1743.4148,-1154.4397 -1741.4301,-1159.0518 -1739.4157,-1165.7045 L -1718.7983,-1233.21 C -1717.5542,-1237.291 -1716.8729,-1239.639 -1716.7543,-1240.2541 C -1716.5766,-1241.3721 -1716.4878,-1242.4622 -1716.4877,-1243.5245 C -1716.4878,-1245.4811 -1717.0506,-1247.0184 -1718.1763,-1248.1367 C -1719.302,-1249.2547 -1720.8423,-1249.8138 -1722.7973,-1249.8139 C -1724.3378,-1249.8138 -1726.7372,-1249.4783 -1729.9957,-1248.8076 L -1731.1509,-1251.8264 L -1701.2025,-1259.709 L -1696.9368,-1259.709 z M -1658.4304,-1258.5258 L -1618.1731,-1258.5258 L -1622.2611,-1244.6892 L -1658.4304,-1244.6892 L -1665.0955,-1230.6849 C -1655.4978,-1229.2313 -1648.0329,-1226.4081 -1642.7008,-1222.2153 C -1634.4656,-1215.6744 -1630.3482,-1206.5339 -1630.3481,-1194.7939 C -1630.3482,-1187.023 -1631.8737,-1179.9929 -1634.9248,-1173.7036 C -1637.9759,-1167.4143 -1641.7085,-1162.1452 -1646.1221,-1157.8965 C -1650.536,-1153.6477 -1655.2608,-1150.2933 -1660.2966,-1147.8336 C -1667.1691,-1144.4792 -1674.1009,-1142.8021 -1681.0918,-1142.8021 C -1687.0755,-1142.8021 -1691.8152,-1144.032 -1695.3107,-1146.4918 C -1697.6805,-1148.169 -1698.8654,-1150.2933 -1698.8654,-1152.865 C -1698.8654,-1154.8776 -1698.0952,-1156.5967 -1696.5548,-1158.0222 C -1695.0145,-1159.4478 -1693.1186,-1160.1606 -1690.8673,-1160.1606 C -1687.7273,-1160.1606 -1684.2318,-1158.6791 -1680.3808,-1155.7162 C -1678.011,-1153.8713 -1676.0559,-1152.6414 -1674.5155,-1152.0264 C -1673.2713,-1151.5232 -1671.7903,-1151.2717 -1670.0722,-1151.2717 C -1663.3774,-1151.2717 -1657.2159,-1154.4163 -1651.5875,-1160.7057 C -1645.9593,-1166.995 -1643.1452,-1175.1711 -1643.1451,-1185.234 C -1643.1452,-1192.2222 -1644.6411,-1198.176 -1647.6329,-1203.0958 C -1650.6249,-1208.0153 -1654.4313,-1211.4815 -1659.0525,-1213.4941 C -1663.6737,-1215.5066 -1669.8352,-1216.9042 -1677.537,-1217.687 L -1658.4304,-1258.5258 z" />
              </svg:g>
              <svg:path
                 style="fill:#000000;fill-rule:evenodd;stroke:none;stroke-width:0.51757997"
                 d="M -2893.9432,-1387.4058 C -2904.4801,-1384.4764 -2913.9806,-1377.9283 -2922.7902,-1367.9339 C -2931.5997,-1357.7672 -2935.9181,-1346.5666 -2935.9181,-1334.5043 C -2935.9181,-1326.9224 -2933.3271,-1318.3065 -2928.3177,-1309.1737 C -2923.3084,-1299.8685 -2915.708,-1293.1481 -2905.862,-1288.8402 C -2902.58,-1288.1509 -2901.0254,-1286.4278 -2901.0254,-1284.0153 C -2901.0254,-1283.1537 -2902.2346,-1282.2921 -2905.1711,-1281.6029 C -2920.8901,-1285.5662 -2933.8453,-1294.0097 -2943.864,-1306.5889 C -2953.8827,-1319.3404 -2959.0648,-1333.8151 -2959.4102,-1350.3575 C -2958.892,-1368.1063 -2953.5372,-1384.6487 -2943.3458,-1399.8127 C -2932.9816,-1415.1489 -2919.6809,-1426.0049 -2903.4437,-1432.3807 L -2915.3625,-1493.5534 C -2941.9639,-1471.4968 -2963.5559,-1448.5785 -2980.3113,-1424.4541 C -2997.0667,-1400.5019 -3005.7035,-1374.482 -3006.3945,-1346.3942 C -3006.049,-1333.8151 -3003.458,-1321.5805 -2998.6213,-1309.8629 C -2993.7847,-1297.973 -2986.5298,-1287.2894 -2976.8566,-1277.4672 C -2957.3374,-1257.9954 -2931.9452,-1247.8286 -2901.0254,-1246.7947 C -2890.4885,-1247.484 -2879.2606,-1249.3795 -2867.1691,-1252.4812 L -2893.9432,-1387.4058 z M -2881.5062,-1389.129 L -2854.5594,-1256.6168 C -2827.958,-1267.3005 -2814.6573,-1290.5634 -2814.6573,-1326.0608 C -2816.2119,-1337.9507 -2819.6667,-1348.6344 -2825.5397,-1358.1118 C -2831.24,-1367.7616 -2838.8404,-1375.3436 -2848.5136,-1380.8577 C -2858.1868,-1386.3719 -2869.0692,-1389.129 -2881.5062,-1389.129 z M -2916.9171,-1568.3392 C -2911.2168,-1571.7855 -2904.6529,-1577.8166 -2897.5707,-1586.2602 C -2890.4885,-1594.5314 -2883.579,-1604.3535 -2877.0151,-1615.3819 C -2870.2784,-1626.5825 -2864.9235,-1637.9555 -2860.9506,-1649.5007 C -2856.9777,-1660.8737 -2855.0776,-1671.7297 -2855.0776,-1681.7241 C -2855.0776,-1686.032 -2855.423,-1690.34 -2856.2867,-1694.131 C -2856.9777,-1700.3344 -2858.8778,-1705.1593 -2862.1598,-1708.4333 C -2865.4417,-1711.535 -2869.5874,-1713.2582 -2874.7695,-1713.2582 C -2885.1337,-1713.2582 -2894.4614,-1706.8825 -2902.7528,-1694.131 C -2909.144,-1683.1026 -2914.4988,-1670.0065 -2918.299,-1655.1872 C -2922.2719,-1640.1956 -2924.5175,-1625.3763 -2924.863,-1610.3847 C -2923.9993,-1593.3252 -2921.2355,-1579.3675 -2916.9171,-1568.3392 z M -2927.7995,-1558.3448 C -2935.5726,-1586.2602 -2939.891,-1614.6926 -2940.7547,-1643.6419 C -2940.582,-1662.2522 -2938.6819,-1679.6563 -2935.0544,-1695.8541 C -2931.5997,-1712.052 -2926.5904,-1726.0097 -2920.0264,-1738.0719 C -2913.6351,-1750.1342 -2906.2075,-1759.267 -2897.9162,-1765.4704 C -2890.4885,-1770.9846 -2885.1337,-1773.914 -2882.1972,-1773.914 C -2879.9516,-1773.914 -2878.0515,-1773.0524 -2876.3241,-1771.5015 C -2874.5968,-1769.9507 -2872.3512,-1767.3659 -2869.5874,-1763.9196 C -2849.0318,-1734.7979 -2838.6676,-1699.6451 -2838.6676,-1658.6336 C -2838.6676,-1639.1617 -2841.2587,-1620.2068 -2846.4408,-1601.2518 C -2851.4501,-1582.4692 -2858.8778,-1564.5482 -2868.7237,-1547.8334 C -2878.7424,-1530.9463 -2890.4885,-1516.2993 -2904.1346,-1503.7201 L -2890.143,-1435.827 C -2882.5426,-1436.6886 -2877.3605,-1437.3779 -2874.424,-1437.3779 C -2861.2961,-1437.3779 -2849.55,-1434.6208 -2838.6676,-1429.1066 C -2827.7852,-1423.5925 -2818.4575,-1416.1828 -2810.8571,-1406.7054 C -2803.2567,-1397.4002 -2797.3837,-1386.7165 -2793.238,-1374.6543 C -2789.2651,-1362.5921 -2787.0195,-1350.0129 -2787.0195,-1336.9168 C -2787.0195,-1316.5833 -2792.3743,-1297.973 -2803.084,-1281.2582 C -2813.7936,-1264.5434 -2829.8581,-1252.3089 -2851.4501,-1244.3823 C -2850.0682,-1235.9387 -2847.6499,-1223.7042 -2844.0225,-1208.0233 C -2840.5677,-1192.1701 -2837.9767,-1179.5909 -2836.2493,-1170.2858 C -2834.522,-1160.9806 -2833.831,-1152.0201 -2833.831,-1143.2319 C -2833.831,-1129.6188 -2837.113,-1117.5566 -2843.677,-1106.8729 C -2850.4137,-1096.1892 -2859.396,-1087.918 -2870.7966,-1082.0592 C -2882.0244,-1076.2004 -2894.4614,-1073.271 -2907.9348,-1073.271 C -2926.9358,-1073.271 -2943.5185,-1078.6129 -2957.6829,-1089.1242 C -2971.8472,-1099.8079 -2979.4476,-1114.1103 -2980.1386,-1132.3759 C -2979.6204,-1140.4748 -2977.7203,-1148.0568 -2974.2656,-1155.2941 C -2970.8108,-1162.5315 -2966.1469,-1168.3903 -2960.1012,-1172.8705 C -2954.2281,-1177.5231 -2947.146,-1179.9355 -2939.0274,-1180.4525 C -2932.2906,-1180.4525 -2925.8994,-1178.557 -2919.8537,-1174.9383 C -2913.9806,-1171.1473 -2909.144,-1166.1501 -2905.5165,-1159.7744 C -2902.0618,-1153.3987 -2900.1617,-1146.3336 -2900.1617,-1138.7517 C -2900.1617,-1128.5849 -2903.6164,-1119.9691 -2910.5259,-1112.904 C -2917.4353,-1105.839 -2926.2449,-1102.2204 -2936.7818,-1102.2204 L -2940.7547,-1102.2204 C -2934.018,-1091.8813 -2922.9629,-1086.5395 -2907.5894,-1086.5395 C -2899.8162,-1086.5395 -2891.8704,-1088.2626 -2883.9245,-1091.3643 C -2875.8059,-1094.6384 -2869.0692,-1098.9463 -2863.3689,-1104.4605 C -2857.6686,-1109.9746 -2853.8684,-1115.8334 -2852.3138,-1122.0369 C -2849.3773,-1129.1019 -2847.9954,-1138.924 -2847.9954,-1151.1585 C -2847.9954,-1159.4298 -2848.8591,-1167.701 -2850.4137,-1175.9722 C -2851.9683,-1184.0712 -2854.3866,-1194.9272 -2857.6686,-1208.3679 C -2860.9506,-1221.6364 -2863.3689,-1231.9754 -2864.7508,-1239.0404 C -2875.115,-1236.4557 -2885.8246,-1235.0772 -2897.0525,-1235.0772 C -2915.8807,-1235.0772 -2933.6725,-1238.8681 -2950.428,-1246.6224 C -2967.1834,-1254.3767 -2981.8659,-1265.0604 -2994.6484,-1278.8458 C -3007.2582,-1292.6312 -3017.1041,-1308.1398 -3024.1863,-1325.7161 C -3031.0958,-1343.1202 -3034.7232,-1361.3859 -3034.896,-1380.3408 C -3034.205,-1397.9172 -3030.923,-1414.8043 -3024.7045,-1430.6575 C -3018.486,-1446.683 -3010.5402,-1461.847 -3000.6942,-1475.977 C -2990.8482,-1490.107 -2980.6568,-1503.0309 -2970.1199,-1514.5761 C -2959.4102,-1525.9491 -2945.4186,-1540.5961 -2927.7995,-1558.3448 z"
                 id="path9" />
            </svg:g>
          </svg:g>
          <svg:g
             transform="translate(0,-20)"
             id="g12025">
            <svg:path
               style="fill:#008000"
               d="M 890.39741,116.66881 C 894.65769,116.71919 897.01337,119.6431 895.65854,123.21887 C 894.3554,126.84817 889.84608,129.72012 885.63436,129.66816 C 881.4242,129.61935 879.07165,126.64506 880.42335,123.06929 C 881.72492,119.49195 886.18725,116.61842 890.39741,116.66881 z"
               id="path11018" />
            <svg:path
               style="fill:#008000"
               id="path11020"
               d="M 920.39741,109.66881 C 924.65769,109.71919 927.01337,112.6431 925.65854,116.21887 C 924.3554,119.84817 919.84608,122.72012 915.63436,122.66816 C 911.4242,122.61935 909.07165,119.64506 910.42335,116.06929 C 911.72492,112.49195 916.18725,109.61842 920.39741,109.66881 z" />
            <svg:path
               style="fill:#008000"
               d="M 950.39741,102.66881 C 954.65769,102.71919 957.01337,105.6431 955.65854,109.21887 C 954.3554,112.84817 949.84608,115.72012 945.63436,115.66816 C 941.4242,115.61935 939.07165,112.64506 940.42335,109.06929 C 941.72492,105.49195 946.18725,102.61842 950.39741,102.66881 z"
               id="path11022" />
            <svg:path
               style="fill:#008000"
               d="M 1010.3974,88.66881 C 1014.6577,88.71919 1017.0134,91.6431 1015.6585,95.21887 C 1014.3554,98.84817 1009.8461,101.72012 1005.6344,101.66816 C 1001.4242,101.61935 999.07165,98.64506 1000.4234,95.06929 C 1001.7249,91.49195 1006.1872,88.61842 1010.3974,88.66881 z"
               id="path11024" />
            <svg:path
               style="fill:#008000"
               id="path11026"
               d="M 1040.3974,81.66881 C 1044.6577,81.71919 1047.0134,84.6431 1045.6585,88.21887 C 1044.3554,91.84817 1039.8461,94.72012 1035.6344,94.66816 C 1031.4242,94.61935 1029.0716,91.64506 1030.4234,88.06929 C 1031.7249,84.49195 1036.1872,81.61842 1040.3974,81.66881 z" />
            <svg:path
               style="fill:#008000"
               d="M 1070.3974,74.66881 C 1074.6577,74.71919 1077.0134,77.6431 1075.6585,81.21887 C 1074.3554,84.84817 1069.8461,87.72012 1065.6344,87.66816 C 1061.4242,87.61935 1059.0716,84.64506 1060.4234,81.06929 C 1061.7249,77.49195 1066.1872,74.61842 1070.3974,74.66881 z"
               id="path11028" />
            <svg:path
               style="fill:#008000"
               id="path11030"
               d="M 1100.3974,67.66881 C 1104.6577,67.71919 1107.0134,70.6431 1105.6585,74.21887 C 1104.3554,77.84817 1099.8461,80.72012 1095.6344,80.66816 C 1091.4242,80.61935 1089.0716,77.64506 1090.4234,74.06929 C 1091.7249,70.49195 1096.1872,67.61842 1100.3974,67.66881 z" />
            <svg:path
               style="fill:#008000"
               d="M 1130.3974,60.66881 C 1134.6577,60.71919 1137.0134,63.6431 1135.6585,67.21887 C 1134.3554,70.84817 1129.8461,73.72012 1125.6344,73.66816 C 1121.4242,73.61935 1119.0716,70.64506 1120.4234,67.06929 C 1121.7249,63.49195 1126.1872,60.61842 1130.3974,60.66881 z"
               id="path11032" />
            <svg:path
               style="fill:#008000"
               id="path11034"
               d="M 1160.3974,53.66881 C 1164.6577,53.71919 1167.0134,56.6431 1165.6585,60.21887 C 1164.3554,63.84817 1159.8461,66.72012 1155.6344,66.66816 C 1151.4242,66.61935 1149.0716,63.64506 1150.4234,60.06929 C 1151.7249,56.49195 1156.1872,53.61842 1160.3974,53.66881 z" />
            <svg:path
               style="fill:#008000"
               d="M 1190.3974,46.66881 C 1194.6577,46.71919 1197.0134,49.6431 1195.6585,53.21887 C 1194.3554,56.84817 1189.8461,59.72012 1185.6344,59.66816 C 1181.4242,59.61935 1179.0716,56.64506 1180.4234,53.06929 C 1181.7249,49.49195 1186.1872,46.61842 1190.3974,46.66881 z"
               id="path11036" />
            <svg:path
               style="fill:#008000"
               id="path11040"
               d="M 860.39741,123.66881 C 864.65769,123.71919 867.01337,126.6431 865.65854,130.21887 C 864.3554,133.84817 859.84608,136.72012 855.63436,136.66816 C 851.4242,136.61935 849.07165,133.64506 850.42335,130.06929 C 851.72492,126.49195 856.18725,123.61842 860.39741,123.66881 z" />
            <svg:path
               d="M 977.37552,95.66886 C 982.65415,95.71774 987.64743,97.77036 988.02787,102.31547 C 988.31321,105.6403 984.03325,108.66886 978.75463,108.66886 C 973.42846,108.61998 968.48421,106.61622 968.05472,102.07264 C 967.76938,98.69741 972.09395,95.66886 977.37552,95.66886 z M 978.65655,107.5448 C 980.94066,107.5448 982.41489,105.29515 982.17712,103.48841 C 981.65252,99.92074 980.46511,97.03576 977.32499,97.03576 C 975.04235,97.03576 973.71082,99.33274 973.99762,101.14101 C 974.52223,104.71021 975.52087,107.5448 978.65655,107.5448 z"
               id="path11044"
               style="fill:#008000" />
            <svg:path
               id="path10935"
               d="M 830.397,130.66811 C 834.65728,130.71849 837.01296,133.6424 835.65813,137.21817 C 834.35499,140.84747 829.84567,143.71942 825.63395,143.66746 C 821.42379,143.61865 819.07124,140.64436 820.42294,137.06859 C 821.72451,133.49125 826.18684,130.61772 830.397,130.66811 z"
               style="fill:#008000" />
            <svg:path
               style="fill:#008000"
               id="path11936"
               d="M 1220.3974,39.66812 C 1224.6577,39.7185 1227.0134,42.64241 1225.6585,46.21818 C 1224.3554,49.84748 1219.8461,52.71943 1215.6344,52.66747 C 1211.4242,52.61866 1209.0716,49.64437 1210.4234,46.0686 C 1211.7249,42.49126 1216.1872,39.61773 1220.3974,39.66812 z" />
            <svg:path
               id="path11942"
               d="M 1250.397,32.667993 C 1254.6573,32.718373 1257.0129,35.642283 1255.6581,39.218053 C 1254.355,42.847353 1249.8456,45.719303 1245.6339,45.667343 C 1241.4238,45.618533 1239.0712,42.644243 1240.4229,39.068473 C 1241.7245,35.491133 1246.1868,32.617603 1250.397,32.667993 z"
               style="fill:#008000" />
            <svg:path
               style="fill:#008000"
               d="M 1280.397,25.668809 C 1284.6573,25.719189 1287.013,28.643099 1285.6582,32.218869 C 1284.355,35.848169 1279.8457,38.720119 1275.634,38.668159 C 1271.4238,38.619349 1269.0713,35.645059 1270.423,32.069289 C 1271.7245,28.491949 1276.1869,25.618419 1280.397,25.668809 z"
               id="path11950" />
          </svg:g>
        </svg:g>
      </svg:g>
      <svg:rect
         ry="2.9721658"
         rx="2.9697518"
         y="-34.831589"
         x="423.54123"
         height="28.999502"
         width="208.9995"
         id="rect2855"
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.00049782;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none" />
      <svg:rect
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.00049782;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none"
         id="rect3627"
         width="208.9995"
         height="28.999502"
         x="843.54126"
         y="-34.831589"
         rx="2.9697518"
         ry="2.9721658" />
      <svg:path
         id="path3649"
         d="M 1292.5411,-5.8316885 L 1266.4535,-5.8316885 C 1264.8399,-5.8316885 1263.5408,-7.1573106 1263.5408,-8.8039355 L 1263.5408,-31.859736 C 1263.5408,-33.506361 1264.8399,-34.831984 1266.4535,-34.831984 L 1292.4967,-34.831984"
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.99970496;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none" />
      <svg:path
         id="path3640"
         d="M -266.43704,388.16812 L -210.40672,388.16812 C -208.77365,388.16812 -207.45897,389.49372 -207.45897,391.14034 L -207.45897,414.19597 C -207.45897,415.84259 -208.77365,417.1682 -210.40672,417.1682 L -266.45905,417.1682"
         style="fill:none;fill-opacity:1;stroke:#000000;stroke-width:0.99992144;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none" />
      <svg:rect
         ry="2.9721658"
         rx="2.9697518"
         y="388.16843"
         x="3.541239"
         height="28.999502"
         width="208.9995"
         id="rect3633"
         style="opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:1.00049782;stroke-linecap:square;stroke-miterlimit:4;stroke-dasharray:none" />
    </svg:g>
  </svg:g>
</svg:svg>

<div style="margin-top:30px">

  <svg:svg width="139" height="15" xmlns:svg="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink">
    <svg:title><?print$vxht_alt?></svg:title>
    <svg:a xlink:href="<?print$vxht_url?>"><svg:use xlink:href="buttons-obj-inkscape.svg#xhtml-mathml-svg" /></svg:a>
  </svg:svg>

  <svg:svg width="90" height="15" xmlns:svg="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink">
    <svg:title><?print$vcss_alt?></svg:title>
    <svg:a xlink:href="<?print$vcss_url?>"><svg:use xlink:href="buttons-obj-inkscape.svg#css" /></svg:a>
  </svg:svg>

  <svg:svg width="90" height="15" xmlns:svg="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink">
    <svg:use xlink:href="buttons-obj-inkscape.svg#javascript" />
  </svg:svg>

</div>

</body>

</html>

