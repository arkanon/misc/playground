<?php

  $types['list']     = array( 'GB', 'GC', 'GCA', 'GCM', 'GCS', 'GCC', 'GE', 'GED', 'GFA', 'GG', 'GH', 'GIT', 'GJ', 'GLS', 'GL', 'GMC', 'GM', 'GMD', 'GMU', 'GPA', 'GP', 'GS', 'GSS', 'GTW', 'GO', 'GU', 'G!', 'GAT' );

  $variables['list'] = array( '@', '()', '>', '$', '?', '!' );

  $sect[1][ 1]['signs'  ]['list'] = array();
  $sect[1][ 1]['options']['list'] = array( 'd++', 'd+', 'd', 'd-', 'd--', 'd---', 'dx', 'd?', '!d', 'dpu' );
  $sect[1][ 2]['signs'  ]['list'] = array();
  $sect[1][ 2]['options']['list'] = array( 's+++:+++', 's++:++', 's+:+', 's:', 's-:-', 's--:--', 's---:---' );
  $sect[1][ 3]['signs'  ]['list'] = array();
  $sect[1][ 3]['options']['list'] = array( 'a+++', 'a++', 'a+', 'a', 'a-', 'a--', 'a---', 'a----', 'a-----', 'a?', '!a' );

  $sect[2][ 1]['signs'  ]['list'] = array();
  $sect[2][ 1]['options']['list'] = array( 'C++++', 'C+++', 'C++', 'C+', 'C', 'C-', 'C--', 'C---' );
  $sect[2][ 2]['signs'  ]['list'] = array( 'B', 'L', 'U', 'A', 'V', 'H', 'I', 'O', 'S', 'C', 'X', '*' );
  $sect[2][ 2]['options']['list'] = array( '++++', '+++', '++', '+', '', '-', '--', '---' );
  $sect[2][ 3]['signs'  ]['list'] = array();
  $sect[2][ 3]['options']['list'] = array( 'P+++++', 'P++++', 'P+++', 'P++', 'P+', 'P', 'P-', 'P--', 'P---', 'P!' );
  $sect[2][ 4]['signs'  ]['list'] = array();
  $sect[2][ 4]['options']['list'] = array( 'L+++++', 'L++++', 'L+++', 'L++', 'L+', 'L', 'L-', 'L--', 'L---' );
  $sect[2][ 5]['signs'  ]['list'] = array();
  $sect[2][ 5]['options']['list'] = array( 'E+++', 'E++', 'E+', 'E', 'E-', 'E--', 'E---', 'E----' );
  $sect[2][ 6]['signs'  ]['list'] = array();
  $sect[2][ 6]['options']['list'] = array( 'W+++', 'W++', 'W+', 'W', 'W-', 'W--' );
  $sect[2][ 7]['signs'  ]['list'] = array();
  $sect[2][ 7]['options']['list'] = array( 'N++++', 'N+++', 'N++', 'N+', 'N', 'N-', 'N--', 'N---', 'N----', 'N*' );
  $sect[2][ 8]['signs'  ]['list'] = array();
  $sect[2][ 8]['options']['list'] = array( 'o+++++', 'o++++', 'o+++', 'o++', 'o+', 'o', 'o-', 'o--' );
  $sect[2][ 9]['signs'  ]['list'] = array();
  $sect[2][ 9]['options']['list'] = array( 'K++++++', 'K+++++', 'K++++', 'K+++', 'K++', 'K+', 'K', 'K-', 'K--', 'K---', 'K----' );
  $sect[2][10]['signs'  ]['list'] = array();
  $sect[2][10]['options']['list'] = array( 'w+++++', 'w++++', 'w+++', 'w++', 'w+', 'w', 'w-', 'w--', 'w---' );
  $sect[2][11]['signs'  ]['list'] = array();
  $sect[2][11]['options']['list'] = array( 'O+++', 'O++', 'O+', 'O', 'O-', 'O--', 'O---', 'O----' );
  $sect[2][12]['signs'  ]['list'] = array();
  $sect[2][12]['options']['list'] = array( 'M++', 'M+', 'M', 'M-', 'M--' );
  $sect[2][13]['signs'  ]['list'] = array();
  $sect[2][13]['options']['list'] = array( 'V+++', 'V++', 'V+', 'V', 'V-', 'V--' );

  $sect[3][ 1]['signs'  ]['list'] = array();
  $sect[3][ 1]['options']['list'] = array( 'PS+++', 'PS++', 'PS+', 'PS', 'PS-', 'PS--', 'PS---' );
  $sect[3][ 2]['signs'  ]['list'] = array();
  $sect[3][ 2]['options']['list'] = array( 'PE+++', 'PE++', 'PE+', 'PE', 'PE-', 'PE--' );
  $sect[3][ 3]['signs'  ]['list'] = array();
  $sect[3][ 3]['options']['list'] = array( 'Y+++', 'Y++', 'Y+', 'Y', 'Y-', 'Y--', 'Y---' );
  $sect[3][ 4]['signs'  ]['list'] = array();
  $sect[3][ 4]['options']['list'] = array( 'PGP++++', 'PGP+++', 'PGP++', 'PGP+', 'PGP', 'PGP-', 'PGP--', 'PGP---', 'PGP----' );

  $sect[4][ 1]['signs'  ]['list'] = array();
  $sect[4][ 1]['options']['list'] = array( 't+++', 't++', 't+', 't', 't-', 't--', 't---', 't*' );
  $sect[4][ 2]['signs'  ]['list'] = array();
  $sect[4][ 2]['options']['list'] = array( '5++++', '5+++', '5++', '5+', '5', '5-', '5--' );
  $sect[4][ 3]['signs'  ]['list'] = array();
  $sect[4][ 3]['options']['list'] = array( 'X++++', 'X+++', 'X++', 'X+', 'X', 'X-', 'X--' );
  $sect[4][ 4]['signs'  ]['list'] = array();
  $sect[4][ 4]['options']['list'] = array( 'R+++', 'R++', 'R+', 'R', 'R-', 'R--', 'R---', 'R*' );
  $sect[4][ 5]['signs'  ]['list'] = array();
  $sect[4][ 5]['options']['list'] = array( 'tv+++', 'tv++', 'tv+', 'tv', 'tv-', 'tv--', '!tv' );
  $sect[4][ 6]['signs'  ]['list'] = array();
  $sect[4][ 6]['options']['list'] = array( 'b++++', 'b+++', 'b++', 'b+', 'b', 'b-', 'b--' );
  $sect[4][ 7]['signs'  ]['list'] = array();
  $sect[4][ 7]['options']['list'] = array( 'DI+++++', 'DI++++', 'DI+++', 'DI++', 'DI+', 'DI', 'DI-', 'DI--', 'DI---' );
  $sect[4][ 8]['signs'  ]['list'] = array();
  $sect[4][ 8]['options']['list'] = array( 'D++++', 'D+++', 'D++', 'D+', 'D', 'D-', 'D--', 'D---', 'D----' );
  $sect[4][ 9]['signs'  ]['list'] = array();
  $sect[4][ 9]['options']['list'] = array( 'G+++++', 'G++++', 'G+++', 'G++', 'G+', 'G', 'G-', 'G--' );

  $sect[5][ 1]['signs'  ]['list'] = array();
  $sect[5][ 1]['options']['list'] = array( 'e+++++', 'e++++', 'e+++', 'e++', 'e+', 'e', 'e-', 'e--', 'e*' );
  $sect[5][ 2]['signs'  ]['list'] = array();
  $sect[5][ 2]['options']['list'] = array( 'h++', 'h+', 'h', 'h-', 'h--', 'h---', 'h----', 'h!', 'h*' );
  $sect[5][ 3]['signs'  ]['list'] = array();
  $sect[5][ 3]['options']['list'] = array( 'r+++', 'r++', 'r+', 'r', 'r-', 'r--', 'r---', '!r', 'r*', 'r%' );
  $sect[5][ 4]['signs'  ]['list'] = array( 'x', 'y', 'z' );
  $sect[5][ 4]['options']['list'] = array( '+++++', '++++', '+++', '++', '+', '', '-', '--', '---', '*', '**', '!', '?', '!+' );

?>
