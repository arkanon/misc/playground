<?php

# $lang    = 'en';
  $lang    = 'pt_BR';

  $version = '3.12';
  $site    = 'http://www.geekcode.com/';
  $author  = 'Robert A. Hayden';
  $email   = 'rhayden@geekcode.com';

  $link['email' ] = "<a href=mailto:$email>$email</a>";
  $link['robert'] = "<a href=mailto:$email>$author</a>";
  $link['author'] = "<a href=mailto:$email>author</a>";

# $hsep    = "<td width=15></td>";
  $hsep    = "";

  $vsep    = "<tr><td height=15></td></tr>\n";

  $showval = true;
# $showval = false;

###

  $hsep2   = ($hsep=='') ? '' : '<td></td>';
  $ncols   = ($hsep=='') ? 3  : 5;
  if (!$showval) $ncols = $ncols - ( ($hsep=='') ? 1 : 2 );

  $style   = "style='margin: 0px; border: 0px solid black; padding: 5px; font-size: 12px; color: white; background-color: black;'";
  $style2  = "style='margin: 0px;'";
  $h1      = "h1";
  $h2      = "h3";
  $h3      = "h2";
  $h4      = "h3";

  $text    = '';

  include("codes.inc.php");
  include("locale/$lang/lc_messages.inc.php");

  $text .= "<tr><td>";
  $text .= "</td>";
  if ($showval)
  {
    $text .= "$hsep<td>";
    $text .= "</td>";
  }
  $text .= "$hsep<td>";
  $text .= "</td></tr>\n";

  $text .= "<tr><td colspan=$ncols><$h1>";
  $text .= $title;
  $text .= "</$h1></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= "<b>";
  $text .= "v$version<br>";
  $text .= "$date<br>";
  $text .= "$siteis <a href=$site target=_blank>$site</a><br>";
  $text .= "$authoring $author <<i>{$link['email']}</i>>";
  $text .= "</b>";
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $intro;
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols><$h2>";
  $text .= $note['title'];
  $text .= "</$h2></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $note['intro'];
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols><$h2>";
  $text .= $instructions['title'];
  $text .= "</$h2></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $instructions['intro'];
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols><$h2>";
  $text .= $index['title'];
  $text .= "</$h2></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $index['example'];
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= "
<dl>
<dd>
<a href=#type>GED/J</a>
<a href=#dress>d--</a>

<a href=#shape>s:++&gt;:</a> 
<a href=#age>a--</a>
<a href=#comp>C++(++++)</a>
<a href=#unix>ULU++</a>
<a href=#perl>P+</a>
<a href=#linux>L++ 
<a href=#emacs>E----</a>
<a href=#web>W+(-)</a>
<a href=#news>N+++</a>

<a href=#oracle>o+</a>
<a href=#kibo>K+++</a>
<a href=#win>w---</a>
<a href=#os2>O-</a>
<a href=#mac>M+</a>
<a href=#vms>V--</a>
<a href=#psoc>PS++>$</a>
<a href=#pecon>PE++>$</a>
<a href=#cypher>Y++</a>

<a href=#pgp>PGP++</a>
<a href=#trek>t-</a>
<a href=#b5>5+++</a>
<a href=#xfile>X++</a>
<a href=#role>R+++>$</a>
<a href=#tv>tv+</a>
<a href=#books>b+</a>
<a href=#dogbert>DI+++</a>
<a href=#descent>D+++</a>

<a href=#geek>G++++</a>
<a href=#ed>e++</a>
<a href=#house>h</a>
<a href=#relat>r--</a>
<a href=#sex>y++**</a>
</dl>
";
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $index['list'];
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= "<ul>";
  for ( $i=1; $i<=sizeof($sect); $i++ ) $text .= "<li><a href='#{$sect[$i]['name']}'>{$sect[$i]['title']}</a>";
  $text .= "<li><a href=#{$thecode['name']}>{$thecode['title']}</a>";
  $text .= "<li><a href=#administrivia>Administrivia</a>";
  $text .= "</ul>";
  $text .= "</td></tr>\n";
  $text .= $vsep;

  if ($showval)
  {

    $text .= "<tr><td colspan=$ncols><$h2>";
    $text .= $variables['title'];
    $text .= "</$h2></td></tr>\n";
    $text .= $vsep;

    $text .= "<tr><td colspan=$ncols>";
    $text .= $variables['intro'];
    $text .= "</td></tr>\n";
    $text .= $vsep;

    for ( $i=0; $i<sizeof($variables['list']); $i++ )
    {
      $text .= "<tr valign=top><td>";
      $text .= "<br>";
      $text .= "</td>";

      $text .= "$hsep2<td align=center><pre>";
      $text .= $variables['list'][$i];
      $text .= "</pre></td>";

      $text .= "$hsep2<td>";
      $text .= $variables['desc'][$i];
      $text .= "</td></tr>\n";
    }
    $text .= $vsep;

  }

  $text .= "<tr><td colspan=$ncols><$h2>";
  $text .= $types['title'];
  $text .= "</$h2></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $types['intro'];
  $text .= "</td></tr>\n";
  $text .= $vsep;

  for ( $i=0; $i<sizeof($types['list']); $i++ )
  {
    $text .= "<tr valign=top><td>";
    $text .= "<input type=checkbox name=type[$i] onclick=mkgc(this.form) $style2 value={$types['list'][$i]}>";
    $text .= "</td>";
    if ($showval)
    {
      $text .= "$hsep2<td><pre>";
      $text .= $types['list'][$i];
      $text .= "</pre></td>";
    }
    $text .= "$hsep2<td>";
    $text .= $types['desc'][$i];
    $text .= "</td></tr>\n";
  }
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols><$h1>";
  $text .= $sections['title'];
  $text .= "</$h1></td></tr>\n";
  $text .= $vsep;

  for ( $i=1; $i<=sizeof($sect); $i++ )
  {

    $text .= "<tr><td colspan=$ncols><$h3>";
    $text .= "<a name={$sect[$i]['name']}>{$sect[$i]['title']}</a>";
    $text .= "</$h3></td></tr>\n";
    $text .= $vsep;

    $text .= "<tr><td colspan=$ncols>";
    $text .= $sect[$i]['intro'];
    $text .= "</td></tr>\n";
    $text .= $vsep;

    for ( $j=1; $j<sizeof($sect[$i])-1; $j++ )
    {

      $text .= "<tr><td colspan=$ncols><$h4>";
      $text .= $sect[$i][$j]['title'];
      $text .= "</$h4></td></tr>\n";
      $text .= $vsep;

      $text .= "<tr><td colspan=$ncols>";
      $text .= $sect[$i][$j]['intro'];
      $text .= "</td></tr>\n";
      $text .= $vsep;

      for ( $k=0; $k<sizeof($sect[$i][$j]['signs']['list']); $k++ )
      {
        $text .= "<tr valign=top><td>";
        $text .= "<input type=radio name=sect[$i][$j]['sign'] onclick=mkgc(this.form) $style2 value={$sect[$i][$j]['signs']['list'][$k]}>";
        $text .= "</td>";
        if ($showval)
        {
          $text .= "$hsep2<td><pre>";
          $text .= $sect[$i][$j]['signs']['list'][$k];
          $text .= "</pre></td>";
        }
        $text .= "$hsep2<td>";
        $text .= $sect[$i][$j]['signs']['desc'][$k];
        $text .= "</td></tr>\n";
      }
      if ( sizeof($sect[$i][$j]['signs']['list'])>0 )
      {
        $text .= $vsep;
      }

      for ( $k=0; $k<sizeof($sect[$i][$j]['options']['list']); $k++ )
      {
        $text .= "<tr valign=top><td>";
        $text .= "<input type=radio name=sect[$i][$j]['opt'] onclick=mkgc(this.form) $style2 value={$sect[$i][$j]['options']['list'][$k]}>";
        $text .= "</td>";
        if ($showval)
        {
          $text .= "$hsep2<td><pre>";
          $text .= $sect[$i][$j]['options']['list'][$k];
          $text .= "</pre></td>";
        }
        $text .= "$hsep2<td>";
        $text .= $sect[$i][$j]['options']['desc'][$k];
        $text .= "</td></tr>\n";
      }
      $text .= $vsep;

      if ($sect[$i][$j]['obs']!='')
      {
        $text .= "<tr><td colspan=$ncols>";
        $text .= $sect[$i][$j]['obs'];
        $text .= "</td></tr>\n";
        $text .= $vsep;
      }

    }

  }

  $text .= "<tr><td colspan=$ncols><$h1>";
  $text .= "<a name=display>{$thecode['title']}</a>";
  $text .= "</$h1></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $thecode['str']['alg'];
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $thecode['str']['code']  = "";
# Example
# $thecode['str']['code'] .= "GED/J d-- s:++>: a-- C++(++++) ULU++ P+ L++ E---- W+(-) N+++ o+ K+++ w---\n";
# $thecode['str']['code'] .= "O- M+ V-- PS++>$ PE++>$ Y++ PGP++ t- 5+++ X++ R+++>$ tv+ b+ DI+++ D+++\n";
# $thecode['str']['code'] .= "G+++++ e++ h r-- y++**";

  $text .= "<tr><td colspan=$ncols><textarea name=gc rows=2 cols=80 readonly $style>";
  $text .= $thecode['str']['code'];
  $text .= "</textarea></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $thecode['block']['alg'];
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $thecode['block']['code']  = "";
# $thecode['block']['code'] .= "-----BEGIN GEEK CODE BLOCK-----\n";
# $thecode['block']['code'] .= "Version: $version\n";
# $thecode['block']['code'] .= $thecode['str']['code'];
# $thecode['block']['code'] .= "------END GEEK CODE BLOCK------";

  $text .= "<tr><td colspan=$ncols><textarea name=gcb rows=5 cols=80 readonly $style>";
  $text .= $thecode['block']['code'];
  $text .= "</textarea></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $thecode['obs'];
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols><$h2>";
  $text .= "<a name=administrivia>{$mirrors['title']}</a>";
  $text .= "</$h2></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $mirrors['intro'];
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= "<b>{$mirrors['list']['title']}</b>";
  $text .= "</td></tr>\n";

  $text .= "<tr><td colspan=$ncols>";
  for ($i=1; $i<=sizeof($mirrors['list']['item']); $i++)
  {
    $url   = $mirrors['list']['item'][$i]['url'];
    $text .= "<dd><a href=$url>$url</a> <b>({$mirrors['list']['item'][$i]['comment']})</b>\n";
  }
  $text .= "</td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols><$h2>";
  $text .= $copyright['title'];
  $text .= "</$h2></td></tr>\n";
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= $copyright['intro'];
  $text .= "</td></tr>\n";
  $text .= $vsep;
  $text .= $vsep;

  $text .= "<tr><td colspan=$ncols>";
  $text .= "<i>{$copyright['note']}</i>";
  $text .= "</td></tr>\n";

  print "
<html>

<head>
  <title></title>
  <style>a{text-decoration:none}</style>
  <script language=javascript>

  function mkgc(form)
  {
    with(form)
    {
      gc.value = '';
      for (i=0; i<elements.length; i++)
      { 
        if (elements[i].checked) gc.value += elements[i].value + ' ';
      }

      gcb.value  = '';
      gcb.value += '-----BEGIN GEEK CODE BLOCK-----\\n';
      gcb.value += 'Version: $version\\n';
      gcb.value += gc.value + '\\n';
      gcb.value += '------END GEEK CODE BLOCK------';
    }
  }

  </script>
</head>

<body background=bwcircuit.gif>
<form name=gcform method=post>
<table border=0 cellpadding=0 cellspacing=0>
$text
</table>
</form
</body>

</html>
";

?>
