
podman run --rm -ti -p 8080:80 --name apache-php -v $PWD:/var/www/html:Z php:8.4-apache bash
podman run --rm -d  -p 8080:80 --name apache-php -v $PWD:/var/www/html:Z php:8.4-apache

cat /etc/os-release # PRETTY_NAME="Debian GNU/Linux 12 (bookworm)"         <http://hub.docker.com/_/debian>
apachectl -v        # Server version: Apache/2.4.62 (Debian)               <http://httpd.apache.org>
php --version       # PHP 8.4.3 (cli) (built: Jan 17 2025 17:31:49) (NTS)  <http://php.net/downloads.php>

http://localhost:8080/sg

podman rm -ft0 apache-php

