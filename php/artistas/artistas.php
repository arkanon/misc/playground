<?php

  # <http://svl.lsd.org.br:8080/artistas/>
  $api_key   = 'ABQIAAAAMYOZrYE051cfXJKTqFkrGBTLJMZncXhYwnVqq2UxOP3MFj4wFRQEBAYUdZZCBeoB-9Q-PpbZlEWtOw';

  $artists   = include('base.php');

  $wikipedia = "http://en.wikipedia.org/favicon.ico";
  $google    = "http://www.google.com/favicon.ico";

  foreach(array_keys($artists) as $group)
  {

    $list .= "  <h2>$group</h2>\n\n";
    $list .= "  <ol>\n";

    foreach($artists[$group] as $name)
    {

      $id  =                        strtolower(str_replace(' ','-',$name));
      $wp  = "http://pt.wikipedia.org/wiki/" . str_replace(' ','_',$name);
      $img = "http://google.com/images?q="   . str_replace(' ','+',$name) . "+art";

      $list .= "
    <li>
      <a href=\"$wp\" target=\"_blank\"><img src=\"$wikipedia\" style=\"margin-right:10px;\" /></a>
      <a href=\"$img\" target=\"_blank\" id=\"$id\" onmouseover=\"preview('$id','$name')\"><img src=\"$google\" style=\"margin-right:5px;\" />$name</a>
    </li>
";

    }

    $list .= "\n  </ol>\n";

  }

?>
<html>

<head>

  <title>Artistas</title>

  <meta http-equiv="content-type" content="text/html;charset=utf-8" />

  <style>

    body
    {
      font-family: sans-serif;
      font-size: 9pt;
    }

    a
    {
      text-decoration: none;
      outline: none;
    }

    a img
    {
      margin: 2px;
      border: 0;
      vertical-align: middle;
    }

    #preview
    {
    }

    div.img
    {
      display: inline-block;
      margin-top: 10px;
      background: white;
    }

    img.img
    {
      border: 1px solid black;
      margin: 5px;
      padding: 3px;
      background: white;
      vertical-align: top;
    }

    #tt
    {
      position: absolute;
      margin-left: 10px;
    }

  </style>

  <!-- http://sixrevisions.com/tutorials/javascript_tutorial/create_lightweight_javascript_tooltip/ -->
  <script src="http://sandbox.scriptiny.com/tooltip/script.js"></script>

  <!-- http://code.google.com/intl/pt-BR/apis/imagesearch/v1/ -->
  <!-- http://code.google.com/apis/ajax/playground/#size_restriction -->
  <script src="http://google.com/jsapi?key=<?=$api_key?>"></script>

  <script>

    var content = 'preview';

    google.load('search','1');

    function preview(id, search_keys)
    {
      tooltip.show('<div id="'+content+'"></div>');
      var imageSearch = new google.search.ImageSearch();
      imageSearch.setResultSetSize(6);
      imageSearch.setRestriction
      (
        google.search.ImageSearch.RESTRICT_IMAGESIZE,
     // google.search.ImageSearch.IMAGESIZE_MEDIUM
        google.search.ImageSearch.IMAGESIZE_LARGE
      );
      imageSearch.setSearchCompleteCallback(this, searchComplete, [imageSearch]);
      imageSearch.execute(search_keys);
      document.getElementById(id).onmouseout = function(){tooltip.hide()};
    }

    function searchComplete(searcher)
    {
      if (searcher.results && searcher.results.length > 0)
      {
        var contentDiv = document.getElementById(content);
        contentDiv.innerHTML = '';
        var results = searcher.results;
        var max_results = results.length;
     // var max_results = 1;
        for (var i = 0; i < max_results; i++)
        {
          var result = results[i];
          var newDiv = document.createElement('div');
          var newImg = document.createElement('img');
          newImg.src = result.tbUrl;
          newImg.setAttribute('class','img');
          newDiv.setAttribute('class','img');
          newDiv.appendChild(newImg);
          contentDiv.appendChild(newDiv);
        }
      }
    }

  </script>

</head>

<body>

<?=$list?>

</body>

</html>
