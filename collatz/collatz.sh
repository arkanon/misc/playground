#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/08/17 (Tue) 00:02:52 -03
# 2019/12/29 (Sun) 01:10:17 -03
# 2017/09/21 (Thu) 11:42:30 BRT
# 2017/09/19 (Tue) 03:41:56 BRT

# The Collatz Conjecture
#
# <http://pagez.com/4280/5-simple-math-problems-no-one>
# <http://youtube.com/watch?v=5mFpVDpKX70> Numberphile - UNCRACKABLE? The Collatz Conjecture
# <http://youtube.com/watch?v=094y1Z2wpJg> Veritasium  - The Simplest Math Problem No One Can Solve
# <http://datacarrierdetect.wordpress.com/2019/12/29/hailstone-sequence>
#
# eg: 63728127 -> 949

if (( $# == 0 ))
then
  echo "Usage: ${0##*/} 0 | [start] <stop|=> [-v]"
  exit
fi

dF="%Y/%m/%d %a %H:%M:%S.%3N %Z"
tF="%s.%3N"

# dir=$HOME
  dir=.

export LC_ALL=C
export TIMEFORMAT=%lR

DONE=false

trap DONE=true HUP INT QUIT ILL ABRT ALRM TERM EXIT

if [[ $* = 0 ]]
then
  l=$dir/${0##*/}.log
# i=$(( 1 + 10#0$(grep -vE 'Start|Stop|Time|^$' $l 2> /dev/null | tail -n1 | cut -f1) ))
  i=$(( 1 + 10#0$(tail -n1 $l 2> /dev/null | cut -d\  -f1) ))
# f=0
  f=21951144
  v=true
  echo=echo
else
  l=/dev/stdout
  if [[ $1 =~ ^[0-9]+$ && ( $2 =~ ^[0-9]+$ || $2 = = ) ]]
  then
    i=$1
    f=$2
    shift 2
  else
    i=1
    if [[ $1 =~ ^[0-9=]+$ || $1 = = || $1 = 0 ]]
    then
      f=$1
      shift
    fi
  fi
  [[ $f ==  = ]] && f=$i
  [[ $1 == -v ]] && v=true || v=false
  echo=
fi

echo "Output in $l" > /dev/stderr

{

# exec 2>&1

# echo -n "Start: "
# date +"$dF"

# time \
# {
    while (( f != 0 && i <= f )) || [[ $f = 0 ]]
    do
#     echo -en "$i\t"
#     I=0
      n=$i
#     time \
#     {
        while (( n > 1 ))
        do
          $v && (( n < i )) && echo -n ">"
          $v && echo -n "$n "
          (( n < i )) && break
          (( n%2 ==  0 )) && n=$((n/2)) || n=$((n*3+1))
#         (( I++ ))
        done
#       $v && echo -en "\t"
        echo "$I"
#       echo -en "$I\t"
#     }
      ((i++))
      $DONE && break
    done
#   echo -n "Stop: "
#   date +"$dF"
#   echo -n "Time: "
# }
# $echo

} >> $l

# awk '$0=NR" "NF' $l

# EOF
