# vim: ft=sh

time bc    <<< 'scale=10^6;  sqrt(2)'      > sqrt2_10^6 # 206m1,192s
time bc    <<< 'scale=10^6; (sqrt(5)+1)/2' >   phi_10^6 # 200m6,615s
time bc -l <<< 'scale=10^6;   e(1)'        >     e_10^6 #
time bc -l <<< 'scale=10^6; 4*a(1)'        >    pi_10^6 #

sed -zi 's/\n//' *10^6

count=$HOME/git/playground/pi/count.sh

$count sqrt2_10^6 # 500.601 dígitos de 1.000.002 são pares (50,0599998800%) 0m8,606s
$count   phi_10^6 # 500.049 dígitos de 1.000.002 são pares (50,0047999904%) 0m8,032s

# EOF
