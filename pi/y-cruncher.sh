#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/02/18 (Thu) 20:00:42 -03
# 2021/02/18 (Thu) 03:43:58 -03
# Arkanon <$username@$domain>

  yv=0.7.8.9507
  yd=y-cruncher-$yv-dynamic

  om=8 # ordem de magnitude da precisão (10^$om)
  th=$(grep -c ^processor /proc/cpuinfo) # threads a serem abertas para a contagem

  conts=(
          e
          pi
          phi
          log:2
          log:3
          sqrt:2
          sqrt:3
        )

  (( UID>0 )) && { sudo $0; exit; }

  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/$yd/Binaries

  mkdir -p calculated/split

  for c in ${conts[*]}; { $yd/y-cruncher custom $c -dec:$((10**om)) -hex:0 -o calculated ; }

  grep 'Computation Time' calculated/*20* \
  | sed -r '
             s|^[^/]+/|  # |
             s/-.+e: +/\t/
             s/\b[0-9]\b/0&/
             s/goldenratio/phi/
             s/ seconds//
           ' \
  | column -ts$'\t'
  # e      02.753
  # phi    00.974
  # log2   24.096
  # log3   27.975
  # pi     09.302
  # sqrt2  00.967
  # sqrt3  00.963

  rename  's/[() ]|primary//g;y/[A-Z]/[a-z]/'  calculated/*
  chmod   644                                  calculated/*
  chown   $SUDO_UID:$SUDO_GID                  calculated/*

  # averiguação da paridade

  # v1
  for i in calculated/*dec*; { echo -e "\n${i##*/}"; ./count.sh $i; }; echo
  # e       49.996.793 dígitos de  99.999.997 são pares ( 49,99 67944999 % )  13:13,800
  # phi     50.005.813 dígitos de  99.999.999 são pares ( 50,00 58135000 % )  13:12,314
  # log  2  49.997.085 dígitos de 100.000.001 são pares ( 49,99 70845000 % )  12:52,208
  # log  3  50.004.205 dígitos de 100.000.000 são pares ( 50,00 42050000 % )  13:09,245
  # pi      50.006.474 dígitos de 100.000.002 são pares ( 50,00 64729998 % )  13:08,904
  # sqrt 2  49.998.597 dígitos de 100.000.001 são pares ( 49,99 85965000 % )  14:13,943
  # sqrt 3  49.999.389 dígitos de  99.999.998 são pares ( 49,99 93899999 % )  13m18,429s

  # v2

  count()
  {
    local s=0 REPLY
    while read -N1
    do
      (( REPLY%2 == 0 )) && (( s++ ))
    done < $1
    echo $s
  }

  for i in calculated/*dec*
  {
    echo -e "\n${i##*/}"
    cut -d. -f2 $i | split -d -a1 -b$((10**om/th)) - calculated/split/
    rm calculated/split/$th # a princípio sobra uma nova linha no split que fica sozinho em um último arquivo
    for j in calculated/split/*
    {
      time count $j &
    }
  }
  #  6.249.374  01:45,647
  #  6.248.470  01:47,876
  #  6.249.329  01:47,911
  #  6.252.484  01:48,346
  #  6.248.175  01:48,504
  #  6.251.053  01:49,167
  #  6.251.524  01:49,790
  #  6.246.384  01:50,283
  # 49.996.793

# EOF
