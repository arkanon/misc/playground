#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/07/11 (Sat) 06:07:43 -03
# 2017/01/08 (Sun) 18:31:49 BRD
# 2017/01/07 (Sat) 20:51:00 BRD
# 2017/01/07 (Sat) 14:17:00 BRD

# sudo apt -y install pi primesieve-bin

dg=10
dl=10
dp=1

(( $# == 0 )) && { echo "
Usage: ${0##*/} <\$last> [\$first] [\$group] [\$line] [int]

  \$last   𝜋 decimal digit position (warning: increase carefully)
  \$first  position, ≥ 1. Def: $dp
  \$group  digits length. Def: $dg
  \$line   groups length. Deg: $dl
   int    include integer digit (3). Def: no include

   Eg:

     dpg=8   # digits per group
     gpl=16  # groups per line

     ${0##*/} 16384 1 \$dpg \$gpl

     li=108 # initial line
     gi=14  # initial group
     pi=7   # initial position in group (1..dpg)
     di=\$((dpg*gpl*(li-1)+dpg*(gi-1)+pi)) # initial digit
     echo \$di

     ${0##*/} \$di \$di \$dpg \$gpl
     ${0##*/} \$di \$di \$dpg \$gpl int
"; exit; }

N=${1}           # posição do último   dígito a ser calculado
p=${2:-$dp}      # posição do primeiro dígito a ser calculado
g=${3:-$dg}      # dígitos por grupo
l=${4:-$dl}      # grupos  por dígito
i=${5}           # 'int' se for para considerar a parte inteira da constante na formação do inteiro a ser testado como primo
l=$((l*g))       # comprimento da linha
L=$((1+(p-1)/l)) # linha inicial

(( p <  1 )) && { echo "First 𝜋 decimal digit position must be ≥ 1"; exit; }
(( p >  N )) && { echo "First 𝜋 decimal digit position must be ≤ the last one"; exit; }

echo

if [ "$i" = int ]
then
  pi $((N+0)) | tr  -d "."        | sed -r "s/.{$l}/&\n/g" | sed -r "s/.{$g}/& /g;/^$/d" | cat -n | { ((p>1)) && tail -n+$((L-1)) || cat; }
else
  echo -e "        3."
  ((L>2)) && echo "        ..."
  pi $((N+1)) | cut -d "." -f2    | sed -r "s/.{$l}/&\n/g" | sed -r "s/.{$g}/& /g;/^$/d" | cat -n | { ((p>1)) && tail -n+$((L-1)) || cat; }
fi

echo

((p>1)) && printf "%0*d" $((p-1)) 0 | tr 0 ? | sed -r "s/.{$l}/&\n/g" | sed -r "s/.{$g}/& /g;/^$/d" | cat -n | { ((p>1)) && tail -n+$((L-1)) || cat; }

(( (p-1)%l == 0 )) && printf "%6d  " $L

trap "echo" SIGINT SIGTERM EXIT

time \
(
  TIMEFORMAT="%lR"
  while ((p<=N))
  do
    n=$( [ "$i" = int ] && tr -d "." <<< $( pi $((p)) ) || cut -d "." -f2 <<< $( pi $((p+1)) ) )
  # r=$( cut -d " " -f3 <<< $( primesieve -q -s32 $n $n ) )
    maxima --disable-readline --very-quiet --batch-string="primep($n);" | tail -n1 | grep -qv true; r=$?
    echo -n $r | tr 0 -
    (( p%l == 0 && p < N )) && { ((L++)); printf "\n%6d  " $L; } || { (( p%g == 0 && p%l != 0 )) && echo -n " "; }
    ((p++))
  done
  echo -en "\n\n"
)

# EOF
