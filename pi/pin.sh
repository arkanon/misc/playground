#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2017/04/25 (Tue) 02:42:31 -03
# 2017/04/24 (Mon) 23:14:40 -03

# <http://turner.faculty.swau.edu/mathematics/materialslibrary/pi/pibases.html>
# <http://en.wikipedia.org/wiki/Bailey%E2%80%93Borwein%E2%80%93Plouffe_formula#BBP_digit-extraction_algorithm_for_.CF.80>
# <http://pythonfiddle.com/bailey-borwein-plouffe-formula/>
#
#  𝜋₁₆ ≈ 3. 24 3F 6A 88 85 A3 08 D3 13 19 8A 2E 03 70 73 44 A4 09 38 22 29 9F 31 D0 08 2E FA 98 EC 4E 6C 89 45 28 21 E6 38 D0 13 77 BE 54 66 CF 34 E9 0C 6C C0 AC
#
# 1 -- 021d48666380e6
# 2 -- 00b2744515fc49



  scale=50



  S()
  {

    local j=$1
    local n=$2

    # left sum
    local s=0
    local k=0
    local r

    while ((k<=n))
    do
      r=$((8*k+j))
      p=$( bc <<< "16^($n-$k)%$r" )
      s=$( bc <<< "scale=$scale; $s+$p/$r" )
# echo $s > /dev/stderr
      ((k++))
    done

    # right sum
    local t=0
          k=$((n+1))
    local newt

    # iterate until t no longer changes
    while ((t!=newt))
    do
      r=$((8*k+j))
      newt=$( bc <<< "$t+(16^($n-$k))/$r" )
      t=$newt
      ((k++))
    done

    bc <<< "$s+$t"

  }



  n=$(($1-1))
  x=$(bc <<< "4*$(S 1 $n) - 2*$(S 4 $n) - 1*$(S 5 $n) - 1*$(S 6 $n)" )
  echo $x
  printf "%014x\n" $(bc <<< "$x*16^14/1")



# # <http://stackoverflow.com/a/28794604>
# pi=0
# for n in $(seq 0 $scale)
# {
#    u=$(bc <<< "scale=$scale; 4/(8*$n+1) - 2/(8*$n+4) - 1/(8*$n+5) - 1/(8*$n+6)")
#    d=$(bc <<< "scale=$scale; 16^$n")
#   pi=$(bc <<< "scale=$scale; $pi + $u/$d")
# }
# echo $pi
# bc -l <<< "scale=$scale; 4*a(1)"



# EOF
