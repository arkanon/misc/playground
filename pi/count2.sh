#!/bin/bash

  time \
  {
    s=0
    while read -N1
    do
      (( REPLY%2 == 0 )) && (( s++ ))
    done < $1
    echo $s
  }

# EOF
