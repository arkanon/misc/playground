#!/bin/bash

  f=$1 # input file
  p=10 # precision

  stty -echoctl

  trap double_ctrl_c INT EXIT

  double_ctrl_c()
  {
    sleep .2 \
    && printf  "%'i dígitos de %'i são pares (%s%%)\n"  $s  $((--n))  $(bc <<< "scale=$p;100*$s/$n" | tr . ,) \
    || break=true
  }

  break=false

  time \
  {
    n=1
    s=0
    while read -N1
    do
      (( n > 2 && REPLY%2 == 0 )) && (( s++ ))
    # echo "dígito $n: $REPLY ($s)"
      (( n++ ))
      $break && break
    done < $f
  }

  stty echoctl

# EOF
