# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2018/02/04 (Sun) 01:43:35 -02

# <http://bellard.org/pi/> / <https://goo.gl/gb6MDb>
wget http://bellard.org/pi/pi1.c

# gcc will not properly include math.h <http://stackoverflow.com/a/11336610>
gcc -static pi1.c -lm -o pi1

# The string 79873884 occurs in 𝜋 at position 79873884, counting from 1st digit after the decimal point <http://fb.com/arkanon/posts/10210418896140030>
time ./pi1 79873884



# https://goo.gl/gb6MDb
wget https://goo.gl/42GPa4 -O pi1.c
gcc -static pi1.c -lm -o pi1
time ./pi1 79873884
# calculating... (Intel Core2 Duo CPU T6570 @ 2.10GHz)



# EOF
