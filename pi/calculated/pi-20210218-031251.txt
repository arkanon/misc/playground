﻿Benchmark Validation File - DO NOT MODIFY! If you do, it will fail validation.

Validation Version:    1.3

Program:               y-cruncher v0.7.8 Build 9507
Tuning:                13-HSW ~ Airi

User:                  None Specified - You can edit this in "Username.txt".

Operating System:      Linux 5.4.0-65-generic x86_64

Processor(s):
    Name:              Intel(R) Core(TM) i7-4770K CPU @ 3.50GHz
    Logical Cores:     8
    Physical Cores:    4
    Sockets:           1
    NUMA Nodes:        1
    Base Frequency:    3,491,866,336 Hz

Motherboard:
    Manufacturer:      Gigabyte Technology Co., Ltd.
    Model:             Z87X-OC-CF
    Version:           To be filled by O.E.M.
    Serial Number:     Suppressed - Personally identifiable information is opt-in only.

Memory:
    Usable Memory:     33,559,498,752 (31.3 GiB)
    Total Memory:      Unable to Detect

Constant:              Pi
Algorithm:             Chudnovsky (1988)
Decimal Digits:        100,000,000
Hexadecimal Digits:    Disabled
Computation Mode:      Ram Only
Threading Mode:        Push Pool  ->  8 / ?  (randomization on)
Working Memory:        495,699,072 ( 473 MiB)
Total Memory:          689,369,472 ( 657 MiB)

Configuration Dump:
{
    Constant : {
        Constant : "pi"
        Algorithm : "chudnovsky"
    }
    ComputeSize : {
        DecimalDigits : 100000000
        EnableHexDigits : "false"
    }
    Output : {
        Path : "calculated"
        OutputEnable : "true"
        DigitsPerFile : 0
    }
    OutputVerify : "true"
    Mode : "ram"
    Parallelism : {
        TaskDecomposition : 8
        Framework : "pushpool"
        WorkerThreads : 0
        Randomization : "true"
        MaxSequentialDispatch : 64
    }
    Allocator : {
        Allocator : "mmap"
        LargePages : "attempt"
        LockedPages : "attempt"
    }
}

Start Date:            Thu Feb 18 03:12:36 2021
End Date:              Thu Feb 18 03:12:51 2021

Total Computation Time:    9.302 seconds
Start-to-End Wall Time:    14.805 seconds

CPU Utilization:           760.36 %  +  1.18 % kernel overhead
Multi-core Efficiency:     95.04 %  +  0.15 % kernel overhead

Last Decimal Digits:
9948682556 3967530560 3352869667 7734610718 4471868529  :  99,999,950
7572203175 2074898161 1683139375 1497058112 0187751592  :  100,000,000

SHA256-dec(100,000,001 - 100,000,100): 7b6f04b385312a5839329e7040de5aa1e83b9fe2fcef9c14cd24b4c55c0cef0b

Dec Counts: {9999922,10002475,10001092,9998442,10003863,9993478,9999417,9999610,10002180,9999521}

Dec Hash: Floor(|x| * 10^dec) mod (2^61 - 1) = 199405406068015690

Spot Check:                 Good through 100,000,000
Timer Sanity Check:         Passed
Frequency Sanity Check:     Disabled in this version of y-cruncher
Reference Clock:            TSC
Reference Clock 0:          0
Reference Clock 1:          98,849,949
Reference Clock 2:          17,013,022
Is Debugger Present:        Unknown
Is Contiguous:              Yes
ECC Recovered Errors:       0
Colors:                     Yes

Event Log:
Thu Feb 18 03:12:36 2021	0.033	Working Memory
Thu Feb 18 03:12:37 2021	0.250	Working Memory:  474 MiB  (locked, spread: 100%/1)
Thu Feb 18 03:12:37 2021	0.250	Twiddle Tables
Thu Feb 18 03:12:37 2021	0.382	Twiddle Tables:  185 MiB  (locked, spread: 100%/1)
Thu Feb 18 03:12:37 2021	0.382	Begin Computation
Thu Feb 18 03:12:37 2021	0.382	Series CommonP2B3...  7,051,378 terms  (Expansion Factor = 2.488)
Thu Feb 18 03:12:37 2021	0.382	Series: A ( 12 ) 0.000%
Thu Feb 18 03:12:37 2021	0.401	Series: A ( 11 ) 0.578%
Thu Feb 18 03:12:37 2021	0.473	Series: A ( 10 ) 2.047%
Thu Feb 18 03:12:37 2021	0.554	Series: E ( 9 ) 3.517%
Thu Feb 18 03:12:37 2021	0.646	Series: E ( 8 ) 4.988%
Thu Feb 18 03:12:37 2021	0.769	Series: E ( 7 ) 6.926%
Thu Feb 18 03:12:37 2021	0.945	Series: E ( 6 ) 9.619%
Thu Feb 18 03:12:38 2021	1.217	Series: E ( 5 ) 13.364%
Thu Feb 18 03:12:38 2021	1.588	Series: E ( 4 ) 18.576%
Thu Feb 18 03:12:38 2021	2.146	Series: E ( 3 ) 25.843%
Thu Feb 18 03:12:39 2021	2.918	Series: E ( 2 ) 35.994%
Thu Feb 18 03:12:40 2021	3.976	Series: E ( 1 ) 50.235%
Thu Feb 18 03:12:42 2021	5.637	Series: E ( 0 ) 70.379%
Thu Feb 18 03:12:44 2021	7.921	Finishing Series
Thu Feb 18 03:12:44 2021	7.947	Large Division
Thu Feb 18 03:12:45 2021	8.469	InvSqrt(10005)...
Thu Feb 18 03:12:45 2021	8.788	Large Multiply
Thu Feb 18 03:12:45 2021	9.033	Base Converting
Thu Feb 18 03:12:46 2021	9.684	Writing Decimal Digits
Thu Feb 18 03:12:48 2021	11.422	Verifying Base Conversion
Thu Feb 18 03:12:48 2021	11.720	Verifying Decimal Output
Thu Feb 18 03:12:51 2021	14.805	End Computation

----

Checksum0: bbcb4b3438fd4991876aa4adb5be91db34282e0fcadcbd6f3891152776872921f239e8ff8f0bb0ff90b4909cd7c187d965ca1fb386e2f33c85dddf433fd2b970
Checksum1: 76efcda0f702d4acdfc90be55baa49ebbb1debbcda42192a04e1dc682a897ddbc6e3aff714a6b6279c82f78a1295975f8d6442aab9d1c56dc581be1155f04b6c
