﻿Benchmark Validation File - DO NOT MODIFY! If you do, it will fail validation.

Validation Version:    1.3

Program:               y-cruncher v0.7.8 Build 9507
Tuning:                13-HSW ~ Airi

User:                  None Specified - You can edit this in "Username.txt".

Operating System:      Linux 5.4.0-65-generic x86_64

Processor(s):
    Name:              Intel(R) Core(TM) i7-4770K CPU @ 3.50GHz
    Logical Cores:     8
    Physical Cores:    4
    Sockets:           1
    NUMA Nodes:        1
    Base Frequency:    3,491,896,304 Hz

Motherboard:
    Manufacturer:      Gigabyte Technology Co., Ltd.
    Model:             Z87X-OC-CF
    Version:           To be filled by O.E.M.
    Serial Number:     Suppressed - Personally identifiable information is opt-in only.

Memory:
    Usable Memory:     33,559,498,752 (31.3 GiB)
    Total Memory:      Unable to Detect

Constant:              Sqrt(3)
Algorithm:             Newton's Method
Decimal Digits:        100,000,000
Hexadecimal Digits:    Disabled
Computation Mode:      Ram Only
Threading Mode:        Push Pool  ->  8 / ?  (randomization on)
Working Memory:        458,478,976 ( 437 MiB)
Total Memory:          652,149,376 ( 622 MiB)

Configuration Dump:
{
    Constant : {
        Constant : "sqrt"
        Argument : 3
        Algorithm : "newton"
    }
    ComputeSize : {
        DecimalDigits : 100000000
        EnableHexDigits : "false"
    }
    Output : {
        Path : "calculated"
        OutputEnable : "true"
        DigitsPerFile : 0
    }
    OutputVerify : "true"
    Mode : "ram"
    Parallelism : {
        TaskDecomposition : 8
        Framework : "pushpool"
        WorkerThreads : 0
        Randomization : "true"
        MaxSequentialDispatch : 64
    }
    Allocator : {
        Allocator : "mmap"
        LargePages : "attempt"
        LockedPages : "attempt"
    }
}

Start Date:            Thu Feb 18 03:14:04 2021
End Date:              Thu Feb 18 03:14:08 2021

Total Computation Time:    0.963 seconds
Start-to-End Wall Time:    4.634 seconds

CPU Utilization:           761.33 %  +  0.00 % kernel overhead
Multi-core Efficiency:     95.17 %  +  0.00 % kernel overhead

Last Decimal Digits:
8363615851 5717304778 0893083629 8265603085 7121008837  :  99,999,950
6925738353 2088567710 6100436863 0529323487 3933714844  :  100,000,000

SHA256-dec(100,000,001 - 100,000,100): 4aac504c0dfa87a64dbea0106b37f93fd4719b84ee550b82c81c5aa91cf658bd

Dec Counts: {9995281,10001670,10001751,10000247,10001384,9995879,9999931,10002655,10001042,10000160}

Dec Hash: Floor(|x| * 10^dec) mod (2^61 - 1) = 412209302056993680

Spot Check:                 Not Available
Timer Sanity Check:         Passed
Frequency Sanity Check:     Disabled in this version of y-cruncher
Reference Clock:            TSC
Reference Clock 0:          0
Reference Clock 1:          122,809,261
Reference Clock 2:          18,140,733
Is Debugger Present:        Unknown
Is Contiguous:              Yes
ECC Recovered Errors:       0
Colors:                     Yes

Event Log:
Thu Feb 18 03:14:04 2021	0.033	Working Memory
Thu Feb 18 03:14:04 2021	0.233	Working Memory:  438 MiB  (locked, spread: 100%/1)
Thu Feb 18 03:14:04 2021	0.233	Twiddle Tables
Thu Feb 18 03:14:04 2021	0.354	Twiddle Tables:  185 MiB  (locked, spread: 100%/1)
Thu Feb 18 03:14:04 2021	0.354	Begin Computation
Thu Feb 18 03:14:04 2021	0.354	InvSqrt(3)...
Thu Feb 18 03:14:04 2021	0.664	Finishing
Thu Feb 18 03:14:04 2021	0.669	Base Converting
Thu Feb 18 03:14:05 2021	1.316	Writing Decimal Digits
Thu Feb 18 03:14:07 2021	2.875	Verifying Base Conversion
Thu Feb 18 03:14:07 2021	3.176	Verifying Decimal Output
Thu Feb 18 03:14:08 2021	4.634	End Computation

----

Checksum0: 4aed7c6b87e7fe2ed540f5007b2de667232c9c10355ea6995ab6b7941ad02673f570ea72cbe481d371accf0343926aafa086c43c8679594198e4140a65f1b227
Checksum1: f842a9b004e41b292e3b0680f5eb802e6ebb2716c9de788c6e593ff8a71e28fdb715f396fc09ded30533dc5bb38a92ddeb3725f317da22430e69fedcbea87564
