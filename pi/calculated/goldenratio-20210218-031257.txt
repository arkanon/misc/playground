﻿Benchmark Validation File - DO NOT MODIFY! If you do, it will fail validation.

Validation Version:    1.3

Program:               y-cruncher v0.7.8 Build 9507
Tuning:                13-HSW ~ Airi

User:                  None Specified - You can edit this in "Username.txt".

Operating System:      Linux 5.4.0-65-generic x86_64

Processor(s):
    Name:              Intel(R) Core(TM) i7-4770K CPU @ 3.50GHz
    Logical Cores:     8
    Physical Cores:    4
    Sockets:           1
    NUMA Nodes:        1
    Base Frequency:    3,491,864,976 Hz

Motherboard:
    Manufacturer:      Gigabyte Technology Co., Ltd.
    Model:             Z87X-OC-CF
    Version:           To be filled by O.E.M.
    Serial Number:     Suppressed - Personally identifiable information is opt-in only.

Memory:
    Usable Memory:     33,559,498,752 (31.3 GiB)
    Total Memory:      Unable to Detect

Constant:              Golden Ratio
Algorithm:             Newton's Method
Decimal Digits:        100,000,000
Hexadecimal Digits:    Disabled
Computation Mode:      Ram Only
Threading Mode:        Push Pool  ->  8 / ?  (randomization on)
Working Memory:        458,478,976 ( 437 MiB)
Total Memory:          652,149,376 ( 622 MiB)

Configuration Dump:
{
    Constant : {
        Constant : "goldenratio"
        Algorithm : "newton"
    }
    ComputeSize : {
        DecimalDigits : 100000000
        EnableHexDigits : "false"
    }
    Output : {
        Path : "calculated"
        OutputEnable : "true"
        DigitsPerFile : 0
    }
    OutputVerify : "true"
    Mode : "ram"
    Parallelism : {
        TaskDecomposition : 8
        Framework : "pushpool"
        WorkerThreads : 0
        Randomization : "true"
        MaxSequentialDispatch : 64
    }
    Allocator : {
        Allocator : "mmap"
        LargePages : "attempt"
        LockedPages : "attempt"
    }
}

Start Date:            Thu Feb 18 03:12:52 2021
End Date:              Thu Feb 18 03:12:56 2021

Total Computation Time:    0.974 seconds
Start-to-End Wall Time:    4.806 seconds

CPU Utilization:           760.59 %  +  1.03 % kernel overhead
Multi-core Efficiency:     95.07 %  +  0.13 % kernel overhead

Last Decimal Digits:
6983266465 0958762067 5922249107 5144125222 8226019880  :  99,999,950
4186130718 6909500836 2519505480 1837059131 8941970031  :  100,000,000

SHA256-dec(100,000,001 - 100,000,100): 0c1670f367140af5975560361348e644f1c02dff2b0173e1a582244af332f06e

Dec Counts: {10003332,10000255,10002116,9999184,9998797,9996151,9996149,9997524,10005419,10001073}

Dec Hash: Floor(|x| * 10^dec) mod (2^61 - 1) = 1811446569750703319

Spot Check:                 Good through 100,000,000
Timer Sanity Check:         Passed
Frequency Sanity Check:     Disabled in this version of y-cruncher
Reference Clock:            TSC
Reference Clock 0:          0
Reference Clock 1:          94,573,222
Reference Clock 2:          17,437,210
Is Debugger Present:        Unknown
Is Contiguous:              Yes
ECC Recovered Errors:       0
Colors:                     Yes

Event Log:
Thu Feb 18 03:12:52 2021	0.033	Working Memory
Thu Feb 18 03:12:52 2021	0.233	Working Memory:  438 MiB  (locked, spread: 100%/1)
Thu Feb 18 03:12:52 2021	0.233	Twiddle Tables
Thu Feb 18 03:12:52 2021	0.364	Twiddle Tables:  185 MiB  (locked, spread: 100%/1)
Thu Feb 18 03:12:52 2021	0.364	Begin Computation
Thu Feb 18 03:12:52 2021	0.364	InvSqrt(5)...
Thu Feb 18 03:12:52 2021	0.678	Finishing
Thu Feb 18 03:12:52 2021	0.687	Base Converting
Thu Feb 18 03:12:53 2021	1.338	Writing Decimal Digits
Thu Feb 18 03:12:55 2021	3.001	Verifying Base Conversion
Thu Feb 18 03:12:55 2021	3.298	Verifying Decimal Output
Thu Feb 18 03:12:56 2021	4.806	End Computation

----

Checksum0: 702db5987a4436bb05bcec79aa431ceb46281afeefc6fbd393a7d62623a875816c0f87681e88f6c466a971238f59e7cb357ceeca8bba2862180d7fe09621322a
Checksum1: 0807d2fbec3a7cc09bc14445b480929eb3cfdcc27f9628760675d95a554d965f05d02591eef397003f82dc599d2cc56364add4a8c67b86428c6d6c6841076d93
