# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2023/10/13 (Fri) 05:29:54 -03
# 2023/10/12 (Thu) 14:25:05 -03
# 2023/10/12 (Thu) 05:11:47 -03
# 2023/10/09 (Mon) 13:43:06 -03
# 2023/10/09 (Mon) 11:45:16 -03

# <http://pt.wikipedia.org/wiki/Base64>
# <http://en.wikipedia.org/wiki/Base64>

# TODO
# ok split e merge dos bytes em sequências de 6 bits apenas com artimtética inteira do bash
# ok complemento do último bloco de 24 bits com caractere =
# ok receber valor por parâmetro ou redirecionamento
# ok diferença entre o comando e a função quando os dados com um linefeed final vêm por redirecionamento
# -- manipulação do caractere 0 (NUL).

# Quando o número de bytes a ser convertido não for divisível por três (se houver somente um ou dois bytes para compor o último bloco de 24 bits):
# -- adiciona-se o número necessário de bytes com valor zero para que haja 3 bytes
# -- assim, o bloco fica completo e a conversão é feita normalmente
# -- se houver somente um byte de entrada significativo, somente os dois primeiros dígitos codificados em base64 (12 bits) são mantidos,
# -- se houverem dois bytes, os três primeiros dígitos codificados (18 bits) são mantidos
# -- o caractere '=' pode ser adicionado para que o último bloco codificado possua 4 caracteres
# -- assim, quando o último grupo contiver um octeto, os 4 bits menos significativos do grupo final de 6 bits são zero;
# -- e quando o último grupo contiver dois octetos, os dois bits menos significativos do grupo de 6 bits são zero.



# [ EXEMPLO                                                                                                                                                                  ] #



s=AbCdEfG

for ((i=0; i<${#s}; i++)); { printf '%d ' \'${s:i:1}; }; echo
# 65 98 67 100 69 102 71

for ((i=0; i<${#s}; i++)); { printf -v a %d \'${s:i:1}; bc <<< 'obase=2; print '$a'," "'; }; echo
# 1000001 1100010 1000011 1100100 1000101 1100110 1000111



b=QWJDZEVmRw==

for ((i=0; i<${#b}; i++)); { declare -p d | grep -oP "\[\K[^]]+(?=.=.${b:i:1})"; } | tr '\n' ' '; echo
# 16 22 9 3 25 4 21 38 17 48

for ((i=0; i<${#b}; i++)); { printf '%d ' \'${b:i:1}; }; echo
# 81 87 74 68 90 69 86 109 82 119 61 61



# [ ANÁLISE DO ALGORITMO                                                                                                                                                     ] #



# ascii  character index            0         1         2         3         4         5         6
# ascii  character                  A         b         C         d         E         f         G
# ascii  8 bit dec code             65        98        67        100       69        102       71
# ascii  8 bit bin code             01000001  01100010  01000011  01100100  01000101  01100110  01000111  00000000  00000000
# ascii  8 bit bin split            010000|01 0110|0010 01|000011 011001|00 0100|0101 01|100110 010001|11 0000|0000 00|000000

# base64 character index            0      1       2       3      4      5       6       7      8      9
# base64 6 bit bin merge            010000 010110  001001  000011 011001 000100  010101  100110 010001 110000  000000  000000
# base64 6 bit dec digit index      16     22      9       3      25     4       21      38     17     48
# base64 digit                      Q      W       J       D      Z      E       V       m      R      w       =       =
# base64 digit ascii dec code       81     87      74      68     90     69      86      109    82     119     61      61

# ascii  8 bit dec code             65         98         67         |  100        69         102        |  71
#  0: 16 =                 65/2^2  [010000]01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  1: 22 =  65%2^2 *2^4 +  98/2^4   010000[01  0110]0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  2:  9 =  98%2^4 *2^2 +  67/2^6   010000|01  0110[0010  01]000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  3:  3 =  67%2^6 *2^0             010000|01  0110|0010  01[000011] |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#
#  4: 25 =                100/2^2   010000|01  0110|0010  01|000011  | [011001]00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  5:  4 = 100%2^2 *2^4 +  69/2^4   010000|01  0110|0010  01|000011  |  011001[00  0100]0101  01|100110  |  010001|11  0000|0000  00|000000
#  6: 21 =  69%2^4 *2^2 + 102/2^6   010000|01  0110|0010  01|000011  |  011001|00  0100[0101  01]100110  |  010001|11  0000|0000  00|000000
#  7: 38 = 102%2^6 *2^0             010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01[100110] |  010001|11  0000|0000  00|000000
#
#  8:    =                   /2^2   010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  | [010001]11  0000|0000  00|000000
#  9:    =                   /2^4   010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001[11  0000]0000  00|000000
# 10:    =                   /2^6   010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000[0000  00]000000
# 11:    =                          010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00[000000]

# ascii  8 bit dec code                           65         98         67         |  100        69         102        |  71
#  0: 16 =                         65/(4*4**0) Q [010000]01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  1: 22 =  65%(4*4**0)*16/4**0 +  98/(4*4**1) W  010000[01  0110]0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  2:  9 =  98%(4*4**1)*16/4**1 +  67/(4*4**2) J  010000|01  0110[0010  01]000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  3:  3 =  67%(4*4**2)*16/4**2                D  010000|01  0110|0010  01[000011] |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#
#  0: 25 =                        100/(4*4**0) Z  010000|01  0110|0010  01|000011  | [011001]00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  1:  4 = 100%(4*4**0)*16/4**0 +  69/(4*4**1) E  010000|01  0110|0010  01|000011  |  011001[00  0100]0101  01|100110  |  010001|11  0000|0000  00|000000
#  2: 21 =  69%(4*4**1)*16/4**1 + 102/(4*4**2) V  010000|01  0110|0010  01|000011  |  011001|00  0100[0101  01]100110  |  010001|11  0000|0000  00|000000
#  3: 38 = 102%(4*4**2)*16/4**2                m  010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01[100110] |  010001|11  0000|0000  00|000000
#
#  0:    =                           /(4*4**0) R  010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  | [010001]11  0000|0000  00|000000
#  1:    =                           /(4*4**1) w  010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001[11  0000]0000  00|000000
#  2:    =                           /(4*4**2) =  010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000[0000  00]000000
#  3:    =                                     =  010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00[000000]



# [ ANÁLISE DO PROBLEMA DE MANIPULAÇÃO DO CARACTERE NUL                                                                                                                      ] #

# <http://unix.stackexchange.com/a/174017> How do I use null bytes in Bash?
# <http://unix.stackexchange.com/a/683824> How do I ignore "command substitution: ignored null byte in input"?
# <http://gist.github.com/maage/85d019b07ba5926d053396ed84bb41bc> How to handle \0 in bash



s='a\0b\n'
printf $s
# ab
printf $s | cat -A
# a^@b$
printf $s | od -tx1
# 0000000 61 00 62 0a
# 0000004
printf $s | od -c
# 0000000   a  \0   b  \n
# 0000004



n=17

dd if=/usr/bin/base64 bs=c count=$n 2>&- | od -tx1 -w$n
# 0000000 7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 03
# 0000021

s=$(dd if=/usr/bin/base64 bs=c count=$n 2>&-)
# -bash: aviso: substituição de comando: byte nulo ignorado na entrada

echo -n "$s" | od -tx1 -w$n
# 0000000 7f 45 4c 46 02 01 01 03
# 0000010

printf "$s" | od -tx1 -w$n
# 0000000 7f 45 4c 46 02 01 01 03
# 0000010

diff <(a64 < <(dd if=/usr/bin/base64 bs=c count=$n)) <(b64 < <(dd if=/usr/bin/base64 bs=c count=$n))
# -bash: warning: command substitution: ignored null byte in input
# 1c1
# < f0VMRgIBAQAAAAAAAAAAAAM=
# ---
# > f0VMRgIBAQM=

dd if=/usr/bin/base64 bs=c count=$n 2>&- | od -tx1 -w$n | sed -rn 's/(^[[:xdigit:]]*)? \b(..)\b/\\x\2/gp'
# \x7f\x45\x4c\x46\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03

h=$(dd if=/usr/bin/base64 bs=c count=$n 2>&- | od -tx1 -w$n | sed -rn 's/(^[[:xdigit:]]*)? \b(..)\b/\\x\2/gp')

echo $h
# \x7f\x45\x4c\x46\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03

printf $h | od -tx1 -w$n
# 0000000 7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 03
# 0000021

printf -v s2 $h

echo "$s2"
# ELF

printf "$s2" | od -tx1 -w$n
# 0000000 7f 45 4c 46 02 01 01
# 0000007



handle0()
{
  local MAPFILE S in=false
  mapfile -d ''
  declare -p MAPFILE
  for S in "${MAPFILE[@]}"
  {
    $in && printf '\0'
    in=true
    echo -nE "$S"
  }
}

dd if=/usr/bin/base64 bs=c count=$n 2>&- > z

handle0 < z | od0
# 177 E L F 002 001 001 \0 \0 \0 \0 \0 \0 \0 \0 \0 003



clear; echo; od0 < z; comp < z
clear; echo; od0 < z; echo; base64 < z; echo; e64 < z

s='Wo\0ma\0n'
clear; echo; printf $s | od0; printf $s | comp
clear; echo; printf $s | od0; echo; printf $s | base64; echo; printf $s | e64



d64()
{
  local -- MAPFILE A i S LC_ALL=C D=( {A..Z} {a..z} {0..9} + / )
  [[ -t 0 ]] && MAPFILE=("${@:-$s}") || mapfile -d ''
  local -i j a in=0
  for i in "${MAPFILE[@]}"
  {
    ((in++)) && A+=(0)
    for ((j=0;j<${#i};j++))
    {
      printf -v a %d "'${i:j:1}"
      A+=($a)
    }
  }
  local -i i=0 e L l=${#A[*]} p
  while ((i<l))
  do
    for ((j=0;j<=3;j++))
    {
      ((p=l<i))
      ((j)) || L=0
      ((j<3)) && a=${A[i++]}
      e=4*4**j
      ((p)) && printf = || printf ${D[L+a/e]}
      L=a%e*64/e
    }
  done
  echo
}



#  0: 16 =               65/e  Q [010000]01  0110|0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  1: 22 =  65%e*64/e +  98/e  W  010000[01  0110]0010  01|000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  2:  9 =  98%e*64/e +  67/e  J  010000|01  0110[0010  01]000011  |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  3:  3 =  67%e*64/e +   0/e  D  010000|01  0110|0010  01[000011] |  011001|00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  0: 25 =   0%e*64/e + 100/e  Z  010000|01  0110|0010  01|000011  | [011001]00  0100|0101  01|100110  |  010001|11  0000|0000  00|000000
#  1:  4 = 100%e*64/e +  69/e  E  010000|01  0110|0010  01|000011  |  011001[00  0100]0101  01|100110  |  010001|11  0000|0000  00|000000
#  2: 21 =  69%e*64/e + 102/e  V  010000|01  0110|0010  01|000011  |  011001|00  0100[0101  01]100110  |  010001|11  0000|0000  00|000000
#  3: 38 = 102%e*64/e                 m  010000|01  0110|0010  01|000011  |  011001|00  0100|0101  01[100110] |  010001|11  0000|0000  00|000000

e64()
{
  local -- MAPFILE i LC_ALL=C D=( {A..Z} {a..z} {0..9} + / )
  [[ -t 0 ]] && MAPFILE=("${@:-$s}") || mapfile -d ''
  local -i j=0 a e=4 L in=0
n=0
  for i in "${MAPFILE[@]}"
  {
    ((in++)) && printf '0\n'
    for ((j=0;j<${#i};j++))
{
((n%4)) || printf '0\n'
  printf '%d\n' "'${i:j:1}"
((n++))
}
    } |
    while read a
    do
    # e=4*4**j
      ((j==0)) && { L=0; e=4; }
  # echo -n "[$L|$a|$e|" >&2
# echo "(j=$j L=$L a=$a e=$e ind=$((L+a/e)))"
      ((j==3)) && printf ${D[L]}-${D[a/4]} || printf "${D[L+a/e]}"
      j='(j+1)%4'
  # echo "$j]" >&2
      e=4*4**j
      L=a%e*64/e
    done
  echo
}



clear; echo; base64<z; e64<z

f0VM-RgIB-AQAA-AAAA-AAAA-AAM=
f0VM-RgIB-AQAA-AAAA-AAAA-AA



d64()
{
  local -- MAPFILE S LC_ALL=C D=( {A..Z} {a..z} {0..9} + / )
  [[ -t 0 ]] && MAPFILE=("${@:-$s}") || mapfile -d ''
  : "${MAPFILE[*]}"
  local -i i=0 ii j a e L t=${#_} l p in=0
echo $t >&2
  for S in "${MAPFILE[@]}"
  {
    ((in)) && printf ${D[0]}
    ii=0
    l=${#S}
    while ((ii<l))
    do
      for ((j=0;j<=3;j++))
      {
        ((p=t<i))
      # ((ii==0 && in)) && i+=1
        ((j)) || L=0
        ((j<3)) && { printf -v a %d "'${S:ii++:1}"; i+=1; }
        e=4*4**j
        ((p)) && printf = || printf ${D[L+a/e]}
        L=a%e*64/e
      }
    done
    in=1
  }
  echo
}



e64()
{
  local -- MAPFILE Str LC_ALL=C D=( {A..Z} {a..z} {0..9} + / ) esc chr dig sep=$'\x01'
  [[ -t 0 ]] && MAPFILE=("${@:-$s}") || mapfile -d ''
  : "${MAPFILE[*]}"
  local -i i=0 ii j asc exp Msig Lsig t=${#_} len Idx pad in=0
  for Str in "${MAPFILE[@]}"
  {
    ((in)) && printf ${D[0]}
    ii=0
    len=${#Str}
    while ((ii<len))
    do
      for ((j=0;j<=3;j++))
      {
        ((pad=t<i)
        ((j)) || Lsig=0
        ((j<3)) && { ((ii)) && printf -v esc %q "${Str:ii:1}" || esc='\0'; eval "chr=$esc"; } || chr=
        printf -v asc %d "'$chr"
         exp=4*4**j
        Msig=asc/exp
         Idx=Lsig+Msig
        ((pad)) && dig== || dig=${D[Idx]}
        echo "t=$t $((t-i)) $pad${sep}i=$i${sep}len=$len $((len-ii)) $pad${sep}ii=$ii${sep}j=$j${sep}esc=${chr:+$esc}${sep}asc=${chr:+$asc}${sep}exp=$exp${sep}Lsig=$Lsig${sep}Msig=$Msig${sep}Idx=$Idx${sep}dig=$dig"
        Lsig=asc%exp*64/exp
        ((j<3)) && { ii+=1; i+=1; }
      }
    done
    in=1
  } | column -ts$sep
  echo
}



# [ FUNÇÕES                                                                                                                                                                  ] #



a64()
{
  local MAPFILE S in=false
  [[ -t 0 ]] && mapfile -d '' <<< $* || mapfile -d ''
  for S in "${MAPFILE[@]}"
  {
    $in && printf '\0'
    in=true
    echo -nE "$S"
  } | base64 -w0
  echo
}



b64()
{
  local -- REPLY S LC_ALL=C D=( {A..Z} {a..z} {0..9} + / )
  [[ -t 0 ]] && S=${*:-$s} || { read -rd ''; S=$REPLY; }
  local -i i=0 j a e L l=${#S} p
  while ((i<l))
  do
    for ((j=0;j<=3;j++))
    {
      ((p=l<i))
      ((j)) || L=0
      ((j<3)) && printf -v a %d "'${S:i++:1}"
      e=4*4**j
      ((p)) && printf = || printf ${D[L+a/e]}
      L=a%e*64/e
    }
  done
  echo
}



c64()
{
  local -- REPLY Str LC_ALL=C D=( {A..Z} {a..z} {0..9} + / ) esc chr dig sep=$'\x01'
  [[ -t 0 ]] && Str=${*:-$s} || { read -rd ''; Str=$REPLY; }
  local -i i=0 j asc exp Msig Lsig len=${#Str} Idx pad
  while ((i<len))
  do
    for ((j=0;j<=3;j++))
    {
      ((pad=len<i))
      ((j)) || Lsig=0
      ((j<3)) && { printf -v esc %q "${Str:i:1}"; eval "chr=$esc"; } || chr=
      printf -v asc %d "'$chr"
       exp=4*4**j
      Msig=asc/exp
       Idx=Lsig+Msig
      ((pad)) && dig== || dig=${D[Idx]}
      echo "len=$len $((len-i)) $pad${sep}i=$i${sep}j=$j${sep}esc=${chr:+$esc}${sep}asc=${chr:+$asc}${sep}exp=$exp${sep}Lsig=$Lsig${sep}Msig=$Msig${sep}Idx=$Idx${sep}dig=$dig"
      Lsig=asc%exp*64/exp
      ((j<3)) && i+=1
    }
  done | column -ts$sep
  echo
}



od0() { od ${1:--c} | cut -sd ' ' -f2- | sed -zr 's/   |  ([^ ])|\n/ \1/g; s/^ +| $//g'; echo; }



comp()
{
  local REPLY s
  [[ -t 0 ]] && s=$* || { read -rd ''; s=$REPLY; }
  printf "\n"
  printf "$s" | c64
  printf "$s" | a64
  printf "$s" | b64
  printf "\n["
  printf "$s" | a64 | base64 -d
  printf "]\n["
  printf "$s" | b64 | base64 -d
  printf "]\n"
  printf "$s" | a64 | base64 -d | od0
  printf "\n"
  printf "$s" | b64 | base64 -d | od0
  printf "\n"
}



# [ COMPARAÇÂO DE EFICÁCIA                                                                                                                                                   ] #



$ s=AbCdEfG; for i in '' . .. ... ; { a64 $s$i; b64 $s$i; }
QWJDZEVmRw==
QWJDZEVmRw==
QWJDZEVmRy4=
QWJDZEVmRy4=
QWJDZEVmRy4u
QWJDZEVmRy4u
QWJDZEVmRy4uLg==
QWJDZEVmRy4uLg==



$ a64 ABC ; b64 ABC
QUJD
QUJD



$ echo ABC | a64 ; echo ABC | b64
QUJDCg==
QUJDCg==



$ echo -n ABC | a64 ; echo -n ABC | b64
QUJD
QUJD



$ a64 <<< ABC ; b64 <<< ABC
QUJDCg==
QUJDCg==



# [ COMPARAÇÂO DE EFICIÊNCIA                                                                                                                                                 ] #



. <(curl -ksL bit.ly/benshmark-v5)
alias bm=benshmark-v5
dropc() { free -m; echo 3 | sudo tee /proc/sys/vm/drop_caches; free -m; }



$ s='Conversão para base64 com built-ins 🤓'

$ a64 ; b64
Q29udmVyc8OjbyBwYXJhIGJhc2U2NCBjb20gYnVpbHQtaW5zIPCfpJM=
Q29udmVyc8OjbyBwYXJhIGJhc2U2NCBjb20gYnVpbHQtaW5zIPCfpJM=

$ dropc

$ bm 2000 a64 b64 %
a64  00:03,568
b64  00:01,594

1º  b64
2º  a64/b64       123,839 % mais lenta



$ s=$(dd if=/dev/random bs=c count=32)
32+0 registros de entrada
32+0 registros de saída
32 bytes copiados, 6,327e-05 s, 506 kB/s

$ printf "$s" | od -c
0000000 374   e   o 177 002 020 037  \f 265 337   D 302   4 335 200   E
0000020 216   7   #   q 310  \n 250 370   J 222 032 005   q 310 367   o
0000040

$ a64 ; b64
/GVvfwIQHwy130TCNN2ARY43I3HICqj4SpIaBXHI928=
/GVvfwIQHwy130TCNN2ARY43I3HICqj4SpIaBXHI928=

$ dropc

$ bm 2000 a64 b64 %
a64  00:04,567
b64  00:01,257

1º  b64
2º  a64/b64  263,325 % mais lenta



# EOF
