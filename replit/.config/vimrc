" ~/.config/vimrc

" Arkanon <arkanon@lsd.org.br>
" 2023/10/18 (Wed) 01:14:51 -03

  let skip_defaults_vim=1
  au FileType sh let g:is_bash=1
  set nocompatible
  set modeline
  set modelines=5
  set scrolloff=0

  au BufReadPost *
   \ if line("'\"") > 0 && line("'\"") <= line("$")
   \ | exe "normal! g`\""
   \ | endif

  au GUIEnter * normal! zz

  set encoding=utf-8

  syntax on
  set nohlsearch

  set redrawtime=10000
  syntax sync fromstart

  au Syntax * syntax match TailSpace /\s\+$\| \+\ze\t/ containedin=ALL
  hi TailSpace ctermbg=lightred guibg=lightred

  set nomousehide
  set ttyfast
  set clipboard+=unnamedplus
  set backspace=indent,eol,start
  set history=50
  set showcmd
  set incsearch
  set nobackup
  set nowrap
  set ruler

  au FileType * set nocindent
  au FileType * set nosmartindent
  au FileType * set noautoindent
  au FileType * set indentexpr=
  au FileType * set indentkeys=
  au FileType * filetype        indent off
  au FileType * filetype plugin indent off



  " signature <http://stackoverflow.com/a/16572822>
  let s1 = $VIMSIGN1
  " replace
  let p1 = 'mz<esc>V/\%V [[:alnum:]]\+ <<cr><esc>c/><cr> '
  let p2 = '<esc>x`z'
  exec ' map <f1>       '.p1.s1.p2
  exec 'imap <f1> <esc> '.p1.s1.p2.'a'
  " insert
  let p1 = 'mz<esc>o '
  let p2 = '<esc>C`z'
  exec ' map <f3>       '.p1.s1.p2
  exec 'imap <f3> <esc> '.p1.s1.p2.'a'



  " date & time
  let p1 = 'mz<esc>:lang en_US.utf8<cr>'
  let p2 = '<c-r>=strftime("%Y/%m/%d (%a) '
  let p3 = '%H:%M:%S'
  let p4 = '")<cr><esc>'
  " update
  let c1 = 'v'
  let c2 = 'lc'
  let t  = ' %Z'
  let s  = '28'
  exec ' map  <f5>        '.p1.c1.s.c2.p2.p3.t.p4.'`z'
  exec 'imap  <f5>  <esc>l'.p1.c1.s.c2.p2.p3.t.p4.'`zi'
  " insert
  let c1 = 'o'
  let c2 = ' '
  let t  = ' %Z'
  let s  = ''
  exec ' map  <f6>        '.p1.c1.s.c2.p2.p3.t.p4.'C`z'
  exec 'imap  <f6>   <esc>'.p1.c1.s.c2.p2.p3.t.p4.'C`za'



  map  <f8> mz<esc>:%s/\s\+$//g<cr>`z



" EOF
