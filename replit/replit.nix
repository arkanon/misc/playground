{ pkgs }: {
  deps = [
    pkgs.bc
#   pkgs.sox
    pkgs.vim
#   pkgs.speechd
    pkgs.bind.host
    pkgs.inetutils
#   pkgs.openssh
    pkgs.bashInteractive
    pkgs.nodePackages.bash-language-server
    pkgs.man
  ];
}
