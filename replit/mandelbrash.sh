# [URL]

# http://replit.com/@bagatini/mandelbrash?v=1

# [NAME]

mandelbrash

# [DESCRIPTION]

Mandelbrot set generator with integer math and 16 colors.
Original code from Charles Cooke.

# [TAGS]

- bash
- math
- fractal
- mandelbrot
- ansi

# [CODE]

#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2023/10/17 (Mon) 09:00:00 -03
# 2023/10/16 (Mon) 23:46:19 -03

# Mandelbrot set generator with integer math and 16 colors.

  . .config/bashrc

  clear
  sleep0 3 # apparently replit bash needs some time to update the actual terminal dimensions info

  d=$'\xe2\x96\x88' # █
  s=$'\e['

  parms=(

    N=30
    I=30
    P=10**8         # precision

    pw=8            # "pixel" width
    ph=17           # "pixel" height

    Px=P*32/10      # plane width
    Py=P*21/10      # plane height

    W               # image width  in terminal, with plane proportion
    H               # image height in terminal, with plane proportion

    C=$(tput cols)  # terminal width
    L=$(tput lines) # terminal height
    L=L+L%2

  )

  declare -i ${parms[*]}

  if (( P*C*pw/L/ph > P*Px/Py ))
  then
    M=height
    H=L-2
    W=H*Px*ph/Py/pw
  else
    M=width
    W=C-1
    H=W*Py*pw/Px/ph-1
  fi

  debug()
  {
    echo "
Full ${M}.

    C = $C
    L = $L
   Px = $Px
   Py = $Py
  C/L = $(( P*C*pw/L/ph ))
Px/Py = $(( P*Px/Py     ))
    W = $W
    H = $H
    "
  }

  debug

  for (( y=-Py/2 ; y<Py/2 ; y+=Py/H ))
  {
    for (( x=-Px/2 ; x<Px/2 ; x+=Px/W ))
    {
      for (( a=b=i=k=c=0
           ; a*a+b*b < 2*P*P && i++ < I
           ; a=x+((c=a)*a-b*b)/P , b=y+2*c*b/P ))
      { :; }
      (( ( j=( i<I ? i%16 : 0 )+N ) > N+7 ? k=1 , j-=8 : 0 ))
      printf $s"$k;$j"m$d
    }
    echo
  }
  printf ${s}0m

# Original Code
# Charles Cooke <ccooke-shell@gkhs.net>
# 2005/10 <http://web.archive.org/web/20130705050758/earth.gkhs.net/ccooke/shell.html>
# for((P=10**8,Q=P/100,X=320*Q/(`tput cols`-1),Y=210*Q/`tput lines`,y=-105*Q,v=-220*Q,x=v;y<105*Q;x=v,y+=Y));do for((;x<P;a=b=i=k=c=0,x+=X));do for((;a*a+b*b<2*P*P&&i++<99;a=((c=a)*a-b*b)/P+x,b=2*c*b/P+y));do :;done;(((j=(i<99?i%16:0)+30)>37?k=1,j-=8:0));echo -ne "\E[$k;$j"mE;done;echo -e \\E[0m;done

# EOF
