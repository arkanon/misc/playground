#!/bin/bash

descr="Wrapper gerador de URL's para plotagem de superfícies 3D com a aplicação online CalcPlot3D (http://c3d.libretexts.org/CalcPlot3D/index.html)."

# Arkanon <arkanon@lsd.org.br>
# 2023/08/27 (Sun) 17:42:21 -03
# 2023/08/27 (Sun) 03:56:06 -03
# 2021/02/20 (Sat) 05:41:42 -03
# 2021/02/19 (Fri) 16:27:42 -03
# 2021/02/19 (Fri) 04:17:03 -03



  main()
  {

    # o primeiro parâmetro do script (obrigatório) é o nome da expressão escolhida entre as expressões cadastradas em lib.sh
    # o segundo parâmetro do script (opcional) é a qualidade da malha da superfície
    local title=$1 cubes=$2 script=$(realpath $0) dir

    local -A def_{implicit,jaxlabel,window}

    # a variável dir contém o diretório em que o script propriamente dito está armazenado, muito possivelmente o clone de seu repositório git
    # nesse diretório estarão os arquivos def.sf e lib.sh, cada um respectivamente com os defaults de renderização da superfície e a biblioteca de superfícies cadastradas
    # dessa forma, basta criar um link em algum diretório presente no PATH para este arquivo principal do script que os arquivos auxiliares default serão encontrados
    # eventualmente pode ser implementada a pesquisa por substitutos para esses dois arquivos em um diretório de configuração e/ou no diretório corrente
    dir=${script%/*}
    . $dir/def.sh

    # lib.sh define a função exprs que, quando executada, criará arrays associativos
    # o nome de cada array indica uma superfície e contém os parâmetros customizados dessa superfície
    . $dir/lib.sh

    # o conteúdo da função exprs definida será processado para gerar a lista de superfícies disponíveis na bibliteca
    # em vez de usar uma função e processá-la até se poderia definir os arrays diretamente no arquivo e processar seu conteúdo, mas numa função o conteúdo é normalizado conforme padrões do próprio bash, o que torna o conteúdo mais previsível que o de um arquivo de script
    # assim, a variável $exprs conterá a lista de todas as superfícies cadastradas em lib.sh na função exprs
    exprs=$(type exprs | grep -oP '^ *\K[^](=]+(?==)')

    # todos os nomes de superfície contidos em $exprs serão declarados como arrays associativos locais
    local -A $exprs

    # agora que os nomes de superfícies cadastradas já foram declarados como arrays associativos (locais), executando a função exprs atribuímos os valores desses arrays
    exprs

    # se não for passado nenhum parâmetro para o script
    # ou o primeiro parâmetro não estiver na lista de superfícies cadastradas em lib.sh
    # ou o segundo parâmetro que é opcional estiver presente mas não for um número inteiro
    # então finaliza mostrando o help
    [[ $# == 0 || ! ${exprs//$'\n'/ } =~ (^| )$1( |$) || $cubes && ! $cubes =~ [0-9]+ ]] && { help >&2; exit 1; }

    # a variável $expr apontará diretamente para o conteúdo do array associativo cujo nome é a superfície escolhida
    local -n expr=$1
    # e a partir de então, a variável com nome genérico $expr é um array associativo que aponta diretamente para o array associativo cujo nome foi passado como parâmetro para o script

    # a função url utiliza os defaults setados em def.sh e as customizações definidas nas superfícies cadastradas em lib.sh para montar a url da superfície
    url=$(url)
    url=${url%&*}
    echo -e "\nMan: http://c3d.libretexts.org/CalcPlot3D/CalcPlot3D-Help/calcplot3d-help.html"
    echo -e "\nURL: $url\n"
  # which firefox >& /dev/null && firefox $url

  }



  help()
  {
    echo -e "\n$descr\n\nUso: ${0##*/} <expressão> [cubes]\n\nExpressões pré-definidas em \e]8;;file://$dir/lib.sh\e\\$dir/lib.sh\e]8;;\e\\:"
    sed  -r "s/^| /- /g" <<< $exprs | sort
    echo -e "\nDefaults de plotagem definidos em \e]8;;file://$dir/def.sh\e\\$dir/def.sh\e]8;;\e\\.\n"
  }



  url()
  {

    # expr e cubes são variáveis definidas em main

                 : "${expr[equation]/=/\~}"
    expr[equation]=$(jq -sRr @uri <<< ${_// /})

             title=${title//_/ }
             title=($title)
             title=${title[*]^}

                 : "${expr[math]/=/\~}"
        expr[math]="\text{$title: }${_// /}"

        expr[text]=${expr[text]/=/\~}

    [[ $cubes        ]] && expr[cubes]=$cubes

    [[ ${expr[math]} ]] && { label=${expr[math]}; expr[textonly]=false ; }
    [[ ${expr[text]} ]] && { label=${expr[text]}; expr[textonly]=true  ; }
    expr[math]=$(jq -sRr @uri <<< $label)

    echo -n 'http://c3d.libretexts.org/CalcPlot3D/index.html?'
    for type in implicit jaxlabel window
    {
      local -n def=def_$type
      echo -n type=$type
      for parameter in ${!def[*]}
      {
        [[ ${expr[$type-$parameter]} ]] && pref=$type- || pref=
        echo -n ";$parameter=${expr[$pref$parameter]:-${def[$parameter]}}"
      }
      echo -n '&'
    } | tr -d ' '
    echo

  }



  main "$@"



# EOF
