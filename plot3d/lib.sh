#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2023/08/27 (Sun) 17:58:38 -03
# 2023/08/26 (Sat) 21:59:17 -03
# 2021/02/20 (Sat) 05:41:42 -03
# 2021/02/19 (Fri) 16:27:42 -03
# 2021/02/19 (Fri) 04:17:03 -03



  exprs()
  {

           heart_zup=(
                       [equation]=" x^2 + 3 y^2 + z^2 = 1 + z ( x^2 + 1/9 y^2 )^(1/3) "
                           [math]=" x^2 + 3 y^2 + z^2 = 1 + z \sqrt[3]{ x^2 + \frac{1}{9} y^2 } "

                         [center]="  5.4540 ,  7.7177 ,  4.4255 "
                             [up]=" -0.2085 , -0.2741 ,  0.9388 "

                  [implicit-xmax]="  2.0 "   [window-xmax]="  1.0 "
                  [implicit-xmin]=" -2.0 "   [window-xmin]=" -1.2 "
                  [implicit-ymax]="  2.0 "   [window-ymax]="  1.0 "
                  [implicit-ymin]=" -2.0 "   [window-ymin]=" -2.0 "
                  [implicit-zmax]="  2.0 "   [window-zmax]="  1.3 "
                  [implicit-zmin]=" -2.0 "   [window-zmin]=" -1.1 "
                 )

           heart_yup=(
                       [equation]=" x^2 + y^2 + 3 z^2 = 1 + y ( x^2 + 1/9 z^2 )^(1/3) "
                           [math]=" x^2 + y^2 + 3 z^2 = 1 + y \sqrt[3]{ x^2 + \frac{1}{9} z^2 } "

                         [center]="  3.4378 ,  4.0345 ,  8.4796 "
                             [up]=" -0.1114 ,  0.9521 , -0.2846 "

                  [implicit-xmax]="  2.0 "   [window-xmax]="  1.1 "
                  [implicit-xmin]=" -2.0 "   [window-xmin]=" -1.1 "
                  [implicit-ymax]="  2.0 "   [window-ymax]="  1.2 "
                  [implicit-ymin]=" -2.0 "   [window-ymin]=" -1.1 "
                  [implicit-zmax]="  2.0 "   [window-zmax]="  1.2 "
                  [implicit-zmin]=" -2.0 "   [window-zmin]=" -3.0 "
                 )

               chest=(
                       [equation]=" x^2 + y^3 + z^3 + 20  cos(x) = 25 "
                           [math]=" x^2 + y^3 + z^3 + 20 \cos{x} = 25 "

                         [center]=" 31.8048 , 28.6370 , 13.9057 "
                             [up]=" -0.3023 , -0.2721 ,  0.9135 "
                           [zoom]="  0.37 "

                  [implicit-xmax]="  5.0 "   [window-xmax]="  7.0 "
                  [implicit-xmin]=" -5.0 "   [window-xmin]=" -9.0 "
                  [implicit-ymax]="  6.0 "   [window-ymax]="  7.0 "
                  [implicit-ymin]=" -6.0 "   [window-ymin]=" -9.0 "
                  [implicit-zmax]="  6.0 "   [window-zmax]="  7.0 "
                  [implicit-zmin]=" -6.0 "   [window-zmin]=" -9.0 "
               )

                flor=(
                       [equation]=" z = 30 -       30 / sqrt( x^2 + y^2 )  + 3  sin( sqrt( x^2 + y^2 )) +        sqrt(  sin(x) +  sin(y) - x^2 - y^2 )/ 100 "
                           [math]=" z = 30 - \frac{30}{\sqrt{ x^2 + y^2 }} + 3 \sin{\sqrt{ x^2 + y^2 }} + \frac{\sqrt{ \sin{x} + \sin{y} - x^2 - y^2 }}{100} "

                         [center]="  36.5846 ,  9.6734 , 24.3512 "
                             [up]="  -0.4791 , -0.4963 ,  0.7238 "
                           [zoom]="   0.16 "
                 [centerxpercent]="   0.40 "
                 [centerypercent]="   0.80 "

                  [implicit-xmax]="  15.0 "   [window-xmax]="  15.0 "
                  [implicit-xmin]=" -15.0 "   [window-xmin]=" -15.0 "
                  [implicit-ymax]="  15.0 "   [window-ymax]="  15.0 "
                  [implicit-ymin]=" -15.0 "   [window-ymin]=" -15.0 "
                  [implicit-zmax]="  35.0 "   [window-zmax]="  35.0 "
                  [implicit-zmin]="   0.0 "   [window-zmin]="  30.0 "
                )

              breast=(
                       [equation]=" z = e^( -       1/ 1000  ((x+4)^2+(y+4)^2)^2 ) + e^( -       1/ 1000  ((x-4)^2+(y-4)^2)^2 ) +       1/ 10  e^( - ((x+4)^2+(y+4)^2)^2 ) +       1/ 10  e^( - ((x-4)^2+(y-4)^2)^2 ) "
                           [math]=" z = e^{ - \frac{1}{1000} ((x+4)^2+(y+4)^2)^2 } + e^{ - \frac{1}{1000} ((x-4)^2+(y-4)^2)^2 } + \frac{1}{10} e^{ - ((x+4)^2+(y+4)^2)^2 } + \frac{1}{10} e^{ - ((x-4)^2+(y-4)^2)^2 } "

                         [center]=" -35.5014 , 49.5330 , 34.4402 "
                             [up]="   0.5419 , -0.3040 ,  0.7835 "
                           [zoom]="   0.31 "
                 [centerxpercent]="   0.50 "
                 [centerypercent]="   0.55 "

                   [zscalefactor]="   7.00 "

                  [implicit-xmax]="  14.0 "   [window-xmax]=
                  [implicit-xmin]=" -14.0 "   [window-xmin]=
                  [implicit-ymax]="  14.0 "   [window-ymax]=
                  [implicit-ymin]=" -14.0 "   [window-ymin]=
                  [implicit-zmax]="   1.5 "   [window-zmax]=
                  [implicit-zmin]="  -1.0 "   [window-zmin]=
              )

       cardioboloide=(
                       [equation]=" z = y^2 - y x^(2/3) + x^2 - 1 "
                           [math]=" z = y^2 - y \sqrt[3]{x^2} + x^2 - 1 "

                         [center]="  5.4911 , 5.7315 ,  9.6565 "
                             [up]=" -0.1799 , 0.9317 , -0.3154 "
                           [zoom]="  1.46 "

                  [implicit-xmax]="  4.0 "   [window-xmax]="  1.5 "
                  [implicit-xmin]=" -4.0 "   [window-xmin]=" -3.5 "
                  [implicit-ymax]="  4.0 "   [window-ymax]="  1.5 "
                  [implicit-ymin]=" -4.0 "   [window-ymin]=" -2.5 "
                  [implicit-zmax]="  2.0 "   [window-zmax]="  3.5 "
                  [implicit-zmin]=" -2.0 "   [window-zmin]=" -2.5 "
       )

    goursat_tangle_1=(
                       [equation]=" ( x^4 + y^4 + z^4 ) - 0.3 ( x^2 + y^2 + z^2 )^2 - 5 ( x^2 + y^2 + z^2 ) + 15 = 0 "
                           [math]=" ( x^4 + y^4 + z^4 ) - 0.3 ( x^2 + y^2 + z^2 )^2 - 5 ( x^2 + y^2 + z^2 ) + 15 = 0 "

                         [center]=" 30.1485 ,  9.6541 , 38.7023 "
                             [up]=" -0.6192 , -0.3476 ,  0.7041 "
                           [zoom]="  0.28 "

                  [implicit-xmax]="  8.0 "   [window-xmax]="   8.0 "
                  [implicit-xmin]=" -8.0 "   [window-xmin]=" -10.0 "
                  [implicit-ymax]="  8.0 "   [window-ymax]="   8.0 "
                  [implicit-ymin]=" -8.0 "   [window-ymin]=" -10.0 "
                  [implicit-zmax]="  8.0 "   [window-zmax]="  10.0 "
                  [implicit-zmin]=" -8.0 "   [window-zmin]=" -10.0 "
    )

    goursat_tangle_2=(
                       [equation]=" ( x^4 + y^4 + z^4 ) - 0   ( x^2 + y^2 + z^2 )^2 - 5 ( x^2 + y^2 + z^2 ) + 12 = 0 "
                           [math]=" ( x^4 + y^4 + z^4 ) - 0   ( x^2 + y^2 + z^2 )^2 - 5 ( x^2 + y^2 + z^2 ) + 12 = 0 "

                         [center]="  8.3641 , 14.4871 , 18.5786 "
                             [up]=" -0.4045 , -0.7006 ,  0.5878 "
                           [zoom]="  0.82 "

                  [implicit-xmax]="  3.0 "   [window-xmax]="  2.5 "
                  [implicit-xmin]=" -3.0 "   [window-xmin]=" -3.0 "
                  [implicit-ymax]="  3.0 "   [window-ymax]="  3.0 "
                  [implicit-ymin]=" -3.0 "   [window-ymin]=" -3.5 "
                  [implicit-zmax]="  3.0 "   [window-zmax]="  4.0 "
                  [implicit-zmin]=" -3.0 "   [window-zmin]=" -6.0 "
    )

                toro=(
                       [equation]=" (  sqrt( x^2 + y^2 ) - 4 )^2 + z^2 = 2 "
                           [math]=" ( \sqrt{ x^2 + y^2 } - 4 )^2 + z^2 = 2 "

                         [center]=" 35.0314 , 20.2254 , 29.3892 "
                             [up]=" -0.4330 , -0.2500 ,  0.8660 "
                           [zoom]="  0.67 "

                  [implicit-xmax]="  8.0 "   [window-xmax]="  6.0 "
                  [implicit-xmin]=" -8.0 "   [window-xmin]=" -7.0 "
                  [implicit-ymax]="  8.0 "   [window-ymax]="  6.0 "
                  [implicit-ymin]=" -8.0 "   [window-ymin]=" -6.0 "
                  [implicit-zmax]="  8.0 "   [window-zmax]="  4.0 "
                  [implicit-zmin]=" -8.0 "   [window-zmin]=" -6.0 "
                )

                piao=(
                       [equation]="      (  sqrt( x^2 + y^2 ) - 4       )^3 + z^2 = 2 "
                           [math]=" \left( \sqrt{ x^2 + y^2 } - 4 \right)^3 + z^2 = 2 "

                         [center]=" 28.8274 , 16.6435 , 10.8156 "
                             [up]=" -0.0000 , -0.0000 ,  2.0000 "
                           [zoom]="  0.44 "

                  [implicit-xmax]="  8.0 "   [window-xmax]="  6.0 "
                  [implicit-xmin]=" -8.0 "   [window-xmin]=" -7.0 "
                  [implicit-ymax]="  8.0 "   [window-ymax]="  6.0 "
                  [implicit-ymin]=" -8.0 "   [window-ymin]=" -6.0 "
                  [implicit-zmax]="  8.0 "   [window-zmax]="  4.0 "
                  [implicit-zmin]=" -8.0 "   [window-zmin]=" -6.0 "
                )

             letra_a=(
                       [equation]=" z = ( 1 + sgn( 1.0 + x - abs(2y) ) ) ( 1 + sgn( 1.0 - x ) ) / 20 ( 1 + sgn( 1.0 + x ) ) / 2
                                      + ( 1 + sgn( 0.4 + x - abs(2y) ) ) ( 1 + sgn( 0.6 - x ) ) / 20 ( 1 - sgn( 0.3 - x ) ) / 2
                                      - ( 1 + sgn( 0.4 + x - abs(2y) ) ) ( 1 + sgn( 1.0 - x ) ) / 20 "
                           [text]=" Letra A "

                         [center]="  0.9052 ,  1.5593 , 9.8361 "
                             [up]=" -0.8660 , -0.4973 , 0.0523 "
                           [zoom]="  2.49 "

                  [implicit-xmax]="  1.1 "   [window-xmax]="   0.6 "
                  [implicit-xmin]=" -1.1 "   [window-xmin]="  -0.6 "
                  [implicit-ymax]="  1.1 "   [window-ymax]="   0.6 "
                  [implicit-ymin]=" -1.1 "   [window-ymin]="  -0.6 "
                  [implicit-zmax]="  1.0 "   [window-zmax]="   3.0 "
                  [implicit-zmin]=" -1.0 "   [window-zmin]=" -10.0 "
             )

  }



# EOF
