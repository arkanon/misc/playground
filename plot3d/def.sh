#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2023/08/26 (Sat) 21:22:04 -03
# 2021/02/20 (Sat) 05:41:42 -03
# 2021/02/19 (Fri) 16:27:42 -03
# 2021/02/19 (Fri) 04:17:03 -03



  def_implicit=(

  ########### [type]=implicit

          [equation]=

             [cubes]=32
             [alpha]=-1
          [constcol]='rgb(255,0,0)'
         [fixdomain]=true
            [format]=normal
              [view]=0
           [visible]=true

              [xmin]=-2
              [xmax]=2
              [ymin]=-2
              [ymax]=2
              [zmin]=-2
              [zmax]=2

  )

  def_jaxlabel=(

  ########### [type]=jaxlabel

              [math]=
          [textonly]=

          [position]=middle

  )

  def_window=(

  ########### [type]=window

            [center]=1,1,1,1
                [up]=0,0,0,1
              [zoom]=2
             [zcmax]=4
             [zcmin]=-4
    [centerxpercent]=0.5
    [centerypercent]=0.5

              [xmin]=${def_implicit[xmin]}
              [xmax]=${def_implicit[xmax]}
              [ymin]=${def_implicit[ymin]}
              [ymax]=${def_implicit[ymax]}
              [zmin]=${def_implicit[zmin]}
              [zmax]=${def_implicit[zmax]}

             [focus]=0,0,0,1

        [xaxislabel]=x
        [yaxislabel]=y
        [zaxislabel]=z

            [xscale]=1
            [yscale]=1
            [zscale]=1

      [xscalefactor]=1
      [yscalefactor]=1
      [zscalefactor]=1

          [autospin]=true
           [edgeson]=false
           [faceson]=true
        [gridplanes]=false
        [gridsonbox]=false
          [nomidpts]=true
       [perspective]=true
          [showaxes]=true
           [showbox]=false
         [showticks]=false
       [transparent]=false
          [twoviews]=false
       [unlinkviews]=false
            [xygrid]=false
            [xzgrid]=false
            [yzgrid]=false

             [alpha]=140
          [anaglyph]=-1
     [axisextension]=0.7
           [hsrmode]=3
     [rotationsteps]=30

         [gridcolor]='rgb(128,128,128)'

  )



# EOF
