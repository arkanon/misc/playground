
# Plot3D

Wrapper gerador de URL's para plotagem de superfícies 3D com a aplicação online [CalcPlot3D](http://c3d.libretexts.org/CalcPlot3D/index.html).

## Instalação

**Opção 1:** clonar todo o repo [playground](http://gitlab.com/arkanon/playground) (atualmente com 660 MB):
```bash
git clone https://gitlab.com/arkanon/playground
ln -s $PWD/playground/plot3d/url.sh /algum_caminho_em_$PATH/plot3d
```

**Opção 2:** baixar apenas este subdiretório [plot3d](http://gitlab.com/arkanon/playground/tree/master/plot3d) do repositório:
```bash
curl -s https://gitlab.com/arkanon/playground/-/archive/master/playground-master.tar.bz2?path=plot3d | tar --strip-components=1 -jxvf -
ln -s $PWD/plot3d/url.sh /algum_caminho_em_$PATH/plot3d
```

## Uso

```shell-session
$ plot3d

Wrapper gerador de URL's para plotagem de superfícies 3D com a aplicação online CalcPlot3D (http://c3d.libretexts.org/CalcPlot3D/index.html).

Uso: plot3d <expressão> [cubes]

Expressões pré-definidas em /caminho_efetivo_do_diretório_com_os_arquivos_do_script/lib.sh:
- breast
- cardioboloide
- chest
- flor
- goursat_tangle_1
- goursat_tangle_2
- heart_yup
- heart_zup
- letra_a
- piao
- toro

Defaults de plotagem definidos em /caminho_efetivo_do_diretório_com_os_arquivos_do_script/def.sh.
```

## Biblioteca

- [Breast](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-14.0;view=0;alpha=-1;zmax=1.5;visible=true;ymax=14.0;zmin=-1.0;cubes=32;xmin=-14.0;constcol=rgb(255,0,0);equation=z~e%5E(-1%2F1000((x%2B4)%5E2%2B(y%2B4)%5E2)%5E2)%2Be%5E(-1%2F1000((x-4)%5E2%2B(y-4)%5E2)%5E2)%2B1%2F10e%5E(-((x%2B4)%5E2%2B(y%2B4)%5E2)%5E2)%2B1%2F10e%5E(-((x-4)%5E2%2B(y-4)%5E2)%5E2)%0A;format=normal;xmax=14.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BBreast%3A%20%7Dz~e%5E%7B-%5Cfrac%7B1%7D%7B1000%7D((x%2B4)%5E2%2B(y%2B4)%5E2)%5E2%7D%2Be%5E%7B-%5Cfrac%7B1%7D%7B1000%7D((x-4)%5E2%2B(y-4)%5E2)%5E2%7D%2B%5Cfrac%7B1%7D%7B10%7De%5E%7B-((x%2B4)%5E2%2B(y%2B4)%5E2)%5E2%7D%2B%5Cfrac%7B1%7D%7B10%7De%5E%7B-((x-4)%5E2%2B(y-4)%5E2)%5E2%7D%0A;textonly=false;position=middle&type=window;ymin=-2;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=2;center=-35.5014,49.5330,34.4402;yaxislabel=y;ymax=2;gridplanes=false;zmin=-2;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-2;rotationsteps=30;up=0.5419,-0.3040,0.7835;xzgrid=false;xmax=2;yscalefactor=1;zoom=0.31;centerxpercent=0.50;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.55;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=7.00;focus=0,0,0,1;transparent=false)
- [Cardioboloide](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-4.0;view=0;alpha=-1;zmax=2.0;visible=true;ymax=4.0;zmin=-2.0;cubes=32;xmin=-4.0;constcol=rgb(255,0,0);equation=z~y%5E2-yx%5E(2%2F3)%2Bx%5E2-1%0A;format=normal;xmax=4.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BCardioboloide%3A%20%7Dz~y%5E2-y%5Csqrt%5B3%5D%7Bx%5E2%7D%2Bx%5E2-1%0A;textonly=false;position=middle&type=window;ymin=-2.5;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=3.5;center=5.4911,5.7315,9.6565;yaxislabel=y;ymax=1.5;gridplanes=false;zmin=-2.5;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-3.5;rotationsteps=30;up=-0.1799,0.9317,-0.3154;xzgrid=false;xmax=1.5;yscalefactor=1;zoom=1.46;centerxpercent=0.5;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.5;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)
- [Chest](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-6.0;view=0;alpha=-1;zmax=6.0;visible=true;ymax=6.0;zmin=-6.0;cubes=32;xmin=-5.0;constcol=rgb(255,0,0);equation=x%5E2%2By%5E3%2Bz%5E3%2B20cos(x)~25%0A;format=normal;xmax=5.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BChest%3A%20%7Dx%5E2%2By%5E3%2Bz%5E3%2B20%5Ccos%7Bx%7D~25%0A;textonly=false;position=middle&type=window;ymin=-9.0;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=7.0;center=31.8048,28.6370,13.9057;yaxislabel=y;ymax=7.0;gridplanes=false;zmin=-9.0;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-9.0;rotationsteps=30;up=-0.3023,-0.2721,0.9135;xzgrid=false;xmax=7.0;yscalefactor=1;zoom=0.37;centerxpercent=0.5;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.5;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)
- [Flor](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-15.0;view=0;alpha=-1;zmax=35.0;visible=true;ymax=15.0;zmin=0.0;cubes=32;xmin=-15.0;constcol=rgb(255,0,0);equation=z~30-30%2Fsqrt(x%5E2%2By%5E2)%2B3sin(sqrt(x%5E2%2By%5E2))%2Bsqrt(sin(x)%2Bsin(y)-x%5E2-y%5E2)%2F100%0A;format=normal;xmax=15.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BFlor%3A%20%7Dz~30-%5Cfrac%7B30%7D%7B%5Csqrt%7Bx%5E2%2By%5E2%7D%7D%2B3%5Csin%7B%5Csqrt%7Bx%5E2%2By%5E2%7D%7D%2B%5Cfrac%7B%5Csqrt%7B%5Csin%7Bx%7D%2B%5Csin%7By%7D-x%5E2-y%5E2%7D%7D%7B100%7D%0A;textonly=false;position=middle&type=window;ymin=-15.0;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=35.0;center=36.5846,9.6734,24.3512;yaxislabel=y;ymax=15.0;gridplanes=false;zmin=30.0;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-15.0;rotationsteps=30;up=-0.4791,-0.4963,0.7238;xzgrid=false;xmax=15.0;yscalefactor=1;zoom=0.16;centerxpercent=0.40;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.80;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)
- [Goursat Tangle 1](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-8.0;view=0;alpha=-1;zmax=8.0;visible=true;ymax=8.0;zmin=-8.0;cubes=32;xmin=-8.0;constcol=rgb(255,0,0);equation=(x%5E4%2By%5E4%2Bz%5E4)-0.3(x%5E2%2By%5E2%2Bz%5E2)%5E2-5(x%5E2%2By%5E2%2Bz%5E2)%2B15~0%0A;format=normal;xmax=8.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BGoursat%20Tangle%201%3A%20%7D(x%5E4%2By%5E4%2Bz%5E4)-0.3(x%5E2%2By%5E2%2Bz%5E2)%5E2-5(x%5E2%2By%5E2%2Bz%5E2)%2B15~0%0A;textonly=false;position=middle&type=window;ymin=-10.0;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=10.0;center=30.1485,9.6541,38.7023;yaxislabel=y;ymax=8.0;gridplanes=false;zmin=-10.0;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-10.0;rotationsteps=30;up=-0.6192,-0.3476,0.7041;xzgrid=false;xmax=8.0;yscalefactor=1;zoom=0.28;centerxpercent=0.5;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.5;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)
- [Goursat Tangle 2](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-3.0;view=0;alpha=-1;zmax=3.0;visible=true;ymax=3.0;zmin=-3.0;cubes=32;xmin=-3.0;constcol=rgb(255,0,0);equation=(x%5E4%2By%5E4%2Bz%5E4)-0(x%5E2%2By%5E2%2Bz%5E2)%5E2-5(x%5E2%2By%5E2%2Bz%5E2)%2B12~0%0A;format=normal;xmax=3.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BGoursat%20Tangle%202%3A%20%7D(x%5E4%2By%5E4%2Bz%5E4)-0(x%5E2%2By%5E2%2Bz%5E2)%5E2-5(x%5E2%2By%5E2%2Bz%5E2)%2B12~0%0A;textonly=false;position=middle&type=window;ymin=-3.5;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=4.0;center=8.3641,14.4871,18.5786;yaxislabel=y;ymax=3.0;gridplanes=false;zmin=-6.0;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-3.0;rotationsteps=30;up=-0.4045,-0.7006,0.5878;xzgrid=false;xmax=2.5;yscalefactor=1;zoom=0.82;centerxpercent=0.5;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.5;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)
- [Heart Yup](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-2.0;view=0;alpha=-1;zmax=2.0;visible=true;ymax=2.0;zmin=-2.0;cubes=32;xmin=-2.0;constcol=rgb(255,0,0);equation=x%5E2%2By%5E2%2B3z%5E2~1%2By(x%5E2%2B1%2F9z%5E2)%5E(1%2F3)%0A;format=normal;xmax=2.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BHeart%20Yup%3A%20%7Dx%5E2%2By%5E2%2B3z%5E2~1%2By%5Csqrt%5B3%5D%7Bx%5E2%2B%5Cfrac%7B1%7D%7B9%7Dz%5E2%7D%0A;textonly=false;position=middle&type=window;ymin=-1.1;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=1.2;center=3.4378,4.0345,8.4796;yaxislabel=y;ymax=1.2;gridplanes=false;zmin=-3.0;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-1.1;rotationsteps=30;up=-0.1114,0.9521,-0.2846;xzgrid=false;xmax=1.1;yscalefactor=1;zoom=2;centerxpercent=0.5;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.5;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)
- [Heart Zup](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-2.0;view=0;alpha=-1;zmax=2.0;visible=true;ymax=2.0;zmin=-2.0;cubes=32;xmin=-2.0;constcol=rgb(255,0,0);equation=x%5E2%2B3y%5E2%2Bz%5E2~1%2Bz(x%5E2%2B1%2F9y%5E2)%5E(1%2F3)%0A;format=normal;xmax=2.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BHeart%20Zup%3A%20%7Dx%5E2%2B3y%5E2%2Bz%5E2~1%2Bz%5Csqrt%5B3%5D%7Bx%5E2%2B%5Cfrac%7B1%7D%7B9%7Dy%5E2%7D%0A;textonly=false;position=middle&type=window;ymin=-2.0;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=1.3;center=5.4540,7.7177,4.4255;yaxislabel=y;ymax=1.0;gridplanes=false;zmin=-1.1;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-1.2;rotationsteps=30;up=-0.2085,-0.2741,0.9388;xzgrid=false;xmax=1.0;yscalefactor=1;zoom=2;centerxpercent=0.5;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.5;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)
- [Letra A](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-1.1;view=0;alpha=-1;zmax=1.0;visible=true;ymax=1.1;zmin=-1.0;cubes=32;xmin=-1.1;constcol=rgb(255,0,0);equation=z~(1%2Bsgn(1.0%2Bx-abs(2y)))(1%2Bsgn(1.0-x))%2F20(1%2Bsgn(1.0%2Bx))%2F2%0A%2B(1%2Bsgn(0.4%2Bx-abs(2y)))(1%2Bsgn(0.6-x))%2F20(1-sgn(0.3-x))%2F2%0A-(1%2Bsgn(0.4%2Bx-abs(2y)))(1%2Bsgn(1.0-x))%2F20%0A;format=normal;xmax=1.1;fixdomain=true&type=jaxlabel;math=%20Letra%20A%20%0A;textonly=true;position=middle&type=window;ymin=-0.6;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=3.0;center=0.9052,1.5593,9.8361;yaxislabel=y;ymax=0.6;gridplanes=false;zmin=-10.0;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-0.6;rotationsteps=30;up=-0.8660,-0.4973,0.0523;xzgrid=false;xmax=0.6;yscalefactor=1;zoom=2.49;centerxpercent=0.5;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.5;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)
- [Piao](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-8.0;view=0;alpha=-1;zmax=8.0;visible=true;ymax=8.0;zmin=-8.0;cubes=32;xmin=-8.0;constcol=rgb(255,0,0);equation=(sqrt(x%5E2%2By%5E2)-4)%5E3%2Bz%5E2~2%0A;format=normal;xmax=8.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BPiao%3A%20%7D%5Cleft(%5Csqrt%7Bx%5E2%2By%5E2%7D-4%5Cright)%5E3%2Bz%5E2~2%0A;textonly=false;position=middle&type=window;ymin=-6.0;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=4.0;center=28.8274,16.6435,10.8156;yaxislabel=y;ymax=6.0;gridplanes=false;zmin=-6.0;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-7.0;rotationsteps=30;up=-0.0000,-0.0000,2.0000;xzgrid=false;xmax=6.0;yscalefactor=1;zoom=0.44;centerxpercent=0.5;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.5;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)
- [Toro](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;ymin=-8.0;view=0;alpha=-1;zmax=8.0;visible=true;ymax=8.0;zmin=-8.0;cubes=32;xmin=-8.0;constcol=rgb(255,0,0);equation=(sqrt(x%5E2%2By%5E2)-4)%5E2%2Bz%5E2~2%0A;format=normal;xmax=8.0;fixdomain=true&type=jaxlabel;math=%5Ctext%7BToro%3A%20%7D(%5Csqrt%7Bx%5E2%2By%5E2%7D-4)%5E2%2Bz%5E2~2%0A;textonly=false;position=middle&type=window;ymin=-6.0;showticks=false;xaxislabel=x;yzgrid=false;xscalefactor=1;alpha=140;anaglyph=-1;gridcolor=rgb(128,128,128);zmax=4.0;center=35.0314,20.2254,29.3892;yaxislabel=y;ymax=6.0;gridplanes=false;zmin=-6.0;twoviews=false;gridsonbox=false;edgeson=false;xygrid=false;xscale=1;hsrmode=3;xmin=-7.0;rotationsteps=30;up=-0.4330,-0.2500,0.8660;xzgrid=false;xmax=6.0;yscalefactor=1;zoom=0.67;centerxpercent=0.5;showaxes=true;unlinkviews=false;perspective=true;zcmin=-4;centerypercent=0.5;zaxislabel=z;showbox=false;zcmax=4;faceson=true;autospin=true;nomidpts=true;axisextension=0.7;zscale=1;yscale=1;zscalefactor=1;focus=0,0,0,1;transparent=false)

## Gráficos 2D de coração criados no Desmos

- [❤️                         ](http://desmos.com/calculator/upun2x7p1l)
- [I ❤️ Math                  ](http://desmos.com/calculator/sok0e2jvrd)
- [Modulação de Onda (Manual)](http://desmos.com/calculator/rjske3fgb9)
- [Modulação de Onda (Slow)  ](http://desmos.com/calculator/5jhdvbup1l)
- [Modulação de Onda (Pulse) ](http://desmos.com/calculator/uutxovicmi)

## Gráfico 3D do coração usado para montar a imagem "I ❤️ Math"

- [Repositório](http://gitlab.com/arkanon/playground/tree/master/ilovemath)
- [Post no FB](http://fb.com/arkanon/posts/pfbid0c1KJGuSHDn7bxuzPTW2u8BW9TtnrK4X5MVChdfS9PivBBwAafLgUPiaLu8Z3iRM1l)
- Gráfico no CalcPlot3D
  - [original         ](http://c3d.libretexts.org/CalcPlot3D/index.html?type=implicit;equation=x%5E2+3y%5E2+z%5E2~1+z(x%5E2+1/9y%5E2)%5E(1/3);cubes=64;alpha=-1;constcol=rgb(255,0,0);fixdomain=true;format=normal;view=0;visible=true;xmin=-2.0;xmax=2.0;ymin=-2.0;ymax=2.0;zmin=-2.0;zmax=2.0&type=jaxlabel;math=x%5E2%2B3y%5E2%2Bz%5E2~1%2Bz%5Csqrt%5B3%5D%7Bx%5E2%2B%5Cfrac%7B1%7D%7B9%7Dy%5E2%7D;position=middle;textonly=false&type=window;xmin=-1.2;xmax=1.0;ymin=-2.0;ymax=1.0;zmin=-1.1;zmax=1.3;center=5.4540,7.7177,4.4255,1;up=-0.2085,-0.2741,0.9388,1;zoom=2.0;focus=0.0,0.0,0.0,1;xaxislabel=x;yaxislabel=y;zaxislabel=z;xscale=1;yscale=1;zscale=1;xscalefactor=1;yscalefactor=1;zscalefactor=1;autospin=true;edgeson=false;faceson=true;gridplanes=false;gridsonbox=false;nomidpts=true;perspective=true;showaxes=true;showbox=false;showticks=false;transparent=false;twoviews=false;unlinkviews=false;xygrid=false;xzgrid=false;yzgrid=false;alpha=140;anaglyph=-1;axisextension=0.7;centerxpercent=0.5;centerypercent=0.5;hsrmode=3;rotationsteps=30;zcmax=4;zcmin=-4;gridcolor=rgb(128,128,128))
  - [tiny.cc/ilovemath](http://tiny.cc/ilovemath)

Links encurtados alternativos (todos com alguma desvantagem técnica em relação ao do `tiny.cc`)
- [bit.ly/il0v3math ](http://bit.ly/il0v3math)
- [bit.ly/iluvmath  ](http://bit.ly/iluvmath)
- [t.ly/ilovemath   ](http://t.ly/ilovemath)
- [rb.gy/7go2a      ](http://rb.gy/7go2a)
- [cutt.ly/il0v3math](http://cutt.ly/il0v3math)
- [lnkiy.in/DSSPP   ](http://lnkiy.in/DSSPP)

## Documentação

CalcPlot3D
- [Rotating the 3D Plot & Key Shortcuts ](http://c3d.libretexts.org/CalcPlot3D/CalcPlot3D-Help/section-2.html)
- [List of Available Functions          ](http://c3d.libretexts.org/CalcPlot3D/CalcPlot3D-Help/appendix-availablefunctions.html)

ANSI
- [ Hyperlinks in Terminal Emulators    ](http://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda)
- [OSC 8 Adoption  in Terminal Emulators](http://github.com/Alhadis/OSC8-Adoption)  
  `OSC`: Operating System Command  
  ` ST`: String Terminator
- Teste (ok no gnome-terminal sem screen)
  ```bash
  curl -Ls gitlab.gnome.org/GNOME/vte/raw/master/perf/hyperlink-demo.txt
  ```

☐
