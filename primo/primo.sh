#!/bin/bash

# primo
#   Testa a primalidade de um inteiro de tamanho arbitrario.
#   Se nao for quadrado perfeito nem divisivel por 2, 3 e 5, retorna a fatoracao prima do numero.
#
# Arkanon <arkanon@lsd.org.br>
# 2013/05/04 (Sáb) 05:51:52 (BRS)  # calculos usando exclusivamente bc, para operar com numeros
#                                    arbitrariamente grandes
# 2013/05/03 (Sex) 14:08:34 (BRS)  # calculos com recursos nativos do bash

if [ $# = 0 ]
then
  echo "Use: $(basename $0) <numero inteiro>"
  exit
fi

n=$1

[ ${n#${n%?}} = 5 ]                   && divisor=5
[ $[($(sed 's/./+&/g'<<<$n))%3] = 0 ] && divisor=3
echo "02468" | grep -q ${n#${n%?}}    && divisor=2

export BC_LINE_LENGTH=0

              out=$(bc<<<"r=sqrt($n);scale=100;r==sqrt($n);r")
quadrado_perfeito=$(echo "$out" | head -n1)
    raiz_quadrada=$(echo "$out" | tail -n1)

[ $quadrado_perfeito == 1 ] && divisor=$raiz_quadrada

if [ "$divisor" ]
then
  [ $quadrado_perfeito = 1 ] && echo "quadrado perfeito"
  echo "divisivel por $divisor"
  exit
fi

# tem chance de ser primo
# echo "testando divisores impares de $n entre 7 e $raiz_quadrada"

divisor=7
while [ $(bc<<<"$divisor<$raiz_quadrada") = 1 ]
do
# echo "$divisor .. $raiz_quadrada"
  [ $(bc<<<"$n%$divisor") = 0 ] && break || ((divisor+=2))
done

if [ $(bc<<<"$raiz_quadrada-$divisor>1") = 1 ]
then
  quociente=$(bc<<<"$n/$divisor")
  $0 $quociente
# echo "$n: divisivel por $divisor. Quociente: $quociente"
  echo -n "$divisor "
else
# echo "$n: primo"
  echo -n "$n "
fi

[ $(bc<<<"$divisor<=7") = 1 ] && echo

# Exemplos
#
#   # <http://pt.wikipedia.org/wiki/Numero_primo>

#   # o numero formado pelo produto dos primeiros 22 primos maiores que 6
#   n=$(bc<<<"7*11*13*17*19*23*29*31*37*41*43*47*53*59*61*67*71*73*79*83*89*97")
#   n=76852265464850614158436738244391869
#
#   primo $n
#     testando divisores impares de 76852265464850614158436738244391869 entre 7 e 277222411548652779
#     testando divisores impares de 10978895066407230594062391177770267 entre 7 e 104780222687333655
#     testando divisores impares de 998081369673384599460217379797297 entre 7 e 31592425827615463
#     testando divisores impares de 76775489974875738420016721522869 entre 7 e 8762162402904647
#     testando divisores impares de 4516205292639749318824513030757 entre 7 e 2125136535058335
#     testando divisores impares de 237695015402092069411816475303 entre 7 e 487539757765550
#     testando divisores impares de 10334565887047481278774629361 entre 7 e 101659066920012
#     testando divisores impares de 356364340932671768233607909 entre 7 e 18877614810475
#     testando divisores impares de 11495623901053928007535739 entre 7 e 3390519709580
#     testando divisores impares de 310692537866322378582047 entre 7 e 557398006693
#     testando divisores impares de 7577866777227375087367 entre 7 e 87050943574
#     testando divisores impares de 176229459935520350869 entre 7 e 13275144441
#     testando divisores impares de 3749562977351496827 entre 7 e 1936378831
#     testando divisores impares de 70746471270782959 entre 7 e 265982088
#     testando divisores impares de 1199092733403101 entre 7 e 34627918
#     testando divisores impares de 19657257924641 entre 7 e 4433650
#     testando divisores impares de 293391909323 entre 7 e 541656
#     testando divisores impares de 4132280413 entre 7 e 64282
#     testando divisores impares de 56606581 entre 7 e 7523
#     testando divisores impares de 716539 entre 7 e 846
#     testando divisores impares de 8633 entre 7 e 92
#     testando divisores impares de 97 entre 7 e 9
#     97: primo
#     8633: divisivel por 89. Quociente: 97
#     716539: divisivel por 83. Quociente: 8633
#     56606581: divisivel por 79. Quociente: 716539
#     4132280413: divisivel por 73. Quociente: 56606581
#     293391909323: divisivel por 71. Quociente: 4132280413
#     19657257924641: divisivel por 67. Quociente: 293391909323
#     1199092733403101: divisivel por 61. Quociente: 19657257924641
#     70746471270782959: divisivel por 59. Quociente: 1199092733403101
#     3749562977351496827: divisivel por 53. Quociente: 70746471270782959
#     176229459935520350869: divisivel por 47. Quociente: 3749562977351496827
#     7577866777227375087367: divisivel por 43. Quociente: 176229459935520350869
#     310692537866322378582047: divisivel por 41. Quociente: 7577866777227375087367
#     11495623901053928007535739: divisivel por 37. Quociente: 310692537866322378582047
#     356364340932671768233607909: divisivel por 31. Quociente: 11495623901053928007535739
#     10334565887047481278774629361: divisivel por 29. Quociente: 356364340932671768233607909
#     237695015402092069411816475303: divisivel por 23. Quociente: 10334565887047481278774629361
#     4516205292639749318824513030757: divisivel por 19. Quociente: 237695015402092069411816475303
#     76775489974875738420016721522869: divisivel por 17. Quociente: 4516205292639749318824513030757
#     998081369673384599460217379797297: divisivel por 13. Quociente: 76775489974875738420016721522869
#     10978895066407230594062391177770267: divisivel por 11. Quociente: 998081369673384599460217379797297
#     76852265464850614158436738244391869: divisivel por 7. Quociente: 10978895066407230594062391177770267

#   # o numero formado pelo produto dos primeiros 165 primos maiores que 6
#
#   export BC_LINE_LENGTH=0
#   n=$(bc<<<"7*11*13*17*19*23*29*31*37*41*43*47*53*59*61*\
#             67*71*73*79*83*89*97*101*103*107*109*113*127*131*137*\
#             139*149*151*157*163*167*173*179*181*191*193*197*199*211*223*\
#             227*229*233*239*241*251*257*263*269*271*277*281*283*293*307*\
#             311*313*317*331*337*347*349*353*359*367*373*379*383*389*397*\
#             401*409*419*421*431*433*439*443*449*457*461*463*467*479*487*\
#             491*499*503*509*521*523*541*547*557*563*569*571*577*587*593*\
#             599*601*607*613*617*619*631*641*643*647*653*659*661*673*677*\
#             683*691*701*709*719*727*733*739*743*751*757*761*769*773*787*\
#             797*809*811*821*823*827*829*839*853*857*859*863*877*881*883*\
#             887*907*911*919*929*937*941*947*953*967*971*977*983*991*997")
#
#   n=6530113548333027810420836066068793682041324130196\
#   4560746275351096562221054599568872839838829298274\
#   4053207651970514533971639650990840508890943076099\
#   6950883007787577130801339313047336986087131211653\
#   1571612523989072240811366890395054092203777117308\
#   2961632997163071906276955996562504581317311296798\
#   9364968516583462135702011278862220227846400337211\
#   9726333466348316884010999165143286643782232716017\
#   7282469352706929704197
#
#   time primo $n | tee log # 00:07:28.50

# EOF
