#!/usr/bin/python3

# Arkanon <arkanon@lsd.org.br>
# 2020/09/13 (Sun) 08:09:58 -03

# <http://fb.com/groups/1567682496877142/permalink/2613185442326837>



import os
import numpy as np
from PIL import Image as im
from IPython.display import display as dp
from decimal import Decimal as dec



side    = 512

sources = [
          # 'src/wvnly0nvcks31.png',
        # # 'src/119029724_2684325851824346_7313868802135393631_n.jpg', # singular
          # 'src/matrizes.png',
          # 'src/arkanon-ng-800x800.png',
          # 'src/EhM44LtUMAAhLOm.jpg',
        # # 'src/contraste.png', # singular
            'src/inversa.png',
          ]



def formula(n):
  return [
           int(n*10**13)%16776957,
         # n*10**13,
           int(1/n)%16776957,
           man_exp(n)[0]*16776957,
         ]

def man_exp(n):
  arr = ('%.e'%n).split('e')
  man = float(arr[-2])/10
  exp = int  (arr[-1])+1
  return [man,exp]

def show(i,j):
  print(          str( matrix1    [i,j] ) \
         + '  ' + str( matrix2    [i,j] ) \
         + '  ' + str( matrix_inv1[i,j] ) \
         + '  ' + str( man_exp(matrix_inv1[i,j]) ) \
       # + '  ' + str( matrix_prod[i,j] ) \
         + '  ' + str( normaliz(matrix_inv1[i,j]) ) \
         + '  ' + str( matrix_inv2[i,j] ) \
       )

rgba2dec = lambda k: k[0]*256**2 + k[1]*256 + k[2]
dec2rgba = lambda n: [ int(n/256**2)%256, int(n/256)%256, n%256, 255 ]



for source in sources:

  base = os.path.basename(source)
  name = os.path.splitext(base)[0]

  for f in range(len(formula(1))):

    normaliz = lambda n: formula(abs(n))[f]

    matrix_name     = 'out/matrix--'+name+'.png'
    matrix_inv_name = 'out/matrix--formula_'+str(f)+'--'+name+'--inv.png'

    matrix_png = im.open(source)
    matrix_png = matrix_png.resize((side,side),im.ANTIALIAS)
    matrix_png.save(matrix_name)

    matrix1     = np.asarray(matrix_png)
    matrix2     = np.zeros(shape=(side,side))
    matrix_inv2 = np.zeros(shape=(side,side,4))

    for i in range(side):
      for j in range(side):
        matrix2[i,j] = rgba2dec(matrix1[i,j])

    matrix_id   = np.identity(side)

    matrix_det  = np.linalg.det(matrix2)

    print( )
    print( 'source  : ' + source )
    print( 'formula : ' + str(f) )
    print( 'matrix  : ' + matrix_name )
    print( 'det     : ' + str(matrix_det) )

    if (matrix_det==0):
      print("\nMatrix determinant is zero. Cannot be inverted.\n")
      exit()

    matrix_inv1 = np.linalg.inv(matrix2)
    matrix_prod = matrix2 @ matrix_inv1

    for i in range(side):
      for j in range(side):
        matrix_inv2[i,j] = dec2rgba(normaliz(matrix_inv1[i,j]))

    r1 = [255, 255, 255]
    r2 = [255, 254, 253]
    n2 = 16776957

    print( 'inverse : ' + matrix_inv_name )
    print( )
    print( rgba2dec(r1) )
    print( rgba2dec(r2) )
    print( dec2rgba(n2) )
    print( )
    print( matrix1.shape )
    print( matrix_inv2.shape )
    print( )
    show (  0,  0)
    show (  0,511)
    show (511,511)
    show (511,  0)
    print( )

    matrix_inv_png = im.fromarray(matrix_inv2.astype(np.uint8)).convert('RGBA')
    matrix_inv_png.save(matrix_inv_name)

    dp(matrix_png,matrix_inv_png)
    print( )



# EOF
