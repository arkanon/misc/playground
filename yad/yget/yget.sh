#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/03/13 (Sat) 07:27:49 -03
# 2021/03/09 (Tue) 11:32:08 -03
#
# Glutanimate <http://askubuntu.com/users/81372/glutanimate>
# 2014/05/11 (Mon) 00:42:55 UTC

# Interface YAD para lançamento e acompanhamento de downloads pelo wget



  # descomentar para teste
  urls=(
         http://proof.ovh.net/files/100Mb.dat
         http://speedtest.wdc01.softlayer.com/downloads/test10.zip
         http://cachefly.cachefly.net/100mb.test # [longo]
         http://proof.ovh.net/files/100Mb.dat
        'https://web.archive.org/web/20200810211554if_/https://download.microsoft.com/download/2/5/2/2526f55d-32bc-410f-be18-164ba67ae07d/XPSEP XP and Server 2003 32 bit.msi' # [espaços]
         http://speedtest.wdc01.softlayer.com/downloads/test10.zip
         http://proof.ovh.et/files/100Mb.dat     # [erro]
         http://speedtest.wdc01.softlayer.com/downloads/test10.zip
         http://proof.ovh.net/files/100Mb.dat
         http://speedtest.wdc01.softlayer.com/downloads/test10.zip
         http://proof.ovh.net/files/100Mb.dat
         http://speedtest.wdc01.softlayer.com/downloads/test10.zip
       )



  script=${0##*/}
   maxdl=3 # valor default do número máximo de downloads simultâneos
    text=Progresso:
    icon=emblem-downloads
   image=browser-download
   theme=$(gsettings get org.gnome.desktop.interface gtk-theme) # [Greybird] tema em uso pelo window manager
   wmthm=/usr/share/themes/${theme//\'/}/gtk-3.0/gtk.css # '
  thmdir=$HOME/.themes/$script/gtk-3.0 # diretório de temas pessoais

# export GTK_DEBUG=interactive
  export GTK_THEME=$script
  export    LC_ALL=C



  main()
  {

    # processamento do array de parâmetros
    parms=($@)
    [[ ${parms[@]: -1} =~ ^[+-]?[0-9]+$ ]] && { maxdl=${parms[@]: -1}; unset parms[$#-1]; }
    (( ${#parms[@]} > 0 )) && urls=(${parms[@]})
    (( ${#urls[@]} == 0 )) && { echo "Uso: $script <url> [url] ... [maxdl:3]" >&2; exit 1; }

    # definição do tema GTK+ <http://developer.gnome.org/pygtk/stable/pango-markup-language.html>
    mkdir -p $thmdir
    cat << EOT > $thmdir/gtk.css
@import url("$wmthm");
grid.horizontal        label    { margin-top    :   6px ; border: 0px solid blue   ; margin-right: 10px; }
progressbar                     { margin-top    :  10px ; border: 0px solid yellow ; color: rgba(0,0,0,1); }
progressbar            text     { margin-bottom :   3px ; border: 0px solid red    ; }
progressbar.horizontal trough   { margin-top    : -20px ; border: 0px solid lime   ; min-height: 16px; min-width: 300px; }
progressbar.horizontal progress { min-height    :  18px ; }
EOT

    # determinação do tamanho, nome e tipo de barra de cada arquivo
    for url in "${urls[@]}"
    {
       header=$(wget -S --spider "$url" 2>&1)
        fsize=$(grep -ioP 'content-length: \K.+' <<< "$header" | numfmt --to=iec-i --suffix=B --format %.1f 2> /dev/null)
        fname=$(grep -ioP 'content-disposition[^"]+"\K[^"]+' <<< "$header" || echo ${url##*/})
        btype=$(grep -iq  'unspecified' <<< "$header" && echo PULSE || echo NORM)
      fsizes+=("$fsize")
      fnames+=("$fname")
      btypes+=("$btype")
    }

    # determinação da maior string de tamanho, para alinhamento à direita em fonte mono-espaçada
    longests=$(IFS=$'\n'; echo "${fsizes[*]}" | wc -L)

    # definição da lista de barras de progresso
    for i in ${!fnames[*]}
    {
      printf -v fsize  "%${longests}s" "${fsizes[i]}"
      fname=${fnames[i]}
      ybars+=(--bar "<span font_family='monospace'>$fsize  </span><span underline='none'><a href='${urls[i]}' title='${urls[i]}'>$fname</a></span>:${btypes[i]}")
    }

    # iteração entre as URLs iniciando o download em background e passando simultaneamente todas as saídas ao yad
    # original: http://pastebin.com/yBL2wjaY
    count=1
    for url in "${urls[@]}"
    {
      get "$url" $count &
      pids[$count]=$!
      while (( $(ps h ${pids[*]} | wc -l) >= maxdl )); do sleep .1; done
      ((count++))
    } | yad --on-top --progress --auto-kill "${ybars[@]}" --title "$script" --text "$text" --window-icon "$icon" --image "$image"

  }



  # download com sed filtrando da saída do wget as informações de
  #   progresso
  #   velocidade
  #   ETA (Estimated Time of Arrival)
  #   tempo total
  #   velocidade média
  # original: http://ubuntuforums.org/showthread.php?t=306515&page=2&p=7455412#post7455412
  get()
  {
    local sed
    sed="

          / [0-9]{1,2}%/ {
            s@.* ([0-9]+)% +([0-9.]+.) (.*)@$2:\1\n$2:#\2/s, ETA \3@
            p
          }

          /100%.+=/ {
            s@.*100%.+=(.+)@\1 @
            N;N;s@\n@@g
            s@([^ ]+).+[^(]+\(([^)]+).+@$2:100\n$2:#Finalizado em \1 \(\2\)@
            p
          }

          /[0-9.]+[A-Z]$/ {
            s@.+ ([0-9.]+[A-Z]$)@$2:0\n$2:#\1/s@
            p
          }

          /[0-9.]+[A-Z]=|saved/ {
            N;N;s@\n@ @g
            s@.+=([^ ]+).+[^(]+\(([^)]+).+@$2:0\n$2:#Finalizado em \1 \(\2\)@
            p
          }

        "
    wget "$1" 2>&1 | sed -rnu "$sed"
    (( PIPESTATUS[0] > 0 )) && echo "$2:#Erro"
  }



  main "$@" &



# EOF
