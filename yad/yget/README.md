# yget

Interface YAD para lançamento e acompanhamento de downloads pelo wget.  
Inspirada na ideia e código apresentados em [How can I create a wget GUI with multiple progress bars?](http://tiny.cc/qjiutz)

![yget dialog](https://gitlab.com/arkanon/playground/-/raw/master/yad/yget/tema-3.png)

## TODO
1. `ok` customizar o tema gtk para a descrição do progresso ficar dentro da barra
1. `ok` executar já em background
1. `ok` trocar o monitoramento da contagem de processos wget para a de processos lançados em background
1. `ok` identificar automaticamente o tema em uso pelo wm
1. `ok` adotar como nome do arquivo o valor do header Content-Disposition, se houver
1. `ok` usar fonte monoespaçada no label das barras para alinhar os tamanhos e os nomes dos arquivos
1. `ok` usar barra pulse para o download de arquivos dos quais não há informação da porcentagem de progresso e ETA
1. `ok` apresentar o nome do arquivo como um link para a url de download
1. `--` parametrizar diretório de destino dos downloads
1. `--` label à direita da barra de progresso
1. `--` usar classe css no span de fsize e fname para mover a definição do estilo para o arquivo css de tema (possível?)
1. `--` abrir imediatamente o diálogo e incluir a identificação do tamanho do arquivo como primeiro passo do progresso
1. `--` apresentar o nome com o qual o arquivo está sendo baixado
1. `--` pause geral e específico

```
# EOF
```
