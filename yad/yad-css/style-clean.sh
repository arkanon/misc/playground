#!/bin/bash

  mytheme=yad-css

  themedir=$HOME/.themes/$mytheme/gtk-3.0

  mkdir -p $themedir

  : $(gsettings get org.gnome.desktop.interface gtk-theme)
  deftheme=${_//\'/}

  url=/usr/share/themes/$deftheme/gtk-3.0/gtk.css

  # <http://docs.gtk.org/gtk3/css-properties.html>
  cat << EOT > $themedir/gtk.css

   @import url("$url");

   window>box>box>box
   {
     background: lightblue;
     margin: 10px;
     padding: 5px 8px 3px 8px;
     border: 1px solid red;
   }

   window>box>box>box>label
   {
     color: green;
     font-family: serif;
     font-size: 30px;
   }

EOT

  nbsp=$'\u00a0'
   msg="Hello${nbsp}World!"

# # <http://wiki.gnome.org/action/show/Projects/GTK/Inspector>
# export GTK_DEBUG=interactive
  export GTK_THEME=$mytheme

  yad --text "$msg"

