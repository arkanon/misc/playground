#!/bin/bash

  theme=yad-css

  themedir=$HOME/.themes/$theme/gtk-3.0

  mkdir -p $themedir

# sudo apt install dconf-cli
# dconf list /org/gnome/desktop/interface/
# dconf read /org/gnome/desktop/interface/gtk-theme
#
# gsettings list-schemas
# gsettings list-keys org.gnome.desktop.interface
#
# ls -l /usr/share/themes/Greybird/*/*.css
#
# gsettings set org.gtk.Settings.Debug enable-inspector-keybinding true
# yad
# ctrl+shift+d or i

  : $(gsettings get org.gnome.desktop.interface gtk-theme)
  deftheme=${_//\'/}

# url=resource:///org/gtk/libgtk/theme/$deftheme/gtk.css
# url=resource:///org/gnome/desktop/interface/$deftheme/gtk-3.0/gtk.css
# url=resource:///com/ubuntu/themes/$deftheme/gtk-3.0/gtk.css
  url=/usr/share/themes/$deftheme/gtk-3.0/gtk.css

  # <http://wiki.gnome.org/action/show/Projects/GTK/Inspector>
  # <http://docs.gtk.org/gtk3/css-properties.html>
  cat << EOT > $themedir/gtk.css

   @import url("$url");

   window>box>box>box
   {
     background: lightblue;
     margin: 10px;
     padding: 5px 8px 3px 8px;
     border: 1px solid red;
   }

   window>box>box>box>label
   {
     color: green;
     font-family: serif;
     font-size: 30px;
   }

EOT

  msg="Hello World!"
# msg="Hello"$'\u00a0'"World!"

# export GTK_DEBUG=interactive
  export GTK_THEME=$theme
  export GTK_CSD=0

  yad --text "$msg"

# EOF
