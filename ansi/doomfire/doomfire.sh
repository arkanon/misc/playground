#! /bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/10/28 (Thu) 15:00:24 -03
# 2019/12/17 (Tue) 08:00:00 -03

# <http://youtube.com/watch?v=9B_thOpe7qw>
# <http://github.com/filipedeschamps/doom-fire-algorithm>
# <http://github.com/filipedeschamps/doom-fire-algorithm/tree/master/playground/BashDoomFire>

# legal o foguinho, olhei mais de uma vez o algoritimo
# e fiquei com aquela coceira na mão de implementar
# tava indo dormir e voltei e fiz em shell script (bash) !!!
# é lento que dói, só não implementei ainda o aumento de chamas
# e o decremento, mas diminuindo os valores de lin e col
# até fica parecido. minha escolha de cores não foi perfeita
# fui no olhômetro olhando uma palheta e não tem as 37 ou 36 cores
# se deixar espaço depois do primeiro "m" no array de cores
# a cor escura aparece, se deixar como está a cor fica transparente
# Have fun !!! :D Se puderem fazer um videozinho pra por no youtube
# se tua máquina for bem rápida. tput é muito lento, o echo salvou
# no final usando escape codes de posicionamento.
# sou viciado nesses screensaver de cli escrito em bash
# tem pipes.sh, vários do matrix e agora tem do fogo do DOOM !
# Meu game favorito, é tosco eu sei, qdoom is ma fav mod.
# viva o doomworld e moddb . Viva o Doom ! Fui !



# lin=$(tput lines)
  lin=40
# col=$(tput cols)
  col=80
  tpx=$(( col * lin ))

  declare -a fpx

  # primeira cor: fundo
# paintbg=true
  paintbg=false
  fcl=( 0 52 88 94 124 1 130 136 160 166 167 172 178 196 202 208 214 220 226 11 190 227 228 229 230 231 15 )



  # est for color array
# for i in {1..38}; { echo -e ${fcl[i]}; }

  Wind=$(( RANDOM % 3 )) # 0=left; 1=nada; 2=right



  start()
  {

    tty=$(stty --save)

    # make cursor disapear
    tput civis

    # clear the screen
    clear

    makeDataArray
    makeFireSauce
  # fireRenderVousz

    stty -echo -icanon min 0
    while ! read -t0
    do
      firePropeler
    done
    read -n1
    stty $tty

    # get cursor back
    tput cnorm

    # include new line to put prompt in a ... new line
  # echo -e '\n\n'

  }



  # function ok, it does what it has to do
  makeDataArray()
  {
    str=0
    for i in $(seq $str $tpx)
    {
      fpx[i]=0
    }
  }



  firePropeler()
  {
    for (( colx=0; colx<col; colx++ ))
    {
      for (( rowx=0; rowx<lin; rowx++ ))
      {
        otPidx=$(( colx + col*rowx ))
        updateFireIntensity $otPidx
      }
    }
    fireRenderVousz
  }



  fireRenderVousz()
  {
    for (( rowr=0; rowr<lin; rowr++ ))
    {
      for (( colr=0; colr<col; colr++ ))
      {
        pidx=$(( colr + col*rowr ))
        fInt=${fpx[pidx]} # fire intensity
        sp=' ' && (( fInt == 0 )) && ! $paintbg && sp=
        nClr="\e[48;5;${fcl[fInt]}m$sp\e[m" # color
      # tput cup $rowr $colr && echo -en $nClr
        echo -en "\e[${rowr};${colr}H${nClr}\e[H"
      }
    }
  }



  updateFireIntensity()
  {
     crPidx=$1                     # currentPixelIndex
     blPidx=$(( crPidx + col ))    # BellowPixelIndex
     cPLUSl=$(( col    * lin ))
    (( blPidx > cPLUSl )) && return
      decay=$(( RANDOM % 3 ))
    blFPint=${fpx[blPidx]}         # belowFirePixelIntensity
    nwFPint=$(( blFPint - decay )) # newFirePixelIntensity
    (( nwFPint <= 0 )) && nwFPint=0
    (( Wind    == 0 )) && fpx[crPidx-decay]=$nwFPint
    (( Wind    == 1 )) && fpx[crPidx      ]=$nwFPint
    (( Wind    == 2 )) && fpx[crPidx+decay]=$nwFPint
  }



  makeFireSauce()
  {
    for (( cols=0; cols<col; cols++ ))
    {
      ovPidx=$(( lin    * col  ))
      nwPidx=$(( ovPidx - col  ))
      nwPidx=$(( nwPidx + cols ))
      frInts=fpx[nwPidx]
      fpx[nwPidx]=36
    }
  }



  start



# EOF
