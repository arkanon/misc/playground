#!/bin/bash

# Matrix ANSI Terminal Screensaver
# Arkanon <arkanon@lsd.org.br>
# v1.4.0 - 2009/09/19 (S�b) 02:23:41 (BRS)
# v1.3   - minha primeira versao
# v1.2   - terceira versao do Julio Neves
# v1.0   - primeira versao do Julio Neves
#
# TODO
#   - restaurar a tela anterior ao lancamento do script
#   - guardar o conteudo da tela antes de comecar a preencher com os caracteres
#   - desabilitar o ^C
#   - perceber que o terminal esta abandonado e iniciar automaticamente o algoritmo
#   - sair quando for pressionada alguma tecla (ou movido o mouse?)
#   - ao sair, autenticar com a senha do usuario que iniciou o processo
#   - transformar em modulo da funcao zz ss
#
# INTRODUCAO
#
# Essa eh uma abordagem com FIFO's na solucao de um problema discutido
# na lista Shell-Script: um screensaver para terminal simulando a sequencia
# descendente de caracteres das telas dos compudadores presentes no filme Matrix.
#
# Aqui ha dois videos mostrando como ele deve funcionar:
#
#   <http://www.youtube.com/watch?v=QWqvLZ_ioCg>
#   <http://www.youtube.com/watch?v=FPPh5H2Ivd0>
#
# Evidentemente ele ainda nao funciona EXATAMENTE da mesma forma, mas ja lembra um pouco...
#
# Sao usados 2 FIFO's: um para a fila de caracteres a serem impressos e outro para
# a fila das colunas descendentes a serem ocupadas.
#
# O codigo foi inicialmente baseado no script do Julio Neves (novidade... :-p)

    FIFO="/tmp/`basename $0`-$$"			# define um nome e local para o fifo que vai controlar a impressao dos caracteres
COL_FIFO="$FIFO-col"					# no mesmo local, define o nome do fifo que vai controlar o uso das colunas do terminal
  oritty=`stty -g`					# guarda as conf do terminal

interruption()						# define o que sera executado quando o algoritmo for interrompido:
{
  set +o noglob;					# habilita o caracter coringa *
  tput cnorm						# torna o cursor visivel
  tput sgr0						# restaura os atributos default do texto
  rm -f $1 $2						# apaga o fifo, o 1o parametro. Todos os outros sao a conf original do tty
  shift 2						# remove o 1o parametro da lista de parametros
  stty "$*"						# restaura as conf originais do terminal
  clear							# limpa a tela
}							# TODO: restaurar a tela anterior ao lancamento do script

mod()							# define uma funcao que...
{
  echo $((asci+$1%(ascf-asci)))				# ... mantem um determinado valor acima de um minimo e abaixo de um maximo, usando modulo.
}

send()							# define uma funcao que...
{
  local asc=$1
  local cor=$2
  local est=$3
  [ $asc = 92 ]   \
    && CHR='\\' \
    || CHR=$(echo "$asc" | awk '{printf("%c",$1)}')	# ... pega o caracter do codigo ascii passado (se for o backslash, precisa ser "escapado")...
  echo "$CHR $cor $est $LIN $COL" > $FIFO		# e envia, junto com as demais informacoes, para o fifo de impressao,
  sleep $delay						# esperando um tempo determinado para ser possivel de acompanhar a impressao na tela (para debug)
}

trap "interruption $FIFO $COL_FIFO $oritty" 0 1 2 3 15	# executa interruption ao fim normal (0) ou no caso de interrupcao (1 2 3 15) do script

							# TODO: guardar o conteudo da tela antes de comecar a preencher com os caracteres
clear							# limpa a tela
tput civis						# torna o cursor invisivel
set -o noglob;						# desabilita o caracter coringa *
#stty -echo -icanon time 0 intr '^-'			# TODO: desabilitar o ^C

  green=2						# da nomes amigaveis aos codigos das cores e estilos no tput
  white=7
 normal="sgr0"
negrito="bold"

  quant=5						# define a quantidade de caracteres "simultaneamente" descendentes na tela
    var=3						# define quantos caracteres adiante o caracter final vai estar do caracter inicial
   asci=33						# codigo do primeiro caracter do intervalo da tabela ascii a ser usado aleatoriamente
   ascf=127						# e codigo do ultimo caracter do intervalo
  delay=0.0						# tempo de espera (em segundos) entre a impressao de cada caracter (se for preciso ler a sequencia)
estilos=($normal $negrito)				# estilos que serao pegos aleatoriamente para definir o tom da cor da coluna

      C=$(tput cols)					# quantidade de colunas do terminal
      L=$(tput lines)					# quantidade de linhas

rm -f  $FIFO $COL_FIFO					# apaga os fifos incondicionalmente
mkfifo $FIFO $COL_FIFO					# cria os fifos

							# INICIO DO ALGORITMO
							# TODO: perceber que o terminal esta abandonado e iniciar automaticamente o algoritmo

while [ -p $FIFO ]					# enquanto o fifo da impressao existir, inicia um processo em background independente que...
do
  col=$((RANDOM%C))					# ... pega aleatoriamente uma coluna.
  [ "${cols[$col]}" -a ${#cols[*]} -lt $C ] && continue	# se ela ja foi escolhida antes mas nem todas foram escolhidas pelo menos uma vez, escolhe outra
  cols[$col]=1						# caso contrario, marca que ela foi escolhida
  echo $col > $COL_FIFO					# e a armazerna no fifo das colunas
done &

seq $quant | while read					# lanca uma quantidade quant de loops em background...
do							#
  while [ -p $FIFO ]					# ... que serao mantidos enquanto o fifo existir. Cada loop:
  do
    while read COL					# le o primeiro valor disponivel no fifo das colunas e o armazena
    do
      LIN=$((RANDOM%L))					# escolhe aleatoriamente uma linha pra comecar a descer a coluna
      EST=${estilos[$((RANDOM%2))]}			# e um estilo tambem aleatorio para definir o tom da cor dessa coluna
      while [ $LIN -lt $L ]				# entao vai descendo na coluna ate a base da tela. Em cada linha da coluna...
      do
        echo $RANDOM > /dev/null			# PERGUNTA: porque sem essa linha o valor de RANDOM nao varia qdo a variavel eh utilizada?
        asc=$(mod $RANDOM)				# ... escolhe aleatoriamente um valor, que sera o codigo ascii do caracter inicial
        for i in $(seq $asc $((asc+var-1)))		# agora inicia a sequencia de caracteres a serem impressos com a cor branca:
        do
          i=$(mod $i)					# se os caracteres adiante da sequencia estiverem alem de ascf, volta a pegar a partir de asci
          send $i $white $negrito			# e envia para o fifo da impressao
        done
        i=$(mod $((asc+var)))				# terminado de imprimir a sequencia de caracteres em branco, pega o proximo
        send $i $green $EST				# e envia para o fifo de impressao, para ser impresso em verde
        let LIN++					# entao vai para a proxima linha.
      done
    done < $COL_FIFO
  done &
done

while [ -p $FIFO ]					# ainda, enquanto o fifo de impresso existir,
							# TODO: sair quando for pressionada alguma tecla (ou movido o mouse?)
							# TODO: ao sair, autenticar com a senha do usuario que iniciou o processo
do
  cat $FIFO | while read chr cor est lin col		# vai lendo as linhas dele e, com as informacoes de cada uma:
  do
    tput $est						# muda o estilo (tonalidade)
    tput setf $cor					# e a cor de frente do texto
    tput cup $lin $col					# move o cursor para a linha l na coluna c
    echo -n $chr					# e escreve o caracter na tela.
  done
done

							# FIM DO ALGORITMO

							# TODO: transformar em modulo da funcao zz ss

# EOF
