#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2019/05/12 (Sun) 21:18:40 -03
# 2018/02/07 (Wed) 22:04:02 -02
# 2018/02/07 (Wed) 20:49:50 -02

# d=●;for((P=10**8,Q=P/100,X=320*Q/(COLUMNS-1),Y=210*Q/(LINES-2),y=-105*Q,v=-220*Q,x=v;y<105*Q;x=v,y+=Y));{ for((;x<P;a=b=i=k=c=0,x+=X));{ for((;a*a+b*b<2*P*P&&i++<99;a=((c=a)*a-b*b)/P+x,b=2*c*b/P+y));{ :;};(((j=(i<99?i%16:0)+30)>37?k=1,j-=8:0));printf "\e[$k;${j}m$d";};printf "\e[0m";}

  C=$(( COLUMNS - 1 ))
  L=$(( LINES   - 2 ))

# C=150
# L=50

  d=${1:-●}

  clear

  for ((
           P  =  10**8
         , Q  =      P/100
         , X  =  320*Q/C
         , Y  =  210*Q/L
         , y  = -105*Q
         , v  = -220*Q
         , x  =  v
         ; y  <  105*Q
         ; x  =  v
         , y +=  Y
      ))
       {
         for ((
                ; x  < P
                ; a  = b = i = k = c = 0
                , x += X
             ))
              {
                for ((
                       ; a*a + b*b < 2*P*P && i++ < 99
                       ; a = ( (c=a)*a - b*b )/P + x
                       , b = (         2*c*b )/P + y
                    ))
                     {
                       :
                     }
                ((
                   ( j = ( i < 99 ? i%16 : 0 ) + 30 ) > 37 ? k = 1, j -= 8 : 0
                ))
                printf "\e[$k;${j}m$d"
              }
         printf "\e[0m\n"
       }

# EOF
