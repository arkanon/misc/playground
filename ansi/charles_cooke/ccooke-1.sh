#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2016/03/07 Mon 15:50:51 BRT
# 2015/01/07 Wed 20:43:00 BRD
#
# Original code by Charles Cooke
# <http://earth.gkhs.net/ccooke/shell.html>

     TERM=xterm

# COLUMNS=$(tput cols)
#   LINES=$(tput lines)

  COLUMNS=80
    LINES=20

  for ((

         P=10**8,
         Q=P/100,
         X=320*Q/(COLUMNS-1),
         Y=210*Q/LINES,
         y=-105*Q,
         v=-220*Q,
         x=v;

         y < 105*Q;

         x=v,
         y+=Y

      ))
  do

    for ((

           ;
           x < P;
           a=b=i=k=c=0,
           x+=X

        ))
    do

      for ((

             ;

             a*a + b*b < 2*P*P && i++ < 99;

             a=( (c=a)*a - b*b )/P + x,
             b=2*c*b/P + y

          ))
      do
        :
      done

##### ANSI SUPPORT TERMS
#     dot="0"
#     (( ( j=( i < 99 ? i%16 : 0 ) + 30 ) > 37 ? k=1, j-=8 : 0 ))
#     chr="\E[$k;$j"m$dot
#      nl="\E[0m"

##### NO ANSI
      out="0"
      in=" "
      ((i < 99)) && chr="$out" || chr="$in"
      nl=""

      echo -ne "$chr"

    done

    echo -e "$nl"

  done

