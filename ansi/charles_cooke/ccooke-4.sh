#!/bin/bash



  export TIMEFORMAT=%lR

  declare -A special
  special=(
             [^\[\[1~]=home
             [^\[\[2~]=ins
             [^\[\[3~]=del
             [^\[\[4~]=end
             [^\[\[5~]=pgup
             [^\[\[6~]=pgdn
              [^\[\[A]=up
              [^\[\[B]=down
              [^\[\[C]=right
              [^\[\[D]=left
               [^\[OP]=f1
               [^\[OQ]=f2
               [^\[OR]=f3
               [^\[OS]=f4
            [^\[\[15~]=f5
            [^\[\[17~]=f6
            [^\[\[18~]=f7
            [^\[\[19~]=f8
            [^\[\[20~]=f9
            [^\[\[21~]=f10
            [^\[\[23~]=f11
            [^\[\[24~]=f12
                 [^\[]=esc
                  [^I]=tab
                  [^?]=bs
                  [\$]=enter
                   [ ]=space
          )



  plot()
  {

    W=$(( COLUMNS - 1 )) # largura do terminal
    H=$(( LINES   - 1 )) # altura  do terminal

    I=${1:-99} ; shift # número de iterações
    p=${1:-●}  ; shift # caracter plotado
    w=${1:-$w} ; shift # largura
    h=${1:-$h} ; shift # altura
    N=${1:-30} ; shift # valor inicial da palete ansi {29..32}

    R=$((   10**8  ))
    Q=$((   R/100  ))
    L=$(( -220*Q   ))
    B=$(( -105*Q   ))
    T=$((  105*Q   ))

    X=$((  320*Q/W ))
    Y=$((  210*Q/H ))

    for (( y=B ; y<T ; y+=Y ))
    {
      for (( x=L ; x<R ; x+=X ))
      {
        local a=0 b=0 i=0 k=0 c=0
        for ((
               ; a*a + b*b < 2*R*R && i++ < I
               ; a = ( (c=a)*a - b*b )/R + x
               , b = (         2*c*b )/R + y
            ))
        {
          :
        }
        ((
           ( j = ( i < I ? i%16 : 0 ) + N ) > N+7 ? k = 1 , j -= 8 : 0
        ))
        printf "\e[$k;${j}m$p"
      }
    }

    printf "\e[0m"

  }



  readkey()
  {
    local K1
    read -rsN1 K1 && while read -sN1 -t.001; do K1+=$REPLY; done
    K2=$(echo -n "$K1" | cat -A)
    REPLY=${special[$K2]:-$K1}
    [[ $1 ]] && { eval $1=$REPLY; unset REPLY; }
  }



# while true; do readkey; done



# mkdir -p frames
# time \
# {
#   for i in {1..9} {10..90..10} {97..103}
#   {
#     printf -v s %03i $i
#     printf "$s  "
#     time plot $i > frames/$s
#   }
# }
  # 001  0m0,567s
  # 002  0m0,704s
  # 003  0m0,818s
  # 004  0m1,009s
  # 005  0m1,047s
  # 006  0m1,084s
  # 007  0m1,176s
  # 008  0m1,218s
  # 009  0m1,393s
  # 010  0m1,363s
  # 020  0m2,000s
  # 030  0m2,319s
  # 040  0m2,761s
  # 050  0m3,495s
  # 060  0m3,818s
  # 070  0m4,250s
  # 080  0m4,708s
  # 090  0m5,515s
  # 097  0m5,525s
  # 098  0m5,323s
  # 099  0m5,400s
  # 100  0m5,373s
  # 101  0m5,393s
  # 102  0m5,415s
  # 103  0m5,557s
  # 1m17,233s

# md5sum frames/*
  # 8b73b22ee079884a0a69b20637a6bb8c  frames/001
  # c962d766338f66ed6e4d740e3bb0cc25  frames/002
  # 0feffa15ab670fd6d8714e40276d3af1  frames/003
  # 66057bf8a209c6709acb9e5683bb9df1  frames/004
  # fc374061ca94647d2f2402d6479c25c6  frames/005
  # 9cc06edfe646a48348198f7099565fd0  frames/006
  # 39b4dcfc2d7d0b29a3f9e80fe1732d44  frames/007
  # 6fe6d43809f63633036a4c9a8d9eb05a  frames/008
  # 7844a228d28c453be98d21c1106177ff  frames/009
  # 29520d05d0561dde1cd75083bc864d46  frames/010
  # ee8a1a20395ea423f10cf14b8679afb6  frames/020
  # f9f768df667f76ffb6a41ec8cb9ed37b  frames/030
  # 2239841989edbae915a7bf991370a092  frames/040
  # 2efdcbd63a9a9c091ae415ca3ff8f12c  frames/050
  # 3c1a133344e0c6139e6904a9ce1332aa  frames/060
  # c812e4843e7d46008a74999cfe2326a4  frames/070
  # ac21b69c1ebea3236464ba7bcdf5efe1  frames/080
  # c724e6cfb759e7db7f6edd33973c24aa  frames/090
  # b042b699c8f029b6a54b987d2b4d9c38  frames/097
  # b042b699c8f029b6a54b987d2b4d9c38  frames/098
  # b042b699c8f029b6a54b987d2b4d9c38  frames/099
  # b042b699c8f029b6a54b987d2b4d9c38  frames/100
  # cee5d02d06eb3df6b0f2baa993e21cab  frames/101
  # 0e34f7c3a25228d2f0a3e2d91a30b53b  frames/102
  # e52c01c979d95f964edea53d65a6ae1c  frames/103



  f=( $(ls -1 frames/*) )
  s=${#f[*]}
  n=0
  while true
  do
    i=$n
    cat ${f[i]}
    echo n=$n i=$i
    readkey
    v=0
    [[ $REPLY == left  ]] && v=-1
    [[ $REPLY == right ]] && v=+1
    [[ $REPLY == esc   ]] && break
    (( i == s-1 )) && v=-1
    (( i == 1-s )) && v=+1
  done



# EOF
