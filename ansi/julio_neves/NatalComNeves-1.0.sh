#!/bin/bash

# Versão 1.0
# by Julio C. Neves
# <http://www.mail-archive.com/shell-script@yahoogrupos.com.br/msg08119.html>

trap "tput reset; exit" 2
clear
tput civis

lin=2
col=$(($(tput cols) / 2))
  c=$((col-1))
tput setaf 2
tput bold

# Montando a Árvore
for ((i=1; i<20; i+=2))
{
  tput cup $lin $col
  for ((j=1; j<=i; j++))
  {
    echo -n \*
  }
  let lin++
  let col--
}
tput sgr0
tput setaf 3
for ((i=1; i<=2; i++))
{
  tput cup $((lin++)) $c
  echo 'mWm'
}
tput setaf 1
tput bold
tput cup $lin $((c - 4))
echo BOAS FESTAS
let c++

# Pendurando as bolas (da árvore, claro!)
k=1
while true
do
  for ((i=1; i<=25; i++))
  {
    # Apagando a bola que foi ligada há 25 rodadas atras
    [ $k -gt 1 ] && {
                      tput setaf 2
                      tput bold
                      tput cup ${linha[$[k-1]$i]} ${coluna[$[k-1]$i]}
                      echo \*
                      tput setaf 1
                      tput bold
                      unset  linha[$[k-1]$i]
                      unset coluna[$[k-1]$i] # Mantenha limpo o vetor
                    }
     li=$((RANDOM % 9 + 3))
    ini=$((c-li+2))
    fim=$((c+li+2))
     co=$((RANDOM % (li-2) * 2 + 1 + ini))
    tput cup $li $co
    echo o
     linha[$k$i]=$li
    coluna[$k$i]=$co
    sleep 0.2
  }
  let k++
done

# EOF
