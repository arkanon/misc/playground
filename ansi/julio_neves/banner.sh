#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2017/04/16 (Sun) 15:52:21 -03
#
# Baseado no script original de Julio Neves
#   em homenagem aos 20 Anos da Dicas-L <http://www.dicas-l.com.br/arquivo/20_anos_de_dicas-l.php>
# 2017/04/04 (Tue)

trap "tput sgr0; tput cnorm; clear; exit" 2 3 15

# ou determinamos manualmente as linhas que formam a arte ascii desejada:
  # lin[0]=" oooo     oo    ooo     ooo     oooo            oo                ooo     ooo            ooo     oo    oo   ooo    oooo  "
  # lin[1]=" oo  oo        oo oo    ooo    oo  oo           oo               oo oo   oo oo           ooo     ooo   oo  oo oo  oo  oo "
  # lin[2]=" oo   oo  oo  oo       oo oo    oo     oooooo   oo                 oo    oo oo          oo oo    oo o  oo  oo oo   oo    "
  # lin[3]=" oo   oo  oo  oo       ooooo      oo   oooooo   oo                oo     oo oo          ooooo    oo  o oo  oo oo     oo  "
  # lin[4]=" oo  oo   oo   oo oo  ooooooo  oo  oo           oo  oo           ooooo   oo oo         ooooooo   oo   ooo  oo oo  oo  oo "
  # lin[5]=" oooo     oo    ooo  oo     oo  oooo            oooooo           ooooo    ooo         oo     oo  oo    oo   ooo    oooo  "

# ou de terminamos a string e a font do comando figlet que sera usada:
# sudo apt -y install figlet
# <http://www.figlet.org/examples.html>
# <http://www.figlet.org/fontdb.cgi>
     lin="Dicas-L - 20 Anos"
  # font=banner3
    font=block
  # font=caligraphy
  # font=doh
  # font=dotmatrix
  # font=epic
  # font=invita
  # font=isometric3
  # font=lean
  # font=letters
  # font=o8
  # font=ogre
  # font=poison
  # font=roman
  # font=speed
  # font=small
  # font=standard
  # font=tinker-toy

    # eventualmente podemos substituir automaticamente algum caractere usado na arte por outro
    frenteori=# # \\*
    frentenew=o

       fundo=' ' # - ' '
    alteraem=3
      espera=.2

      normal=$(tput sgr0)
        bold=$(tput bold)
       cinza=$(tput setaf 0)
      branco=$(tput setaf 7)

  boldfrente=true
   boldfundo=false
    corfundo=$branco

(( ${#lin[*]} == 1 )) && \
{
  export FIGLET_FONTDIR=~/.figlet
  mkdir -p $FIGLET_FONTDIR
  [ -e $FIGLET_FONTDIR/$font.flf ] || curl -s http://www.figlet.org/fonts/$font.flf -o $FIGLET_FONTDIR/$font.flf
  IFS=$'\r\n' GLOBIGNORE='*' lin=($(figlet -tf $font $lin | sed -r '/^ +$/d'))
}

tela=$(tput cols)
lini=$(( ( $(tput lines) - ${#lin[*]} ) / 2 ))

$boldfrente && boldfrente=$bold || boldfrente=
$boldfundo  &&  boldfundo=$bold ||  boldfundo=$normal

(( tela <= ${#lin[0]} )) && \
{
  echo A tela precisa ter mais de ${#lin[0]} colunas
  exit 1
}

for ((i=0; i<${#lin[*]}; i++))
{
  lin[i]=$(printf "%${tela}s" "${lin[i]}")
  lin[i]=${lin[i]//$frenteori/$frentenew}
  lin[i]=${lin[i]// /$fundo}
}

tput civis

clear

while true
do
  tput cup $lini 0
  for ((i=0; i<${#lin[*]}; i++))
  {
    sed -r "s/\\$fundo+/$boldfundo$corfundo&$normal$boldfrente$corfrente/g" <<< "${lin[i]}"
    lin[i]=${lin[i]:1}${lin[i]:0:1}
  }
  ((++j%$alteraem)) || corfrente=$(tput setaf $((RANDOM%8)))
  sleep $espera
done

# EOF
