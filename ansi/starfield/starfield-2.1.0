#!/bin/bash

# Starfield ANSI Terminal Screensaver
# Arkanon <arkanon@lsd.org.br>
# v2.1.0 - 2009/09/19 (S�b) 15:04:00 (BRS)
#
# TODO
#   - guardar o conteudo da tela antes de comecar a preencher com os caracteres
#   - detectar automaticamente a cor de fundo
#   - nao pegar uma posicao l,c se ela ja estiver no fifo
#   - restaurar a tela anterior ao lancamento do script
#   - desabilitar o ^C
#   - sair quando for pressionada alguma tecla (ou movido o mouse?)
#   - ao sair, autenticar com a senha do usuario que iniciou o processo
#   - perceber que o terminal esta idle a um determinado tempo, e iniciar
#     automaticamente o algoritmo
#   - transformar em modulo da funcao zz ss
#
# INTRODUCAO
#
# Em 1993, no CESUP (Centro de Super Computacao) da UFRGS havia uma sala
# com alguns computadores (Mac e 486 com DOS) a disposicao de eventuais
# usuarios. Havia tambem um terminal preto e branco sempre rodando um
# screensaver em modo texto que era (pelo que lembro) um conjunto de
# caracteres cuja posicao e tonalidade ficava variando aleatoriamente,
# numa simulacao muito simpatica de um ceu noturno estrelado.
#
# Muitas vezes procurei um screensaver para terminal equivalente, mas
# como nunca soube o nome do original, nem a linguagem em que foi feito
# ou mesmo o SO em que rodava, todas as buscas acabaram em vao.
#
# Em 15/09/2009, pensando novamente no assunto, resolvi produzir algo
# semelhante, mas em shell script, supondo que a complexidade nao devesse
# ser muito grande.
#
# Realmente, o resultado final nao chega a assustar, mas ha um pulo do gato,
# sem o qual os caracteres nao sao impressos na tela da forma correta: eh
# necessario controlar a concorrencia pelo uso do cursor.
#
# Felizmente o Unix possui a ferramenta adequada para isso:
#
#				��� FIFO ���
#
# Um agradecimento especial ao Julio Neves pela valiosa informacao, divulgada
# na palestra sobre Shell-Script proferida na UNIVATES (Lajeado-RS-Brasil), no
# audit�rio do pr�dio 3, em 24/07/2003.

  FIFO="/tmp/`basename $0`-$$"			# define um nome e local para o fifo, associado ao PID do script
oritty=`stty -g`				# guarda as conf do terminal

interruption()					# define o que sera executado quando o algoritmo for interrompido:
{
  set +o noglob;				# habilita o caracter coringa *
  tput cnorm					# torna o cursor visivel
  tput sgr0					# restaura os atributos default do texto
  rm -f $1					# apaga o fifo, o 1o parametro. Todos os outros sao a conf original do tty
  shift						# remove o 1o parametro da lista de parametros
  stty "$*"					# restaura as conf originais do terminal
  clear						# limpa a tela
}						# TODO: restaurar a tela anterior ao lancamento do script

trap "interruption $FIFO $oritty" 0 1 2 3 15	# executa interruption ao fim normal (0) ou no caso de interrupcao (1 2 3 15) do script

						# TODO: guardar o conteudo da tela antes de comecar a preencher com os caracteres
clear						# limpa a tela
tput civis					# torna o cursor invisivel
set -o noglob;					# desabilita o caracter coringa *
#stty -echo -icanon time 0 intr '^-'		# TODO: desabilitar o ^C

#    black=0					# da nomes amigaveis aos codigos das cores no tput
#     blue=1
#    green=2
#     cyan=3
#      red=4
#  magenta=5
#   yellow=6
#    white=7

    normal=0					# da mones amigaveis aos codigos ansi das cores e estilos (\e[#;...;#m)
      bold=1
underscore=4					# (mono display only)
     blink=5
   reverse=7
 invisible=8

        fg=3					# codigo indicando que a cor se refere a frente
        bg=4					# codigo referente ao fundo

     black=0					# codigo da cor := ${bg|fg}${cor}
       red=1
     green=2
    yellow=3
      blue=4
   magenta=5
      cyan=6
     white=7

     quant=200					# define a quantidade de caracteres simultaneos na tela
      mind=2					# define o tempo minimo de espera entre as variacoes de cor e tonalidade dos caracteres
      maxd=8.15					# define o tempo maximo dessa espera (em segundos)
   charset=(* 0 O o + .)			# define os caracteres a serem pegos aleatoriamente para uma variacao sequencial na cor/tonalidade
   bgcolor=$black				# define a cor de fundo do terminal (a ultima cor a ser usada pelos caracteres sera essa)
						# TODO: detectar automaticamente a cor de fundo
  colorset=($red $yellow $blue $white $bgcolor)	# define as cores de frente a serem usadas

rm -f $FIFO					# apaga o fifo incondicionalmente
mkfifo $FIFO					# cria o fifo

						# INICIO DO ALGORITMO
						# TODO: perceber que o terminal esta idle a um determinado tempo, e iniciar automaticamente o algoritmo

seq $quant | while read				# lanca uma quantidade quant de loops em background...
do
  while [ -p $FIFO ]				# ... que serao mantidos enquanto o fifo existir. Cada loop:
  do
    p=$[$RANDOM%${#charset[*]}]			# escolhe aleatoriamente uma posicao valida em charset
    s=${charset[$p]}				# pega o caracter nessa posicao do array
    l=$[$RANDOM%`tput lines`]			# escolhe aletoriamente uma linha...
    c=$[$RANDOM%`tput cols` ]			# ... e uma coluna do terminal
						# TODO: nao pegar uma posicao l,c se ela ja estiver no fifo
    for color in ${colorset[*]}			# e, para cada cor em colorset...
    do
      for style in $bold $normal		# ... e cada estilo desejado
      do
        echo "$s $color $style $l $c" > $FIFO	# envia a conf do caracter na cor e no estilo para o fifo
        calc="$mind+$RANDOM%($maxd-$mind)"	# define o calculo do tempo de delay, que sera efetuado pelo bc
        sleep `echo "$calc" | bc`		# e espera um tempo aleatorio entre mind e maxd antes do proximo laco
      done
    done
  done &
done



while [ -p $FIFO ]				# ainda, enquanto o fifo existir,
						# TODO: sair quando for pressionada alguma tecla (ou movido o mouse?)
						# TODO: ao sair, autenticar com a senha do usuario que iniciou o processo
do
  cat $FIFO | while read s color style l c	# vai lendo as linhas do fifo e, com as informacoes de cada uma:
  do
    ansi="\e[$style;$fg$color;$bg${bgcolor}m"	# define o codigo ansi referente ao estilo, cor de frente e cor de fundo do caracter
    tput cup $l $c				# move o cursor para a linha l na coluna c
    echo -en "$ansi$s"				# e imprime o caracter com o codigo ansi
  done
done

						# FIM DO ALGORITMO

						# TODO: transformar em modulo da funcao zz ss

# EOF
