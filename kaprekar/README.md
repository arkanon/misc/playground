# Constante de Kaprekar



## Apresentação

- Vídeo: http://fb.com/reel/1433046957536207
- Wikipedia: https://pt.wikipedia.org/wiki/6174



## Implementação em BASh

```shell-session

kaprekar1()
{
  local i1=${1:-${def?}} i2 i3
  while true
  do
    i2=$(
      while [[ $i1 ]]
      do
        echo ${i1:0:1}
        i1=${i1:1}
      done | sort -r | tr -d '\n'
    )
    printf -v i3 %0${#1}i $(( 10#$i2 - 10#$(rev<<<$i2) ))
    echo $i3
    (( 10#$i1 == 10#$i3 )) && break || i1=$i3
  done
}

kaprekar2()
{
  local i1=${1:-${def?}} i2 i3
  while true
  do
    i2=$(
      while [[ $i1 ]]
      do
        echo ${i1:0:1}
        i1=${i1:1}
      done | sort -r
    )
    printf -v i3 %0${#1}i $(( 10#${i2//$'\n'/} - 10#$(rev<<<${i2//$'\n'/}) ))
    echo $i3
    (( 10#$i1 == 10#$i3 )) && break || i1=$i3
  done
}

kaprekar3()
{
  local i1=${1:-${def?}} i2 i3
  while true
  do
    i2=$(
      while [[ $i1 ]]
      do
        echo ${i1:0:1}
        i1=${i1:1}
      done | sort -r
    )
    i2=${i2//$'\n'/}
    printf -v i3 %0${#1}i $(( 10#$i2 - 10#$(rev<<<$i2) ))
    echo $i3
    (( 10#$i1 == 10#$i3 )) && break || i1=$i3
  done
}



. <(curl -ksL bit.ly/benshmark-v5)
alias bm=benshmark-v5



$ unset def

$ kaprekar1
-bash: def: parâmetro não inicializado

$ kaprekar1 1234
3087
8352
6174
6174

$ def=1234

$ kaprekar1
3087
8352
6174
6174



# [comparação dos resultados]

$ eval paste \<\(kaprekar{1..3}\)
3087    3087    3087
8352    8352    8352
6174    6174    6174
6174    6174    6174



# [comparação da eficiência]

$ bm 1000 kaprekar{1..3}

kaprekar1  00:15,689
kaprekar2  00:13,779
kaprekar3  00:14,072

1º  kaprekar2
2º  kaprekar3/kaprekar2       0,021 vez mais lenta
3º  kaprekar1/kaprekar2       0,138 vez mais lenta

```



☐
