Loop com `curl` e `parallel` nos arquivos do código fonte dos comandos do pacote `coreutils`, o qual engloba a maioria dos comandos de uso comum (`ls`, `cut`, etc).
Vários incluem o arquivo `getopt.h`. O `ldd` não acusa a lib "getopt" linkada porque não é uma biblioteca compartilhada.

## Procedimento

1. salvar o json com a árvore do repositório, obtida por consulta à api: `curl -s $api > $json`
2. consultar com `jq` o json para extrair apenas as url's dos arquivos .c: `jq -r $jq $json`
3. percorrer a lista de arquivos `.c` usando a função `inspect` que faz um `curl|grep` no código à procura da string `getopt.h`.  
   Um loop sequencial com `while read` ou `xargs` finaliza em 30 a 60s. Um `parallel -j10` o faz em 3 a 4s :-)



A lista de pacotes da GNU Software Foudation está disponível em seu site. Infelizmente nem todos têm uma cópia do código fonte hospedada no GitHub.
```bash
lynx -dump -listonly -nonumbers gnu.org/software | grep -E software/[^/]+/
```



Alguns parâmetros para enxugar os comandos.
```bash
 pkg=coreutils
 api=https://api.github.com/repos/$pkg/$pkg/git/trees/master?recursive=1
json=data-$pkg.json # arquivo com os dados obtidos via api
  jq='.tree[]|select(.path|match("[.]c$"))|.path' # consulta no json
 raw=https://raw.githubusercontent.com/$pkg/$pkg/master
```



Função de inspeção, pra simplificar a chamada do `parallel`.
```bash
inspect()
{
  echo -en "$1\t"
  curl -s $raw/$1 | grep getopt.h || echo
}
```



Exportação da variável e função usadas no `parallel`.
```bash
export    raw
export -f inspect
```



Teste simples em um comando específico do pacote.
```bash
inspect src/ls.c
# src/ls.c        #include <getopt.h>
```



Consulta à api com os dados da árvore do repo. O salvamento local do json é para não extrapolar o uso da api, a quota por ip sem autenticação se esgota rapidamente.
```bash
curl -s $api > $json
```



Inspeção por `grep` do código fonte de cada arquivo `.c` obtido via `curl`
```bash
time jq -r $jq $json | while read i; do       inspect $i; done | sort > 1 # 0m57,240s
time jq -r $jq $json | xargs    -n1  bash -c 'inspect "$@"' _  | sort > 2 # 0m29,791s

sudo apt install parallel
time jq -r $jq $json | parallel -j10 inspect | sort > 3 # 0m03,700s
time jq -r $jq $json | parallel -j10 'echo -en "{}\t"; curl -s $raw/{} | grep getopt.h || echo' | sort > 4 # 0m04,347s
```



☐
