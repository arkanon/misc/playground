#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/07/11 (Sat) 06:42:39 -03
# 2015/08/12 (Qua) 14:47:45 BRS

# Projeto Euler
#
# Problema 1 <http://projecteuler.net/problem=1>
# Os naturais menores que 10 e múltiplos de 3 ou 5 são 3, 5, 6 e 9. A soma deles é 23. Encontre a soma dos múltiplos de 3 ou 5 menores que 1000.

# <http://lists.nongnu.org/archive/html/shell-script-pt/2015-08/msg00071.html>
# <http://lists.nongnu.org/archive/html/shell-script-pt/2015-08/msg00072.html>



# Como um programador tradicional resolveria no bash:
time ( i=3; a=$((10**i-1)); s=0; for n in $(seq $a); do ( ((n%3==0)) ) || ((n%5==0)) && ((s+=n)); ((n++)); done; echo $s )
# 233168
# 0m5.638s

# Como um bash scripter resolve:
time ( i=3; a=$((10**i-1)); echo $(( $(eval set {3..$a..3} {5..$a..5}; IFS=+; echo "$*") $(eval set {15..$a..15}; IFS=-; ((i>1)) && echo "-$*") )) )
# 233168
# 0m0.015s

# Como um matemático resolve: #temadecasa

# Legal é o padrão que a soma segue :)

for i in {1..6}; do a=$((10**i-1)); echo $(( $(eval set {3..$a..3} {5..$a..5}; IFS=+; echo "$*") $(eval set {15..$a..15}; IFS=-; ((i>1)) && echo "-$*") )); done
# 23
# 2318
# 233168
# 23331668
# 2333316668
# 233333166668



# EOF
