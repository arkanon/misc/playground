#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/09/11 (Sat) 02:57:22 -03

# Persistência Multiplicativa

# Numberphile - What's special about 277777788888899? <http://youtube.com/watch?v=Wim9WJeDTHQ>

  nl='nl -nrz -v0 -w2 -s:'

  n=277777788888899

  s1()
  {
    local n=${1:-$n} m
    while (( ${#n}>1 ))
    do
      echo $n
      m=$n
      n=$((
             $(
                while (( ${#m}>1 ))
                do
                  echo -n "${m:0:1}*"
                  m=${m:1}
                done
                echo $m
              )
          ))
    done
    echo $n
  }

  s2()
  {
    local n=${1:-$n} m p
    while (( ${#n}>1 ))
    do
      echo $n
      m=$n
      p=
      while [[ $m ]]
      do
        p+=${m:0:1}\*
        m=${m:1}
      done
      n=$(($p 1))
    done
    echo $n
  }

  s1 $n | $nl
  s2 $n | $nl

  #  0: 277777788888899
  #  1: 4996238671872
  #  2: 438939648
  #  3: 4478976
  #  4: 338688
  #  5: 27648
  #  6: 2688
  #  7: 768
  #  8: 336
  #  9: 54
  # 10: 20
  # 11: 0

  benshmark 2000 s1 s2
  # s1  0m21.136s
  # s2  0m0.702s
  bc <<< ' scale=3; 21.136 / 0.702 ' # 30.108



# EOF
