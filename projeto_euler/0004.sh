#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/09/11 (Sat) 23:35:30 -03
# 2020/07/11 (Sat) 06:42:47 -03
# 2015/09/01 (Ter) 09:39:30 BRS
# 2015/08/13 (Qui) 14:30:34 BRS

# Projeto Euler
#
# Problema 4 <http://projecteuler.net/problem=4>
# Um número palíndromo é lido da mesma forma em ambos os sentidos. O maior palíndromo feito com o produto de dois números de 2 dígitos é 9009 = 91×99.
# Encontre o maior palíndromo feito com o produto de dois números de 3 dígitos.

# Uma ideia evitando loops explícitos e aplicando sugestões postadas no stackoverflow para identificação de palíndromos por expressão regular em
#   <http://stackoverflow.com/questions/233243/how-to-check-that-a-string-is-a-palindrome-using-regular-expressions>
# Usei tee (a primeira vez) para fazer um cache dos valores para agilizar a exploração das técnicas de identificação dos palíndromos e deixei listar todos os identificados.

# <http://lists.nongnu.org/archive/html/shell-script-pt/2015-08/msg00084.html>
# <http://lists.nongnu.org/archive/html/shell-script-pt/2015-08/msg00086.html>



# 2015/08/13 (Qui) 14:30:34 BRS
time ( TIMEFORMAT=%lR; ( a={100..999}; eval set $a*$a; IFS=$'\n' ; echo "$*" ) | bc | sort -nu | tee prod | grep -wE -e '(.)(.).?\2\1' -e '(.)(.)(.)\3\2\1' )
# ...
# 906609
# 0m49.688s



# 2015/08/13 (Qui) 17:11:57 BRS
time ( TIMEFORMAT=%lR; ( a={100..999} ; eval set $a*$a; IFS=$'\n'; paste -d= <(echo "$*") <(bc<<<"$*") ) | sort -nu -t= -k2 | grep -wE              -e '(.)(.).?\2\1' -e '(.)(.)(.)\3\2\1'   ) >| palin0-2 #   9.449B  0m50.448s



# 2015/09/01 (Ter) 09:19:30 BRS
time ( TIMEFORMAT=%lR; ( a={0..999}   ; eval set $a*$a; IFS=$'\n'; paste -d= <(echo "$*") <(bc<<<"$*") ) | sort -nu -t= -k2 | grep -wE -e '(.).?\1' -e '(.)(.).?\2\1' -e '(.)(.)(.).?\3\2\1' ) >| palin1-0 # 692.579B  1m04.198s
time ( TIMEFORMAT=%lR; ( a={0..99}    ; eval set $a*$a; IFS=$'\n'; paste -d= <(echo "$*") <(bc<<<"$*") ) | sort -nu -t= -k2 | grep -wE -e '(.).?\1' -e '(.)(.).?\2\1' -e '(.)(.)(.).?\3\2\1' ) >| palin1-1 #   5.538B  0m00.636s
time ( TIMEFORMAT=%lR; ( a={100..999} ; eval set $a*$a; IFS=$'\n'; paste -d= <(echo "$*") <(bc<<<"$*") ) | sort -nu -t= -k2 | grep -wE -e '(.).?\1' -e '(.)(.).?\2\1' -e '(.)(.)(.).?\3\2\1' ) >| palin1-2 #

time ( TIMEFORMAT=%lR; ( a={0..999}   ; eval set $a*$a; IFS=$'\n'; paste -d= <(echo "$*") <(bc<<<"$*") ) | sort -nu -t= -k2 ) >| prod

time ( TIMEFORMAT=%lR; ( a={0..999}   ; eval set $a*$a; IFS=$'\n'; paste -d= <(echo "$*") <(bc<<<"$*") ) | sort -nu -t= -k2 | grep -E '=(.|(.).?\2|(.)(.).?\4\3|(.)(.)(.).?\7\6\5)$'         ) >| palin2-0 #  12.654B  1m01.460s
time ( TIMEFORMAT=%lR; ( a={0..99}    ; eval set $a*$a; IFS=$'\n'; paste -d= <(echo "$*") <(bc<<<"$*") ) | sort -nu -t= -k2 | grep -E '=(.|(.).?\2|(.)(.).?\4\3|(.)(.)(.).?\7\6\5)$'         ) >| palin2-1 #   1.019B  0m00.613s
time ( TIMEFORMAT=%lR; ( a={100..999} ; eval set $a*$a; IFS=$'\n'; paste -d= <(echo "$*") <(bc<<<"$*") ) | sort -nu -t= -k2 | grep -E '=(.|(.).?\2|(.)(.).?\4\3|(.)(.)(.).?\7\6\5)$'         ) >| palin2-2 #   9.449B  0m49.733s



# Original em awk
# Itamar <itamarnet@yahoo.com.br>
# 2015/08/13 (Qui) 13:39:02 BRS
awk '
 function rev(texto) {
         if (length(texto) == 0) return ""
         return (substr(texto, length(texto), 1) rev(substr(texto, 1, length(texto)-1)))
 }

 BEGIN {
         comando = "sort -n | tail -n 1"
         for (f1=999;f1>=100;f1--) {
                 for (f2=999;f2>=100;f2--) {
                         produto = f1 * f2
                         if (produto == rev(produto)) print produto, "=", f1, "*", f2 | comando
                 }
         }
         close(comando)
 }
'



# Adaptação
# Arkanon <paulo.bagatini@<user>.com.br>
# 2015/09/01 (Ter) 09:18:45 BRS
awk '
     function rev(t) { return length(t)==0 ? "" : substr(t,length(t),1) rev(substr(t,1,length(t)-1)) }
     BEGIN { for (f1=0;f1<=999;f1++) { for (f2=0;f2<=999;f2++) { p=f1*f2; if (p==rev(p)) printf "%i*%i=%i\n",f1,f2,p } } }
    ' | sort -nu -t= -k2 >| palin5



  e=2; time for ((i=0;i<10**e;i++)); { for ((j=0;j<10**e;j++)); { echo "$i*$j"; }; } | wc -l
  # 10000
  # 0m0,078s

  e=3; time for ((i=0;i<10**e;i++)); { for ((j=0;j<10**e;j++)); { echo "$i*$j"; }; } | wc -l
  # 1000000
  # 0m4,461s

  e=4; time for ((i=0;i<10**e;i++)); { for ((j=0;j<10**e;j++)); { echo "$i*$j"; }; } | wc -l
  # 100000000
  # 8m39,124s



# EOF
