#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2018/07/28 (Sat) 06:22:03 -03
# 2018/07/28 (Sat) 00:59:20 -03

# How to create a video from images with FFmpeg? <http://stackoverflow.com/a/24966617>

 # teste
 #   00:10:01.447
 #   00:00:00.613
 #   00:10:01.548
 repeat=100
   incr=1
   size=200

## facebook - 20180728-051042
##   01:33:11.068
##   00:00:19.954
##   01:33:31.022
#repeat=200
#  incr=.1
#  size=800

   time=$(date +%Y%m%d-%H%M%S)
    svg=frame.svg
  start=-2
    end=135
    dir=frames-$time
   qual=70%
  video=eclipse-$time.mp4
   rate=30/1

  mkdir -p $dir

  time \
  {

    time \
    {

      lst=$(bc <<< "1+2*$repeat+($end-($start))/$incr")
      pos=$start

      # TODO: parametrizar os 3 loops abaixo, criando um genérico em uma função chamada 3 vezes

      # repete $repeat vezes o primeiro frame
      one=1
      one=$(printf %0${#lst}d $one)
      sed -ri "/tat/{n;s/\([^)]+\)/($pos)/}" frame.svg
      convert $svg -resize $size -quality $qual $dir/f$one.jpg
      for frm in $(seq 2 $((repeat+1)))
      {
        frm=$(printf %0${#lst}d $frm)
        echo "frame $frm/$lst: $pos"
        cp -al $dir/f$one.jpg $dir/f$frm.jpg
      }

      pos=$(bc <<< "$pos+$incr")
      frm=$((repeat+2))
      while (( $(bc <<< "$pos < $end + $incr") == 1 ))
      do
        frm=$(printf %0${#lst}d $frm)
        echo "frame $frm/$lst: $pos"
        sed -ri "/tat/{n;s/\([^)]+\)/($pos)/}" frame.svg
        convert $svg -resize $size -quality $qual $dir/f$frm.jpg
        pos=$(bc <<< "$pos+$incr")
        ((frm=10#$frm+1))
      done

      # repete $repeat vezes o último frame
      one=$((frm-1))
      one=$(printf %0${#lst}d $one)
      for frm in $(seq $((one+1)) $((one+repeat)))
      {
        frm=$(printf %0${#lst}d $frm)
        echo "frame $frm/$lst: $pos"
        cp -al $dir/f$one.jpg $dir/f$frm.jpg
      }

    }

    time ffmpeg -r $rate -i $dir/f%04d.jpg -c:v libx264 -pix_fmt yuv420p $video

  }

# EOF
