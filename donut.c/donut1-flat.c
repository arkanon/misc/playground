// donut1-flat.c

// Original by Andy Sloane <andy@a1k0n.net>
// 2006/09/15 (Fri)
//
// Untangled by Arkanon <arkanon@lsd.org.br>
// 2023/02/25 (Sat) 00:49:29 -03
// 2022/05/08 (Sun) 09:44:50 -03
// 2022/05/04 (Wed) 04:13:56 -03
// 2022/05/03 (Tue) 14:17:21 -03

// gcc -o donut1-flat{,.c} -lm



#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>



void main( int argc, char *argv[] )
{

  int
    fd,
    k,
    W = 80,
    H = 22;

  float
    i,
    j,
    z[W*H],
    A = 0,
    B = 0;

  char
    b[W*H],
    C[] = ".,-~:;=!*#$@";

  if( argc == 2 )
  {
    fd = (int)( atof(argv[1])*pow(10,6) );
  }
  else
  {
    printf( "Usage: %s <frame delay (s)>\nTry with 0.02\n", argv[0] );
    exit(1);
  }

  printf("\e[2J");

  for(;;)
  {

    printf("\e[H");

    memset( b, 32, W*H  );
    memset( z,  0, 7040 );

    for( j=0; j<2*3.14; j+=0.07 )
    {

      for( i=0; i<2*3.14; i+=0.02 )
      {

        float
          c = sin(i),
          d = cos(j),
          e = sin(A),
          f = sin(j),
          g = cos(A),
          h = d + 2,
          D = 1/( c*h*e + f*g + 5 ),
          l = cos(i),
          m = cos(B),
          n = sin(B),
          t = c*h*g - f*e;

        int
          x = W/2     + 30*D*( l*h*m - t*n ),
          y = H/2 + 1 + 15*D*( l*h*n + t*m ),
          o = x + W*y,
          N = 8*( ( f*e - c*d*g )*m - c*d*e - f*g - l*d*n );

        if( H>y && y>0 && x>0 && W>x && D>z[o] )
        {
          z[o] = D;
          b[o] = C[ N>0 ? N : 0 ];
        }

      }

    }

    for( k=0; k<W*H+1; k++ )
    {
      putchar( k%W ? b[k] : 10 );
    }

    A += 0.04;
    B += 0.02;

    usleep(fd);

  }

}



// EOF
