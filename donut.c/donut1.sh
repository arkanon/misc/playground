#!/bin/bash

# donut1.sh

# Arkanon <arkanon@lsd.org.br>
# 2022/05/05 (Thu) 02:24:35 -03
# 2022/05/04 (Wed) 04:27:18 -03

# Translated from <http://gitlab.com/arkanon/playground/-/blob/master/donut.c/donut1-flat.c>

  W=80
  H=22
  A=0
  B=0
  C=( \. \, \- \~ \: \; \= \! \* \# \$ \@ )

  if (( $# == 1 ))
  then
    fd=$1
  else
    printf "Usage: %s <frame delay (s)>\nTry 0.02\n" ${0##/*}
    exit 1
  fi

  printf "\x1b[2J"

  for((;;))
  {

    printf -v b '" " %.0s' $( seq $((W*H)) )
    eval b=( $b )

    printf -v z   '0 %.0s' $( seq 7040 )
    z=( $z )

    for (( j=0; j<2*314; j+=7 ))
    {

      for (( i=0; i<2*314; i+=2 ))
      {

        out=$(
        bc -l <<< "

          # float
          scale = 10
          c = s($i/100)
          d = c($j/100)
          e = s($A/100)
          f = s($j/100)
          g = c($A/100)
          h = d + 2
          k = 1/( c*h*e + f*g + 5 )
          l = c($i/100)
          m = c($B/100)
          n = s($B/100)
          t = c*h*g - f*e

          # int
          x = $W/2     + 30*k*( l*h*m - t*n )
          y = $H/2 + 1 + 15*k*( l*h*n + t*m )
          o = x + $W*y
          p = 8*( ( f*e - c*d*g )*m - c*d*e - f*g - l*d*n )
          scale = 0
          k/1
          o/1
          p/1

          H>y && y>0 && x>0 && W>x && k>${z[o]}

        " )

        read k o p test <<< $(echo $out)

      # echo $test $k $o $p

        (( test )) &&
        {
          z[o]=$k
          b[o]=${C[p>0?p:0]}
        }

      }

    }
    # 1m25,419s

    printf "\x1b[H"

  # for (( k=0; k<W*H+1; k++ )); { printf -v hex '\\x%x' $(( k%W ? b[k] : 10 )); printf $hex; }
    for (( k=0; k<W*H+1; k++ )); { ((k%W)) && printf "${b[k]}" || printf '\x0a'; }

    ((A+=4))
    ((B+=2))

    sleep $fd

  }



  test()
  {

    unset W H A B C b z k o p test

    W=80
    H=22
    A=0
    B=0
    C=( \. \, \- \~ \: \; \= \! \* \# \$ \@ )

    printf -v b '" " %.0s' $( seq $((W*H)) )
    eval b=( $b )

    printf -v z   '0 %.0s' $( seq 7040 )
    z=( $z )

    time \
    for (( j=0; j<2*314; j+=7 ))
    {

      for (( i=0; i<2*314; i+=2 ))
      {

        out=$(
        bc -l <<< "
          scale = 10
          c = s($i/100)
          d = c($j/100)
          e = s($A/100)
          f = s($j/100)
          g = c($A/100)
          h = d + 2
          k = 1/( c*h*e + f*g + 5 )
          l = c($i/100)
          m = c($B/100)
          n = s($B/100)
          t = c*h*g - f*e
          x = $W/2     + 30*k*( l*h*m - t*n )
          y = $H/2 + 1 + 15*k*( l*h*n + t*m )
          o = x + $W*y
          p = 8*( ( f*e - c*d*g )*m - c*d*e - f*g - l*d*n )
          scale = 0
          k/1
          o/1
          p/1
          H>y && y>0 && x>0 && W>x && k>${z[o]}
        " )

        read k o p test <<< $(echo $out)

      # echo $test $k $o $p

        (( test )) &&
        {
          b[o]=${C[p>0?p:0]}
          z[o]=$k
        }

      }

    }
    # 1m30,612s

    echo "${b[*]}"
    echo "${z[*]}"

}



# EOF
