# [Qual é o código C mais ofuscado que você já viu?](http://quora.com/What-is-the-most-obfuscated-C-code-you-have-ever-seen/answer/Andreas-Blixt)



Códigos originais de [Andy Sloane](andy@a1k0n.net).



## Versão 1

[Coma um donut.](http://www.a1k0n.net/2006/09/15/obfuscated-c-donut.html)

`donut1.c`
```c
             k;double sin()
         ,cos();main(){float A=
       0,B=0,i,j,z[1760];char b[
     1760];printf("\x1b[2J");for(;;
  ){memset(b,32,1760);memset(z,0,7040)
  ;for(j=0;6.28>j;j+=0.07)for(i=0;6.28
 >i;i+=0.02){float c=sin(i),d=cos(j),e=
 sin(A),f=sin(j),g=cos(A),h=d+2,D=1/(c*
 h*e+f*g+5),l=cos      (i),m=cos(B),n=s\
in(B),t=c*h*g-f*        e;int x=40+30*D*
(l*h*m-t*n),y=            12+15*D*(l*h*n
+t*m),o=x+80*y,          N=8*((f*e-c*d*g
 )*m-c*d*e-f*g-l        *d*n);if(22>y&&
 y>0&&x>0&&80>x&&D>z[o]){z[o]=D;;;b[o]=
 ".,-~:;=!*#$@"[N>0?N:0];}}/*#****!!-*/
  printf("\x1b[H");for(k=0;1761>k;k++)
   putchar(k%80?b[k]:10);A+=0.04;B+=
     0.02;}}/*****####*******!!=;:~
       ~::==!!!**********!!!==::-
         .,~~;;;========;;;:~-.
             ..,--------,*/
```

```shell-session
$ gcc -o donut1{,.c} -lm
$ sudo apt install cpulimit
$ ./donut1 & cpulimit -qe donut1 -l 20

$ gcc -o donut1-flat{,.c} -lm
$ ./donut1-flat
Usage: ./donut1-flat <frame delay (s)>
Try with 0.02

$ ./donut1-flat .02
```
![donut1.c execution example](donut1.gif "donut1.c")



Procuranto pelo ponto em que o primeiro frame se repete:

```shell-session
$ ./donut1-frame 0 > frame0.txt
$ hash0=$(md5sum frame0.txt | cut -d\  -f1)
$ echo $hash0
aab61d00379782f01c31fa7a6ac15037

$ n=1
$ time { while ! ./donut1-frame $n | md5sum | cut -d\  -f1 | grep -q $hash0; do echo -n "$n "; ((n++)); done; }
1 2 3 4 5 ... 4431770 ^C
440m49,917s
```

Nada em 4.431.770 frames...



## Versão 2

[Embelezando o donut: um clichê do CG old-school](http://www.a1k0n.net/2006/09/20/obfuscated-c-donut-2.html)

`donut2.c`
```c
_,x,y,o       ,N;char       b[1840]       ;p(n,c)
{for(;n       --;x++)       c==10?y       +=80,x=
o-1:x>=       0?80>x?       c!='~'?       b[y+x]=
c:0:0:0       ;}c(q,l       ,r,o,v)       char*l,
       *r;{for       (;q>=0;       )q=("A"       "YLrZ^"
       "w^?EX"           "novne"     "bYV"       "dO}LE"
       "{yWlw"      "Jl_Ja|[ur]zovpu"   ""       "i]e|y"
       "ao_Be"   "osmIg}r]]r]m|wkZU}{O}"         "xys]]\
x|ya|y"        "sm||{uel}|r{yIcsm||ya[{uE"  "{qY\
w|gGor"      "VrVWioriI}Qac{{BIY[sXjjsVW]aM"  "T\
tXjjss"     "sV_OUkRUlSiorVXp_qOM>E{BadB"[_/6  ]-
62>>_++    %6&1?r[q]:l[q])-o;return q;}E(a){for (
       o= x=a,y=0,_=0;1095>_;)a= " <.,`'/)(\n-"  "\\_~"[
       c  (12,"!%*/')#3"  ""     "+-6,8","\"(.$" "01245"
       " &79",46)+14],  p(""       "#$%&'()0:439 "[ c(10
       , "&(*#,./1345" ,"')"       "+%-$02\"! ", 44)+12]
-34,a);  }main(k){float     A=0,B= 0,i,j,z[1840];
puts(""  "\x1b[2J");;;      for(;; ){float e=sin
(A), n=  sin(B),g=cos(      A),m=  cos(B);for(k=
0;1840>   k;k++)y=-10-k/    80   ,o=41+(k%80-40
       )* 1.3/y+n,N=A-100.0/y,b[k]=".#"[o+N&1],  z[k]=0;
       E(  80-(int)(9*B)%250);for(j=0;6.28>j;j   +=0.07)
       for  (i=0;6.28>i;i+=0.02){float c=sin(    i),  d=
       cos(  j),f=sin(j),h=d+2,D=15/(c*h*e+f     *g+5),l
=cos(i)        ,t=c*h*g-f*e;x=40+2*D*(l*h*  m-t*n
),y=12+       D  *(l*h*n+t*m),o=x+80*y,N  =8*((f*
e-c*d*g       )*m   -c*d*e-f*g-l*d*n)     ;if(D>z
[o])z[o       ]=D,b[     o]=" ."          ".,,-+"
       "+=#$@"       [N>0?N:       0];;;;}       printf(
       "%c[H",       27);for       (k=1;18       *100+41
       >k;k++)       putchar       (k%80?b       [k]:10)
       ;;;;A+=       0.053;;       B+=0.03       ;;;;;}}
```

```shell-session
$ gcc -o donut2{,.c} -lm
$ sudo apt install cpulimit
$ ./donut2 & cpulimit -qe donut2 -l 20

$ gcc -o donut2-flat{,.c} -lm
$ ./donut2-flat
Usage: ./donut2-flat <frame delay (s)>
Try with 0.02

$ ./donut2-flat .02
```

![donut2.c execution example](donut2.gif "donut2.c")



☐
