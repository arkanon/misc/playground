// donut2-flat.c

// Original by Andy Sloane <andy@a1k0n.net>
// 2006/09/20 (Wed)

// Untangled by Arkanon <arkanon@lsd.org.br>
// 2023/02/25 (Sat) 00:45:26 -03
// 2023/02/24 (Fri) 15:59:31 -03

// gcc -o donut2-flat{,.c} -lm



#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>



#define W 80
#define H 23



int
  _,
  x,
  y,
  o,
  N;

char
  b[W*H];



int p(int n, int c)
{
  for (; n--; x++)
  {
    (c==10)
    ? (
        y += W,
        x  = o - 1
      )
    : (
        (x>=0)
        ? (
            (W>x)
            ? (
                (c!='~')
                ? b[y+x] = c
                : 0
              )
            : 0
          )
        : 0
      );
  }
}



int c(q, l, r, o, v)
int   q, o, v;
char *l, *r;
{
  for (; q >= 0;)
  {
    q = (
          "A"
          "YLrZ^"
	  "w^?EX"
          "novne"
          "bYV"
          "dO}LE"
	  "{yWlw"
          "Jl_Ja|[ur]zovpu"
          ""
          "i]e|y"
	  "ao_Be"
          "osmIg}r]]r]m|wkZU}{O}"
          "xys]]\
x|ya|y"   "sm||{uel}|r{yIcsm||ya[{uE"
          "{qY\
w|gGor"   "VrVWioriI}Qac{{BIY[sXjjsVW]aM"
          "T\
tXjjss"   "sV_OUkRUlSiorVXp_qOM>E{BadB"[_ / 6] - 62 >> _++ % 6 & 1 ? r[q] : l[q]
        ) - o;
  }
  return q;
}



int E(int a)
{
  for (o = x = a, y = 0, _ = 0; 1095 > _;)
  {
    a = " <.,`'/)(\n-"
        "\\_~"
        [
          c(
             12,
             "!%*/')#3"  ""       "+-6,8",
             "\"(.$"     "01245"  " &79",
             46
           ) + 14
        ],
        p (
            ""
            "#$%&'()0:439 "
            [
             c(
                10,
                "&(*#,./1345",
                "')" "+%-$02\"! ",
                44
              ) + 12
            ] - 34,
            a
          );
  }
}



void main( int argc, char *argv[] )
{

  int
    fd,
    k;

  float
    i,
    j,
    z[W*H],
    A = 0,
    B = 0;

  if( argc == 2 )
  {
    fd = (int)( atof(argv[1])*pow(10,6) );
  }
  else
  {
    printf( "Usage: %s <frame delay (s)>\nTry with 0.02\n", argv[0] );
    exit(1);
  }

  printf("\e[2J");

  for (;;)
  {

    printf("\e[H");

    float
      e = sin (A),
      n = sin (B),
      g = cos (A),
      m = cos (B);

    for (k = 0; W*H > k; k++)
    {
      y    = -10 -  k / W,
      o    =  41 + (k % W - 40) * 1.3 / y + n,
      N    = A - 100.0 / y,
      b[k] = ".#"[o + N & 1],
      z[k] = 0;
    }

    E(W - (int) (9 * B) % 250);

    for (j = 0; 2*3.14 > j; j += 0.07)
    {
      for (i = 0; 2*3.14 > i; i += 0.02)
      {

        float
          c = sin (i),
          d = cos (j),
          f = sin (j),
          h = d + 2,
          D = 15 / (c * h * e + f * g + 5),
          l = cos (i),
          t = c * h * g - f * e;

        x = 40 + 2 * D * (l * h * m - t * n),
        y = 12 + D * (l * h * n + t * m),
        o = x + W * y,
        N = 8 * ((f * e - c * d * g) * m - c * d * e - f * g - l * d * n);

        if (D > z[o])
        {
          z[o] = D,
          b[o] = " ." ".,,-+" "+=#$@"[N > 0 ? N : 0];
        }

      }
    }

    for (k = 1; 18 * 100 + 41 > k; k++) putchar (k % W ? b[k] : 10);

    A += 0.053;
    B += 0.03;

    usleep(fd);

  }

}



// EOF
