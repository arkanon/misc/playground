#!/bin/bash

# donut2.sh

# Arkanon <arkanon@lsd.org.br>
# 2023/02/23 (Thu) 16:18:00 -03
# 2022/05/05 (Thu) 02:24:35 -03

# DEBUG
#
#   nf=15; gvim -d <(./donut2-flat $nf) <(./donut2.sh $nf)
#   nf=15; diff    <(./donut2-flat $nf) <(echo -e "$(./donut2.sh $nf)")
#   time echo -e "$(./donut2.sh $nf)" # 0m2,759s
#
#   . <(curl -sL bit.ly/benshmark-v3); alias bm=benshmark-v3
#   s1(){ ./donut2-flat $nf; }
#   s2(){ echo -e "$(./donut2.sh $nf)"; }
#   bm 20 s1 s2
#   # s1  00:00,171
#   # s2  00:53,937
#   #
#   # 1º  s1
#   # 2º  s2/s1     314,421 vezes mais lenta



  fn=${1:?$'\n'Uso: printf \"\$($0 <frame number>)\"$'\n'Experimente 10}

  C='.,-~:;=!*#$@'

  for (( k=0; k<${#C}; k++ )); { printf -vc %d "'${C:k:1}"; D+=u[$k]=$c\;; }

  BC_LINE_LENGTH=0 bc -l << EOT

    scale = 6

    $D

    w = 80
    h = 22
    a = $fn*0.04
    q = $fn*0.02

    for( k=0; k<w*h  ; k++ ) b[k]=32
    for( k=0; k<7040 ; k++ ) z[k]=0

    for( j=0; j<=2*3.14; j+=0.07 )
    {

      for( i=0; i<=2*3.14; i+=0.02 )
      {

      # float
          c = s(i)
          d = c(j)
          e = s(a)
          f = s(j)
          g = c(a)
          v = d + 2
          r = 1/( c*v*e + f*g + 5 )
          l = c(i)
          m = c(q)
          n = s(q)
          t = c*v*g - f*e

      # int
          x = w/2     + 30*r*( l*v*m - t*n )
          y = h/2 + 1 + 15*r*( l*v*n + t*m )
          p = 8*( ( f*e - c*d*g )*m - c*d*e - f*g - l*d*n )

          scale = 0
          x = x/1
          y = y/1
          o = x + w*y
          p = p/1
          scale = 6

        if( h>y && y>0 && x>0 && w>x && r>z[o] )
        {
          z[o]=r
          if (p>0) b[o]=u[p] else b[o]=u[0]
        }

#       print h, ">", y, "\t", w, ">", x, "\t0", r, ">0", z[o], "\t", p, "\t", b[o], "\n"

      }

    }

    scale = 0
    obase = 16
    for( k=0; k<w*h+1; k++ )
    {
    # print k, " ", w, " ", k%w, "\n"
      if (k%w) print "\\\\x", b[k] else print "\n"
    }

EOT



# EOF
