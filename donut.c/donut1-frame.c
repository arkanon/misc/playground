// donut1-flat.c

// Untangled by Arkanon <arkanon@lsd.org.br>
// 2023/02/23 (Thu) 16:18:04 -03
// 2022/05/08 (Sun) 09:44:50 -03

// gcc -o donut2-flat{,.c} -lm
// time ./donut2-flat 10 # 0m0,019s



#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

void main( int argc, char *argv[] )
{

  int
    fn,
    k,
    w = 80,
    h = 22;

  float
    i,
    j,
    z[w*h],
    a,
    q;

  char
    b[w*h],
    u[] = ".,-~:;=!*#$@";

  if( argc == 2 )
  {
    fn = (int)( atof(argv[1]) );
  }
  else
  {
    printf( "Uso: %s <frame number>\nExperimente 10\n", argv[0] );
    exit(1);
  }

  a = fn*0.04,
  q = fn*0.02;

  memset( b, 32, w*h  );
  memset( z,  0, 7040 );

  for( j=0; j<2*3.14; j+=0.07 )
  {

    for( i=0; i<2*3.14; i+=0.02 )
    {

      float
        c = sin(i),
        d = cos(j),
        e = sin(a),
        f = sin(j),
        g = cos(a),
        v = d + 2,
        r = 1/( c*v*e + f*g + 5 ),
        l = cos(i),
        m = cos(q),
        n = sin(q),
        t = c*v*g - f*e;

      int
        x = w/2     + 30*r*( l*v*m - t*n ),
        y = h/2 + 1 + 15*r*( l*v*n + t*m ),
        o = x + w*y,
        p = 8*( ( f*e - c*d*g )*m - c*d*e - f*g - l*d*n );

      if( h>y && y>0 && x>0 && w>x && r>z[o] )
      {
        z[o] = r;
        b[o] = u[ p>0 ? p : 0 ];
      }

//    printf("%i>%i\t%i>%i\t%f>%f\t%i\t%i\n", h, y, w, x, r, z[o], p, b[o]);

    }

  }

  for( k=0; k<w*h+1; k++ )
  {
    putchar( k%w ? b[k] : 10 );
  }

}



// EOF
