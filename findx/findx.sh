#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2015/08/06 (Qui) 15:03:08 BRS
#
# <http://fb.com/PiMathAddicts/posts/391305147733661>
# Puzzle by Aziz Inan, Professor of Electrical Engineering at the University of Portland
#
# Find the integer X that satisfies:
#   1. the digits of X add up to a number Y where X equals Y times the number you get when you reverse the digits of Y.
#   2. reverse the digits of X and find the prime factors of the number you get. Sum of the squares of these prime factors
#      and halve it. Removing the digit 0 from the new number yields back X.
#
#      X    1729
#      sdX  1+7+2+9+0
#      Y    19
#      rY   91
#      YrY  1729
#      rX   9271
#      frX  73 127
#      sfrX (73*73+ 127*127+0)/2
#      S    10729
#
# Uma solução em BASh, executada serialmente e em paralelo com a última versão do Gnu Parallel (20150722) numa máquina
#   com 1 processador i5 com 4 núcleos sem hyper-threading (não que esse problema exija muita capacidade computacional).
#
# Versão compacta:
#
#   findx()
#   {
#     local X=$1 Y=$(($(sed 's/./&+/g' <<< $X)0)) S
#     S=$(( ( $(factor $(rev <<< $X) | cut -d: -f2 | sed -r 's/ [^ ]+/&*&+/g')0 ) / 2 ))
#     ((X==Y*$(rev <<< $Y))) && tr -d 0 <<< $S | grep -q $X && echo $X $S || return 1
#   }
#
# Versão human friendly:)

  findx()
  {

    local X sdX Y rY YrY rX frX sfrX S

       X=$1
     sdX=$(sed 's/./&+/g' <<< $X)0
       Y=$((sdX))
      rY=$(rev <<< $Y)
     YrY=$((Y*rY))
      rX=$(rev <<< $X)
     frX=$(factor $rX | cut -d: -f2)
    sfrX=($(sed -r 's/[^ ]+/&*&+/g' <<< $frX)0)/2
       S=$((sfrX))

    ((X==YrY)) && tr -d 0 <<< $S | grep -q $X \
                                   && echo -e "X\t$X\nsdX\t$sdX\nY\t$Y\nrY\t$rY\nYrY\t$YrY\nrX\t$rX\nfrX\t$frX\nsfrX\t$sfrX\nS\t$S" \
                                   || return 1

  }

  export -f findx

  time ( TIMEFORMAT=%lR; x=0; while ! findx $x; do ((x++)); done )
  # 0m18.281s

  time ( TIMEFORMAT=%lR; seq 1000000 | parallel --no-notice --halt now,success=1 findx 2> /dev/null )
  # 0m9.156s

# EOF
