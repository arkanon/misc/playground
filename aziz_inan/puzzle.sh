#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/08/05 (Wed) 02:38:03 -03

# 2015/08/05 Wed 12:30:00 -03 <http://fb.com/MathAddictsHub/posts/391305147733661>
# 2015/08/05 Wed 19:40:00 -03 <http://fb.com/arkanon/posts/10204315837727384>
# 2015/08/06 Thu 10:34:00 -03 <http://fb.com/arkanon/posts/10204318613676781>
#
# Encontre X com as propriedades A e B:
#
# 1. Y = soma dos dígitos de X
# 2. R = reverso de Y
# 3. P = Y * R
# A) P == X
# 4. R = reverso de X
# 5. F = conjunto dos fatores primos de R
# 6. S = soma do quadrado de cada elemento de F
# 7. M = S / 2
# 8. Z = M sem o dígito 0
# B) Z == X
#
# (Quebra-cabeça de Aziz Inan, professor de engenharia elétrica da Universidade de Portland)



  time \
  {
    for (( x=10; ; x++ ))
    {

      # [ 1 ]
      n=${#x}
      y=0
      for (( i=0; i<n; i++ )) { ((y+=x%10**(i+1)/10**i)); }

      # [ 2 ]
      n=${#y}
      r=
      for (( i=0; i<n; i++ )) { r=${y:i:1}$r; }

      # [ 3 ]
      ((p=y*r))

      # [ A ]
      (( p == x )) && echo -en "x=$x\ty=Σ(xᵢ)=$y\tp=y*R(y)=$p\tp=x" || continue

      # [ 4 ]
      n=${#x}
      r=
      for (( i=0; i<n; i++ )); { r=${x:i:1}$r; }

      # [ 5 ]
      f=$(factor $r | cut -d\  -f2-)

      # [ 6 ]
      s=0
      for i in $f; { ((s+=i**2)); }

      # [ 7 ]
      ((m=s/2))

      # [ 8 ]
      z=${m//0/}

      echo -en "\tf[R(x)]=$f\ts=Σ(fᵢ²)=$s\tm=s/2=$m\tz=Z(m)=$z"

      # [ B ]
      (( z == x )) && { echo -e "\tz=x"; break; } || echo

    }
  }



  calc()
  {
    local X=$1 Y=$(($(sed 's/./&+/g' <<< $X)0)) S
    S=$(( ( $(factor $(rev <<< $X) | cut -d: -f2 | sed -r 's/ [^ ]+/&*&+/g')0 ) / 2 ))
    ((X==Y*$(rev <<< $Y))) && tr -d 0 <<< $S | grep -q $X && echo $X $S || return 1
  }
  export -f calc

# time ( TIMEFORMAT=%lR; x=0; while ! calc $x; do echo $x; ((x++)); done )
  # 1729 10729
  # 0m14.330s

# time ( TIMEFORMAT=%lR; seq 1000000 | parallel --no-notice --halt now,success=1 calc 2> /dev/null )
  # 1729 10729
  # 0m8.394s



# EOF
