
# Para Minúsculo
  string="A FEW WORDS"
  echo "${string,}"        # a FEW WORDS
  echo "${string,,}"       # a few words
  echo "${string,,[AEOD]}" # a FeW WoRdS

# Para Maiúsculo
  string="a few words"
  echo "${string^}"        # A few words
  echo "${string^^}"       # A FEW WORDS
  echo "${string^^[aeod]}" # A fEw wOrDs

# Inversão
  string="A Few Words"
  echo "${string~~}"       # a fEW wORDS

  string="A FEW WORDS"
  echo "${string~}"        # a FEW WORDS

  string="a few words"
  echo "${string~}"        # A few words

# Título
  string="a few words"
  string=($string)
  string="${string[@]^}"
  echo "$string"           # A Few Words

  string="A FEW WORDS"
  string=($string)
  string=(${string[@],})
  echo "${string[@]~~}"    # A Few Words



  file=/DIRA/DIRB/FILE.EXT
  echo "file      ${file}"
  echo "dirname   ${file%/*}  == $(dirname  $file)"
  echo "basename  ${file##*/} == $(basename $file)"
  echo "filename  ${file%.*}"
  echo "extension ${file##*.}"

