#!/bin/bash
# linha 2
# <https://stackoverflow.com/a/2871034> Automatic exit from bash shell script on error
# <http://stackoverflow.com/a/24399288> How to get the real line number of a failing Bash command
# linha 5
set -e
# linha 7
trap 'echo ">>> erro na linha $LINENO ($BASH_COMMAND)"' ERR
# linha 9
echo linha $LINENO
# linha 11
cat /oi
# linha 13
