
  # 1. Quando usamos o comando sudo para executar algum comando como root, por um período de tempo default de 15min
  #    configurável no suduers pelo parâmetro timestamp_timeout, não será requisitada novamente a senha do usuário.
  #    Para forçar o sudo a "esquecer" a senha e pedí-la novamente na próxima execução, basta executar:
  sudo -k

  # 2. Se cancelarmos com Control+C a execução de um comando enquanto ele está num prompt de senha (o qual não ecoa
  #    os caracteres para a tela), voltaremos para o prompt de comando.
  #    Aparentemente o terminal estará "bugado" pois ele não mostrará os caracteres digitados, mas ele apenas estará
  #    na mesma configuração do prompt de senha (sem echo de caracteres).
  #    Para ele voltar à ecoar os caracteres, basta digitar o comando:
  stty echo

  #
  # 3. Eventualmente o direcionamento de um arquivo binário para a tela pode desconfigurar o terminal, mostrando caracteres
  #    estranhos quando letras forem digitadas. Na maioria das vezes, o comando restaura o comportamento normal do terminal:
  setterm -reset

