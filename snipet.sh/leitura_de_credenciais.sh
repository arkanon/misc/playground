#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2017/11/21 (Tue) 16:14:08 -02



  defcredvar=CRED
  defcredfil=\$HOME/.crd/ad

  eval credfil=\${$defcredvar:-$defcredfil}

  [[ -e $credfil ]] && . $credfil

  user=$username
  pass=$password
  domn=$domain

  data=$1



  if [[ $# < 1 || $* == -h ]]
  then
    cat << EOT

Uso: ${0##*/} -h | -t | [[domínio/]usuário[%senha]@]<host>[:port]

     Credenciais (domínio, usuário e senha) obtidas por default do arquivo $defcredfil
     Arquivo default de credenciais pode ser alterado setando seu caminho na variável global $defcredvar
     Procurando credenciais em $credfil

EOT
    exit
  fi



  if [[ $* == -t ]]
  then
    d=dom
    u=user
    s=p@:%ss
    h=host
    p=port
    bash $0          $h
    bash $0          $h:$p
    bash $0       $u@$h
    bash $0    $u%$s@$h:$p
    bash $0    $d/$u@$h:$p
    bash $0 $d/$u%$s@$h:$p
    echo
    exit
  fi



  host=$data

  if grep -q @ <<< $host
  then
    user=$(awk -F@ '{for (i=1;i<NF-1;i++) print $i FS; print $i}' <<< $host | tr -d '\n')
    host=$(awk -F@ '{print $NF}' <<< $host)
    pass=
  fi

  if grep -q : <<< $host
  then
    port=$(cut -d: -f2 <<< $host)
    host=$(cut -d: -f1 <<< $host)
  fi

  if grep -q % <<< $user
  then
    pass=$(cut -d% -f2- <<< $user)
    user=$(cut -d% -f1  <<< $user)
  fi

  if grep -q / <<< $user
  then
    domn=$(cut -d/ -f1 <<< $user)
    user=$(cut -d/ -f2 <<< $user)
  fi

# [[ $domn == ~ ]] && domn= || domn="-d $domn"
  [[ $domn      ]] && domn="-d $domn"
  [[ $pass      ]] && pass="-p $pass"



  echo -e "\nINPUT=$data\nD=$domn U=$user S=$pass H=$host P=$port"



# EOF
