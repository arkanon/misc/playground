#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2024/07/07 (Sun) 09:04:40 -03

# <https://github.com/jichu4n/bash-command-timer>
# https://stackoverflow.com/questions/51557238/why-is-the-debug-trap-executed-more-times-than-expected

# ./write.sh
# ctrl-c

# rm nohup.out; nohup ./write.sh & pid=$!
# rm z
# kill -6 $pid



export LC_ALL=C
shopt -s extglob



dbg()
{
  if (( $1 == BASH_LINENO ))
  then
    declare -p LINENO
    declare -p BASH_LINENO
    echo Linha $1 executada: $BASH_COMMAND
  else
    true
  fi
}

err()
{
  echo -e "[ERRO] Comando finalizado com o status $? na linha $BASH_LINENO: $BASH_COMMAND"
  end ERR
}

end()
{
  local sig=$1 pid=$$ f
  f=s-$pid-$sig
  rm -f s-!($pid-*)
  > $f
  [[ $sig != EXIT ]] && echo -e "\npid=$pid"
  ls -gG --full-time $f
  if [[ $sig == EXIT ]]
  then
    ls -gG --full-time z
  # read -p "Pressione ENTER "
  fi
  exit
}



# trap '          '  RETURN
  trap ' err      '  ERR
  trap ' dbg 74   '  DEBUG
  trap ' end EXIT '  EXIT
  trap ' end HUP  '  1
  trap ' end INT  '  2
  trap ' end QUIT '  3
  trap ' end ABRT '  6
  trap ' end ALRM ' 14
  trap ' end TERM ' 15



> z

  true
# false

trap

while true
do
  ls z >& /dev/null
  sleep 1
done



# EOF
