
# <http://ss64.com/bash/syntax-keyboard.html>

  #   Tab        completa nome de arquivos e diretórios

  #   Ctrl + L   limpa a tela (similar ao comando 'clear')

  #   Ctrl + a   vai para o início da linha (Home)
  #   Ctrl + e   vai para o fim da linha (End)

  #    Alt + b   vai para o início da palavra (alfanumérica)
  #    Alt + f   vai para o fim da palavra (alfanumérica)

  #   Ctrl + w   corta até o início da palavra (separada por espaços)
  #    Alt + d   corta até o fim da palavra (alfanumérica)

  #   Ctrl + k   corta até o fim da linha
  #   Ctrl + u   corta até o início da linha

  #   Ctrl + y   cola (yank) o que foi cortado

  #    Alt + u   deixa MAIÚSCULAS as letras até o fim da palavra
  #    Alt + l   deixa minúsculas as letras até o fim da palavra

  #   Ctrl + _   undo (múltiplo)

