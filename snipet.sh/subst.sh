# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2024/07/31 (Wed) 14:05:38 -03
# 2024/07/31 (Wed) 11:49:00 -03

subst(){ bash -c $'eval "cat << EOT\n$(<$1)\nEOT"' -- $1; }

var=variável
export envvar1=valor1
export envvar2=valor2
export envvar3=
export literal=string

cat << \EOT > tpl
Teste
var: $var
envvar1: $envvar1
envvar2: ${envvar2:-def}
envvar3: ${envvar3:-def}
literal: \$literal
Fim
EOT

subst tpl
# Teste
# var:
# envvar1: valor1
# envvar2: valor2
# envvar3: def
# literal: $literal
# Fim

# EOF
