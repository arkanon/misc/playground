
# DICA >> mover e dimensionar janela <<
  WID=$(xwininfo -name Sky -all | grep 'xwininfo: Window id:' | cut -d\  -f4)
  xdotool windowmove $WID 742 47 windowsize $WID 282 1228



# DICA >> listar janelas com WID e PID <<
  wmctrl -lp
  # 0x00e00024 -1 10162  soft070-008 panel
  # 0x00e00035 -1 10162  soft070-008 panel
  # 0x00e00039 -1 10162  soft070-008 panel
  # 0x01000003 -1 1709   soft070-008 pcmanfm
  # 0x01000028 -1 1709   soft070-008 pcmanfm
  # 0x01000031 -1 1709   soft070-008 pcmanfm
  # 0x01c00004  0 30271  soft070-008 soft070-008[pts/9]~
  # 0x01e00010  0 5182   soft070-008 xorg - Get X window id from process in bash - Stack Overflow - Mozilla Firefox
  # 0x0220010e  0 13133  soft070-008 Zoiper
  # 0x0260000d  0 13144          N/A Sky

  ps -P 13144
  # PID PSR TTY      STAT   TIME COMMAND
  # 13144   2 ?        Sl     0:02 /usr/bin/sky

  pid=$(ps ho pid -C sky)
  cat /proc/$pid/environ | tr '\0' '\n' | sort

  xwininfo -name Sky -all | grep 'xwininfo: Window id:' | cut -d\  -f4
  xprop    -name Sky      | grep 'window id'            | cut -d\  -f5

