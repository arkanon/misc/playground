
  export MAN_DISABLE_SECCOMP=1
  man -P cat parallel | grep -q no-notice && quiet="--no-notice"
  man -P cat parallel | grep -q will-cite && quiet="--will-cite"

  task() { printf "Task %02i/%02i: %s\n" $1 $2 $3; sleep $((RANDOM%5)); }; export -f task

  input_data=$(seq -w 50 | sed 's/^/line-/')

  echo "$input_data" | time parallel --gnu --progress $quiet -j5 task {#} {= '$_=total_jobs()' =} {}

