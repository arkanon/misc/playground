#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2024/02/18 (Sun) 01:53:11 -03
# 2023/04/26 (Wed) 22:43:11 -03

# versão 1:  while read -p  '$ '; do eval "$REPLY"; done
# versão 2:  while read -ep '$ '; do eval "$REPLY"; done
# versão 3:  while read -ep '$ '; do history -s "$REPLY"; eval "$REPLY"; done
# versão 4:

  exemplo=(

'
relansi()
+ {
+   while :
+   do
+     printf "\e7\e[1;$((COLUMNS-8))H%(%H:%M:%S)T\e8" -1
+     sleep 1
+   done &
+ }
'

'
relansi()
{
  local sc=$(tput sc) cup=$(tput cup 0 $((COLUMNS-8))) rc=$(tput rc) el=$(tput el)
  exec 3>&2 2>&- # suprime as msg de entrada em bg dos loops abaixo
  while :
  do
    printf "$sc$cup%(%H:%M:%S)T$rc" -1
    sleep 1
  done & relansi=$!
  {
    while kill -0 $relansi; do sleep .1; done
    printf "$sc$cup$el$rc"
  } &
  disown $relansi $! # suprime as msg de finalização dos processo em bg indicados
  exec 2>&3
  echo "Interrompa com [kill \$relansi]"
}
'
)



  sep=$'\u2500' # ─ Box Drawings Light Horizontal

  clear
  printf -v line %${COLUMNS}s
  line=${line// /$sep}
  printf "$exemplo\n$line\n\n"

  export       HISTFILE=~/.whilehist
  export HISTTIMEFORMAT='%Y/%m/%d %a %T %z  '
  export     TIMEFORMAT=$'\e[0;43m%lR\e[0m'
  export PROMPT_COMMAND='history -a' # não funciona porque não há um prompt propriamente dito

# # opção 1: inicia com o histórico vazio
# > $HISTFILE

# # opção 2: inicia com o histórico anterior
# history -r

# # opção 3: inicia com um histórico padrão pré-definido
  printf '#1\necho um\n#1\necho dois\n#1\ncat $HISTFILE\n' > $HISTFILE; history -c; history -r

  # habilita aliases se estiver em um script
  shopt -s expand_aliases

  # define alguns aliases especialmente úteis
  alias q=break # interrompe o loop
  alias h=history
  alias l="BLOCK_SIZE=\'1 ls -lap"

# while read -ep "$ "
# do
#   history -s "$REPLY"
#   history -a
#   eval "$REPLY"
# done

  while read -ep "$ "
  do
    if [[ ${REPLY::1} == + ]]
    then
      cmd+=${REPLY:1}
    else
      [[ $REPLY != lf ]] && cmd=$REPLY
    fi
    if [[ $REPLY == lf && $cmd != lf ]]
    then
      history -s "$cmd"
      history -a
      eval "$cmd"
    fi
  done

  echo 'Até logo!'

# EOF
