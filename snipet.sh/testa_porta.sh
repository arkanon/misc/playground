# testa_porta.sh

# Arkanon <arkanon@lsd.org.br>
# 2023/04/16 (Sun) 11:07:02 -03
# 2023/03/20 (Mon) 17:15:25 -03
# 2023/03/20 (Mon) 14:31:42 -03

# $ time testa-porta google.com 80; echo $?
# 0m0,507s
# 0
#
# $ time testa-porta google.com 123; echo $?
# 0m3,514s
# 1
#
# A variável oktout com o timeout para conexões estabelecidas com sucesso pode ter que variar de acordo com a responsividade. Esse é um ponto a ser eventualmente melhorado.
#
# Sobram dois comandos não build-in: kill e sleep

testa-porta()
{
  local host=${1:?host?} port=${2:?porta?} oktout=.5 noktout=3 pid
  exec 3>&2 2>&- # esconde msg de $pid sendo enviado para bg
  : < /dev/null > /dev/tcp/$host/$port &
  pid=$!
  disown $pid # esconde msg de kill em $pid
  exec 2>&3
  sleep $oktout
  kill -0 $pid 2>&- &&
  {
    sleep $noktout
    kill $pid
    return 1
  } || return 0
}

# EOF
