#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2017/10/13 (Fri) 13:03:14 BRT

  # testa se o file descriptor 0 (entrada primária) está aberto no terminal

  [[ -t   0 ]] || echo "Entrada pelo stdin: $(< /dev/stdin)"

  [[ $# > 0 ]] && echo "Entrada por parâmetro: $@"

# EOF
