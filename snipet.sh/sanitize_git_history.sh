# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2023/06/21 (Wed) 15:40:53 -03

# [remoção de informações sensíveis do histórico]
  sudo apt install git-filter-repo
     repo='git@gitserver:namespace/reponame.git'
   search='admin@S0ftplan'
  replace='<senha>'
  : ${repo##*/}
    local=${_%.*}-tmp
  time git clone $repo $local
# time grep -r "$search"
  time git filter-repo --replace-text <(echo "$search==>$replace")
# time grep -r "$search"
  git remote add origin $repo
# git branch -r
  git fetch --all
# git branch -r
### se necessário, desproteger a branch
  git push origin --force --all
### se tiver sido desprotegida, reproteger a branch

# EOF
