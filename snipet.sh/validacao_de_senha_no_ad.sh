#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2017/10/06 (Fri) 15:49:36 BRT



  user=$1
    ip=$2
  host=$3

  ldom=dominio.com.br

    dc=$(sed -r 's/(^|\.)/,DC=/g' <<< $ldom | cut -c2-)

  succ=false
  while ! $succ
  do
    read -sp "$user domain password: " pass
    echo
    LDAPTLS_REQCERT=ALLOW ldapsearch -H ldaps://$ldom -D $user@$ldom -w $pass -b $dc sAMAccountName=$user &> /dev/null
    succ=$([[ $? == 0 ]] && echo true || echo false)
    $succ || echo "Tente novamente."
  done



# EOF
