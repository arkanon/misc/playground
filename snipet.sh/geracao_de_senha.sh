# geracao_de_senha.sh

# Arkanon <arkanon@lsd.org.br>
# 2020/09/10 (Thu) 15:20:32 -03

# <http://howtogeek.com/howto/30184/10-ways-to-generate-a-random-password-from-the-command-line>

  size=16
   set=_+@:%=A-Z-a-z-0-9

  < /dev/urandom tr -dc $set | head -c$size; echo

# EOF
