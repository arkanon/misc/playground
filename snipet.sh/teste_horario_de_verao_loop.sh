#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2023/08/27 (Sun) 04:18:32 -03

opts="
       -i $HOME/.ssh/sp-tiscripts/i-tiscripts
       -o StrictHostKeyChecking=no
       -o UserKnownHostsFile=/dev/null
       -o LogLevel=quiet
     "

#      -o PreferredAuthentications=publickey,password,keyboard-interactive

  NMAP="sudo nmap -O -p 22,80,445,65123,56123"



  # lista manual
  list="
         192.168.226.221
         192.168.224.1
         192.168.226.191
       "

# # lista automatica a partir de csv
#  csv="http://sistemas.com.br/log/vms-by-cluster/vms-by-cluster.txt"
#  vms=$(curl -kLs $csv | awk -F'\t' '{print$9FS$8FS$4FS$16FS$6FS$11}' | sed -r 's/;[^\t]+//' | grep -v template | grep poweredOn | sort -n -t. -k1,1 -k2,2 -k3,3 -k4,4 | cut -f1-4)
#    l=$(echo "$vms" | grep linux | cut -f1,2,3)
#    o=$(echo -e "172.23.1.77\tfísica\tELOS/SIM\n172.23.1.98\tfísica\tELOS/SIM") # oracle rac (físicas)
# list=$(echo -e "$l\n$o")

# # lista de arquivo
# list=$(cat /caminho/para/arquivo)



    list=$(sed -r 's/^[ \t]+//;s/#.*//;/^$/d' <<< "$list")

    N=$(wc -l <<< "$list")



# >> VERIFICAÇÃO DO ACESSO POR CHAVE SSH SEM PASSPHRASE <<

  time \
  (
    n=1
    while read -u10 h v d
    do
      printf "%3d\t$N" $n
      if [ ! "$h" -o "$h" = - ]
      then
        echo -en "\tnoip"
      else
        echo -en "\t$h"
        if ping -c1 -w1 $h &> /dev/null
        then
          echo -en "\tping"
          ( ssh $sshopt root@$h exit && echo -en "\tok" || echo -en "\tnokey" )

          # testar se é windows
          # testar se não entrou com chave e tentar senha
          for user in $users
          {
            for pass in $passes
            {
               sshpass -p $pass ssh $user@$h exit
            }
          }

        else
          echo -en "\tunreach"
          echo -en "\t"
        fi
        echo -en "\t$v\t$d"
      fi
      echo
      ((n++))
    done 10<<< "$list"
  ) | tee 1.log
  # 00:47.879



  time \
  (
    list=$(cat 1.log | grep -w ok | cut -f3,6,7)
    N=$(wc -l <<< "$list")
    n=1
    while read -u10 h v d
    do
      printf "%3d\t$N" $n
      if [ ! "$h" -o "$h" = - ]
      then
        echo -en "\tnoip"
      else
        echo -en "\t$h"
        if ping -c1 -w1 $h &> /dev/null
        then
          echo -en "\tping"
          ( ssh $sshopt root@$h exit && echo -en "\tok" || echo -en "\tnokey" )
        else
          echo -en "\tunreach"
          echo -en "\t"
        fi
        echo -en "\t$v\t$d"
        text=$(
                ssh -T $sshopt root@$h \
                '
                  export LC_ALL=C
                  export TZ=/usr/share/zoneinfo/Brazil/East
                  fmt="%Y/%m/%d %a %H:%M:%S %:z %Z"
                  echo -en "\t$(date -d "2016/10/15 23:59:59" +"$fmt")"
                  echo -en "\t$(date -d "2016/10/16 01:00:00" +"$fmt")"
                  echo -en "\t$(date -d "2017/02/18 23:59:59" +"$fmt")"
                  echo -en "\t$(date -d "2017/02/19 00:00:01" +"$fmt")"
                '
              )
        eval mime=$(echo "$text" | file -bi -) # mime=text/plain; charset=iso-8859-1
        [ "$charset" == "utf-8" ] && echo -n "$text" || echo -n "$text" | iconv -f iso-8859-1 -t utf-8
      fi
      echo
      ((n++))
    done 10<<< "$list"
  ) | tee 2.log



# EOF
