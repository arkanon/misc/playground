# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2023/06/15 (Thu) 17:20:17 -03
# 2021/06/12 (Sat) 03:44:00 -03
# 2015/06/12 (Fri) 08:59:00 -03

# <http://fb.com/arkanon/posts/pfbid08eftZCsw7CaxVjLpkA6Cpsr325oP9f3tHpQtp9WeGTQuuKMhJmrAG7AAFFjp8yo4l>
# <http://fb.com/arkanon/posts/pfbid0J2kq88cE5uX9FDj3UfBaE1WMX4vSCfv5pAtutb9Mg3bEYVSCDe6oGatBhrQCxEH8l>



  bits=00100000001000000010000000100000001000000010000001011111000010100010000000100000001000000010000001011111001010000010000000101001010111110000101000100000001000000010000000101000010111110010100000100101001010010101111100101001000010100010000000100000001000000010000000101111001010000101111100101001000010100101111101011111001000000111110000001010010111000101111101011100011111000000101000100000001000000010000001111100000010100010000000100000001000000111110000001010

  s1(){ local bits=$bits; while [ "$bits" ]; do b=$(echo $bits | cut -c-8); h=$(echo "obase=16;ibase=2;$b" | bc); echo -en "\x$h"; bits=$(echo $bits | cut -c9-); done; }

  s2(){ local bits=$bits; while [ "$bits" ]; do echo -en \\x$(printf %x $(echo $((2#$(echo $bits | cut -c-8))))); bits=$(echo $bits | cut -c9-); done; }

  s3(){ local bits=$bits; while [[ $bits ]]; do printf -vx x%x $((2#${bits:0:8})); printf \\$x; bits=${bits:8}; done; }

  . <(curl -ksL bit.ly/benshmark-v5)
  alias bm=benshmark-v5

  bm 100 s{1..3}
  # s1  01:10,988
  # s2  00:38,120
  # s3  00:00,079
  #
  # 1º  s3
  # 2º  s2/s3     481,531 vezes mais lenta
  # 3º  s1/s3     897,582 vezes mais lenta

termux moto  e22
s1  14:05,733
s2  07:09,321
s3  00:00,573

1º  s3
2º  s2/s3     748,251 vezes mais lenta
3º  s1/s3   1.474,973 vezes mais lenta

bm 100 s2 s3

s2  07:28,000
s3  00:00,584

1º  s3
2º  s2/s3     766,123 vezes mais lenta



# EOF
