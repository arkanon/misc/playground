#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/06/15 (Mon) 20:13:27 -03



  run=(
    "### 0 ###"
    "### 1 ###"
    "### 2 ###"
    "### 3 ###"
    "### 4 ###"
    "### 5 ###"
    "### 6 ###"
    "### 7 ###"
    "### 8 ###"
    "### 9 ###"
  )

  tmux new     -y $LINES -x $COLUMNS -d

  tmux set     -g default-command 'bash -l'
  tmux set     -g set-titles on
  tmux set     -g set-titles-string "Particionando..."
  tmux set     -g status off
  tmux set     -g mouse  on

  # copy and paste with a mouse with tmux <http://unix.stackexchange.com/a/318285>
  tmux bind    -n         WheelUpPane  if-shell -F -t= "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'select-pane -t=; copy-mode -e; send-keys -M'"
  tmux bind    -T root MouseDown2Pane run-shell    -b  "tmux paste-buffer"

  tmux splitw  -h

  # o primeiro elemento do array é executado na coluna da esquerda
  i=0
  tmux selectp -t $i
  tmux send    -t $i "clear"      enter
  tmux send    -t $i "${run[$i]}" enter

  i=1
  tmux selectp -t $i
  tmux resizep -t $i -x 150
# tmux send    -t $i "clear"      enter
# tmux send    -t $i "${run[$i]}" enter

  # o segundo até o penúltimo elementos do array são executados cada um em uma linha na coluna da direita com mesma altura
  for i in $( seq $((${#run[*]}-2)) )
  {
    tmux splitw  -v
    tmux resizep -t $i -y 4
    tmux send    -t $i "clear"      enter
    tmux send    -t $i "${run[$i]}" enter
  }

  # o último elemento do array é executado na última linha, que terá a altura restante, da coluna da direita
  i=$((${#run[*]}-1))
  tmux send    -t $i "clear"      enter
  tmux send    -t $i "${run[$i]}" enter

  {
    sleep 5
    err=false
    tmux set -g set-titles-string "$($err && echo "#### ERRO ####" || echo "OK")"
  } &

  tmux attach



# EOF
