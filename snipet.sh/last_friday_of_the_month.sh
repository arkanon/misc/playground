#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/05/07 (Thu) 11:45:11 -03

if (( $# == 0 ))
then

  mes=$(date +%m)
  ano=$(date +%Y)

else

  if (( $1 >= 1 && $1 <= 12 ))
  then
    mes=$1
    ano=$(date +%Y)
  else
    echo "mês inválido"
    exit
  fi

  if (( $# == 2 ))
  then
    if [[ $2 =~ ^[0-9]{4}$ ]]
    then
      ano=$2
    else
      echo "ano inválido: use 4 digitos (zeros à esquerda, se necessário)"
      exit
    fi
  fi

fi

ultimodia=$(date -d "$ano/$mes/01 + 1 month - 1 day" +%Y/%m/%d)
   voltar=$(( ( $(date -d $ultimodia +%u) - 5 + 7 ) % 7 ))
ultimasex=$(date -d "$ultimodia - $voltar days" +"%Y/%m/%d %a")

echo $ultimasex

# cat << EOT
#
# mês: $mes
# ano: $ano
# último dia do mês: $ultimodia
# voltar: $voltar dias
# última sexta-feira do mês: $ultimasex
#
# EOT

# EOF
