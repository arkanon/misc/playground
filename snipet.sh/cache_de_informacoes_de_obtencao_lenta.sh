
# Arkanon <arkanon@lsd.org.br>
# 2017/10/26 (Thu) 10:56:13 BRST

  [[ "${@: -1}" == qsn ]] && qsn=true || qsn=false # query server now

  cred=${AD_CRED:-$HOME/.crd/ad}
  test -e $cred && . $cred # username, password, domain

    server=172.23.1.1
     cache=/fs/sbin/estacoes-linux/cache/dhcp
  cxexpire=$((30*60)) # (30min) tempo em segundos de validade do cache da listagem de tenants do nova
     cxage=$(( $(date +%s) - $(test -s $cache && stat -c %Z $cache 2> /dev/null || echo 0) ))

  echo qsn $qsn
  echo cache time $cxage
  echo cache lease $cxexpire

  data=$(
          { ! $qsn && (( cxage < cxexpire )); } \
          && cat $cache \
          || {
               time psexec $username%$password@$server ps "Get-DHCPServerV4Scope | Get-DHCPServerv4Lease -AllLeases" \
                    | sort -V \
                    | tee $cache.part
               mv $cache.part $cache
             }
        )

# EOF
