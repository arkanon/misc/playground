#!/bin/bash

# Arkanon <$username@$domain>
# 2020/11/09 (Mon) 16:36:17 -03

  defu=root

  sudo true

  read    -p "User ($defu): " user
  read -s -p "Senha: " pass
  echo

   user=${user:-$defu}
   line=$(sudo grep ^$user: /etc/shadow | cut -d: -f2)
   salt=$(cut -d$ -f3 <<< $line)
  crypt=$(cut -d$ -f4 <<< $line)
  check=$(mkpasswd -m sha-512 $pass $salt 2>&- | cut -d$ -f4)

  echo -e "$user\n$salt\n$crypt\n$check"

# EOF
