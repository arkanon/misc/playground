#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, requests

s = requests.Session()
p = 'https://clm.com.br/ccm/'
u = [ 'authenticated/identity', 'authenticated/j_security_check', 'oslc/contexts/_moJeAH_FEei-sv3TdZXl1w/workitems/services.xml' ]
d = { 'j_username': os.environ['USER'], 'j_password': os.environ['p'] }

s.get(p+u[0])
s.post(p+u[1], data=d)
r = s.get(p+u[2])

print (r.text)
print (s.cookies)

# EOF
