
  unset arr1

  arr1=( a1 a2 a3 )

  arr1+=(b)
  arr1+=(c)
  arr1+=(d)

  echo ${arr1[@]}
  # a1 a2 a3 b c d

  echo ${!arr1[@]}
  # 0 1 2 3 4 5



  unset arr2

  declare -A arr2

  arr2=(
         [zero]=a1
           [um]=a2
         [dois]=a3
       )

  arr2[tres]+=b
  arr2[quatro]+=c
  arr2[cinco]+=d

  echo ${arr2[@]}
  # a3 b c a2 d a1

  echo ${!arr2[@]}
  # dois tres quatro um cinco zero



  [[ ${arr2[@]} =~ a        ]] && echo está || echo não está
  # está

  [[ ${arr2[@]} =~ '\ba\b'  ]] && echo está || echo não está
  # não está

  [[ ${arr2[@]} =~ '\ba1\b' ]] && echo está || echo não está
  # não está

  shopt -s compat31; [[ ${arr2[@]} =~ '\ba1\b' ]] && echo está || echo não está; shopt -u compat31
  # está

  shopt -s compat31; [[ ${arr2[@]} =~ '\ba\b'  ]] && echo está || echo não está; shopt -u compat31
  # não está

