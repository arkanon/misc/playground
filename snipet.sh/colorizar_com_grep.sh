
  colorize()
  {
    {
      #  30  black
      #  31  red
      #  32  green
      #  33  yelllow
      #  34  blue
      #  35  magenta
      #  36  cyan
      #  37  white
      cat \
      | column -ents$'\t' \
      | GREP_COLOR='1;37' grep --color=always -E      '.+ sshport .+|$' \
      | GREP_COLOR='1;31' grep --color=always -E         '.+ fail .+|$' \
      | GREP_COLOR='1;34' grep --color=always -E          '.+ off .+|$' \
      | GREP_COLOR='1;35' grep --color=always -E        '.+ block .+|$' \
      | GREP_COLOR='1;33' grep --color=always -E             '.+ out|$' \
      | GREP_COLOR='1;32' grep --color=always -E           '.+ ok .+|$' \
      | GREP_COLOR='1;30' grep --color=always -E '.+ 172\.[^2][^3].+|$'
      echo
    } | uniq
  }

