#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2023/05/05 (Fri) 06:15:02 -03
# 2023/05/04 (Thu) 12:21:28 -03
# 2023/05/04 (Thu) 00:41:44 -03
# 2023/05/03 (Wed) 09:21:57 -03

# TODO
#
# -- parâmetros por getopt[s]
# -- linha/coluna negativos
# -- centro de gravidade



  unset -v menu prompt largur marge margd linha coluna selection option
  unset -f app tquery textbox finish menu {t,fim,teste}{0,1}



# [execução]



  app()
  {

    local menu prompt largur marge margd linha coluna selection option

    menu=(
      'Opção 1'
      'Opção 2  🐢 👾'
      'Opcao 3'
      'Opcao Extra'
      'Fim'
    )

    # [parâmetros opcionais]

  # prompt=Opções: # def: vazio
  # largur=20      # def: largura da maior opção
  #  marge=' '     # string de margem à esquerda e à direita na barra de seleção
  #  margd=' '     # string de margem à esquerda e à direita na barra de seleção

  # canto superior esquerdo:
  #    linha: 0 .. $((  LINES-1)) , unset: centro da tela , empty: próxima linha
  #   coluna: 0 .. $((COLUMNS-1)) , unset: centro da tela , empty: 0

     linha=
    coluna=
  #  linha=2
  # coluna=4

    menu
    echo "[$selection] [$option]"

  }



# [biblioteca]



  # [posição]
  #
  # $ tquery 6n
  # 5;1R

  # [cursor]
  #
  # $ tput civis # invisível
  # $ tquery ?25\$p
  # ?25;2$y
  #
  # $ tput cnorm # visível
  # $ tquery ?25\$p
  # ?25;1$y

  # [tela]
  #
  # $ tput smcup # tela alternativa
  # $ tquery ?1049\$p
  # ?1049;1$y
  #
  # $ tput rmcup # tela normal
  # $ tquery ?1049\$p
  # ?1049;2$y
  tquery()
  {
    : ${1?Uso: $FUNCNAME <terminal capability ANSI code sequence (eg: 6n, ?25\\\$p)>}
    exec < /dev/tty
    local otty=$(stty -g)
    stty -icanon -echo min 0 time 5
    printf "\e[$1" > /dev/tty
    dd count=1 2>&- | cat -A | cut -d[ -f3
    stty $otty
  }



  textbox() # <titulo> <texto> [linha] [coluna]
  {

    ho='-'
    ve='|'
    se='+'
    sd='+'
    ie='+'
    id='+'

  # ho=$'\2500'
  # ve=$'\2502'
  # se=$'\250c'
  # sd=$'\2510'
  # ie=$'\2514'
  # id=$'\2518'

    # baseado em DesenhaCaixaComTexto, de Julio Neves
    mapfile <<< $2
    local i bordah altura=${#MAPFILE[*]} largur=$(wc -L <<< ${2//$'\e'\[[07]m/})
    [[ $1 ]] && hasp=1 || hasp=0

    [[ -v linha  ]] &&
    {
      [[ $linha  ]] ||
      {
        rc=0
        IFS=';R' read linha lx <<< $(tquery 6n)
        ((linha--))
      }
    } ||
    {
      rc=1
      linha=${3:-$(((LINES-altura-2-hasp)/2))}
    }

    [[ -v coluna ]] &&
    {
      [[ $coluna ]] ||
      coluna=0
    } ||
    coluna=${4:-$(((COLUMNS-largur-4  )/2))}

    printf -v bordah "%$((largur+2))s" # cria sequencia de $largur espaços
    bordah=${bordah// /$ho}            # gera borda horizontal
    ((rc)) && tput sc                  # salva a posição do cursor
  # tput civis                         # deixa o cursor invisível
    ((hasp)) && { tput cup $linha $coluna; echo "$1"; }
    tput cup $((linha+hasp)) $coluna
    printf "$se$bordah$sd"
    for  ((i=0; i<altura; i++))
    {
      tput cup $((linha+i+hasp+1)) $coluna
      echo -n $ve" ${MAPFILE[i]}"
      tput cup $((linha+i+hasp+1)) $((coluna + largur + 3))
      printf "$ve"
    }
    tput cup $((linha+i+hasp+1)) $coluna
    printf "$ie$bordah$id"
    ((rc)) && tput rc # restaura a posição do cursor
  # tput cnorm # deixa o cursor visível
  }



  finish()
  {
    tput cnorm cud $((f-i+1)) cub 2
    stty $otty
    set +o errtrace +o functrace
    echo $((f-i+1))
  }



  menu()
  {

    : ${menu:?}

    local opcao opcoes b d l i f REPLY c{1..5} key special sigs otty
    local -A keys

    keys=(
      [$(tput kcuu1 | tr O \[)]=up
      [$(tput kcud1 | tr O \[)]=down
      [$(tput khome | tr O \[)]=home
      [$(tput kend  | tr O \[)]=end
      [$'\x0a']=^\J # linefeed
      [$'\x0c']=^\L # formfeed
      [$'\x0d']=^\M # linefeed
      [$'\x12']=^\R # carriage return
      [$'\x18']=^\X # cancela a leitura e sai
      [$'\x1b']=^\[ # (escape) cancela a leitura e sai
    )

    special=$'\x0a\x0c\x0d\x12\x18\x1b'

    sigs=(
         # exit   #  0
           return #  0
         # hup    #  1
           int    #  2 ^C
       #   quit   #  3 ^\
       #   term   # 15
         # cont   # 18 ^Q
         # stop   # 19 ^S
       #   tstp   # 20 ^Z
         )

    if [[ ! $largur ]]
    then
      largur=0
      for opcao in "${menu[@]}"
      {
        d=$(wc -L <<< $opcao) # conta caracteres de 2 bytes
        ((d-1>largur)) && largur=$((d-1))
      }
    fi

  # opcoes=$(
  #   for opcao in "${menu[@]}"
  #   {
  #     b=$(wc -c <<< $opcao) # conta bytes
  #     c=${#opcao} # conta caracteres
  #     q=$(grep -o . <<< $opcao | cat -A | grep -E '(M-\^?[^M]){4}' | wc -l) # conta caracteres de 4 bytes
  #     l=$((largur+b-c-q))
  #     echo $largur + $b - $c - $q = $l >&2
  #     printf "\e[7m %-${l}s \e[0m\n" "$opcao"
  #   }
  # )

    opcoes=$(
      for opcao in "${menu[@]}"
      {
        b=$(wc -c <<< $opcao) # conta bytes
        d=$(wc -L <<< $opcao) # conta caracteres de 2 bytes
        l=$((largur+b-d))
        printf -- "$marge%-${l}s$margd\n" "$opcao"
      }
    )

    shopt -s extglob
  # shopt -s extdebug
  # set -o errtrace -o functrace # <http://superuser.com/a/257591> error propagation not working in bash
    trap "finish; trap - ${sigs[*]}; ${sigs[0]}" ${sigs[*]}
    tput civis
    otty=$(stty -g)
    stty -echo # -ixon

    textbox "$prompt" "$opcoes" # $linha $coluna

    ((rc)) && tput sc

    i=0
    f=$((${#menu[@]}-1))
    while true
    do

      tput cup $((linha+i+hasp+1)) $((coluna+2))
      opcao=${menu[i]}
      b=$(wc -c <<< $opcao) # conta bytes
      d=$(wc -L <<< $opcao) # conta caracteres de 2 bytes
      l=$((largur+b-d))
      printf -- "\e[7m$marge%-${l}s$margd\e[0m\n" "$opcao"

      read -sN1
      c1=$REPLY
      if [[ $c1 && $c1 == [$special] ]]
      then
        read -sN1 -t.01 c2
        read -sN1 -t.01 c3
        read -sN1 -t.01 c4
        read -sN1 -t.01 c5
        key=${keys[$c1$c2$c3$c4$c5]}
      else
        key=$c1
      fi

      tput cup $((linha+i+hasp+1)) $((coluna+2))

      case $key in
        up      ) ((i==0)) && i=$f  || ((i--)) ;;
        down|\  ) ((i==f)) && i=0   || ((i++)) ;;
        home    ) i=0  ;;
        end     ) i=$f ;;
        ^[JLMR] ) ((rc)) && tput rc || { tput cud1; selection=$i option=$opcao; ${sigs[0]}; } ;;
        ^[X[]   ) ((rc)) && tput rc || { tput cud1; ${sigs[0]}; } ;;
      esac

      printf -- "$marge%-${l}s$margd\n" "$opcao"

    done

  }



# echo $(stty)
# stty -a | grep --color -oE -- '-?[^ ]*echo[^ ]*'
# trap
# set -o | grep trace



  s(){ stty echo; tput cnorm; }



  unset -f fim1 t1

  fim1()
  {
    tput cud1 cnorm
    stty echo
    set +o errtrace +o functrace
    echo fim
  }

  t1()
  {
    local n REPLY sigs=( return int )
    set -o errtrace -o functrace
    trap "fim1; trap - ${sigs[*]}; ${sigs[0]}" ${sigs[*]}
  # trap "tput cud1 cnorm; stty echo; set +o errtrace +o functrace; echo fim; trap - ${sigs[*]}; ${sigs[0]}" ${sigs[*]}
    tput civis
    stty -echo
    while ((n++<5))
    do
      echo -n 'teste '
      read -N1
    # sleep 1
    done
    echo
  }



  fim2()
  {
    tput cud1 cnorm
    stty echo
    set +o errtrace +o functrace
    echo -e '\nfim'
  }

  t2()
  {

    local n key special sigs REPLY c{1..5}
    local -A keys

    keys=( [$(tput kcuu1 | tr O \[)]=up )
    special=$'\x1b'
    sigs=( return int )

    set -o errtrace -o functrace
    trap "fim2; trap - ${sigs[*]}; ${sigs[0]}" ${sigs[*]}
  # trap "tput cud1 cnorm; stty echo; set +o errtrace +o functrace; echo -e '\nfim'; trap - ${sigs[*]}; ${sigs[0]}" ${sigs[*]}
    tput civis
    stty -echo

    while ((n++<5))
    do

      read -N1 < /dev/tty
      c1=$REPLY

      if [[ $c1 && $c1 == [$special] ]]
      then
        read -N1 -t.01 c2 < /dev/tty
        read -N1 -t.01 c3 < /dev/tty
        key=${keys[$c1$c2$c3]}
      else
        key=$c1
      fi

      case $key in
        \ ) echo -n 'teste1 ';;
        up) echo -n 'teste2 ';;
      esac

    done

  }



# app
# menu
# echo "[$selection] [$option]"



# EOF
