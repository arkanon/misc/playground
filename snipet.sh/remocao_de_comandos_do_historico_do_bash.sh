
string=''; history -d $((HISTCMD-1))

match=$(history | grep -E "\b$string\b"); history -d $((HISTCMD-1))
echo "$match"; history -d $((HISTCMD-1))

linhas=$(grep -Eo "^ *[^ ]+ " <<< "$match"); history -d $((HISTCMD-1))
echo "$linhas"; history -d $((HISTCMD-1))

for l in $(tac <<< "$linhas"); do history -d $l; done; history -d $((HISTCMD-1))

