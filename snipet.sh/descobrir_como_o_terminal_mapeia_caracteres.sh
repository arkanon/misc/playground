
# <https://vi.stackexchange.com/a/2363>

1. check what chars are send by your terminal when you press ALT+J:

   cat
   # press ALT+J and see that the chars on the screen are ^[j

2. replace ^[ with \e (that is what is sent by terminal when press esc)

3. write it to .vimrc

   execute "set <M-j>=\ej"
   nnoremap <M-j> j

