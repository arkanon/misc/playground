#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2017/11/21 (Tue) 14:51:55 -02

#  xrandr
#  xte
#  xwininfo
#  xdotool
#  wmctrl



# # <http://superuser.com/a/603618>
# # get screens with resolutions and offsets
# data0=$(xrandr 2> /dev/null)
# data1=$(
#          grep -wP 'connected|^\*' <<< "$data0" \
#          | sed  -r 's/ x /x/; s/^([^ ]+) connected (primary )?([0-9][^+]+)\+([^+]+)\+.*/\1 \3 \4/' \
#          | sort -t\  -k3 -n
#        )
# echo -e "\n$data1\n"



# # get screen info
# if grep -Pq '^\*' <<< "$data1"
# then
#  scr=(default $(cut -d\  -f2 <<< $data1) 0)
# else
#  IFO=$IFS
#  IFS=$'\n'
#  _scr=($data1)
#  IFS=$IFO
# fi
# echo -e "${_scr[*]}\n"



  tout=3
  # get mouse position
  ( xte "sleep $tout" "mouseclick 1" & ( xwininfo; killall xte ) ) &> /dev/null
  . <(xdotool getmouselocation --shell)
  echo -e "X = $X\n"



# # figure out which screen contains the position
# for i in ${!_scr[*]}
# {
#   p=(${_scr[$i]})
#   echo -e "${p[*]} (${p[2]} + ${p[1]%x*})"
#   (( X >= ${p[2]} && X < ${p[2]} + ${p[1]%x*} )) && break
# }
# scr=(${_scr[$i]})
# echo -e "\nMOUSE SCREEN = ${scr[*]}"



  app='xev'
  nam='Event Tester'
  $app &>/dev/null &
  pid=$!
  wid=$(until wmctrl -l | grep "$nam"; do :; done | cut -d\  -f1)
  wmctrl -r "$nam" -b toggle,maximized_vert,maximized_horz
  geo=$(wmctrl -lG | grep $wid | sed -r 's/ +/\t/g' | cut -f5,6 | tr '\t' x)
  wmctrl -c "$nam"
  echo -e "USEFULL SIZE = $geo\n"



# EOF
