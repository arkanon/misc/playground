# multiplo_to_bytes.sh

# Arkanon <arkanon@lsd.org.br>
# 2023/04/16 (Sun) 01:19:08 -03
# 2023/04/12 (Wed) 08:26:06 -03
# 2023/04/11 (Tue) 17:24:58 -03



# @vcatafesta



sh_kbytestobytesawk()
{
  local str="$1"
  local bytes=0
  local lastletter=${str:0-1}
  str=${str^^}
  str=${str//,/.}
  bytes="${str//@(K|M|G|T)}"
  case ${lastletter^^} in
  K) bytes=$(awk -v v1=$bytes 'BEGIN {printf "%.2f\n", v1 * 1024 }');;
  M) bytes=$(awk -v v1=$bytes 'BEGIN {printf "%.2f\n", v1 * 1024 * 1024}');;
  G) bytes=$(awk -v v1=$bytes 'BEGIN {printf "%.2f\n", v1 * 1024 * 1024 * 1024}');;
  T) bytes=$(awk -v v1=$bytes 'BEGIN {printf "%.2f\n", v1 * 1024 * 1024 * 1024 * 1024}');;
  *) bytes=$1;;
  esac
  printf "${bytes%.*}\n"
}



# @nanognu



tama()
{
  ent=${1^^}
  mut=${ent//[^BKMGT]}
  num=${ent//[^[:digit:]]}
  ent=${ent//$mut}
  fra=${ent//[^,.]}
  fra=${fra:+${ent//*[,.]}}
  ent=0BKMGT;
  ent=${ent//$mut*};
  ((mut=${#ent}-1,sai=num*1024**mut))
  ((ent=${#sai}-${#fra}))
  echo ${sai:0:$ent}
}



# @b4g4t1n1



mult2B-v1-dbg()
{

  set "${*^^}"

  local bas val{,B} mul mov b
  local d=:
# local d='declare -p'

  [[ $1 =~ I ]] && bas=2 || bas=10 ; $d bas

  local -A mults2=(  [B]=0 [K]=10 [M]=20 [G]=30 [T]=40 [P]=50 )
  local -A mults10=( [B]=0 [K]=3  [M]=6  [G]=9  [T]=12 [P]=15 )
  local -n mults=mults$bas

  $d mults$bas mults

  : "${1%%[A-Z]*}"   ; val=${_// }     ; $d val
  : "${1##*[0-9,.]}" ; mul=${_//[ IB]} ; $d mul

  valB=$(( ${val//[,.]} * bas**${mults[${mul:-B}]} )) ; $d valB

  [[ $val =~ [,.] ]] &&
  { mov=${val#*[,.]}; $d mov; b=${valB:0:-${#mov}}.${valB: -${#mov}}; } ||
  b=$valB

  echo ${b%.*} B

}



mult2B-v1()
{
  set "${*^^}"
  local bas val{,B} mul mov
  [[ $1 =~ I ]] && bas=2 || bas=10
  local -A mults2=(  [B]=0 [K]=10 [M]=20 [G]=30 [T]=40 [P]=50 )
  local -A mults10=( [B]=0 [K]=3  [M]=6  [G]=9  [T]=12 [P]=15 )
  local -n mults=mults$bas
  : "${1%%[A-Z]*}"   ; val=${_// }
  : "${1##*[0-9,.]}" ; mul=${_//[ IB]}
  valB=$(( ${val//[,.]} * bas**${mults[${mul:-B}]} ))
  [[ $val =~ [,.] ]] &&
  { mov=${val#*[,.]}; echo ${valB:0:-${#mov}}; } ||
  echo $valB
}



mult2B-v1-lite()
{
  set "${*^^}"
  local val{,B} mul mov
  local -A mults=( [B]=0 [K]=10 [M]=20 [G]=30 [T]=40 [P]=50 )
  : "${1%%[A-Z]*}"   ; val=${_// }
  : "${1##*[0-9,.]}" ; mul=${_//[ IB]}
  valB=$(( ${val//[,.]} * 2**${mults[${mul:-B}]} ))
  [[ $val =~ [,.] ]] &&
  { mov=${val#*[,.]}; echo ${valB:0:-${#mov}}; } ||
  echo $valB
}



mult2B-v2()
{
  set "${*^^}"
  local bas val{,B} mul mov
  local -A mults=( [B]=0 [K]=1 [M]=2 [G]=3 [T]=4 [P]=5 )
  [[ $1 =~ I ]] && { bas=2 fat=10; } || { bas=10 fat=3; }
  : "${1%%[A-Z]*}"   ; val=${_// }
  : "${1##*[0-9,.]}" ; mul=${_//[ IB]}
  valB=$(( ${val//[,.]} * bas**(fat*${mults[${mul:-B}]}) ))
  [[ $val =~ [,.] ]] &&
  { mov=${val#*[,.]}; echo ${valB:0:-${#mov}}; } ||
  echo $valB
}



mult2B-v3()
{
  set "${*^^}"
  local bas fat val{,B} mul mov mults=BKMGTP
  [[ $1 =~ I ]] && { bas=2 fat=10; } || { bas=10 fat=3; }
  : "${1%%[A-Z]*}"   ; val=${_// }
  : "${1##*[0-9,.]}" ; mul=${_//[ IB]}
  : "${mults//${mul:-B}*}"
  valB=$(( ${val//[,.]} * bas**(fat*${#_}) ))
  [[ $val =~ [,.] ]] &&
  { mov=${val#*[,.]}; echo ${valB:0:-${#mov}}; } ||
  echo $valB
}



mult2B-v3-lite()
{
  set "${*^^}"
  local val{,B} mul mov mults=BKMGTP
  : "${1%%[A-Z]*}"   ; val=${_// }
  : "${1##*[0-9,.]}" ; mul=${_//[ IB]}
  : "${mults//${mul:-B}*}"
  valB=$(( ${val//[,.]} * 2**(10*${#_}) ))
  [[ $val =~ [,.] ]] &&
  { mov=${val#*[,.]}; echo ${valB:0:-${#mov}}; } ||
  echo $valB
}



# ------------------------------------------------------------------------------------------------ #



{
  sh_kbytestobytesawk 1.4G  # 1503238553
  tama                1.4G  # 1503238553
  mult2B-v1           1.4Gi # 1503238553
  mult2B-v1-lite      1.4G  # 1503238553
  mult2B-v2           1.4Gi # 1503238553
  mult2B-v3           1.4Gi # 1503238553
  mult2B-v3-lite      1.4G  # 1503238553
  sh_kbytestobytesawk 4G    # 4294967296
  tama                4G    # 4294967296
  mult2B-v1           4Gi   # 4294967296
  mult2B-v1-lite      4G    # 4294967296
  mult2B-v2           4Gi   # 4294967296
  mult2B-v3           4Gi   # 4294967296
  mult2B-v3-lite      4G    # 4294967296
}



v=v1-dbg
v=v1
v=v1-lite
v=v2
v=v3
v=v3-lite

{
  mult2B-$v 123     # 123
  mult2B-$v 123B    # 123
  mult2B-$v 123 B   # 123
  mult2B-$v 1.4 kB  # 1400
  mult2B-$v 1.4 kiB # 1433
  mult2B-$v 1,4 kB  # 1400
  mult2B-$v 1,4 kiB # 1433
  mult2B-$v 1,4 k   # 1400
  mult2B-$v 1,4 ki  # 1433
  mult2B-$v 1.4 G   # 1400000000
  mult2B-$v 1   GB  # 1000000000
  mult2B-$v 1.4 GB  # 1400000000
  mult2B-$v 1   GiB # 1073741824
  mult2B-$v 1.4 GiB # 1503238553
}



shopt -s expand_aliases # necessário habilitar a expansão de aliases dentro de scripts
source <(curl -sL bit.ly/benshmark-v5)
alias bm=benshmark-v5

s0(){ sh_kbytestobytesawk 1.4G  ; }
s1(){ tama                1.4G  ; }
s2(){ mult2B-v1           1.4Gi ; }
s3(){ mult2B-v1-lite      1.4G  ; }
s4(){ mult2B-v2           1.4Gi ; }
s5(){ mult2B-v3           1.4Gi ; }
s6(){ mult2B-v3-lite      1.4G  ; }

eval s{0..6}\;
# 1503238553
# 1503238553
# 1503238553
# 1503238553
# 1503238553
# 1503238553
# 1503238553

bm 2000 s{0..6}
# s0  00:05,693
# s1  00:00,078
# s2  00:00,221
# s3  00:00,159
# s4  00:00,168
# s5  00:00,117
# s6  00:00,098
#
# 1º  s1
# 2º  s6/s1   0,256 vezes mais lenta
# 3º  s5/s1   0,500 vezes mais lenta
# 4º  s3/s1   1,038 vezes mais lenta
# 5º  s4/s1   1,153 vezes mais lenta
# 6º  s2/s1   1,833 vezes mais lenta
# 7º  s0/s1  71,987 vezes mais lenta



# EOF
