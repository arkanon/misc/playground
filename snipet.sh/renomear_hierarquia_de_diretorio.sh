# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2023/06/16 (Fri) 20:07:06 -03
# 2023/06/16 (Fri) 15:58:47 -03



  # Sem recorrer à alguma ferramenta mágica, podemos recorrer apenas à mágica do bash 😉
  # -- para executar efetivamente, remova o echo que antecede o mv.
  # -- é importante a referência ao caminho começar com ./.

  spath='./te s te 1/teste  2/teste3/teste 4'

  (
    IFS=/
    apath=($spath)
    for ((i=${#apath[@]}-1; i>0; i--))
    {
      [[ ${apath[i]} =~ ' ' ]] &&
        echo mv "$spath" "${spath%/*}/${apath[i]// /_}"
      spath=${spath%/*}
    }
  )
  # mv ./te s te 1/teste  2/teste3/teste 4 ./te s te 1/teste  2/teste3/teste_4
  # mv ./te s te 1/teste  2 ./te s te 1/teste__2
  # mv ./te s te 1 ./te_s_te_1



  # [oneliner]

  sp='./te s te 1/teste  2/teste3/teste 4'

  (IFS=/;ap=($sp);for((i=${#ap[@]}-1;i>0;i--));{ [[ ${ap[i]} =~ ' ' ]]&&mv "$sp" "${sp%/*}/${ap[i]// /_}";sp=${sp%/*};})



  # [comparação]

  # (IFS=/;ap=($sp);for((i=${#ap[@]}-1;i>0;i--));{ [[ ${ap[i]} =~ ' ' ]]&&echo mv "$sp" "${sp%/*}/${ap[i]// /_}";sp=${sp%/*};})
  # find "te s te 1" -regex ".* .*" -type d|sort -r|awk 'BEGIN{FS=OFS="/"};{ori=$0;gsub(/ /,"",$NF);print"echo mv \042" ori "\042 \042" $0 "\042"}'|bash

  s1(){ local sp=$sp IFS=/ ap i;ap=($sp);for((i=${#ap[@]}-1;i>0;i--));{ [[ ${ap[i]} =~ ' ' ]]&&echo mv "$sp" "${sp%/*}/${ap[i]// /_}";sp=${sp%/*};};}
  s2(){ find "te s te 1" -regex ".* .*" -type d|sort -r|awk 'BEGIN{FS=OFS="/"};{ori=$0;gsub(/ /,"",$NF);print"echo mv \042" ori "\042 \042" $0 "\042"}'|bash;}

  sp='./te s te 1/teste  2/teste3/teste 4'

  mkdir -p "$sp"



#   |   . <(curl -ksL bit.ly/benshmark-v5)    |   . <(curl -ksL bit.ly/benshmark-v6)    |
#   |   alias bm=benshmark-v5                 |   alias bm=benshmark-v6                 |
#   |                                         |                                         |
#   |   bm 1000 s1 s2                         |   bm 1000 s1 s2                         |
#   |   s1  00:00,100                         |   s1  00:00,106                         |
#   |   s2  00:02,364                         |   s2  00:02,990                         |
#   |                                         |                                         |
#   |   1º  s1                                |   1º  s1                                |
#   |   2º  s2/s1   22,640 vezes mais lenta   |   2º  s2/s1   27,207 vezes mais lenta   |
#   |                                         |                                         |
#   |   bm 2000 s1 s2                         |   bm 2000 s1 s2                         |
#   |   s1  00:00,167                         |   s1  00:00,170                         |
#   |   s2  00:04,910                         |   s2  00:11,133                         |
#   |                                         |                                         |
#   |   1º  s1                                |   1º  s1                                |
#   |   2º  s2/s1   28,401 vezes mais lenta   |   2º  s2/s1   64,488 vezes mais lenta   |
#   |                                         |                                         |
#   |   bm 5000 s1 s2                         |   bm 5000 s1 s2                         |
#   |   s1  00:00,385                         |   s1  00:00,372                         |
#   |   s2  00:18,609                         |   s2  00:21,043                         |
#   |                                         |                                         |
#   |   1º  s1                                |   1º  s1                                |
#   |   2º  s2/s1   47,335 vezes mais lenta   |   2º  s2/s1   55,567 vezes mais lenta   |
#   |                                         |                                         |
#   |   $ bm 20000 s1 s2                      |   $ bm 20000 s1 s2                      |
#   |   s1  00:01,337                         |   s1  00:01,413                         |
#   |   s2  02:35,935                         |   s2  03:46,619                         |
#   |                                         |                                         |
#   |   1º  s1                                |   1º  s1                                |
#   |   2º  s2/s1  115,630 vezes mais lenta   |   2º  s2/s1  159,381 vezes mais lenta   |



# EOF
