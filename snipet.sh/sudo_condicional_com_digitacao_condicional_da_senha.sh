
  if (( UID != 0 ))
  then
    echo -e "\nEntering as root..."
    [[ -e $SUDO_S ]] && { cat $SUDO_S | sudo -S true &> /dev/null || echo "Wrong password in '$SUDO_S'"; }
    LC_ALL=C sudo true
    sudo $0 "$@"
    exit
  fi

