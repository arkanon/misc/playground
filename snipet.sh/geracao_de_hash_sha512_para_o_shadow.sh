
  uname=arkanon

  read -sp "Password for $uname: " upass; echo

  usalt=$(head /dev/urandom | tr -dc './[:alnum:]' | cut -c-8) # gera um salt de 8 caracteres alfanuméricos e "./"
  uhash=$(mkpasswd -m sha-512 $upass $usalt)

  sed -ri "/^$uname:/s/^($uname:)[^:]*(:.*)/\1$uhash\2/" /etc/shadow

