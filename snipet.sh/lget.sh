#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2023/06/19 (Mon) 18:32:44 -03

     url=${1:?URL no formato <protocolo>://[<user>:[senha]]@<host>[:<porta>]/<caminho>/<arquivo>}
  blocks=${2:-5}



{
  url=ftp://softplan:s:en/h@a@10.6.48.232:2121/app/SIDER\$06_Full_20230610.bak
  [[ $url =~ ^(([^:]+)://)?(([^:]+)?(:(.+)?@)?)([^:/]+)(:([0-9]+))?(/.*) ]]
  declare -p BASH_REMATCH
  echo -e "protocolo=${BASH_REMATCH[2]}\nuser=${BASH_REMATCH[3]}\nsenha=${BASH_REMATCH[5]}\nhost=${BASH_REMATCH[6]}\nporta=${BASH_REMATCH[8]}\ncaminho=${BASH_REMATCH[9]}\n"
}



  esp=( : / @ )

  declare -A ports=(
    [ftp]=21
    [http]=80
    [https]=443
  )



  proto=${url%%:*}



  : ${url#*:}
  : ${_%@*}
  [[ $_ =~ :$ ]] && { read -sp 'Senha: ' flat_pass; echo; } || flat_pass=${_#*:}
  code_pass=$flat_pass
  for chr in ${esp[*]}
  {
    printf -v hex %X \'$chr
    code_pass=${code_pass//$chr/%$hex}
  }
  url=${url/$flat_pass/$code_pass}



  : ${url##*@}
  sock=${_%%/*}
   IFS=: read host port <<< $sock
#  : ${port:=${ports[$proto]}}



  echo [$flat_pass] [$code_pass] [$host] [$port] [$url]
  exit


  lftp="
    set ssl:verify-certificate false
    set net:timeout 5
    set net:max-retries 1
    pget -n $blocks -c $url
    exit
  "



  n=1
  while ! lftp -e "$lftp"
  do
    echo "Testando conexão..."
    time while ! telnet $host $port <<< '' |& grep -q Connected
    do
      sleep 1
    done
    echo "Tentativa #$((++n))"
  done



# EOF
