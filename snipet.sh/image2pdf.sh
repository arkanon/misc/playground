#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2025/02/10 (Mon) 12:58:48 -03

# <http://leimao.github.io/blog/JPEG-Image-to-PDF-Ghostscript>

args=(

  -dBATCH
  -dQUIET
  -dNOCACHE
  -dNOPAUSE
  -dNOSAFER

  -sDEVICE=pdfwrite
  -sPAPERSIZE=a4         # a4 letter
  -dPDFSETTINGS=/printer # default screen:72 ebook:150 printer:300 prepress:300

  -c '(IMG-0001.JPG) viewJPEG showpage'
     '(IMG-0002.JPG) viewJPEG showpage'

  -sOutputFile=output.pdf

  viewjpeg.ps

)

gs "${args[@]}"

# EOF
