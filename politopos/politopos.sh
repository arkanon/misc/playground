#/bin/bash

# politopos.sh

# Arkanon <arkanon@lsd.org.br>
# 2024/01/19 (Fri) 14:11:09 -03
# 2021/08/10 (Tue) 15:59:48 -03
# 2020/09/04 (Fri) 21:00:16 -03
# 2017/09/04 (Mon) 23:19:00 -03

# Resultado: <http://tiny.cc/cyqhuz> (facebook)

   out=./out
   gif=https://i.giphy.com/media/KDbWf1b1dc1Ko6WgbV/source.gif
  name=politopos
  text="Numa escala hiper-platônica de polítopos convexos regulares quadri dimensionais, como você está se sentindo hoje? Pentácoro, tesserato, hexadecácoro, icositetrácoro, hecatonicosácoro - ou hexacosícoro?"

  mkdir -p $out

  # instala os pacotes dos comandos necessários
  pkg=(
        imagemagick               # animate convert
      # gifsicle
        ffmpeg
        mbrola-br{1..4} espeak-ng # espeak
        sox                       # play
        mpg123
        mplayer
      )
  sudo apt install ${pkg[*]}

  # baixa o gif animado e executa apenas um laço do loop
  wget $gif -O $out/$name.gif
# animate -loop 1 $out/$name.gif

# # desativa o loop infinito do gif
# # <http://davidwalsh.name/prevent-gif-loop>
# gifsicle --no-loopcount $out/$name.gif > $out/$name-1x.gif

  # converte o gif em mp4
  # <http://clas.kitasato-u.ac.jp/~fujiwara/Mathematica/GIFtoMP4.html>
  ffmpeg -r 10 -i $out/$name.gif -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" $out/$name-noaudio.mp4

  # sintetiza o texto em um arquivo de áudio no formato wav e executa-o
  espeak -v mb-br1 "$text" -s 140 -b 1 -p 10 -a 50 -w $out/$name.wav
# play $out/$name.wav

  # converte o wav em mp3 e executa-o
  # <http://lonewolfonline.net/convert-wav-mp3-linux>
  ffmpeg -i $out/$name.wav -acodec mp3 $out/$name.mp3
# mpg123 $out/$name.mp3

# # gera um frame em preto e repete-o tantas vezes quantas necessárias para entrelaçá-lo em um vídeo apenas de áudio
#
# frame=frame.png
# audio=audio.wav
# video-video.mp4
#
# convert -size 200x200 xc:black $frame
# time ffmpeg -loop 1 -i $frame -i $audio -c:v libx264 -tune stillimage -c:a aac -b:a 192k -pix_fmt yuv420p -shortest $video

  # entrelaça áudio e vídeo, repetindo o vídeo tantas vezes quantas necessárias para que fique com a duração do áudio e executa-o
  # <http://ffmpeg.org/pipermail/ffmpeg-user/2015-April/026118.html>
  yes "file $name-noaudio.mp4" | head -n100 > $out/$name.txt
  ffmpeg -f concat -safe 0 -i $out/$name.txt -i $out/$name.mp3 -map 0 -map 1 -c copy -shortest $out/$name.mp4
  mplayer $out/$name.mp4

# EOF
