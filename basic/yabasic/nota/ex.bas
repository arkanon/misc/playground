#!/usr/bin/env yabasic

rem ex.bas

rem Arkanon <arkanon@lsd.org.br>
rem 2022/12/18 (Sun) 22:03:46 -03

rem https://rosettacode.org/wiki/Musical_scale#Yabasic
rem Galileo, 03/2022

sample_rate = 44100
duration    = 8
dataLength  = sample_rate * duration
hdrSize     = 44
fileLen     = dataLength + hdrSize - 8

data 261.6, 293.6, 329.6, 349.2, 392.0, 440.0, 493.9, 523.3

sub int_to_bytes(dato, long)
    local dato$, esp, esp$, i
    esp$  = "00000000"
    dato$ = hex$(dato)
    esp   = long * 2
    dato$ = right$(esp$ + dato$, esp)
    for i = esp - 1 to 1 step -2
        poke #fn, dec(mid$(dato$, i, 2))
    next
end sub

fn = open("notesyab.wav", "wb")

print #fn, "RIFF";
int_to_bytes(fileLen, 4)
print #fn, "WAVEfmt ";
int_to_bytes(16, 4)          : rem length of format data (= 16)
int_to_bytes(1, 2)           : rem type of format (= 1 (PCM))
int_to_bytes(1, 2)           : rem number of channels (= 1)
int_to_bytes(sample_rate, 4) : rem sample rate
int_to_bytes(sample_rate, 4) : rem sample rate * bps(8) * channels(1) / 8 (= sample rate)
int_to_bytes(1,2)            : rem bps(8) * channels(1) / 8  (= 1)
int_to_bytes(8,2)            : rem bits per sample (bps) (= 8)
print #fn, "data";
int_to_bytes(dataLength, 4)  : rem size of data section

for j = 1 to duration
    read f
    omega = 2 * PI * f
    for i = 0 to dataLength/duration-1
        y = 32 * sin(omega * i / sample_rate)
        byte = and(y, 255)
        poke #fn, byte
    next
next

close(fn)

if peek$("os") = "windows" then
   system("notesyab.wav")
else : rem Linux
   system("aplay notesyab.wav")
endif

rem EOF
