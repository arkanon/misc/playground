#!/usr/bin/env yabasic

rem fern.bas

rem Barnsley Fern
rem http://wikipedia.org/wiki/Barnsley_fern

rem Arkanon <arkanon@lsd.org.br>
rem 2023/02/15 (Wed) 14:53:09 -03
rem 2023/02/15 (Wed) 12:29:49 -03
rem
rem http://fb.com/groups/2057165187928233/?multi_permalinks=3314880195490053
rem http://2484.de/yabasic/yabasic.htm#top_graphics
rem http://dbfinteractive.com/forum/?topic=2187.0
rem
rem http://blog.adafruit.com/2019/03/29/raster-crt-typography-the-glyphs-drawn-by-dec-vt100-and-vt220-terminals-typeography-dec-vintagecomputing-fonts
rem http://github.com/svofski/glasstty



rem if @main then
rem
rem   mode -1,-1,0
rem
rem   x = 0.0
rem   y = 0.0
rem
rem   nextx = 0.0
rem   nexty = 0.0
rem
rem   origin screenwidth()/2,0
rem   setfg 144,238,144
rem   setalpha 24
rem
rem   t = timer()
rem   s = min(screenwidth(),screeheight())/10.0
rem
rem   for i = 1 to 1000000
rem     a = rndf(1.0)
rem     if a <  0.01               : nextx =  0.00 * x + 0.00 * y : nexty =  0.00 * x + 0.16 * y + 0.00
rem     if a >= 0.01 and a <= 0.08 : nextx =  0.20 * x - 0.26 * y : nexty =  0.23 * x + 0.22 * y + 1.60
rem     if a >= 0.08 and a <= 0.15 : nextx = -0.15 * x + 0.28 * y : nexty =  0.26 * x + 0.24 * y + 0.44
rem     if a >  0.15               : nextx =  0.85 * x + 0.04 * y : nexty = -0.04 * x + 0.85 * y + 1.60
rem     x = nextx
rem     y = nexty
rem     plot x*s,y*s
rem   next
rem
rem   'print sys timer()-t
rem   repeat until waitKey(0) or appCloseRequested()
rem
rem end if



    screenWidth  =  550
    screenHeight = 1000

    open window screenWidth,screenHeight
    window origin "cb"
    backcolor 0,0,0
    clear window

rem setalpha 24

    x  = 0
    y  = 0

    nx = 0
    ny = 0

    s  = 99

    m  = 10^6

    for i = 1 to m
      a = ran()
      if ( a <  0.01               ) nx =  0.00 * x + 0.00 * y : ny =  0.00 * x + 0.16 * y + 0.00
      if ( a >= 0.01 and a <= 0.08 ) nx =  0.20 * x - 0.26 * y : ny =  0.23 * x + 0.22 * y + 1.60
      if ( a >= 0.08 and a <= 0.15 ) nx = -0.15 * x + 0.28 * y : ny =  0.26 * x + 0.24 * y + 0.44
      if ( a >  0.15               ) nx =  0.85 * x + 0.04 * y : ny = -0.04 * x + 0.85 * y + 1.60
      x = nx
      y = ny
  rem color 144*ran()          , 238*ran()          , 144*ran()          : rem captura 01
  rem color 144*(0.5+ran(0.5)) , 238*(0.5+ran(0.5)) , 144*(0.5+ran(0.5)) : rem captura 02
  rem color 144                , 238                , 144                : rem captura 03
  rem color 0                  , 255*i/m            , 64                 : rem captura 04
      color 0                  , 255*i/m            , 0                  : rem captura 05
      dot x*s,y*s
    next



rem EOF
