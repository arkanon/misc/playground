#!/usr/bin/env yabasic

REM 2023/02/27 (Mon) 01:35:10 -03



screenWidth  = 1000
screenHeight = 1000

x1    = -400
y1    =    0
x2    =  400
y2    =    0
ordem =    5



SUB koch(x1, y1, x2, y2, ordem)

  IF ordem = 0 THEN

    LINE x1, y1, x2, y2

  ELSE

        dx = (x2 - x1) / 3
        dy = (y2 - y1) / 3
        x_1 = x1 + dx
        y_1 = y1 + dy
        x_2 = x2 - dx
        y_2 = y2 - dy
        x_3 = x_1 + (cos(pi/3) * (x_2 - x_1) - sin(pi/3) * (y_2 - y_1))
        y_3 = y_1 + (sin(pi/3) * (x_2 - x_1) + cos(pi/3) * (y_2 - y_1))

        koch( x1  , y1  , x_1 , y_1 , ordem - 1 )
        koch( x_1 , y_1 , x_3 , y_3 , ordem - 1 )
        koch( x_3 , y_3 , x_2 , y_2 , ordem - 1 )
        koch( x2  , y2  , x_2 , y_2 , ordem - 1 )

  END IF

END SUB



open window screenWidth,screenHeight
window origin "cc"
backcolor 0,0,0
color 0,255,0
clear window

koch(x1, y1, x2, y2, ordem)



REM EOF
