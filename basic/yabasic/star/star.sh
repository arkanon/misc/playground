#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2022/12/18 (Sun) 17:14:23 -03

# <http://logiker.com/Vintage-Computing-Christmas-Challenge-2022>

# $ star n=4
# ⬜⬜⬜⬜⬛⬜⬜⬜⬜⬜⬜⬜⬛⬜⬜⬜⬜
# ⬜⬜⬜⬜⬛⬛⬜⬜⬜⬜⬜⬛⬛⬜⬜⬜⬜
# ⬜⬜⬜⬜⬛⬛⬛⬜⬜⬜⬛⬛⬛⬜⬜⬜⬜
# ⬜⬜⬜⬜⬛⬛⬛⬛⬜⬛⬛⬛⬛⬜⬜⬜⬜
# ⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛
# ⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜
# ⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜
# ⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜⬜
# ⬜⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜⬜⬜
# ⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜⬜
# ⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜
# ⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜
# ⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛
# ⬜⬜⬜⬜⬛⬛⬛⬛⬜⬛⬛⬛⬛⬜⬜⬜⬜
# ⬜⬜⬜⬜⬛⬛⬛⬜⬜⬜⬛⬛⬛⬜⬜⬜⬜
# ⬜⬜⬜⬜⬛⬛⬜⬜⬜⬜⬜⬛⬛⬜⬜⬜⬜
# ⬜⬜⬜⬜⬛⬜⬜⬜⬜⬜⬜⬜⬛⬜⬜⬜⬜



  # <http://fb.com/groups/2057165187928233/posts/3266037697040970>
  star1()
  {

    local off on n
    eval $@ 2>/dev/null || set -- -h
    grep -q -- -h <<< $@ && { type $FUNCNAME | grep [p]arâmetros | sed -r "s/^( +([^ ]+)){3}/Uso: $FUNCNAME/; s/[$;]//g" >&2; return 1; }
    : parâmetros opcionais ${off:=$'\xe2\xac\x9c'} ${on:=$'\xe2\xac\x9b'} ${n:=LINES/4}
    local B=( "$off" "$on" ) i j x y

    for (( i=0; i<=4*n; i++ ))
    {
      for (( j=0; j<=4*n; j++ ))
      {
        x=$(( j-2*n ))
        y=$(( i-2*n ))
        echo -n "${B[
                      x >= -n && y >= -n+x && y <= +n ||
                      x <= +n && y >= -n-x && y <= +n ||
                      x >= -n && y <= +n-x && y >= -n ||
                      x <= +n && y <= +n+x && y >= -n
                    ]}"
      }
      echo
    }

  }



  # <http://fb.com/groups/2057165187928233/posts/3264227027222037>
  star2()
  {

    local off on n
    eval $@ 2>/dev/null || set -- -h
    grep -q -- -h <<< $@ && { type $FUNCNAME | grep [p]arâmetros | sed -r "s/^( +([^ ]+)){3}/Uso: $FUNCNAME/; s/[$;]//g" >&2; return 1; }
    : parâmetros opcionais ${off:=$'\xe2\xac\x9c'} ${on:=$'\xe2\xac\x9b'} ${n:=LINES/4}
    local B=( "$on" "$off" ) i j x y

    for (( y=-2*n; y<=2*n; y++ ))
    {
      for (( x=-2*n; x<=2*n; x++ ))
      {
        j=$y && ((j<0)) && ((j=-j))
        i=$x && ((i<0)) && ((i=-i))
        echo -n "${B[ (i>n)*(j>n)+(i<j-n)+(j<i-n) > 0 ]}"
      }
      echo
    }

  }



  star3()
  {
    bc << EOT
n=4
define a(n){ if (n<0) return -n else return n }
for ( y=-2*n; y<=2*n; y++ )
{
  for ( x=-2*n; x<=2*n; x++ )
  {
    j=a(y)
    i=a(x)
    if ( (i>n)*(j>n)+(i<j-n)+(j<i-n) > 0 ) print "⬜" else print "⬛"
  }
  print "\n"
}
EOT
  }



  star4()
  {
    local x y B=(⬛ ⬜)
    for y in {8..0} {1..8}
    {
      for x in {8..0} {1..8}
      {
        echo -n ${B[$(((x>4)*(y>4)+(x<y-4)+(y<x-4)>0))]}
      }
      echo
    }
  }



  star5()
  {

    B=(⬛ ⬜)
    B=('*' ' ')
    for y in {8..0} {1..8}; { for x in {8..0} {1..8}; { echo -n "${B[(x>4)*(y>4)+(x<y-4)+(x>y+4)>0]}"; }; echo; }

  }



# EOF
