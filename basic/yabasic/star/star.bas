#!/usr/bin/env yabasic

rem star.bas

rem Arkanon <arkanon@lsd.org.br>
rem 2022/12/18 (Sun) 22:03:46 -03

rem $ ./star.bas 4
rem ⬜⬜⬜⬜⬛⬜⬜⬜⬜⬜⬜⬜⬛⬜⬜⬜⬜
rem ⬜⬜⬜⬜⬛⬛⬜⬜⬜⬜⬜⬛⬛⬜⬜⬜⬜
rem ⬜⬜⬜⬜⬛⬛⬛⬜⬜⬜⬛⬛⬛⬜⬜⬜⬜
rem ⬜⬜⬜⬜⬛⬛⬛⬛⬜⬛⬛⬛⬛⬜⬜⬜⬜
rem ⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛
rem ⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜
rem ⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜
rem ⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜⬜
rem ⬜⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜⬜⬜
rem ⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜⬜
rem ⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜
rem ⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜
rem ⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛
rem ⬜⬜⬜⬜⬛⬛⬛⬛⬜⬛⬛⬛⬛⬜⬜⬜⬜
rem ⬜⬜⬜⬜⬛⬛⬛⬜⬜⬜⬛⬛⬛⬜⬜⬜⬜
rem ⬜⬜⬜⬜⬛⬛⬜⬜⬜⬜⬜⬛⬛⬜⬜⬜⬜
rem ⬜⬜⬜⬜⬛⬜⬜⬜⬜⬜⬜⬜⬛⬜⬜⬜⬜

n = val(peek$("argument"))
if n=0 n=4

for y = -2*n to 2*n
  for x = -2*n to 2*n
    j=abs(y)
    i=abs(x)
    b=(i>n)*(j>n)+(i<j-n)+(j<i-n)>0
rem print chr$(32+10*b);
    print chr$(226),chr$(172),chr$(155+b);
  next x
  print
next y

rem EOF
