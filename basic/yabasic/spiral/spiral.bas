#!/usr/bin/env yabasic

rem spiral.bas

rem Arkanon <arkanon@lsd.org.br>
rem 2022/10/06 (Thu) 02:42:21 -03
rem 2022/10/05 (Wed) 22:17:57 -03
rem
rem http://fb.com/groups/2057165187928233/posts/3191227651188642/?comment_id=3203230953321645
rem http://2484.de/yabasic/yabasic.htm#top_graphics
rem http://dbfinteractive.com/forum/?topic=2187.0
rem
rem http://blog.adafruit.com/2019/03/29/raster-crt-typography-the-glyphs-drawn-by-dec-vt100-and-vt220-terminals-typeography-dec-vintagecomputing-fonts
rem http://github.com/svofski/glasstty



rem cls
rem randomize (timer)
rem x = 320
rem y = 240
rem r = 100
rem c = 1
rem screen 12
rem pset (x, y - r), 0
rem for a = 0 to 72 step .01
rem     line -(x + r * sin(a), y - r * cos(a)), c
rem     r = r - .01
rem     c = rnd(1) * 15
rem next



w = 500
s = .01
n = 1

open window w,w
backcolor 0,0,0
clear window

d = w/2 - 10
for a = 0 to 72/n*pi step s
    x = w/2 + d*sin(a)
    y = w/2 - d*cos(a)
    r = mod(x,256)
    g = mod(y,256)
    b = mod(d,256)
    color r,g,b
    line  x,y
    d = d - n*s
    sleep .0001
next



rem EOF
