#!/usr/bin/env yabasic

rem hat.bas

rem Arkanon <arkanon@lsd.org.br>
rem 2022/10/16 (Sun) 23:17:33 -03
rem 2022/10/17 (Mon) 05:27:53 -03
rem
rem http://fb.com/groups/2057165187928233/posts/3212998459011561
rem
rem http://codepen.io/Mamboleoo/pen/eEvJKo
rem http://wikipedia.org/wiki/Perlin_noise
rem http://rtouti.github.io/graphics/perlin-noise-algorithm
rem http://github.com/daniilsjb/perlin-noise/blob/master/db_perlin.hpp
rem http://github.com/ZXDunny/SpecBAS
rem http://github.com/ZXDunny/SpecBAS-Demos/blob/master/Graphics/flux
rem
rem http://2484.de/yabasic/yabasic.htm#top_graphics
rem http://dbfinteractive.com/forum/?topic=2187.0
rem
rem http://blog.adafruit.com/2019/03/29/raster-crt-typography-the-glyphs-drawn-by-dec-vt100-and-vt220-terminals-typeography-dec-vintagecomputing-fonts
rem http://github.com/svofski/glasstty



rem    zxascii
rem    auto -1
rem    prog /demos/graphics/flux
rem    changed false
rem 10 n=10:
rem    dim l(n,7):
rem    for i=1 to n:
rem       l(i,1),l(i,2)=rnd,l(i,3),l(i,4)=rnd*0.005,
rem       l(i,5)=0,l(i,6)=rnd+1,l(i,7)=rnd*.125:
rem    next i:
rem    mj=0
rem 20 paper 0:
rem    for i=1 to 255: palette hsv i,i*1.41,255,255: next i:
rem    cls
rem 30 screen lock:
rem    do:
rem       cls:
rem       mj2=min(mj+1,5):
rem       for i=1 to n:
rem          plot 0,240:
rem          for x=0 to scrw-1:
rem             plot ink (frac(l(i,7)+(x*.0007))*254)+1;x,240+(sin(pi/scrw*x)*((noise((x-l(i,5))/50,l(i,1),l(i,2))-.5)*(100+(mj*20)))):
rem          next x:
rem          l(i,1)+=l(i,3)*mj2,l(i,2)+=l(i,4)*mj2,
rem          l(i,5)+=l(i,6)*mj,l(i,7)+=.001:
rem       next i:
rem       mj+=iif(mousebtn<>0,.05,-.03),
rem       mj=min(5,max(0,mj)):
rem       wait screen:
rem    loop



scrw = 1000
scrh = 500

open window scrw,scrh
backcolor 0,0,0

n=3

dim l(n,7)
for i=1 to n
    l(i,1) = ran()
    l(i,2) = ran()
    l(i,3) = ran()*0.005
    l(i,4) = ran()*0.005
    l(i,5) = 0
    l(i,6) = ran()+1
    l(i,7) = ran()*0.125
next i

mj=0
do

    clear window

    mj2 = min(mj+1,5)

    for i=1 to n
        dot 0 , 240
        for x=0 to scrw-1
            xi = (x-l(i,5))/50
            yi = l(i,1)
            zi = l(i,2)
            cmd$  = "perlin3d(" + str$(xi) + "," + str$(yi) + "," + str$(zi) + ")"
        rem print cmd$
            cmd$  = "bc -l perlin3d.bc <<< '" + cmd$ + "'"
            cmd$  = "bash -c \"" + cmd$ + "\""
            noise = val( system$( cmd$ ) )
        rem print noise
            r = mod(int(abs(100*xi)),256)
            g = mod(int(abs(100*yi)),256)
            b = mod(int(abs(100*zi)),256)
            color r,g,b
            dot x , 240 + sin(pi/scrw*x)*(noise-.5)*(100+mj*20)
        next x
        l(i,1) = l(i,1) + l(i,3)*mj2
        l(i,2) = l(i,2) + l(i,4)*mj2
        l(i,5) = l(i,5) + l(i,6)*mj
        l(i,7) = l(i,7) + .001
    next i

rem mj = mj + iif( mousebtn<>0 , .05 , -.03 )
    mj = min( 5 , max(0,mj) )

loop



rem EOF
