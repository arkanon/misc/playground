#!/usr/bin/env yabasic

rem hat.bas

rem Archimedes Spiral
rem http://archive.org/details/analog-computing-magazine-07/page/n61/mode/2up
rem http://goto10.substack.com/p/archimedes-spiral

rem Arkanon <arkanon@lsd.org.br>
rem 2022/10/06 (Thu) 02:38:50 -03
rem 2022/10/05 (Wed) 14:48:24 -03
rem
rem http://fb.com/arkanon/posts/pfbid028t51EF4SBD8W44XPfWe7sMpKL22JTveK7huG9YCpDGb46r19ucGzTK3eE3N1F42Zl
rem http://fb.com/groups/cidadedosclassicos/posts/5742600962464226/?comment_id=5743921452332177
rem http://2484.de/yabasic/yabasic.htm#top_graphics
rem http://dbfinteractive.com/forum/?topic=2187.0
rem
rem http://blog.adafruit.com/2019/03/29/raster-crt-typography-the-glyphs-drawn-by-dec-vt100-and-vt220-terminals-typeography-dec-vintagecomputing-fonts
rem http://github.com/svofski/glasstty



rem cls gfx
rem color 0,0,0
rem rect 0,0,ScreenWidth,ScreenHeight
rem
rem xs=1.5
rem xp=xs*96
rem xr=xs*pi
rem xf=xr/xp
rem ys=56
rem
rem rem color 0,255,0
rem
rem for zi=-64 to 64
rem   rem zt=zi*2.25
rem   zt=zi*xp/64
rem   zs=zt*zt
rem   xl=int(sqr(xp*xp-zs)*0.5)
rem   for xi=-xl to xl
rem     xt=sqr(xi*xi+zs)*xf
rem     yy=(sin(xt)+sin(xt*3)*0.4)*ys
rem     x1=xi+zi+ScreenWidth/2
rem     y1=ScreenHeight/2-yy+zi
rem     color 0,255,0
rem     point x1,y1,0
rem     color 0,0,0
rem     line x1,y1+1,x1,ScreenHeight/2+205
rem   next xi
rem next zi
rem
rem for i=1 to 12
rem   print
rem next i
rem
rem print "Done!"



ScreenWidth  = 400
ScreenHeight = 300

wireframe    = false

open window ScreenWidth,ScreenHeight
backcolor 0,0,0
clear window

xs = 1.5
xp = xs*96
xr = xs*pi
xf = xr/xp
ys = 56

for zi = -64 to 64
    zt = zi*xp/64
    zs = zt*zt
    xl = int(sqrt(xp*xp-zs) + 0.5)
    for xi = -xl to xl
        xt = sqrt(xi*xi+zs)*xf
        yy = (sin(xt) + sin(xt*3)*0.4)*ys
        x1 = xi + zi + ScreenWidth/2
        y1 = ScreenHeight/2 - yy + zi
        color 0,255,0
        dot x1,y1
        if (not wireframe) then
           color 0,0,0
           line x1, y1+1, x1, ScreenHeight/2 + 205
        endif
    next xi
    sleep .05
next zi



rem EOF
