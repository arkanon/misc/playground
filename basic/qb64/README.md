#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2022/12/24 (Sat) 03:13:30 -03
# 2022/12/24 (Sat) 01:44:58 -03

# http://qb64.com
# http://github.com/DualBrain/QB64
# http://github.com/QB64Official/qb64



  user=QB64Official
  name=qb64
  repo=https://github.com/$user/$name

  url=$(curl -s https://api.github.com/repos/$user/$name/releases | jq -r '.[0] | .assets[] | select(.name|contains("lnx")) | .browser_download_url')
  echo $url

  curl -sLjO $url
  tar zxf ${url##*/}
  rm ${url##*/}

  IFS=_. read app x date hash plat x <<< ${url##*/}
  dir=${app}_${date}_${hash}_${plat}
  mv qb64*/ $dir
# cd $dir
  export PATH=$PATH:$PWD/$dir



  qb64
  l $dir/internal/config.ini



  ./qb64 --help
  # QB64 Compiler V2.1
  #
  # Usage: qb64 [switches] <file>
  #
  # Options:
  #   <file>                  Source file to load
  #   -c                      Compile instead of edit
  #   -o <output file>        Write output executable to <output file>
  #   -x                      Compile instead of edit and output the result to the console
  #   -w                      Show warnings
  #   -q                      Quiet mode (does not inhibit warnings or errors)
  #   -m                      Do not colorize compiler output (monochrome mode)
  #   -e                      Enable OPTION _EXPLICIT, making variable declaration mandatory (per-compilation; doesn't affect the source file or global settings)
  #   -s[:switch=true/false]  View/edit compiler settings
  #   -l:<line number>        Start the IDE at the specified line number
  #   -p                      Purge all pre-compiled content first
  #   -z                      Generate C code without compiling to executable



  qb64 -s
  # QB64 Compiler V2.1
  # debuginfo     = false
  # exewithsource = false



  qb64 -x star.bas
  # QB64 Compiler V2.1
  #
  # Beginning C++ output from QB64 code...
  # [..................................................] 100%
  #
  # Compiling C++ code into executable...
  # ERROR: C++ compilation failed.
  # Check ./internal/temp/compilelog.txt for details.

  cat $dir/internal/temp/compilelog.txt
  # In file included from ../../src/freeglut.h:17,
  #                  from ../../src/freeglut_callbacks.c:31:
  # ../../src/freeglut_std.h:122:10: fatal error: GL/gl.h: Arquivo ou diretório inexistente
  #   122 | #include <GL/gl.h>
  # ...



  sudo apt install freeglut3-dev



  qb64 -o $PWD/star          -x star.bas
  qb64 -s:exewithsource=true -x star.bas
  ./star



  qb64 -x -o s <(cat << \EOT
n=4
for y = -2*n to 2*n
  for x = -2*n to 2*n
    j=abs(y)
    i=abs(x)
    b=(i>n)*(j>n)-(i<j-n)-(j<i-n)>0
    print chr$(42+10*b);
  next x
  print
next y
EOT
)
  # QB64 Compiler V2.1
  #
  # Cannot locate source file: /dev/fd/63.BAS



# EOF
