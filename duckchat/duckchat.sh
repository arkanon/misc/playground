# duckchat.sh

# Arkanon <arkanon@lsd.org.br>
# 2024/11/25 (Mon) 16:24:15 -03
# 2024/11/22 (Fri) 12:09:28 -03

# <http://github.com/ephiguxta/curlduck/blob/1e42b408369faaeb3e194f1865ffaf3b2906cae7/curlduck.sh>
# <http://duckduckgo.com/duckduckgo-help-pages/aichat/chat-models>
# <http://ai.meta.com/blog/meta-llama-3>
# <http://github.com/mumu-lhl/duckduckgo-ai-chat/blob/main/index.ts>



set 'Whats the python code to print sign info about pdf files?'

api=https://duckduckgo.com/duckchat/v1
mod=meta-llama/Meta-Llama-3.1-70B-Instruct-Turbo
vqd=$(curl -sw %header{x-vqd-4} -H x-vqd-accept:1 $api/status -o /dev/null)

msg="{
       'model': '$mod',
       'messages': [ { 'content': '$1', 'role': 'user' } ]
     }"

arg=(
  -s
  -X POST
  -H Content-Type:application/json
  -H x-vqd-4:$vqd
  -d "${msg//\'/\"}"
)

out=$(curl "${arg[@]}" $api/chat)




time {
  : "$out"
  : "${_//data:}"
  : "${_//\[DONE\]}"
}
# 0m0,016s
# [ou]
time {
  : "$out"
  : "${_//@(data:|\[DONE\])}"
}
# 242m52,707s

ans=$(jq <<< $_ -rs '[.[].message] | join("")')

echo "$ans"
md5sum <<< $ans # 5bf7d253cc1d316405a7935e32c0d480



unset ans
while IFS=\" read -r x x x x x x x msg1 msg2 x
do
  [[ ${msg1: -1} == \\ ]] &&
   msg=${msg1::-1}\"$msg2 ||
   msg=$msg1
  ans+=$msg
done <<< $out

echo -e "$ans"
echo -e "$ans" | md5sum # 853d6ee9849fa7c3163b846918e975dc



# EOF
