#!/bin/bash

# Teste de conhecimento do Código Morse
#
# Arkanon <arkanon@lsd.org.br>
  V=1.4 ; D=2017/02/28 W=Tue T=22:28:48 Z=BRS
# V=1.3 ; D=2017/02/28 W=Tue T=21:36:10 Z=BRS
# V=1.2 ; D=2017/02/27 W=Seg T=21:56:20 Z=BRS
# V=1.1 ; D=2017/02/27 W=Seg T=18:35:55 Z=BRS
# V=1.0 ; D=2017/02/27 W=Seg T=16:05:11 Z=BRS
#         D=2017/02/26 W=Dom T=23:48:44 Z=BRS
#
# <http://pt.wikipedia.org/wiki/Código_Morse#Letras>
#
# ### 'cw' é um pacote destinado especialmente à execução de código morse <http://unix.stackexchange.com/a/273454>
#   $ sudo apt -y install cw
#   $ cw <<< "morse code"
#
# TODO
# -- testar presença do sox e outros comando menos comuns
# -- melhorar controle do buffer de teclado no loop de montagem do código morse
# -- começar contagem de tempo no primeiro sinal digitado
# -- espaço capturado de forma muito imprecisa pelo loop
# -- parâmetros para ativar/desativar características

  chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ   1234567890   .,?/:=-_@   '\''!()&;"$'   # "

# for char in $(sed 's/./\u& /g' <<< $chars); { printf '%s %x\n' $char \'$char; }
#
# # printf %x \'A
# # printf \\x41

  typeset -A morse

  morse[41]=.-      #  A
  morse[42]=-...    #  B
  morse[43]=-.-.    #  C
  morse[44]=-..     #  D
  morse[45]=.       #  E
  morse[46]=..-.    #  F
  morse[47]=--.     #  G
  morse[48]=....    #  H
  morse[49]=..      #  I
  morse[4a]=.---    #  J
  morse[4b]=-.-     #  K
  morse[4c]=.-..    #  L
  morse[4d]=--      #  M
  morse[4e]=-.      #  N
  morse[4f]=---     #  O
  morse[50]=.--.    #  P
  morse[51]=--.-    #  Q
  morse[52]=.-.     #  R
  morse[53]=...     #  S
  morse[54]=-       #  T
  morse[55]=..-     #  U
  morse[56]=...-    #  V
  morse[57]=.--     #  W
  morse[58]=-..-    #  X
  morse[59]=-.--    #  Y
  morse[5a]=--..    #  Z

  morse[31]=.----   #  1
  morse[32]=..---   #  2
  morse[33]=...--   #  3
  morse[34]=....-   #  4
  morse[35]=.....   #  5
  morse[36]=-....   #  6
  morse[37]=--...   #  7
  morse[38]=---..   #  8
  morse[39]=----.   #  9
  morse[30]=-----   #  0

  morse[2e]=.-.-.-  #  .
  morse[2c]=--..--  #  ,
  morse[3f]=..--..  #  ?
  morse[2f]=-..-.   #  /
  morse[3a]=---...  #  :
  morse[3d]=-...-   #  =
  morse[2d]=-....-  #  -
  morse[5f]=..--.-  #  _
  morse[40]=.--.-.  #  @

  morse[27]=.----.  #  '
  morse[21]=-.-.--  #  !
  morse[28]=-.--.   #  (
  morse[29]=-.--.-  #  )
  morse[26]=.-...   #  &
  morse[3b]=-.-.-.  #  ;
  morse[22]=.-..-.  #  "
  morse[24]=...-..- #  $

          texto=${*^^}
      tela_cols=$(tput cols)
      tela_rows=$(tput lines)
        tamanho=${#texto}
#    centro_col=$(((tela_cols-tamanho)/2))
#    centro_row=$((tela_rows/2))
     centro_col=20
     centro_row=10
      start_row=$((centro_row+2))
              b=$(tput bold)
              n=$(tput sgr0)
          verde=2
       vermelho=1
     # vermelho=4

       invalido=$(grep -vFf <(echo -n "$chars" | sed 's/./&\n/g') <(echo -n "${texto^^}" | sed 's/./&\n/g') | sort -u)

  curto()
  {
    play -q "|sox -np synth .05"
  # play -qtwav <(sox -nV0 -r8000 -twav - synth .05 sine 880)
  # echo e | cw &> /dev/null
  }

  longo()
  {
    play -q "|sox -np synth .15"
  # play -qtwav <(sox -nV0 -r8000 -twav - synth .15 sine 880)
  # echo t | cw &> /dev/null
  }

  search()
  {
    # <http://stackoverflow.com/a/13219811>
    local array_name=$1
    local  key_value=$2
    hex=$(set | grep "^$array_name=" | grep -Eo '\[[^]]+\]="'$(sed 's/\./\\&/g' <<< $key_value)'"' | tr -d '[]' | cut -d= -f1)
    [ "$hex" ] && printf \\x$hex || printf ' '
  }

  if [ $# = 0 ]
  then
    echo -e "\n${b}terse $V$n - ${b}Te${n}ste de conhecimento do Código Mo${b}rse$n"
    echo -e "  Arkanon - $D\n"
    echo -e "Uso: $b${0##*/} <linha de texto>$n"
    echo -e "     $b${0##*/} \$(cat <arquivo.txt>)$n\n"
    echo -e "     O texto deve conter apenas caracteres com correspondência no Código Morse:\n"
    echo -e "       $b$chars$n\n"
    echo -e "       $b' ! ( ) & ; \" \$$n precisam ser \"escapados\": $b\' \! \( \) \& \; \\\" \\\$$n\n"
    echo -e "     A digitação do código morse relativo ao caracter enfatizado aceita:\n"
    echo -e "       $b.$n        sinal curto"
    echo -e "       $b-$n        sinal longo"
    echo -e "       $b[BS]$n     volta um sinal, para correção"
    echo -e "       $b[ESPAÇO]$n passa para a digitação dos sinais do caracter seguinte"
    echo -e "       $b[ESC]$n    abandona o teste\n"
    exit
  fi

  if [ "$invalido" ]
  then
    echo -en "\nEntrada contém caracteres sem correspondência no Código Morse: $b"
    cat <<< $invalido
    echo -e  "$n\nCaracteres aceitos: $b$chars$n\n"
    exit
  fi

  trap "stty $(stty -g)" EXIT HUP INT QUIT PIPE TERM # ao sair devolve o terminal no estado original
  stty -echo -icanon time 30 || exit $?              # desabilita echo e caracteres especiais, define o timeout da entrada em 0.1 s

  clear
  tput cup $centro_row $centro_col
  echo $texto
  tput cup $((start_row+2)) $centro_col
  printf "$b| %${#tamanho}s/%${#tamanho}s | correto    | resposta   |  acerto |$n\n" '#' t

      col=$centro_col
   inicio=$(date +%s.%2N)
   resumo=true
  acertos=0
  for caracter in $(sed 's/./& /g' <<< "${texto// /§}")
  {

    i=$((col-centro_col+1))

    if [ "$caracter" != § ]
    then

      tput cup $centro_row $col
      echo -n $b$caracter

      tput cup $start_row $col
      morse_tentativa=

      while [ "$REPLY" != " " -o "$morse_tentativa" == "" ] && [ ${#morse_tentativa} -lt 7 ]
      do

        read -rsn1
	read -rsn1 -t.1 MAIS
	read -rs   -t.1 RESTO

	xREPLY=$(printf %x \'$REPLY)

        if [ "$xREPLY" = 1b -a "$MAIS" == "" ]
        then
          tput cup $((start_row+1+i)) $centro_col
	  resumo=false
          break 2
        fi

        if [ "$xREPLY" = 7f ]
        then
          if [ "$morse_tentativa" ]
          then
            tput cub1
            echo -n ' '
            tput cub1
            morse_tentativa=${morse_tentativa::-1}
          fi
        else
          if grep -qF "$REPLY" <<< .-
          then
            echo -n $REPLY
	    [ "$REPLY" == "." ] && curto
	    [ "$REPLY" == "-" ] && longo
            morse_tentativa=$morse_tentativa$REPLY
          fi
        fi

      done

      unset REPLY

      tput cup $centro_row $col
      echo -n $n$caracter
      tput cup $start_row 0
      printf "%${tela_cols}s" ""

           morse_correto=${morse[$(printf %x \'$caracter)]}
      caracter_tentativa=$(search morse $morse_tentativa)

      if [ $morse_correto == $morse_tentativa ]
      then
        cor=$verde
        ((acertos++))
      else
        cor=$vermelho
      fi

      acerto=$(t=${texto// /}; bc <<< "scale=1;100*$acertos/${#t}")

      tput cup $((start_row+2+i)) $centro_col
      tput setaf $cor
      LC_ALL=C printf "$b| %${#tamanho}d/%${#tamanho}d | %s  %-7s | %-7s  %s | %5.1f %% |$n" \
                      $i \
                      $tamanho \
                      $caracter \
                      $morse_correto \
                      $morse_tentativa \
                     "$caracter_tentativa" \
                      $acerto

    fi

    ((col++))

  }

  tput cup $((start_row+4+i)) $centro_col

  if $resumo
  then

      fim=$(date +%s.%2N)
    tempo=$(bc <<< "scale=2;$fim-$inicio")
    veloc=$(bc <<< "scale=2;$tamanho/$tempo")

                                              echo -e "Caracteres: $b$tamanho$n\n"
    tput cup $((start_row+5+i)) $centro_col ; echo -e "    Acerto: $b$acerto %$n\n"
    tput cup $((start_row+6+i)) $centro_col ; echo -e "     Tempo: $b$tempo s$n\n"
    tput cup $((start_row+7+i)) $centro_col ; echo -e "Velocidade: $b$veloc cps$n\n\n"

  fi

  echo

# EOF
